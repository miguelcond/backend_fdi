## Sistema de Seguimiento a Proyectos de Desarrollo Productivo

Sistema de seguimiento a proyectos de desarrollo productivo desde territorio, mismo que involucra desde el registro del proyecto, registro de avance de obra, generación de planillas de avance y modificaciones al proyecto; para el Fondo de Desarrollo Indigena (FDI).

### Objetivo General
Proporcionar a la institución de información veraz, oportuna y actualizada de los proyectos de desarrollo productivo, a través del seguimiento físico y financiero de los mismos.

### Objetivos específicos
* Digitalizar y automatizar los procedimientos de registro y seguimiento de la ejecución del proyecto.
* Proporcionar un seguimiento y monitoreo en tiempo real del proyecto, desde su creación hasta su cierre.
* Reportar datos e información de la obra desde territorio.
* Disminuir tiempos en los procesos de revisión y validación de la información de los proyectos.
* Validar y contrastar información y datos de las personas relacionadas al proyecto y las  empresas ejecutoras, a través de procesos de interoperabilidad.
* Incrementar la transparencia en los procesos de ejecución del proyecto.

## Módulos e Interoperabilidad

### Módulos

* Registro/Evaluación del proyecto
* Avance de obra (supervisiones)
* Georeferenciación de los proyectos para las supervisiones ([módulo mobile](https://gitlab.geo.gob.bo/agetic/seguimiento-proyectos-android))
* Modificaciones

### Interoperabilidad

* **SEGIP**.- para validar los datos de las personas con cédula de identidad.

## Tecnologías utilizadas

- **Express** como servidor web ejecutar como npm start
- **Sequelize** como ORM
- **Passport** jwt y LDAP como mecanismos de autenticacion
- **Mocha** como framework de de testing ejecutar como npm testing
- **Chai** soporte para asserts de mocha
- **ApiDoc** documentacion del apiRest ejecutar como npm run apidoc
- **Babel** como compilador de ecma6 a ecma 5
- **eslint** como validador del codigo fuente
- **jsdoc** como validador de la documentación
- **ejs** como motor de plantillas

## Documentación

* [Diagramas de flujo de estados](docs/flujos/flujo_estados.md)
* [Diseño de base de datos](docs/base_datos/diseno_base_datos.md)
* [Mockups](docs/mockups/mockups.md)

## Instalación

* [Manual de instalación](INSTALL.md)

## Crear token de acceso para el sistema Khipus
Para crear el token de acceso para el sistema **Seguimiento de proyectos** con un tiempo de expiración por defecto de 10 años.

$ `npm run cli token`

Para crear el token de acceso para el sistema **Seguimiento de proyectos** con un tiempo de expiración de 86400 segundos (1 día):

$ `npm run cli token 86400`

URL del servicio `http://localhost:4000/servicio/khipus/`

Se puede modificar esta URL en el archivo de configuración `config.json`
```
servicio: {
  khipus: '/servicio/khipus/'
},
```
