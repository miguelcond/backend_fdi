### Actualización del código fuente

Ingresar a la carpeta donde se encuentra el código fuente del backend.

```sh
$ git checkout FDI-master
$ git pull origin FDI-master
```

### Instalar dependencias

```sh
$ npm install
```

### Actualizar Base de datos

```sh
$ npm run update
```

### Verificar las configuraciónes

Abrir el archivo `nano src/config/config.js`

> Comparar con las configuraciones existentes en el archivo `src/config/config-example.js` de acuerdo a las configuraciones necesarias para desarrollo o producción

Abrir el archivo `nano src/config/config.json`

> Comparar con las configuraciones existentes en el archivo `src/config/config-example.json`  de acuerdo a las configuraciones necesarias para desarrollo o producción


### Instalar wkhtmltopdf

Verificar si esta instalado el programa
```sh
$ wkhtmltopdf -V
```

Si no esta instalado debe ser instalado siguiendo uno de los siguientes pasos

#### wkhtmltopdf (Jessie)

Instalar paquetes requeridos para `wkhtmltopdf`.

```sh
sudo apt-get install libxfont1 xfonts-75dpi \
xfonts-base xfonts-encodings xfonts-utils fontconfig \
libfontconfig1 libjpeg62-turbo libx11-6 libxext6 libxrender1
```

Descargar el programa [wkhtmltopdf](https://wkhtmltopdf.org/downloads.html). O descargar con wget.

```sh
wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.2.1/wkhtmltox-0.12.2.1_linux-jessie-amd64.deb
```
Luego de tener el paquete descargado, instalar con:

```
sudo dpkg -i wkhtmltox-0.12.2.1_linux-jessie-amd64.deb
```

#### wkhtmltopdf (Stretch)

Instalar paquetes requeridos para `wkhtmltopdf`.

```sh
sudo apt-get install -y openssl build-essential libssl-dev libxrender-dev git-core libx11-dev libxext-dev libfontconfig1-dev libfreetype6-dev fontconfig

wget -c https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.3/wkhtmltox-0.12.3_linux-generic-amd64.tar.xz

tar xvf wkhtmltox*.tar.xz
sudo mv wkhtmltox/bin/wkhtmlto* /usr/bin
```

### Reiniciar el manejador de procesos [PM2](SERVER.md)

> Debe estar instalado y configurado PM2

Ver nombre del del proceso

```sh
$ pm2 status
┌───────────────────┬────┬──────┬───────┬─────────┬─────────┬────────┬──────┬────────────┬────────────┬──────────┐
│ App name          │ id │ mode │ pid   │ status  │ restart │ uptime │ cpu  │ mem        │ user       │ watching │
├───────────────────┼────┼──────┼───────┼─────────┼─────────┼────────┼──────┼────────────┼────────────┼──────────┤
│ seguimiento-api   │ 0  │ fork │ 0     │ online  │ 0       │ 0      │ 0%   │ 100.5 MB   │ fdidev     │ disabled │
└───────────────────┴────┴──────┴───────┴─────────┴─────────┴────────┴──────┴────────────┴────────────┴──────────┘
```

Detener el proceso para el backend

```sh
$ pm2 restart seguimiento-api
┌───────────────────┬────┬──────┬───────┬─────────┬─────────┬────────┬──────┬────────────┬────────────┬──────────┐
│ App name          │ id │ mode │ pid   │ status  │ restart │ uptime │ cpu  │ mem        │ user       │ watching │
├───────────────────┼────┼──────┼───────┼─────────┼─────────┼────────┼──────┼────────────┼────────────┼──────────┤
│ seguimiento-api   │ 0  │ fork │ 0     │ stopped │ 1       │ 0      │ 0%   │ 0 B        │ fdidev     │ disabled │
└───────────────────┴────┴──────┴───────┴─────────┴─────────┴────────┴──────┴────────────┴────────────┴──────────┘
```

Iniciar de nuevo el proceso si no existe.

```sh
$ env NODE_ENV=production pm2 start server.js --name seguimiento-api
┌───────────────────┬────┬──────┬───────┬─────────┬─────────┬────────┬──────┬────────────┬────────────┬──────────┐
│ App name          │ id │ mode │ pid   │ status  │ restart │ uptime │ cpu  │ mem        │ user       │ watching │
├───────────────────┼────┼──────┼───────┼─────────┼─────────┼────────┼──────┼────────────┼────────────┼──────────┤
│ seguimiento-api   │ 0  │ fork │ 0     │ stopped │ 8       │ 0      │ 0%   │ 0 B        │ fdidev     │ disabled │
└───────────────────┴────┴──────┴───────┴─────────┴─────────┴────────┴──────┴────────────┴────────────┴──────────┘
```
