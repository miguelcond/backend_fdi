import shortid from 'shortid';
import util from '../../../lib/util';
import errors from '../../../lib/errors';
import logger from '../../../lib/logger';
import fs from 'fs-extra';
import _ from 'lodash';
import moment from 'moment';
const qr = require("qrcode");

module.exports = (app) => {
  const _app = app;
  const models = app.src.db.models;
  const sequelize = app.src.db.sequelize;

  const adminObtener = async (req, res, next) => {
    const idRol = req.body.audit_usuario.id_rol;
    const idUsuario = req.body.audit_usuario.id_usuario;
    const consulta = app.src.lib.util.formarConsulta(req.query, models.proyecto);
    try {
      let result = await app.dao.proyecto.adminObtener(consulta, idRol, idUsuario, req.query);
      res.json({
        finalizado: true,
        mensaje: 'Proyectos recuperados correctamente.',
        datos: result
      });
    } catch (error) {
      next(error);
    }
  };

  const adminObtenerUsuario = async (req, res, next) => {
    const idRol = req.body.audit_usuario.id_rol;
    const idUsuario = req.body.audit_usuario.id_usuario;
    const codigo = req.params.codigo;
    const consulta = app.src.lib.util.formarConsulta(req.query, models.proyecto);
    try {
      let result = await app.dao.proyecto.adminObtenerUsuario(codigo, consulta, idRol, idUsuario, req.query);
      res.json({
        finalizado: true,
        mensaje: 'Proyecto recuperado correctamente.',
        datos: result
      });
    } catch (error) {
      next(error);
    }
  };

  const adminModificarUsuario = async (req, res, next) => {
    const idRol = req.body.audit_usuario.id_rol;
    const idUsuario = req.body.audit_usuario.id_usuario;
    const codigo = req.params.codigo;
    const consulta = app.src.lib.util.formarConsulta(req.query, models.proyecto);
    return sequelize.transaction(async(t) => {
      let result = await app.dao.proyecto.adminModificarUsuario(codigo, consulta, idRol, idUsuario, req.body, t);
      // Guardar registro log.
      let proyecto = await app.dao.proyecto.obtenerEstado(codigo);
      let us1 = await app.dao.usuario.getUsuarioPersona(req.body.nro_usuario);
      let us2 = await app.dao.usuario.getUsuarioPersona(req.body.nuevo_nro_usuario);
      let rol = await app.dao.rol.getId(req.body.nro_rol);
      await app.dao.log.crearLog(codigo,'proyecto',req.body.audit_usuario,proyecto.estado_actual.codigo,`Reasignación de ${rol.descripcion}. ${us1.persona.nombres||''} ${us1.persona.primer_apellido||''} -> ${us2.persona.nombres||''} ${us2.persona.primer_apellido||''}`);
      res.json({
        finalizado: true,
        mensaje: 'Usuarios reasignados correctamente.',
        datos: result
      });
    }).catch(error => {
      next(error);
    });
  };

  const obtener = async (req, res, next) => {
    const ID_ROL = req.body.audit_usuario.id_rol;
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const CONSULTA = app.src.lib.util.formarConsulta(req.query, models.proyecto);
    try {
      let result = await app.dao.proyecto.getPendientes(CONSULTA, ID_ROL, ID_USUARIO, req.query);

      if(ID_ROL==16){ //SUPERVISORES
        for(let a = 0; a < result.rows.length;a++){
          if(result.rows[a].adjudicaciones){
            for(let j=0; j < result.rows[a].adjudicaciones.length; j++){
              result.rows[a].adjudicaciones[j].supervisores = await app.dao.equipo_tecnico.obtenerSupervisores(result.rows[a].id_proyecto, result.rows[a].adjudicaciones[j].id_adjudicacion);
              result.rows[a].adjudicaciones[j].avance = {};
              result.rows[a].adjudicaciones[j].esSupervisor = false;
              if (result.rows[a].adjudicaciones[j].supervisores[0] && req.body.audit_usuario.id_usuario === result.rows[a].adjudicaciones[j].supervisores[0].fid_usuario) {
                result.rows[a].adjudicaciones[j].avance.completo = await app.dao.item.estanCompletados(result.rows[a].id_proyecto, undefined, result.rows[a].adjudicaciones[j].id_adjudicacion);
                for(let k=0;k<result.rows[a].adjudicaciones[j].supervisores.length;k++){
                  let _supervisor = util.json(result.rows[a].adjudicaciones[j].supervisores[k]);
                  if(ID_USUARIO == _supervisor.fid_usuario) result.rows[a].adjudicaciones[j].esSupervisor = true;
                }
              } else {
                result.rows[a].adjudicaciones.splice(j,1);
                j--;
              }

            }
          }
        }
      }

      res.json({
        finalizado: true,
        mensaje: 'Proyectos recuperados correctamente.',
        datos: result
      });
    } catch (error) {
      next(error);
    }
  };
  const scanqr = async (req, res, next) =>{
    let url = req.body.url
    qr.toDataURL(url, (err, src) => {
      console.log("qr generado!")
        if (err) res.send("Error occured");
      
        // Let us return the QR code image as our response and set it to be the source used in the webpage
        res.json({
          finalizado: true,
          mensaje: 'QR generado',
          datos : src
        })
    });
  }
  const obtenerPorQr = async (req, res, next) => {
    const codigoPro = req.query.codigo;
    console.log(codigoPro,"CODIGO PROOYECTOOOOOOO")
   
    try{
      let result = await app.dao.proyecto.getProyectoQR(codigoPro);
      console.log(result,"auquii")
      
      res.json({
        finalizado: true,
        mensaje: 'Proyecto recuperado correctamente por qr.',
        datos: result
      });
    } catch (error){
      logger.error(error.message);
      next(error);
    }
  }

  const obtenerPorId = async (req, res, next) => {
    const ID_ROL = req.body.audit_usuario.id_rol;
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id;
    const flagAdjudicacion = req.params.flag && req.params.flag === 'agregar-adjudicaciones' ? true : false;
    const CONSULTA = {
      condicionProyecto: {
        id_proyecto: ID_PROYECTO
      }
    };
    try {
      let result = await app.dao.proyecto.get(CONSULTA, ID_ROL, ID_USUARIO, req.query, flagAdjudicacion);
      if (result.rows.length !== 1) {
        throw new errors.NotFoundError('No se encontró el proyecto.');
      }

      let _usuario_asignado = await app.dao.usuario.getUsuarioPersona(result.rows[0].fid_usuario_asignado);
      _usuario_asignado = util.json(_usuario_asignado);
      let result_proyecto = result.rows[0];
      result_proyecto = util.json(result_proyecto);
      result_proyecto.revisor = _usuario_asignado;
      //revisar la tabla transicion para ver que usuario le asigno al tecnico supervisor el proyecto
      let _usuario_designacion = await app.dao.proyecto.getUsuarioTransicion(result.rows[0].id_proyecto,'ASIGNACION_TECNICO_FDI');
      _usuario_designacion = util.json(_usuario_designacion);
      _usuario_designacion = await app.dao.usuario.getUsuarioPersona(_usuario_designacion.fid_usuario_asignado);
      _usuario_designacion = util.json(_usuario_designacion);
      result_proyecto.jefe_area = _usuario_designacion;

      let _usuario_jefe_depto = await app.dao.proyecto.getParametrica('ORG_INSTITUCION','JEFE_DEPTO_TECNICO');
      _usuario_jefe_depto = util.json(_usuario_jefe_depto);
      result_proyecto.jefe_depto = {nombre:'',cargo:''};
      result_proyecto.jefe_depto.nombre = _usuario_jefe_depto.nombre;
      result_proyecto.jefe_depto.cargo = _usuario_jefe_depto.descripcion;

      let _usuario_director_gral_ejecutivo = await app.dao.proyecto.getParametrica('ORG_INSTITUCION','DIRECTOR_GRAL_EJECUTIVO');
      _usuario_director_gral_ejecutivo = util.json(_usuario_director_gral_ejecutivo);
      result_proyecto.director_gral = {nombre:'',cargo:''};
      result_proyecto.director_gral.nombre = _usuario_director_gral_ejecutivo.nombre;
      result_proyecto.director_gral.cargo = _usuario_director_gral_ejecutivo.descripcion;

      if (result_proyecto.autoridad_beneficiaria && !result_proyecto.autoridad_beneficiaria.complemento_visible) {
        delete result_proyecto.autoridad_beneficiaria.complemento;
      }

      res.json({
        finalizado: true,
        mensaje: 'Proyecto recuperado correctamente.',
        datos: result_proyecto
      });
    } catch (error) {
      logger.error(error.message);
      next(error);
    }
  };
  // Funcion para unir fecha en YYYYMMDD
  function fechaUnida(fecha){
    let separar = fecha.split('T');
    let unir = separar[0].split('-');
    return unir[0] + unir[1] + unir[2];
  }

function diasRestantes(datos){

  let diaActual = moment();
  let fechaInicio = moment(datos.orden_proceder)
  if(!datos.convenio){
    console.log("no tiene convenio")
    let finPlazo =  moment(fechaInicio).add(datos.plazo_ejecucion,'days');
    return finPlazo.from(diaActual);
  }
    let finPlazo =  moment(fechaInicio).add(datos.plazo_ejecucion,'days').add(datos.convenio.plazo_convenio_ampliado,'days').add(datos.convenio.plazo_convenio_ampliado2,'days').add(datos.convenio.plazo_convenio_ampliado3,'days');
     return finPlazo.from(diaActual);
  // console.log(datos.convenio.version,"ampliacion")
  // if(!datos.convenio.plazo_convenio_ampliado){
   
  // }
  // let finPlazo =  moment(fechaInicio,"YYYYMMDD").add(datos.plazo_ejecucion,'days').add(datos.convenio.plazo_convenio_ampliado,'days').add(datos.convenio.plazo_convenio_ampliado2,'days').add(datos.convenio.plazo_convenio_ampliado3,'days');
  // console.log(fechaInicio,"fecha inicio");
  // console.log(diaActual)
  // console.log(finPlazo)
  // return finPlazo.from(diaActual);
  // console.log(diasRest)

 
}
  const listarProcesados = async (req, res, next) => {
    const ID_ROL = req.body.audit_usuario.id_rol;
    // console.log(ID_ROL,"idrol de listar procesados")
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const QUERY = req.query || {};
    try {
      console.time('listarProcesados')
      const result = await app.dao.proyecto.listarProyectosProcesados_v2(QUERY, ID_ROL, ID_USUARIO);
      console.timeEnd('listarProcesados')
      console.log(result.rows.length)
      // for(let i in result.rows){
      //   result.rows[i].finPlazoC = diasRestantes(result.rows[i])
      // }
      // console.log(result);
      // console.log("1.2",result);
      res.json({
        finalizado: true,
        mensaje: 'Proyectos recuperados correctamente.',
        datos: result
      });
    } catch (error) {
      next(error);
    }
  };


  const listarProcesadosv2 = async (req, res, next) => {
    const ID_ROL = req.body.audit_usuario.id_rol;
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const QUERY = req.query || {};
    try {
      
      const result = await app.dao.proyecto.listarProyectosProcesados_v3(QUERY, ID_ROL, ID_USUARIO);
      // console.log('resulst', result.rows[0].convenio)

      console.log(result.rows.length)
      for(let i =0; i< result.rows.length; i++){
        result.rows[i].finPlazoC = diasRestantes(result.rows[i])
        // console.log(result.rows[i].convenio.version)
       }
      res.json({
        finalizado: true,
        mensaje: 'Proyectos recuperados correctamente.',
        datos: result
      });
    } catch (error) {
      next(error);
    }
  };

  const listarProcesadosMovil = async (req, res, next) => {
    const ID_ROL = req.body.audit_usuario.id_rol;
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const QUERY = req.query || {};
    try {
      const result = await app.dao.proyecto.listarProyectosProcesados_v2(QUERY, ID_ROL, ID_USUARIO, 'geom');
      // console.log(result);
      for (let i in result.rows) {
        delete result.rows[i].financiamiento;
        delete result.rows[i].evaluacion;
        delete result.rows[i].informe;
        delete result.rows[i].beneficiarios;
        delete result.rows[i].convenio;
        delete result.rows[i].res_administrativa;
        delete result.rows[i].cronograma_desembolsos;
      }
      res.json({
        finalizado: true,
        mensaje: 'Proyectos recuperados correctamente.',
        datos: result
      });
    } catch (error) {
      next(error);
    }
  };

  const obtenerDuplicados = async(req,res,next) =>{
    const ID_ROL = req.body.audit_usuario.id_rol;
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id;
    try {
      let proyecto = util.json(await app.dao.proyecto.getPorId(ID_PROYECTO));
      proyecto.duplicados = [];
      let duplicados = await models.proyecto.findAll({ attributes: ['nro_convenio', 'nombre'], where: { nro_convenio: proyecto.nro_convenio, $not: { estado_proyecto: 'RECHAZADO_FDI' } } });
      if (duplicados.length>1) {
        proyecto.duplicados = [];
        for (let j in duplicados) {
          proyecto.duplicados.push(duplicados[j].nombre);
        }
      }
      res.json({
        finalizado: true,
        mensaje: 'Proyectos recuperados correctamente.',
        datos: proyecto.duplicados
      });
    } catch (error) {
      next(error);
    }
  };

  const obtenerProcesado = async (req, res, next) => {
    const idUsuario = req.body.audit_usuario.id_usuario;
    const idRol = req.body.audit_usuario.id_rol;
    const consulta = app.src.lib.util.formarConsulta(req.query, models.proyecto);
    try {
      consulta.condicionProyecto = { id_proyecto: req.params.id };
      await app.dao.transicion.verificarProcesoUsuario(req.params.id, idUsuario, idRol);
      const PROYECTO = await app.dao.proyecto.getProcesado(consulta, idUsuario);
      let proyProcesado = util.json(PROYECTO).rows[0];

      // Obtener codigos repetidos
      let duplicados = await models.proyecto.findAll({ attributes: ['nro_convenio', 'nombre'], where: { nro_convenio: proyProcesado.nro_convenio, estado: 'ACTIVO', $not: { estado_proyecto: 'RECHAZADO_FDI' } } });
      if (duplicados.length>1) {
        proyProcesado.duplicados = [];
        for (let j in duplicados) {
          proyProcesado.duplicados.push(duplicados[j].nombre);
        }
      }

      // Obtener formularios extras
      for (let i in proyProcesado.formularios) {
        proyProcesado[proyProcesado.formularios[i].nombre] = proyProcesado.formularios[i].form_model;
      }
      delete proyProcesado.formularios;

      proyProcesado.dpa = app.dao.municipio.getDpa(proyProcesado.municipio);
      delete proyProcesado.municipio;
      proyProcesado.estado_actual.areas = app.dao.estado.getDatosPermitidos(proyProcesado.estado_actual.areas, idRol);
      proyProcesado.estado_actual.areas = await app.dao.estado.revisarCondiciones(proyProcesado, proyProcesado.estado_actual.areas);
      res.json({
        finalizado: true,
        mensaje: 'Proyecto recuperado correctamente.',
        datos: proyProcesado
      });
    } catch (error) {
      next(error);
    }
  };

  const obtenerProcesadoMovil = (req, res, next) => {
    const idUsuario = req.body.audit_usuario.id_usuario;
    const idRol = req.body.audit_usuario.id_rol;
    const consulta = app.src.lib.util.formarConsulta(req.query, models.proyecto);
    let unico = false;
    if (req.params.id) {
      if (!consulta.condicionProyecto) {
        consulta.condicionProyecto = {};
      }
      consulta.condicionProyecto.id_proyecto = req.params.id;
      unico = true;
    }
    models.proyecto.proyectoProcesadoMovil(consulta, idUsuario, idRol)
    .then((result) => {
      if (unico) {
        result = result.rows[0];
      }
      res.json({
        finalizado: true,
        mensaje: 'Proyectos recuperados correctamente.',
        datos: result
      });
    }).catch((error) => {
      next(error);
    });
  };

  const obtenerMatricula = (req, res, next) => {
    if (req.params.matricula !== 'undefined') {
      models.proyecto.findAndCountAll({
        where: {
          matricula: req.params.matricula
        }
      })
      .then((result) => {
        res.json({
          finalizado: true,
          mensaje: 'Proyectos recuperados correctamente.',
          datos: result
        });
      }).catch((error) => {
        return next(error);
      });
    } else {
      const idUsuario = req.body.audit_usuario.id_usuario;
      let idsProyectos;
      // console.log('entra false');
      // entra para el caso de supervisor o fiscal
      models.equipo_tecnico.findAll({
        where: {
          fid_usuario: idUsuario
        }
      }).then(result => {
        idsProyectos = result.map(row => row.fid_proyecto);
        return models.proyecto.findAndCountAll({
          where: {
            id_proyecto: { $in: idsProyectos }
          }
        });
      }).then(result => {
        res.json({
          finalizado: true,
          mensaje: 'Proyectos recuperados correctamente.',
          datos: result
        });
      }).catch((error) => {
        next(error);
      });
    }
  };

  const obtenerMatriculaId = (req, res, next) => {
    const idRol = req.body.audit_usuario.id_rol;
    app.dao.proyecto.getMatricula({
      condicionProyecto: {
        id_proyecto: req.params.id,
        matricula: req.params.matricula
      }
    }, idRol).then((result) => {
      if (result && result.rows && result.rows.length === 1) {
        const datos = util.json(result.rows[0]);
        app.dao.reporte.obtenerListaArchivos(datos.id_proyecto).then(result => {
          datos.formularios = result.formularios;
          let fechasAjustadas = app.dao.proyecto.calcularFechasRecepcion(datos);
          Object.assign(datos, fechasAjustadas);
          res.json({
            finalizado: true,
            mensaje: 'Proyecto recuperado correctamente.',
            datos: datos
          });
        });
      } else {
        throw new Error('No se encontró el proyecto.');
      }
    }).catch((error) => {
      next(error);
    });
  };

  const adjudicar = async (req, res, next) => {
    const adj = req.body;
    const idUsuario = req.body.audit_usuario.id_usuario;
    const idRol = req.body.audit_usuario.id_rol;
    return sequelize.transaction(async(t) => {
      const PROYECTO = await app.dao.proyecto.getPorCodigoAcceso(adj.codigo_acceso);
      if (PROYECTO) {
        const ESTADO_SIGUIENTE = util.json(await app.dao.estado.getPorCodigo(util.json(PROYECTO).estado_proyecto)).acciones[1].estado;
        let logObs = '';
        let empresa = await app.dao.empresa.obtenerEmpresa(req.body.matricula);
        if (empresa) {
          logObs = empresa.nombre||'';
        }
        await app.dao.transicion.registrarTransicion(PROYECTO.id_proyecto, 'proyecto', ESTADO_SIGUIENTE, null, idUsuario, idRol, logObs);
        PROYECTO.matricula = adj.matricula;
        PROYECTO.estado_proyecto = ESTADO_SIGUIENTE;
        res.json({
          finalizado: true,
          mensaje: `Se adjudicó al proyecto "${PROYECTO.dataValues.nombre}" correctamente.`,
          datos: await PROYECTO.save({ transaction: t })
        });
      } else {
        throw new errors.ValidationError('No se encontró el código indicado.');
      }
    }).catch(error => {
      next(error);
    });
  };

  const crear = async (req, res, next) => {
    const idRol = req.body.audit_usuario.id_rol;
    const idUsuario = req.body.audit_usuario.id_usuario;
    let proyecto = req.body;
    let resp = {};
    return sequelize.transaction(async(t) => {
      const USUARIO = util.json(await app.dao.usuario.getPorId(idUsuario));
      if (proyecto.fcod_municipio) {
        const cod = proyecto.fcod_municipio.substr(0, 2);
        if (USUARIO.fcod_departamento.indexOf(cod) === -1) {
          throw new errors.ValidationError('No tiene privilegios para asignar un municipio de este departamento.');
        }
      } else {
        throw new errors.ValidationError('Debe establecer el municipio al que corresponde el proyecto.');
      }
      // Verificar tabla instituciones
      const ESTADO_INICIAL = util.json(await app.src.db.models.estado.estadoInicial('PROYECTO-FDI', t)).codigo;
      proyecto.estado_proyecto = ESTADO_INICIAL;
      proyecto.version = 1;
      const PROYECTO_VALIDO = await app.dao.proyecto.validar(proyecto, req.body.audit_usuario, t);
      PROYECTO_VALIDO._usuario_creacion = idUsuario;
      // adicion campo cartera de proyecto
      PROYECTO_VALIDO.cartera = req.body.cartera;
      PROYECTO_VALIDO.org_sociales = req.body.org_sociales;
      proyecto = PROYECTO_VALIDO;
      PROYECTO_VALIDO.codigo = shortid.generate();

      if(req.body.id_proyecto_referencia){
        PROYECTO_VALIDO.id_proyecto_referencia = req.body.id_proyecto_referencia;
        PROYECTO_VALIDO.nro_convenio_referencia = req.body.nro_convenio_referencia;
        PROYECTO_VALIDO.nro_convenio = await app.dao.proyecto.codigoProyectoReferencia(req.body.id_proyecto_referencia);
      }else{
        PROYECTO_VALIDO.nro_convenio = await app.dao.proyecto.verNroCodigo(PROYECTO_VALIDO.nro_convenio);
      }

      const PROYECTO_CREADO = await models.proyecto.create(PROYECTO_VALIDO, { transaction: t });

      // Buscar campos tipo formulario dinamico y guardarlos
      let form_proy = {
        id_proyecto: PROYECTO_CREADO.id_proyecto,
        estado_proyecto: ESTADO_INICIAL
      };
      for (let i in proyecto) {
        if(proyecto[i] && typeof(proyecto[i])!='function' && proyecto.hasOwnProperty(i)) {
          if (proyecto[i].$_plantilla && proyecto[i].$_nombre) {
            if (!proyecto[i].version) {
              proyecto[i].version = 1;
            }
            resp[i] = await app.dao.formulario.crearActualizar(req.body.audit_usuario,form_proy, proyecto[i]);
          }
        }
      }

      resp = PROYECTO_CREADO;
      if (proyecto.doc_convenio) {
        await app.dao.proyecto.guardarArchivo(req.body.doc_convenio, PROYECTO_CREADO.id_proyecto, 'C');
      }
      if (proyecto.doc_convenio_extendido) {
        await app.dao.proyecto.guardarArchivo(req.body.doc_convenio_extendido, PROYECTO_CREADO.id_proyecto, 'CXT');
      }
      if (proyecto.doc_resmin_cife) {
        await app.dao.proyecto.guardarArchivo(req.body.doc_resmin_cife, PROYECTO_CREADO.id_proyecto, 'RM');
      }
      if (proyecto.doc_ev_externa) {
        await app.dao.proyecto.guardarArchivo(req.body.doc_ev_externa, PROYECTO_CREADO.id_proyecto, 'EVE');
      }
      await app.dao.transicion.registrarTransicion(PROYECTO_CREADO.id_proyecto, 'proyecto', ESTADO_INICIAL, null, idUsuario, idRol);
      const PROYECTO_ACTUALIZADO = await app.dao.proyecto.siguiente(PROYECTO_CREADO.id_proyecto, idRol, t);
      await app.dao.transicion.registrarTransicion(PROYECTO_ACTUALIZADO.id_proyecto, 'proyecto', PROYECTO_ACTUALIZADO.estado_proyecto, null, idUsuario, idRol);
    }).then(() => {
      logger.info(`El proceso de creación del proyecto '${resp.nombre}' finalizó correctamente.`);
      res.json({
        finalizado: true,
        mensaje: 'Proyecto registrado correctamente.',
        datos: resp
      });
    }).catch((error) => {
      next(error);
    });
  };

  const _registrarBoletas = async (proyecto, boletas, idProyecto, idUsuario) => {
    const BOLETAS_CCONTRATO = app.dao.boleta.getBoletaPorTipo(boletas, 'TB_CUMPLIMIENTO_CONTRATO');
    const BOLETAS_ANTICIPO = app.dao.boleta.getBoletaPorTipo(boletas, 'TB_CORRECTA_INVERSION');
    if (proyecto.anticipo === 0 && BOLETAS_ANTICIPO.length > 0) {
      throw new errors.ValidationError(`El monto del anticipo es 0 por lo que no es necesario registrar boletas de anticipo.`);
    }
    if (BOLETAS_CCONTRATO.length === 0) {
      throw new errors.ValidationError(`Debe registrar boletas de cumplimiento de contrato`);
    }
    return app.dao.boleta.crearPorLote(proyecto, boletas, idUsuario);
  };

  const _transicionRegistroEmpresaAAprobacionTecnico = async (idProyecto, idUsuario, body, t) => {
    let datosProyecto = await app.dao.proyecto.obtenerDatosProyecto(idProyecto);
    let montoTotal = await app.dao.item.obtenerMontoProyecto(idProyecto);
    let plazoEjecucion = body.plazo_ejecucion;
    if (montoTotal > datosProyecto.monto_total_convenio) {
      throw new errors.ValidationError(`El suma del precio de todos los items debe ser menor o igual al monto total de convenio.`);
    }
    if (plazoEjecucion > datosProyecto.plazo_ejecucion_convenio) {
      throw new errors.ValidationError(`El plazo de ejecución del convenio debe ser menor o igual al plazo de ejecución de convenio.`);
    }
    const modificarProyecto = {
      monto_total: montoTotal,
      plazo_ejecucion: plazoEjecucion,
      _usuario_modificacion: idUsuario
    };
    await models.proyecto.update(modificarProyecto, {where: {id_proyecto: idProyecto}, transaction: t});
    await app.dao.reporte.crearFormularios(idProyecto);
  };

  const _verificarTipoModificacion = (proyecto) => {
    if (proyecto.plazo_ampliacion) {
      if (typeof proyecto.plazo_ampliacion.por_volumenes !== 'number' || typeof proyecto.plazo_ampliacion.por_compensacion !== 'number') {
        throw new errors.ValidationError(`Los datos de ampliación por volúmenes y compensación deben ser números y
          ambos son necesarios para la ampliación de plazo.`);
      }
    }
    return proyecto;
  };

  const actualizar = async (req, res, next) => {
    const idRol = req.body.audit_usuario.id_rol;
    const idUsuario = req.body.audit_usuario.id_usuario;
    const idInstitucion = req.body.audit_usuario.institucion.id;

    let proyecto = req.body;
    let resp = {};
    let estadoActual;
    const estadoCliente = proyecto.estado_proyecto;
    proyecto.id_proyecto = req.params.id;
    return sequelize.transaction(async(t) => {
      const PROYECTO_VALIDO = await app.dao.proyecto.validar(proyecto, req.body.audit_usuario, t);
      estadoActual = PROYECTO_VALIDO.estado_proyecto;
      const CAMBIO_ESTADO = PROYECTO_VALIDO.cambio;
      PROYECTO_VALIDO.cambio = undefined;
      PROYECTO_VALIDO._usuario_modificacion = idUsuario;
      if (PROYECTO_VALIDO.fid_usuario_asignado && proyecto.fid_usuario_asignado) {
        let rol = await models.usuario_rol.findOne({
          attributes: ['fid_rol','fid_usuario'],
          where: { fid_usuario:PROYECTO_VALIDO.fid_usuario_asignado, fid_rol: PROYECTO_VALIDO.fid_usuario_asignado_rol }
        });
        if (!rol) {
          throw new errors.ValidationError(`El usuario asignado no tiene el rol permitido.`);
        }
      }

      // Validar que el monto asignado al FDI no excede el techo presupuestario
      if (CAMBIO_ESTADO && (CAMBIO_ESTADO.origen === 'REGISTRO_PROYECTO_FDI') && (CAMBIO_ESTADO.destino === 'REGISTRO_CONVENIO_FDI')) {
        let montoFDI = 0;
        if (proyecto.financiamiento) {
          montoFDI = proyecto.financiamiento.monto_fdi;
        } else {
          montoFDI = proyecto.monto_fdi;
        }
        if (!PROYECTO_VALIDO.org_sociales){
          if (!await app.dao.techo_presupuestario.esMontoValido(proyecto.id_proyecto, montoFDI)) { // SIEMPRE DEVUELVE TRUE
          throw new errors.UnauthorizedError(`El monto asignado al FDI según la tabla de estructura de financiamiento excede el techo presupuestario para el municipio seleccionado`);
          }
        } 
      }

      if (CAMBIO_ESTADO && (CAMBIO_ESTADO.destino === "CERRADO_FDI")) {
        if (await app.dao.proyecto.tienePlanillaAbierta(proyecto.id_proyecto)) {
          throw new errors.ValidationError('No se puede cerrar el proyecto porque tiene una o más planillas de supervisión en proceso');
        }

        if (await app.dao.proyecto.tieneAdjudicacionesSinModulos(proyecto.id_proyecto)) {
          throw new errors.ValidationError('No se puede cerrar el proyecto porque tiene una o más adjudicaciones sin items');
        }

        if (!await app.dao.item.estanCompletados(proyecto.id_proyecto)) {
          throw new errors.ValidationError('No se puede cerrar el proyecto porque una o más adjudicaciones no han sido concluídas');
        }
      }

      if(req.body.nro_convenio && req.body.nro_convenio!=""){
        PROYECTO_VALIDO.nro_convenio = req.body.nro_convenio;
      }
      if(req.body.estado && req.body.estado!="" && idRol == 1){
        PROYECTO_VALIDO.estado = req.body.estado;
      }

      // Rol Legal y req_extension_convenio
      // const esLegal = await app.dao.rol.esLegal(idRol);
      // if (esLegal && estadoActual && estadoActual === 'EN_EJECUCION_FDI' && PROYECTO_VALIDO.doc_convenio_extendido) {
      //   PROYECTO_VALIDO.req_extension_convenio = false;
      // }

      // Actualizar orden de proceder y monto de supervision de adjudicacion
      if (CAMBIO_ESTADO && CAMBIO_ESTADO.destino === 'EN_EJECUCION_FDI') {
        if (CAMBIO_ESTADO.origen !== 'MODIFICADO_FDI') {
          let datosAdjudicacion = proyecto.adj;
          let ordenProcederArray = datosAdjudicacion.orden_proceder.split("-");
          let ordenProceder = datosAdjudicacion.orden_proceder;
          if (ordenProcederArray[0].length===2)
            ordenProceder = ordenProcederArray[2]+","+ordenProcederArray[1]+","+ordenProcederArray[0];
          datosAdjudicacion.orden_proceder = new Date(ordenProceder).getTime();
          datosAdjudicacion._usuario_modificacion = idUsuario;
          datosAdjudicacion.monto = datosAdjudicacion.monto_supervision;
          const idAdjudicacion = datosAdjudicacion.id_adjudicacion;
          delete datosAdjudicacion.id_adjudicacion;
          delete proyecto.adj;
          await app.dao.adjudicacion.actualizar(datosAdjudicacion, idAdjudicacion);
        }
      }
      // if (CAMBIO_ESTADO && CAMBIO_ESTADO.origen === 'APROBACION_TECNICO' && (CAMBIO_ESTADO.destino === 'REGISTRO_GARANTIAS_CIFE'||CAMBIO_ESTADO.destino === 'REGISTRO_GARANTIAS')) {
      //   PROYECTO_VALIDO.desembolso = null;
      // }
      let proyectoActualizado = await PROYECTO_VALIDO.save();
      proyecto = proyectoActualizado;
      resp = util.json(proyectoActualizado);

      // TODO Llevar estos datos a formulario contactos (considerar registro de responsable GAM)
      if (proyecto.autoridad_beneficiaria && proyecto.autoridad_beneficiaria.nombres && proyecto.estado_proyecto!='REGISTRO_SOLICITUD') {
        const AUTORIDAD = await app.dao.persona.crearRecuperar(proyecto.autoridad_beneficiaria, idUsuario, t);
        proyectoActualizado.fid_autoridad_beneficiaria = AUTORIDAD.id_persona;
        await proyectoActualizado.save({ transaction: t });
        resp.autoridad_beneficiaria = util.json(AUTORIDAD);

        const EXISTE_USUARIO = await app.dao.usuario.existe(AUTORIDAD.numero_documento, AUTORIDAD.fecha_nacimiento);
        const ID_ROL = 18; // Responsable de GAM/GAIOC
        let persona = JSON.parse(JSON.stringify(AUTORIDAD));
        persona.noVerificar = proyecto.autoridad_beneficiaria.noVerificar;
        const DATOS_USUARIO = await app.dao.usuario.crearRecuperar(persona, {idRol:ID_ROL, cod:'TODOS', idUsuario, idInstitucion});

        // TODO registro en equipo tecnico
        // const regEquipoTecnico = await _app.controller.equipo_tecnico.registrarUsuarioEquipoTecnico (persona, 'CG_RESP_GAM', idUsuario, idInstitucion);
      }

      // Buscar campos tipo formulario dinamico y guardarlos
      let form_proy = {
        id_proyecto: PROYECTO_VALIDO.id_proyecto,
        estado_proyecto: PROYECTO_VALIDO.estado_proyecto
      };
      if (CAMBIO_ESTADO) {
        form_proy.estado_proyecto = CAMBIO_ESTADO.origen;
      }
      for (let i in proyecto) {
        if(proyecto[i] && typeof(proyecto[i])!='function' && proyecto.hasOwnProperty(i)) {
          if (proyecto[i].$_plantilla && proyecto[i].$_nombre) {
            if (!proyecto[i].version) {
              proyecto[i].version = 1;
            }
            resp[i] = await app.dao.formulario.crearActualizar(req.body.audit_usuario,form_proy, proyecto[i]);
            if (resp[i].form_model) {
              resp[i] = resp[i].form_model;
            }
          }
        }
      }

      // console.log('=================================>');
      // console.log(proyecto.beneficiarios);
      // if (proyecto.beneficiarios) {
      //   resp.beneficiarios = await app.dao.formulario.crearActualizar(proyectoActualizado, proyecto.beneficiarios, proyecto.beneficiarios.plantilla);
      // }
      //
      // console.log('=================================>');
      // console.log(proyecto.financiamiento);
      // if (proyecto.financiamiento) {
      //   resp.financiamiento = await app.dao.formulario.crearActualizar(proyectoActualizado, proyecto.financiamiento, proyecto.financiamiento.plantilla);
      // }

      if (proyecto.contrato && proyecto.contrato.empresa) {
        const EMPRESA = await app.dao.empresa.crearRecuperar(proyecto.contrato.empresa, idUsuario, t);
        // console.log("DEVUELVE empresa");
        resp.contrato = {
          empresa: util.json(EMPRESA)
        };
        // console.log(" Crea contrato");
        const CONTRATO = await app.dao.contrato.crearRecuperar({
          fid_proyecto: resp.id_proyecto,
          fid_empresa: resp.contrato.empresa.id_empresa,
          nro_invitacion: proyecto.contrato.nro_invitacion,
          nro_cuce: proyecto.contrato.nro_cuce,
          fecha_inscripcion: proyecto.contrato.fecha_inscripcion,
          ci_representante_legal: proyecto.contrato.ci_representante_legal,
          nombre_representante_legal: proyecto.contrato.nombre_representante_legal,
          numero_testimonio: proyecto.contrato.numero_testimonio,
          lugar_emision: proyecto.contrato.lugar_emision,
          fecha_expedicion: proyecto.contrato.fecha_expedicion,
          fax: proyecto.contrato.fax,
          correo: proyecto.contrato.correo
        }, idUsuario, t);
        resp.contrato = Object.assign(util.json(CONTRATO), resp.contrato);
      }

      if (CAMBIO_ESTADO && (CAMBIO_ESTADO.origen === 'REGISTRO_EMPRESA') && (CAMBIO_ESTADO.destino === 'APROBACION_TECNICO')) {
        await _transicionRegistroEmpresaAAprobacionTecnico(proyecto.id_proyecto, idUsuario, req.body, t);
      }

      // Actualizar el techo presupuestario para el municipio del proyecto
      if (CAMBIO_ESTADO && (CAMBIO_ESTADO.origen === 'REGISTRO_PROYECTO_FDI') && (CAMBIO_ESTADO.destino === 'REGISTRO_CONVENIO_FDI')) {
        await app.dao.techo_presupuestario.actualizarEnRegistro(proyecto.id_proyecto);
      }

      // Notificar a responsable de GAM para registro de equipo tecnico
      if (CAMBIO_ESTADO && (CAMBIO_ESTADO.origen === 'REGISTRO_DESEMBOLSO_FDI') && (CAMBIO_ESTADO.destino === 'ASIGNACION_EQUIPO_TECNICO_FDI')) {
        const autoridadEnProyecto = await models.proyecto.findOne({
          attributes: ['fid_autoridad_beneficiaria'],
          where: { id_proyecto: proyecto.id_proyecto }
        });
        const AUTORIDAD = await app.dao.persona.buscarID(autoridadEnProyecto.fid_autoridad_beneficiaria);
        const ID_ROL = 18; // Responsable de GAM/GAIOC
        let persona = JSON.parse(JSON.stringify(AUTORIDAD));
        persona.noVerificar = true;
        const DATOS_USUARIO = await app.dao.usuario.crearRecuperar(persona, {idRol:ID_ROL, cod:'TODOS', idUsuario, idInstitucion});
        if (DATOS_USUARIO && AUTORIDAD) {
          await app.dao.proyecto.notificarRegistroEquipoTecnico(proyecto, DATOS_USUARIO, AUTORIDAD, idUsuario, t);
        }
      }

      if (estadoActual && (estadoActual === 'REGISTRO_EMPRESA')) {
        // console.log("ACTUALIZANDO EN ESTADO REGISTRO EMPRESA, SIN CAMBIAR DE ESTADO, se devuelve el campo estados -- obtiene todos los datos del proyecto");
        const MATRICULA = await app.dao.proyecto.getMatricula({
          condicionProyecto: {
            id_proyecto: proyecto.id_proyecto,
            matricula: proyecto.matricula
          }
        }, idRol);

        if (MATRICULA && MATRICULA.rows && MATRICULA.rows.length === 1) {
          resp = util.json(MATRICULA.rows[0]);
        }
      }
      if (proyecto.boletas) {
        await _registrarBoletas(proyecto, proyecto.boletas, proyectoActualizado.id_proyecto, idUsuario);
      }
      if (estadoActual && (estadoActual === 'MODIFICADO_FDI')) {
        proyectoActualizado = _verificarTipoModificacion(proyectoActualizado);
        proyectoActualizado.save();
        resp = null;
      }
      if (estadoCliente) {
        // console.log("Validando el paso a la siguiente transición");
        const PROYECTO_COMPLETO = await app.dao.proyecto.getConDatos(proyectoActualizado.id_proyecto);
        await app.dao.estado.validarSiguiente(PROYECTO_COMPLETO, CAMBIO_ESTADO, idRol);
        await app.dao.estado.ejecutarMetodos(PROYECTO_COMPLETO, CAMBIO_ESTADO, idRol, app.dao.proyecto, idUsuario);
        let logObs = '';
        if (idRol == 8 && PROYECTO_VALIDO.documentacion_exp && PROYECTO_VALIDO.documentacion_exp.datos_empresa) {
          logObs = PROYECTO_VALIDO.documentacion_exp.datos_empresa.razon_social||'';
        }
        await app.dao.transicion.registrarTransicion(proyectoActualizado.id_proyecto, 'proyecto', proyectoActualizado.estado_proyecto, PROYECTO_VALIDO.observacion, idUsuario, idRol, logObs);
      }
      return resp;
    }).then(result => {
      res.json({
        finalizado: true,
        mensaje: 'Proyecto actualizado correctamente.',
        datos: result
      });
    }).catch(error => {
      next(error);
    });
  };

  // Obtener usuarios responsables de acuerdo a roles indicados
  const obtenerUsuarios = (req, res, next) => {
    // Si es FDI
    if (req.body.audit_usuario.institucion && req.body.audit_usuario.institucion.id==2) {
      req.query.roles = [13];
    }
    if (req.query.roles) {
      let result = [];
      models.proyecto.findOne({
        attributes: ['id_proyecto','fcod_municipio'],
        where: {
          id_proyecto: req.params.id
        }
      }).then((proyecto)=>{
        // Usuario tecnico o tecnico departamental
        if (req.query.roles.indexOf(4)>=0 || req.query.roles.indexOf(5)>=0) {
          req.query.roles = [4,5];
        }
        return models.usuario.findAll({
          attributes: ['id_usuario','fcod_departamento','cargo'],
          where: {
            fcod_departamento: {
              $contains: [proyecto.fcod_municipio.substr(0,2)]
            },
            estado: 'ACTIVO',
          },
          include: [
            {
              model: models.usuario_rol,
              as: 'usuarios_roles',
              attributes: ['fid_rol'],
              where: {
                fid_rol: {
                  $in: req.query.roles
                },
                estado: 'ACTIVO',
              },
              include: {
                model: models.rol,
                as: 'rol',
                attributes: ['nombre','descripcion'],
              }
            },
            {
              model: models.persona,
              as: 'persona',
              attributes: ['id_persona','nombres','primer_apellido','segundo_apellido']
            }
          ]
        })
      }).then((resp)=>{
        resp = util.json(resp)
        for (let i in resp) {
          for(let j in resp[i].usuarios_roles) {
            result.push({
              cod_departamento: resp[i].fcod_departamento,
              id_usuario: resp[i].id_usuario,
              id_rol: resp[i].usuarios_roles[j].fid_rol,
              nombre: `${resp[i].persona.nombres||''} ${resp[i].persona.primer_apellido||''} ${resp[i].persona.segundo_apellido||''} (${resp[i].cargo})`,
              rol_nombre: '',
            });
          }
        }
        res.json({
          finalizado: true,
          mensaje: 'Usuarios recuperados correctamente.',
          datos: result
        });
      }).catch((error) => {
        next(error);
      });
      return;
    }
    // Si no se envia los roles, por defecto se toma como tecnico respoansable del proyecto
    models.proyecto.usuarios(req.params.id)
    .then((result) => {
      res.json({
        finalizado: true,
        mensaje: 'Usuarios recuperados correctamente.',
        datos: result
      });
    }).catch((error) => {
      next(error);
    });
  };

  const obtenerDocumento = (req, res, next) => {
    models.proyecto.findById(req.params.id)
    .then((result) => {
      if (result.dataValues[req.params.documento]) {
        res.json({
          finalizado: true,
          mensaje: 'Documento recuperado correctamente.',
          datos: app.dao.proyecto.leerArchivo(result.dataValues[req.params.documento])
        });
      } else {
        res.json({
          finalizado: false,
          mensaje: 'El archivo no fue cargado.',
          datos: null
        });
      }
    }).catch((error) => {
      next(error);
    });
  };


  const crearModificacionAdjudicacion = async (req, res, next) => {
    const ID_PROYECTO = req.params.id;
    const ID_ADJUDICACION = req.params.id_adjudicacion;
    const ID_ROL = req.body.audit_usuario.id_rol;
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    app.dao.common.crearTransaccion(async (_t) => {
      try {
        const PROYECTO = await app.dao.proyecto.getPorId(ID_PROYECTO);
        const ADJUDICACION = await app.dao.adjudicacion.obtenerPorId(ID_ADJUDICACION);

        if (await app.dao.supervision.existeAdjudicacionSupervisionEnCurso(PROYECTO,ID_ADJUDICACION)) {
          throw new errors.ValidationError(`Hay una supervisión en curso y no es posible iniciar una modificación.`);
        }

        const VERSION_ACTUAL = ADJUDICACION.version;
        if (['APROBADO_FDI','EN_EJECUCION_FDI'].indexOf(PROYECTO.estado_proyecto) === -1 ) {
          throw new errors.ValidationError(`El proyecto debe estar aprobado para una modificación`);
        }

        if (['ADJUDICADO_FDI'].indexOf(ADJUDICACION.estado_adjudicacion) === -1 ) {
          throw new errors.ValidationError(`El componente ${ADJUDICACION.referencia} tiene una modificación pendiente`);
        }
        //console.log("*************************************");
        //console.log(ADJUDICACION);
        //throw new errors.ValidationError(`NO SE PUDO CREAR LA MODIFICACION`);

        //await app.dao.proyecto_historico.guardar(PROYECTO.dataValues);
        await app.dao.adjudicacion_historico.crear(ADJUDICACION.dataValues);

        await app.dao.modulo_historico.guardarAdjudicacion(ID_PROYECTO, ID_ADJUDICACION, VERSION_ACTUAL);

        const ESTADO_MODIFICACION = 'MODIFICADO_FDI';
        const CAMPOS = {
          version: VERSION_ACTUAL + 1,
          tipo_modificacion: req.query.tipo,
          estado_adjudicacion: ESTADO_MODIFICACION,
          doc_respaldo_modificacion: null,
          plazo_ampliacion: {por_volumenes:0,por_compensacion:0},
          fecha_modificacion: null,
          documentos: null,
          _fecha_creacion: new Date(),
          _fecha_modificacion: new Date(),
          _usuario_modificacion: ID_USUARIO,
          _usuario_creacion: ID_USUARIO,
        };
        Object.assign(ADJUDICACION, CAMPOS);

        await ADJUDICACION.save();

        await app.dao.transicion.registrarTransicion(ID_ADJUDICACION, 'adjudicacion', ESTADO_MODIFICACION, null, ID_USUARIO, ID_ROL);

        let proyecto = await app.dao.proyecto.obtenerDatosParaPlanilla(ID_PROYECTO, ID_ROL, true);
        proyecto.dataValues.modulos = await app.dao.modulo.obtenerDatosProyectoHistoricosAdjudicacion(ID_PROYECTO, ID_ADJUDICACION, VERSION_ACTUAL);
        proyecto.dataValues.version_inicial = await app.dao.adjudicacion_historico.obtenerDatosModificadosVersion(ID_PROYECTO,ID_ADJUDICACION, 1);
        proyecto.dataValues.version_anterior = await app.dao.adjudicacion_historico.obtenerDatosModificadosVersion(ID_PROYECTO,ID_ADJUDICACION, VERSION_ACTUAL);
        const HISTORIAL = util.json(await app.dao.adjudicacion_historico.obtenerHistoricoModificaciones(ID_PROYECTO,ID_ADJUDICACION, proyecto.tipo_modificacion, proyecto.version));
        proyecto.dataValues.modificaciones = {
          nombre: `${await app.dao.parametrica.obtenerNombre(CAMPOS.tipo_modificacion)} N° ${HISTORIAL.length + 1}`,
          historial: HISTORIAL
        };
        let adjudicacion = await app.dao.adjudicacion.obtenerPorId(ID_ADJUDICACION);
        proyecto.dataValues.adjudicacion = adjudicacion;

        res.json({
          finalizado: true,
          mensaje: 'Modificación iniciada correctamente.',
          datos: proyecto
        });
      } catch (e) {
        _t.rollback();
        next(e);
      }
    });
  };

  const crearModificacion = async (req, res, next) => {
    const ID_PROYECTO = req.params.id;
    const ID_ROL = req.body.audit_usuario.id_rol;
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    app.dao.common.crearTransaccion(async (_t) => {
      try {
        const PROYECTO = await app.dao.proyecto.getPorId(ID_PROYECTO);
        if (await app.dao.supervision.existeSupervisionEnCurso(PROYECTO)) {
          throw new errors.ValidationError(`Hay una supervisión en curso y no es posible iniciar una modificación.`);
        }
        const VERSION_ACTUAL = PROYECTO.version;
        if (PROYECTO.estado_proyecto !== 'EN_EJECUCION_FDI') {
          throw new errors.ValidationError(`El proyecto debe estar aprobado para una modificación`);
        }
        await app.dao.proyecto_historico.guardar(PROYECTO.dataValues);
        await app.dao.modulo_historico.guardar(ID_PROYECTO, VERSION_ACTUAL);
        const ESTADO_MODIFICACION = 'MODIFICADO_FDI';
        const CAMPOS = {
          version: VERSION_ACTUAL + 1,
          tipo_modificacion: req.query.tipo,
          estado_proyecto: ESTADO_MODIFICACION,
          doc_respaldo_modificacion: null,
          plazo_ampliacion: {por_volumenes:0,por_compensacion:0},
          fecha_modificacion: null
        };
        Object.assign(PROYECTO, CAMPOS);
        await PROYECTO.save();
        await app.dao.transicion.registrarTransicion(ID_PROYECTO, 'proyecto', ESTADO_MODIFICACION, null, ID_USUARIO, ID_ROL);
        let proyecto = await app.dao.proyecto.obtenerDatosParaPlanilla(ID_PROYECTO, ID_ROL, true);
        proyecto.dataValues.modulos = await app.dao.modulo.obtenerDatosProyectoHistoricos(ID_PROYECTO, VERSION_ACTUAL);
        proyecto.dataValues.version_inicial = await app.dao.proyecto_historico.obtenerDatosModificadosVersion(ID_PROYECTO, 1);
        proyecto.dataValues.version_anterior = await app.dao.proyecto_historico.obtenerDatosModificadosVersion(ID_PROYECTO, VERSION_ACTUAL);
        const HISTORIAL = util.json(await app.dao.proyecto_historico.obtenerHistoricoModificaciones(ID_PROYECTO, proyecto.tipo_modificacion, proyecto.version));
        proyecto.dataValues.modificaciones = {
          nombre: `${await app.dao.parametrica.obtenerNombre(proyecto.tipo_modificacion)} N° ${HISTORIAL.length + 1}`,
          historial: HISTORIAL
        };
        res.json({
          finalizado: true,
          mensaje: 'Modificación iniciada correctamente.',
          datos: proyecto
        });
      } catch (e) {
        _t.rollback();
        next(e);
      }
    });
  };

  const obtenerModificacion = async (req, res, next) => {
    const ID_PROYECTO = req.params.id;
    const ID_ROL = req.body.audit_usuario.id_rol;
    let version = req.query.version ? parseInt(req.query.version) : null;
    return app.dao.common.crearTransaccion(async (_t) => {
      try {
        let proyecto = await app.dao.proyecto.obtenerDatosParaPlanilla(ID_PROYECTO, ID_ROL, true);
        version = !version ? proyecto.version : version;
        if (version > proyecto.version) {
          throw new errors.ValidationError(`No existe la versión del proyecto que solicita`);
        }
        if (proyecto.version !== version) {
          proyecto = await app.dao.proyecto_historico.obtener(ID_PROYECTO, version);
          proyecto.dataValues.modulos = await app.dao.modulo_historico.obtener(ID_PROYECTO, version);
        } else {
          proyecto.dataValues.modulos = await app.dao.modulo.obtenerDatosProyectoHistoricos(ID_PROYECTO, version - 1);
        }
        proyecto.dataValues.version_inicial = await app.dao.proyecto_historico.obtenerDatosModificadosVersion(ID_PROYECTO, 1);
        proyecto.dataValues.version_anterior = await app.dao.proyecto_historico.obtenerDatosModificadosVersion(ID_PROYECTO, version - 1);
        const HISTORIAL = version > 1 ? util.json(await app.dao.proyecto_historico.obtenerHistoricoModificaciones(ID_PROYECTO, proyecto.tipo_modificacion, version)) : [];
        let nro = [0,0,0]; // Nombres contrato modificatorio
        for(let i in HISTORIAL) {
          switch(HISTORIAL[i].tipo_modificacion) {
            case 'TM_ORDEN_TRABAJO': nro[0]++; HISTORIAL[i].tipo_modificacion_proyecto.nombre += ` N° ${nro[0]}`; break;
            case 'TM_ORDEN_CAMBIO': nro[1]++; HISTORIAL[i].tipo_modificacion_proyecto.nombre += ` N° ${nro[1]}`; break;
            case 'TM_CONTRATO_MODIFICATORIO': nro[2]++; HISTORIAL[i].tipo_modificacion_proyecto.nombre += ` N° ${nro[2]}`; break;
          }
        }
        proyecto.dataValues.modificaciones = {
          // nombre: proyecto.tipo_modificacion ? (version > 1 ? `${await app.dao.parametrica.obtenerNombre(proyecto.tipo_modificacion)} N° ${HISTORIAL.length + 1}` : null) : 'Contrato original',
          nombre: proyecto.tipo_modificacion ? (version > 1 ? `${await app.dao.parametrica.obtenerNombre(proyecto.tipo_modificacion)}` : null) : 'Contrato original',
          historial: HISTORIAL
        };
        switch(proyecto.tipo_modificacion) {
          case 'TM_ORDEN_TRABAJO': nro[0]++; proyecto.dataValues.modificaciones.nombre += ` N° ${nro[0]}`; break;
          case 'TM_ORDEN_CAMBIO': nro[1]++; proyecto.dataValues.modificaciones.nombre += ` N° ${nro[1]}`; break;
          case 'TM_CONTRATO_MODIFICATORIO': nro[2]++; proyecto.dataValues.modificaciones.nombre += ` N° ${nro[2]}`; break;
        }

        res.json({
          finalizado: true,
          mensaje: 'Datos del proyecto obtenidos correctamente.',
          datos: proyecto
        });
      } catch (e) {
        _t.rollback();
        next(e);
      }
    });
  };

  const obtenerModificacionAdjudicacion = async (req, res, next) => {
    const ID_PROYECTO = req.params.id;
    const ID_ADJUDICACION = req.params.id_adjudicacion;
    const ID_ROL = req.body.audit_usuario.id_rol;
    let version = req.query.version ? parseInt(req.query.version) : null;
    return app.dao.common.crearTransaccion(async (_t) => {
      try {
        let proyecto = await app.dao.proyecto.obtenerDatosParaPlanilla(ID_PROYECTO, ID_ROL, true);
        let adjudicacion = await app.dao.adjudicacion.obtenerPorId(ID_ADJUDICACION);
        adjudicacion = util.json(adjudicacion);

        version = !version ? adjudicacion.version : version;
        if (version > adjudicacion.version) {
          throw new errors.ValidationError(`No existe la versión de la adjudicación que solicita`);
        }

        let version_actual = adjudicacion.version;
        if (version < adjudicacion.version) {
          adjudicacion = await app.dao.adjudicacion_historico.obtenerPorId(ID_ADJUDICACION, version);
          adjudicacion = util.json(adjudicacion);
        }

        adjudicacion.monto_inicial = version==1?adjudicacion.monto:(await app.dao.adjudicacion_historico.obtenerPorId(ID_ADJUDICACION,1)).monto;
        adjudicacion.modificaciones = await app.dao.adjudicacion_historico.obtenerModificaciones(ID_ADJUDICACION, version);

        if (version_actual !== version) {
          //proyecto = await app.dao.proyecto_historico.obtener(ID_PROYECTO, version);
          proyecto.dataValues.modulos = await app.dao.modulo_historico.obtener(ID_PROYECTO, ID_ADJUDICACION, version);
        } else {
          proyecto.dataValues.modulos = await app.dao.modulo.obtenerDatosProyectoHistoricosAdjudicacion(ID_PROYECTO,ID_ADJUDICACION, version - 1);
        }

        proyecto.dataValues.version_inicial = await app.dao.adjudicacion_historico.obtenerDatosModificadosVersion(ID_PROYECTO,ID_ADJUDICACION, 1);
        proyecto.dataValues.version_anterior = await app.dao.adjudicacion_historico.obtenerDatosModificadosVersion(ID_PROYECTO,ID_ADJUDICACION, version - 1);
        let amp1 = proyecto.dataValues.convenio.plazo_convenio_ampliado;
        let amp2 = proyecto.dataValues.convenio.plazo_convenio_ampliado2;
        let amp3 = proyecto.dataValues.convenio.plazo_convenio_ampliado3;
        let amp4 = proyecto.dataValues.convenio.plazo_convenio_ampliado4;
        let amp5 = proyecto.dataValues.convenio.plazo_convenio_ampliado5;
        proyecto.dataValues.plazo_ejecucion = await app.dao.supervision.sumaAdendas(proyecto.dataValues.plazo_ejecucion, amp1, amp2, amp3, amp4, amp5);
        const HISTORIAL = version > 1 ? util.json(await app.dao.adjudicacion_historico.obtenerHistoricoModificaciones(ID_PROYECTO, ID_ADJUDICACION, adjudicacion.tipo_modificacion, version)) : [];
        // Nombres contrato modificatorio
        let nro = [0,0,0];
        for(let i in HISTORIAL) {
          switch(HISTORIAL[i].tipo_modificacion) {
            case 'TM_ORDEN_TRABAJO': nro[0]++; HISTORIAL[i].tipo_modificacion_adjudicacion.nombre += ` N° ${nro[0]}`; break;
            case 'TM_ORDEN_CAMBIO': nro[1]++; HISTORIAL[i].tipo_modificacion_adjudicacion.nombre += ` N° ${nro[1]}`; break;
            case 'TM_CONTRATO_MODIFICATORIO': nro[2]++; HISTORIAL[i].tipo_modificacion_adjudicacion.nombre += ` N° ${nro[2]}`; break;
          }
        }
        proyecto.dataValues.modificaciones = {
          // nombre: adjudicacion.tipo_modificacion ? (version > 1 ? `${await app.dao.parametrica.obtenerNombre(adjudicacion.tipo_modificacion)} N° ${HISTORIAL.length + 1}` : null) : 'Contrato original',
          nombre: adjudicacion.tipo_modificacion ? (version > 1 ? `${await app.dao.parametrica.obtenerNombre(adjudicacion.tipo_modificacion)}` : null) : 'Contrato original',
          historial: HISTORIAL
        };
        switch(adjudicacion.tipo_modificacion) {
          case 'TM_ORDEN_TRABAJO': nro[0]++; proyecto.dataValues.modificaciones.nombre += ` N° ${nro[0]}`; break;
          case 'TM_ORDEN_CAMBIO': nro[1]++; proyecto.dataValues.modificaciones.nombre += ` N° ${nro[1]}`; break;
          case 'TM_CONTRATO_MODIFICATORIO': nro[2]++; proyecto.dataValues.modificaciones.nombre += ` N° ${nro[2]}`; break;
        }
        proyecto.dataValues.tipo_modificacion = adjudicacion.tipo_modificacion;
        proyecto.dataValues.fecha_modificacion = adjudicacion.fecha_modificacion;
        //proyecto.dataValues.fecha_inicio_obra = adjudicacion.orden_proceder;

        proyecto.dataValues.adjudicacion = adjudicacion;
        proyecto.dataValues.plazo_ampliacion = adjudicacion.plazo_ampliacion;
        if(!adjudicacion.orden_proceder) adjudicacion.orden_proceder = proyecto.dataValues.orden_proceder;
        let fechasRecepcionAdjudicacion = await app.dao.adjudicacion.calcularFechasRecepcion(adjudicacion, proyecto);
        if(fechasRecepcionAdjudicacion){
          proyecto.dataValues.fecha_recepcion_provisional  = fechasRecepcionAdjudicacion.fecha_recepcion_provisional;
          proyecto.dataValues.fecha_recepcion_definitiva  = fechasRecepcionAdjudicacion.fecha_recepcion_definitiva;
        }

        await app.dao.estado.prepararDatosPermitidos(adjudicacion, ID_ROL);
        proyecto.dataValues.estado_actual = adjudicacion.estado_actual;


        res.json({
          finalizado: true,
          mensaje: 'Datos de la adjudicación obtenidos correctamente.',
          datos: proyecto
        });
      } catch (e) {
        _t.rollback();
        next(e);
      }
    });
  };

  const obtenerHistorialModificaciones = (req, res, next) => {
    const ID_PROYECTO = req.params.id;
    app.dao.common.crearTransaccion(async (_t) => {
      try {
        const HISTORIAL = await app.dao.proyecto_historico.obtenerHistoricoModificaciones(ID_PROYECTO);
        const ACTUAL = await app.dao.proyecto.obtenerDatosModificacion(ID_PROYECTO);
        let lista = _.groupBy(ACTUAL.concat(HISTORIAL), (e) => { return e.tipo_modificacion; });
        let nlista = [];
        for (let i in lista) {
          let modificaciones = _.orderBy(lista[i], (e) => { return e.version; }, 'asc');
          for (let j = 0; j < modificaciones.length; j++) {
            if (modificaciones[j].version > 1 && modificaciones[j].tipo_modificacion_proyecto && modificaciones[j].tipo_modificacion_proyecto.hasOwnProperty('nombre')) {
              modificaciones[j].nombre = `${modificaciones[j].tipo_modificacion_proyecto.nombre} N° ${j + 1}`;
            }
            delete modificaciones[j].dataValues.tipo_modificacion_proyecto;
            nlista.push(modificaciones[j]);
          }
        }
        res.json({
          finalizado: true,
          mensaje: 'Datos del proyecto obtenidos correctamente.',
          datos: _.orderBy(nlista, (e) => { return e.version; }, 'desc')
        });
      } catch (e) {
        _t.rollback();
        next(e);
      }
    });
  };

  const obtenerHistorialModificacionesAdjudicacion = (req, res, next) => {
    const ID_PROYECTO = req.params.id;
    const ID_ADJUDICACION = req.params.id_adjudicacion;
    app.dao.common.crearTransaccion(async (_t) => {
      try {
        const HISTORIAL = await app.dao.adjudicacion_historico.obtenerHistoricoModificaciones(ID_PROYECTO,ID_ADJUDICACION);
        const ACTUAL = await app.dao.adjudicacion.obtenerDatosModificacion(ID_PROYECTO,ID_ADJUDICACION);
        let lista = _.groupBy(ACTUAL.concat(HISTORIAL), (e) => { return e.tipo_modificacion; });
        let nlista = [];
        for (let i in lista) {
          let modificaciones = _.orderBy(lista[i], (e) => { return e.version; }, 'asc');
          for (let j = 0; j < modificaciones.length; j++) {
            modificaciones[j].dataValues.nombre = "Contrato Original";
            let _tipo_modificacion_adjudicacion = modificaciones[j].dataValues.hasOwnProperty('tipo_modificacion_adjudicacion')?util.json(modificaciones[j].dataValues.tipo_modificacion_adjudicacion):{};
            //if(modificaciones[j].dataValues.tipo_modificacion_adjudicacion) modificaciones[j].tipo_modificacion_adjudicacion = util.json(modificaciones[j].dataValues.tipo_modificacion_adjudicacion);

            if (modificaciones[j].dataValues.version > 1 && _tipo_modificacion_adjudicacion && _tipo_modificacion_adjudicacion.hasOwnProperty('nombre')) {
              modificaciones[j].dataValues.nombre = `${_tipo_modificacion_adjudicacion.nombre} N° ${j + 1}`;
            }
            //delete modificaciones[j].dataValues.tipo_modificacion_adjudicacion;
            nlista.push(modificaciones[j]);
          }
        }

        let adjudicacion = util.json(await app.dao.adjudicacion.obtenerPorId(ID_ADJUDICACION));
        res.json({
          finalizado: true,
          mensaje: 'Datos de la Adjudicación obtenidos correctamente.',
          datos: _.orderBy(nlista, (e) => { return e.version; }, 'desc'),
          en_modificacion: adjudicacion.estado_adjudicacion !== 'ADJUDICADO_FDI'
        });
      } catch (e) {
        _t.rollback();
        next(e);
      }
    });
  };

  let empresasParalizar = (req, res, next) => {
    app.dao.common.crearTransaccion(async (t) => {
      const ID_PROYECTO = req.params.id;
      await app.dao.proyecto.paralizarProyecto(req.body.audit_usuario, ID_PROYECTO, t);
    }).then(resp => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Proyecto paralizado.',
        datos: null
      });
    }).catch(error => {
      return next(error);
    });
  }

  let empresasAvance = (req, res, next) => {
    app.dao.common.crearTransaccion(async (t) => {
      const ID_PROYECTO = req.params.id;
      return await app.dao.proyecto.empresasAvance(req.body.audit_usuario, ID_PROYECTO, t);
    }).then(resp => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Porcentajes de avance fisico/financiero de las empresas.',
        datos: resp
      });
    }).catch(error => {
      return next(error);
    });
  }

  async function notificar (req, res, next) {
    const CORREO = req.body.correo;
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    const ID_CONTACTO = req.params.id_contacto;
    return app.dao.common.crearTransaccion(async (t) => {
      if (!app.dao.rol.esTecnico(req.body.audit_usuario.id_rol)) {
        throw new errors.NotFoundError(`No tiene acceso a la ruta especificada.`);
      }
      const datosProyecto = await app.dao.proyecto.obtenerDatosProyecto(ID_PROYECTO);
      const datosContacto = await app.src.db.models.usuario.buscarIncluyeOne(ID_CONTACTO, app.src.db.models.persona, app.src.db.models.usuario_rol, app.src.db.models.rol);
      if (CORREO !== datosContacto.persona.correo) {
        await app.dao.persona.actualizarDatos(datosContacto.persona.id_persona, {correo: CORREO}, ID_USUARIO);
        datosContacto.persona.correo = CORREO;
      }
      await app.dao.proyecto.notificarRegistroEquipoTecnico (datosProyecto, datosContacto, datosContacto.persona, datosContacto.id_usuario, t);
    }).then(() => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se reenvió una notificación por correo exitosamente.',
        datos: null
      });
    }).catch(error => {
      return next(error);
    });
  }

  async function reemplazarContacto (req, res, next) {
    const ID_INSTITUCION = req.body.audit_usuario.institucion.id;
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    const ID_CONTACTO = req.params.id_contacto;
    const persona = req.body;
    return app.dao.common.crearTransaccion(async (t) => {
      let PROYECTO = await app.dao.proyecto.getPorId(ID_PROYECTO);
      PROYECTO._usuario_modificacion = ID_USUARIO;

      const AUTORIDAD = await app.dao.persona.crearRecuperar(req.body, ID_USUARIO, t);

      PROYECTO.fid_autoridad_beneficiaria = AUTORIDAD.id_persona;
      await PROYECTO.save({ transaction: t });

      let resp = {};
      resp.autoridad_beneficiaria = util.json(AUTORIDAD);
      const EXISTE_USUARIO = await app.dao.usuario.existe(AUTORIDAD.numero_documento, AUTORIDAD.fecha_nacimiento);
      const ID_ROL = 18; // Responsable de GAM/GAIOC
      let persona = JSON.parse(JSON.stringify(AUTORIDAD));
      persona.noVerificar = req.body.noVerificar;
      const DATOS_USUARIO = await app.dao.usuario.crearRecuperar(persona, {idRol:ID_ROL, cod: 'TODOS', idUsuario: ID_USUARIO, idInstitucion: ID_INSTITUCION});

      // const datosProyecto = await app.dao.proyecto.getProyectoId(ID_PROYECTO);

      if (DATOS_USUARIO && AUTORIDAD) {
        await app.dao.proyecto.notificarRegistroEquipoTecnico(PROYECTO, DATOS_USUARIO, AUTORIDAD, ID_USUARIO, t);
      }

      let datos = {
        estado: 'ACTIVO',
        _usuario_modificacion: ID_USUARIO
      }

      // const usuarioAnterior = await app.src.db.models.usuario.buscarIncluyePorPersona(ID_CONTACTO, app.src.db.models.persona, app.src.db.models.usuario_rol, app.src.db.models.rol);

      // if (usuarioAnterior) {
      //   await app.src.db.models.transicion.update({
      //     fid_usuario: DATOS_USUARIO.id_usuario
      //   },{ where: { fid_usuario: ID_CONTACTO } },{ transaction: t })
      //   await app.src.db.models.transicion.update({
      //     fid_usuario_asignado: DATOS_USUARIO.id_usuario
      //   },{ where: { fid_usuario_asignado: ID_CONTACTO } },{ transaction: t })
      // }

      await app.dao.transicion.reemplazarUsuario(ID_USUARIO, ID_CONTACTO, DATOS_USUARIO.id_usuario, ID_PROYECTO, t);

      await app.src.db.models.usuario.update(datos, {where: {id_usuario: ID_CONTACTO}}, {transaction: t});

      // Guardar registro log
      let proyecto = await app.dao.proyecto.obtenerEstado(ID_PROYECTO);
      let rol = await app.dao.rol.getId(ID_ROL);
      let us1 = await app.dao.usuario.getUsuarioPersona(ID_CONTACTO);
      await app.dao.log.crearLog(ID_PROYECTO, 'proyecto', req.body.audit_usuario, proyecto.estado_actual.codigo,
        `Reasignación de ${rol.descripcion}. ${us1.persona.nombres||''} ${us1.persona.primer_apellido||''} -> ${persona.nombres||''} ${persona.primer_apellido||''}`);

      return resp;
    }).then((response) => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se reemplazó los datos exitosamente.',
        datos: response
      });
    }).catch(error => {
      return next(error);
    });
  }

  _app.controller.proyecto = {
    adminObtener,
    adminObtenerUsuario,
    adminModificarUsuario,
    obtener,
    obtenerPorId,
    obtenerPorQr,
    scanqr,
    crear,
    actualizar,
    adjudicar,
    obtenerProcesadoMovil,
    obtenerMatricula,
    obtenerMatriculaId,
    obtenerUsuarios,
    obtenerDocumento,
    listarProcesados,
    listarProcesadosv2,
    listarProcesadosMovil,
    obtenerProcesado,
    crearModificacion,
    crearModificacionAdjudicacion,
    obtenerModificacion,
    obtenerModificacionAdjudicacion,
    obtenerHistorialModificaciones,
    obtenerHistorialModificacionesAdjudicacion,
    empresasParalizar,
    empresasAvance,
    notificar,
    reemplazarContacto,
    obtenerDuplicados
  };
};
