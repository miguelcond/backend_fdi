const util = require('../../../lib/util');
const errors = require('../../../lib/errors');

module.exports = (app) => {
  const _app = app;
  const models = app.src.db.models;
  const moduloModel = app.src.db.models.modulo;
  const itemModel = app.src.db.models.item;
  const parametricaModel = app.src.db.models.parametrica;
  const sequelize = app.src.db.sequelize;

  const actualizar = async (idModulo, modulo, atributos, idUsuario, t) => {
    let moduloRes = await moduloModel.findById(idModulo, {
      transaction: t
    });
    if (!moduloRes) {
      throw new Error('No se encontró el modulo.');
    }
    util.asignarValorAtributos(moduloRes, modulo, atributos.modulo.put);
    moduloRes._usuario_modificacion = idUsuario;
    moduloRes = await moduloRes.save({
      transaction: t
    });
    if (modulo.items) {
      for (let i = 0; i < modulo.items.length; i++) {
        let item = {
          id_item: modulo.items[i].id_item
        };
        util.asignarValorAtributos(item, modulo.items[i], atributos.item.put);
        if (item.id_item) {
          item._usuario_modificacion = idUsuario;
          await itemModel.update(item, {
            where: {
              id_item: item.id_item
            },
            transaction: t
          });
        } else {
          item.fid_modulo = idModulo;
          item.nro_item = 0;
          item._usuario_creacion = idUsuario;
          await itemModel.create(item, {
            transaction: t
          });
        }
      }
    }
    await app.dao.item.actualizarNro(moduloRes.fid_proyecto, t);
    const res = await moduloModel.moduloIncluye({
      condicionModulo: {
        id_modulo: idModulo
      }
    }, t);
    return res.rows[0];
  };

  const obtenerModuloId = async (idModulo) => {
    const MODULO = await moduloModel.findById(idModulo);
    if (!MODULO) {
      throw new errors.NotFoundError('No se encontró el módulo.');
    }
    return MODULO;
  };

  const obtenerModulosProyecto = async (idProyecto) => {
    let query = {
      where: {
        fid_proyecto: idProyecto,
        estado: 'ACTIVO'
      }
    };
    return models.modulo.findAll(query);
  };

  const obtenerModulosAdjudicacionProyecto = async (idProyecto,idAdjudicacion) => {
    let query = {
      where: {
        fid_proyecto: idProyecto,
        fid_adjudicacion: idAdjudicacion,
        estado: 'ACTIVO'
      }
    };
    return models.modulo.findAll(query);
  };

  const obtenerEstadoProyecto = async (idModulo) => {
    let query = {
      include: [{
        attributes: ['id_proyecto', 'tipo_modificacion'],
        model: models.proyecto,
        as: 'proyecto',
        include: [
          {
            attributes: ['codigo', 'nombre', 'atributos_detalle'],
            model: models.estado,
            as: 'estado_actual'
          }
        ]
      },{
        attributes: ['id_adjudicacion', 'tipo_modificacion'],
        model: models.adjudicacion,
        as: 'adjudicacion',
        include: [
          {
            attributes: ['codigo', 'nombre', 'atributos_detalle'],
            model: models.estado,
            as: 'estado_actual'
          }
        ]
      }],
      where: {
        id_modulo: idModulo
      }
    };
    const MODULO = await models.modulo.findOne(query);
    if (!MODULO) {
      throw new Error('No se encontró el proyecto.');
    }
    return MODULO;
  };

  const filtrosPorProyecto = (idProyecto, idAdjudicacion) => {
    let query = {
      attributes: ['id_modulo', 'nombre'],
      where: {
        fid_proyecto: idProyecto,
        fid_adjudicacion: idAdjudicacion,
        estado: 'ACTIVO'
      },
      include: {
        attributes: ['id_item', 'nro_item', 'nombre', 'unidad_medida', 'cantidad', 'precio_unitario', 'peso_ponderado', 'especificaciones', 'analisis_precios', 'estado', 'cantidad_anterior_fis', 'cantidad_actual_fis'],
        model: itemModel,
        as: 'items',
        where: {
          $or: [{estado: 'ACTIVO'}, {estado: 'CERRADO'}]
        },
        include: [
          {
            attributes: ['nombre', 'descripcion'],
            model: parametricaModel,
            as: 'unidad'
          }
        ]
      },
      order: [[{model: itemModel, as: 'items'}, 'nro_item']]
    };
    return query;
  };

  const filtrosPorAdjudicacionProyecto = (idProyecto, idAdjudicacion) => {
    console.log(`************id adjudicacion: ${idAdjudicacion}******************`);
    let query = {
      attributes: ['id_modulo', 'nombre'],
      where: {
        fid_proyecto: idProyecto,
        fid_adjudicacion: idAdjudicacion,
        estado: 'ACTIVO'
      },
      include: {
        attributes: ['id_item', 'nro_item', 'nombre', 'unidad_medida', 'cantidad', 'precio_unitario', 'peso_ponderado', 'especificaciones', 'analisis_precios', 'estado', 'cantidad_anterior_fis', 'cantidad_actual_fis', 'regularizar','variacion'],
        model: itemModel,
        as: 'items',
        where: {
          $or: [{estado: 'ACTIVO'}, {estado: 'CERRADO'}]
        },
        include: [
          {
            attributes: ['nombre', 'descripcion'],
            model: parametricaModel,
            as: 'unidad'
          }
        ]
      },
      order: [[{model: itemModel, as: 'items'}, 'nro_item']]
    };
    return query;
  };

  const obtenerDatosProyecto = async (idProyecto, idSupervision, idAdjudicacion, conHistoricos = false) => {
    let consulta = filtrosPorAdjudicacionProyecto(idProyecto, idAdjudicacion);
    // consulta.include.attributes.push([sequelize.fn('COALESCE', sequelize.literal(`(SELECT SUM(cantidad_parcial) FROM computo_metrico WHERE fid_item = "items"."id_item" AND estado <> 'ELIMINADO' AND fid_supervision < ${idSupervision})::Decimal(12, 2)::float`), 0), 'cantidad_anterior_fis']);
    // consulta.include.attributes.push([sequelize.fn('COALESCE', sequelize.literal(`(SELECT SUM(cantidad_parcial) FROM computo_metrico WHERE fid_item = "items"."id_item" AND estado <> 'ELIMINADO' AND fid_supervision = ${idSupervision})::Decimal(12, 2)::float`), 0), 'cantidad_actual_fis']);
    consulta.include.attributes.push([sequelize.literal(`(CASE WHEN(SELECT cantidad FROM item_historico WHERE id_item="items"."id_item" ORDER BY "version" DESC LIMIT 1) = "items"."cantidad" THEN false ELSE true END)`), 'modificado']);
    if (conHistoricos) {
      consulta.include.include.push({
        model: models.item_historico,
        as: 'item_historico',
        attributes: ['version', 'cantidad', 'precio_unitario', 'peso_ponderado',
          [sequelize.literal(`("items->item_historico"."cantidad" * "items->item_historico"."precio_unitario")::Decimal(12, 2)::float`), 'total'],
          [sequelize.literal(`(CASE WHEN "items->item_historico".version = 1 OR (SELECT ih.cantidad FROM item_historico ih WHERE ih.id_item="items->item_historico"."id_item" AND ih.version="items->item_historico".version - 1) = "items->item_historico"."cantidad" THEN false ELSE true END)`), 'modificado'],
          [sequelize.literal(`(CASE WHEN "items->item_historico".version = 1 THEN 'INICIAL' ELSE (SELECT tipo_modificacion FROM proyecto_historico WHERE id_proyecto = ${idProyecto} AND version = "items->item_historico".version) END)`), 'modificacion']
        ]
      });
      consulta.order.push([sequelize.literal('"items->item_historico".version'), 'ASC']);
    }
    const MODULOS = await moduloModel.findAll(consulta);
    if (!MODULOS) {
      throw new errors.ValidationError(`No se encontraron módulos para el proyecto solicitado`);
    }
    return MODULOS;
  };

  const obtenerDatosProyectoHistoricos = async (idProyecto, version) => {
    const ULTIMA_SUPERVISION = await app.dao.supervision.obtenerUltimaAprobada(idProyecto);
    let consulta = filtrosPorProyecto(idProyecto);
    consulta.include.attributes.push([sequelize.literal(`(
      SELECT COALESCE(SUM(cm.cantidad_parcial), 0)
      FROM computo_metrico cm
      WHERE cm.fid_item = "items".id_item and cm.fid_supervision <= ${ULTIMA_SUPERVISION ? ULTIMA_SUPERVISION.id_supervision : 0}
        AND cm.estado = 'ACTIVO'
    )`), 'avance_actual']);
    consulta.include.include.push({
      attributes: ['cantidad'],
      model: models.item_historico,
      as: 'version_anterior',
      where: {
        version
      },
      required: false
    });
    consulta.include.include.push({
      attributes: ['cantidad'],
      model: models.item_historico,
      as: 'version_inicial',
      where: {
        version: 1
      },
      required: false
    });
    const MODULOS = await moduloModel.findAll(consulta);
    if (!MODULOS) {
      throw new errors.ValidationError(`No se encontró los módulos para el proyecto solicitado`);
    }
    return MODULOS;
  };

  const obtenerDatosProyectoHistoricosAdjudicacion = async (idProyecto, idAdjudicacion, version) => {
    const ULTIMA_SUPERVISION = await app.dao.supervision.obtenerUltimaAprobada(idProyecto,idAdjudicacion);
    let consulta = filtrosPorAdjudicacionProyecto(idProyecto,idAdjudicacion);
    consulta.include.attributes.push([sequelize.literal(`(
      SELECT COALESCE(SUM(cm.cantidad_parcial), 0)
      FROM computo_metrico cm
      WHERE cm.fid_item = "items".id_item and cm.fid_supervision <= ${ULTIMA_SUPERVISION ? ULTIMA_SUPERVISION.id_supervision : 0}
        AND cm.estado = 'ACTIVO'
    )`), 'avance_actual']);
    consulta.include.include.push({
      attributes: ['cantidad'],
      model: models.item_historico,
      as: 'version_anterior',
      where: {
        version
      },
      required: false
    });
    consulta.include.include.push({
      attributes: ['cantidad'],
      model: models.item_historico,
      as: 'version_inicial',
      where: {
        version: 1
      },
      required: false
    });
    const MODULOS = await moduloModel.findAll(consulta);
    if (!MODULOS) {
      throw new errors.ValidationError(`No se encontró los módulos para el proyecto solicitado`);
    }
    return MODULOS;
  };

  const eliminar = async (idModulo) => {
    await models.modulo.destroy({ where: { id_modulo: idModulo } });
  };

  _app.dao.modulo = {
    actualizar,
    obtenerModuloId,
    obtenerModulosProyecto,
    obtenerModulosAdjudicacionProyecto,
    obtenerEstadoProyecto,
    filtrosPorProyecto,
    filtrosPorAdjudicacionProyecto,
    obtenerDatosProyecto,
    obtenerDatosProyectoHistoricos,
    obtenerDatosProyectoHistoricosAdjudicacion,
    eliminar
  };
};
