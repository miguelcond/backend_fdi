const tmp = require('tmp');
const util = require('../../../lib/util');
const fs = require('fs-extra');
const fileType = require('file-type');
const formidable = require('formidable');
const errors = require('../../../lib/errors');
const _ = require('lodash');

module.exports = (app) => {
  const _app = app;
  _app.controller.item = {};
  const itemController = _app.controller.item;
  const itemModel = app.src.db.models.item;
  const sequelize = app.src.db.sequelize;

  const importarCsv = async (req, res, next) => {
    const idUsuario = req.body.audit_usuario.id_usuario;
    const items = req.body;
    const partes = items.base64.split('base64,');
    let decoded;
    if (partes.length === 2) {
      decoded = Buffer.from(partes[1], 'base64');
    } else {
      decoded = Buffer.from(items.base64, 'base64');
    }
    const file = tmp.tmpNameSync();
    fs.writeFileSync(file, decoded);
    const type = fileType(decoded);
    if (type && type.mime === 'application/zip') {
      try {
        app.dao.item.sheet_to_csv(file);
      } catch (error) {
        res.status(412).json({
          finalizado: false,
          mensaje: 'El contenido del archivo es incorrecto.',
          datos: null
        });
        return;
      }
    }
    app.dao.common.crearTransaccion(async (_t) => {
      //try {
        const datos = await util.csvAjson(file);
        if (datos.length <= 0) {
          throw new errors.ValidationError(`El archivo NO debe contener filas vacias al inicio`);
        }
        if (datos.length > 1500) {
          throw new errors.ValidationError(`El archivo tiene mas de 1500 filas?`);
        }
        const tieneComponente = !util.esNuloOIndefinido(datos[0].componente);
        const tieneModulo = !util.esNuloOIndefinido(datos[0].modulo);
        const tieneNombre = !util.esNuloOIndefinido(datos[0].nombre);
        const tieneUnidad = !util.esNuloOIndefinido(datos[0].unidad);
        const tieneCantidad = !util.esNuloOIndefinido(datos[0].cantidad);
        const tienePrecioUnitario = !util.esNuloOIndefinido(datos[0].precio_unitario);
        if (!tieneComponente || !tieneModulo || !tieneNombre || !tieneUnidad || !tieneCantidad || !tienePrecioUnitario) {
          throw new errors.ValidationError(`El archivo .ods debe contener las columnas: componente, modulo, nombre, unidad, cantidad y precio_unitario.`);
        }
        const estadoModulo = await _app.dao.proyecto.obtenerEstado(items.id_proyecto, _t);
        if (!estadoModulo.estado_actual.atributos_detalle || (estadoModulo.estado_actual.atributos_detalle.item.acciones.indexOf('put') === -1)) {
          throw new errors.ValidationError(`No puede modificar un proyecto en estado "${estadoModulo.estado_actual.nombre}".`);
        }
        // const nItems = await app.dao.item.cargarArchivo(items.id_proyecto, file, idUsuario, _t);
        let nItems = await app.dao.item.cargarArchivoJson(items.id_proyecto, items.id_adjudicacion, datos, idUsuario, _t);
        nItems=util.json(nItems);
        res.json({
          finalizado: true,
          mensaje: 'Items registrados correctamente.',
          datos: nItems
        });
      // } catch (e) {
      //   _t.rollback();
      //   next(e);
      // }
    }).catch(e=>{
      console.error('ERROR',e.message);
      next(e)
    });
  };

  itemController.postBinary = (req, res) => {
    const idUsuario = req.body.audit_usuario.id_usuario;
    const form = new formidable.IncomingForm();
    form.parse(req, (errUpload, fields, files) => {
      try {
        if (!files.file) {
          throw new Error('Debe enviar un archivo.');
        }
        let data = fs.readFileSync(files.file.path);
        const type = fileType(data);
        if (type && type.mime === 'application/zip') {
          try {
            app.dao.item.sheet_to_csv(files.file.path);
          } catch (error) {
            throw new Error('El contenido del archivo es incorrecto.');
          }
        }
        sequelize.transaction().then((t) => {
          app.dao.item.cargarArchivo(parseInt(fields.id_proyecto), files.file.path, idUsuario, t)
          .then((result) => {
            t.commit();
            res.json({
              finalizado: true,
              mensaje: 'Items registrados correctamente.',
              datos: result
            });
          }).catch((error) => {
            t.rollback();
            res.status(412).json({
              finalizado: false,
              mensaje: error.message,
              datos: null
            });
          });
        });
      } catch (error) {
        res.status(412).json({
          finalizado: false,
          mensaje: error.message,
          datos: null
        });
      }
    });
  };

  itemController.get = (req, res) => {
    itemModel.findAndCount({
      include: {
        model: sequelize.models.modulo,
        as: 'modulo',
        where: {
          $and: [
            app.src.lib.util.formarConsulta(req.query, itemModel, app.src.db.models).condicionModulo,
            {
              estado: 'ACTIVO'
            }
          ]
        }
      },
      where: {
        $and: [
          app.src.lib.util.formarConsulta(req.query, itemModel, app.src.db.models).condicionItem,
          {
            estado: 'ACTIVO'
          }
        ]
      }
    }).then((result) => {
      res.json({
        finalizado: true,
        mensaje: 'Items recuperados correctamente.',
        datos: result
      });
    }).catch((error) => {
      res.status(412).json({
        finalizado: false,
        mensaje: error.message,
        datos: null
      });
    });
  };

  async function getProyecto (req, res, next) {
    try {
      const proyecto = await app.dao.proyecto.obtenerDatosProyecto(req.params.id);
      if (!proyecto) {
        throw new errors.NotFoundError(`El proyecto no existe.`);
      }
      const consulta = app.src.lib.util.formarConsulta(req.query, itemModel, app.src.db.models);
      const items = await app.dao.item.obtenerDatosItems(req.params.id, consulta);
      res.json({
        finalizado: true,
        mensaje: 'Items recuperados correctamente.',
        datos: items
      });
    } catch (e) {
      next(e);
    }
  };

  async function getModulo (req, res, next) {
    try {
      const consulta = app.src.lib.util.formarConsulta(req.query, itemModel, app.src.db.models);
      const items = await app.dao.item.obtenerDatosItemsModulo(req.params.id, consulta);

      res.json({
        finalizado: true,
        mensaje: 'Items recuperados correctamente.',
        datos: items
      });
    } catch (e) {
      next(e);
    }
  };

  itemController.getId = (req, res) => {
    app.dao.item.obtenerPorId(req.params.id, req.query).then((result) => {
      res.json({
        finalizado: true,
        mensaje: 'Item recuperado correctamente.',
        datos: result
      });
    }).catch((error) => {
      res.status(412).json({
        finalizado: false,
        mensaje: error.message,
        datos: null
      });
    });
  };

  const _validarEdicion = (proyecto, accion) => {
    if (!proyecto.estado_actual.atributos_detalle || (proyecto.estado_actual.atributos_detalle.item.acciones.indexOf(accion) === -1)) {
      throw new errors.ValidationError(`No puede modificar los datos del proyecto porque se encuentra en estado "${proyecto.estado_actual.nombre}".`);
    }
    if (accion === 'post' && proyecto.estado_actual.codigo === 'MODIFICADO_FDI') {
      if (!app.dao.proyecto.esContratoModificatorio(proyecto.tipo_modificacion)) {
        throw new errors.ValidationError(`No puede crear items, porque no corresponde al tipo de modificación que permite realizar esta acción.`);
      }
    }
  };

  const _esValidoSegunUnidad = async (itemOriginal, item) => {
    const UNIDAD_ITEM = !item.unidad_medida ? itemOriginal.unidad_medida : item.unidad_medida;
    const NOMBRE_ITEM = !item.nombre ? itemOriginal.nombre : item.nombre;
    const UNIDAD = await app.dao.parametrica.obtenerPorCodigo(UNIDAD_ITEM);
    switch (UNIDAD_ITEM) {
      // case 'UN_GLOBAL':
      //   if (item.cantidad !== 1) {
      //     throw new errors.ValidationError(`El item ${NOMBRE_ITEM} tiene como unidad "${UNIDAD.nombre}", por tanto debe ser 1.`);
      //   }
      //   break;
      case 'UN_GLOBAL':
      case 'UN_PIEZA':
      case 'UN_PUNTO':
        if (item.cantidad !== parseInt(item.cantidad, 10)) {
          throw new errors.ValidationError(`El item ${NOMBRE_ITEM} tiene como unidad "${UNIDAD.nombre}", por tanto debe ser un entero.`);
        }
        break;
      default:
        if (typeof item.cantidad !== 'number') {
          throw new errors.ValidationError(`La cantidad del item ${NOMBRE_ITEM} debe ser un número.`);
        }
    }
  };

  const guardar = async (req, res, next) => {
    const idUsuario = req.body.audit_usuario.id_usuario;
    const item = req.body;
    const ID_MODULO = item.fid_modulo;
    app.dao.common.crearTransaccion(async (_t) => {
      try {
        const MODULO = await _app.dao.modulo.obtenerEstadoProyecto(ID_MODULO);
        _validarEdicion(MODULO.proyecto, 'post');
        const ID_PROYECTO = MODULO.proyecto.id_proyecto;
        const ID_ADJUDICACION = MODULO.adjudicacion.id_adjudicacion;
        const itemRes = {};
        util.asignarValorAtributos(itemRes, item, MODULO.proyecto.estado_actual.atributos_detalle.item.post);
        await _esValidoSegunUnidad(itemRes, item);
        if (itemRes.especificaciones) {
          itemRes.especificaciones = await app.dao.proyecto.guardarArchivo(itemRes.especificaciones, ID_PROYECTO, 'ET');
        }
        itemRes.nro_item = 0;
        itemRes._usuario_creacion = idUsuario;
        let nItem = await itemModel.create(itemRes);
        await app.dao.item.calcularPrecioModulo(ID_PROYECTO, ID_ADJUDICACION);
        await app.dao.item.actualizarNro(ID_PROYECTO);
        nItem = await itemModel.findById(nItem.dataValues.id_item);
        res.json({
          finalizado: true,
          mensaje: 'Item guardado correctamente.',
          datos: nItem
        });
      } catch (e) {
        _t.rollback();
        next(e);
      }
    });
  };

  const _validarConAvanceActual = async (itemOriginal, item, modulo) => {
    const SUPERVISION = await app.dao.supervision.obtenerUltimaAprobada(modulo.fid_proyecto);
    if (!SUPERVISION) {
      return;
    }
    const AVANCE_ACTUAL = await app.dao.computoMetrico.calculoCantidadAvance(itemOriginal.id_item, 'lte', SUPERVISION.id_supervision);
    if (item.cantidad < AVANCE_ACTUAL) {
      throw new errors.ValidationError(`El item tiene una cantidad de avance de ${AVANCE_ACTUAL}, la cantidad a modificar no debe ser menor al avance mencionado.`);
    }
  };

  /**
   * Si el proyecto está en estado de modificación se valida que solo sean modificadas las cantidades en caso de modificaciones de tipo
   * orden de trabajo u orden de cambio o si es contratro modificatorio y el estado del módulo es distinto a MODIFICACION.
   */
  const obtenerDatosPermitidos = async (itemOriginal, item, modulo) => {
    // if (app.dao.estado.esModificacionProyecto(modulo.proyecto.estado_actual)) {
    if (app.dao.estado.esModificacionAdjudicacion(modulo.adjudicacion.estado_actual)) {
      // const TIPO_MODIFICACION = modulo.proyecto.tipo_modificacion;
      const TIPO_MODIFICACION = modulo.adjudicacion.tipo_modificacion;
      if (app.dao.proyecto.esOrdenTrabajo(TIPO_MODIFICACION) || app.dao.proyecto.esOrdenCambio(TIPO_MODIFICACION) || (app.dao.proyecto.esContratoModificatorio(TIPO_MODIFICACION) && modulo.estado !== 'MODIFICACION')) {
        await _validarConAvanceActual(itemOriginal, item, modulo);
        item = {
          cantidad: item.cantidad,
          especificaciones: item.especificaciones,
          precio_unitario: item.precio_unitario,
          analisis_precios: item.analisis_precios,
          peso_ponderado: item.peso_ponderado,
          variacion: item.variacion,
          regularizar: item.regularizar
        };
      }
    }
    return item;
  };

  const actualizar = async (req, res, next) => {
    const idUsuario = await req.body.audit_usuario.id_usuario;
    let item = req.body;
    // console.log(req.body)
    app.dao.common.crearTransaccion(async (_t) => {
      try {
        let itemRes = await app.dao.item.obtenerPorId(req.params.id, {}, false);
        // console.log(itemRes,"Item Res...")
        if (itemRes.estado === 'CERRADO') {
          throw new errors.ValidationError(`No puede modificar un item completado.`);
        }
        if (util.esNumero(item.cantidad) && !isNaN(item.cantidad)) {
          await _esValidoSegunUnidad(itemRes, item);
        }
        const MODULO = await _app.dao.modulo.obtenerEstadoProyecto(itemRes.fid_modulo);
        _validarEdicion(MODULO.proyecto, 'put');
        const ID_PROYECTO = MODULO.proyecto.id_proyecto;
        const ID_ADJUDICACION = MODULO.adjudicacion.id_adjudicacion;
        item = await obtenerDatosPermitidos(itemRes, item, MODULO);
        // await util.asignarValorAtributos(itemRes, item, MODULO.proyecto.estado_actual.atributos_detalle.item.put);
        if (MODULO.adjudicacion.estado_actual && MODULO.adjudicacion.estado_actual.atributos_detalle && MODULO.adjudicacion.estado_actual.atributos_detalle.item && MODULO.adjudicacion.estado_actual.atributos_detalle.item.put) {
          await util.asignarValorAtributos(itemRes, item, MODULO.adjudicacion.estado_actual.atributos_detalle.item.put);
        } else {
          throw new errors.ValidationError(`No puede realizar esta modificación en el estado actual.`);
        }
        if (item.especificaciones && item.especificaciones.base64) {
          itemRes.especificaciones = await app.dao.proyecto.guardarArchivo(itemRes.especificaciones, ID_PROYECTO, `ET_${itemRes.id_item}`);
        }
        if (item.analisis_precios && item.analisis_precios.base64) {
          itemRes.analisis_precios = await app.dao.proyecto.guardarArchivo(itemRes.analisis_precios, ID_PROYECTO, `APU_${itemRes.id_item}`);
        }
        if (item.cantidad || item.precio_unitario) {
          itemRes.dataValues.precio_unitario = util.redondear(itemRes.dataValues.precio_unitario);
        }
        itemRes._usuario_modificacion = idUsuario;
        const ITEM_ACTUALIZADO = await itemRes.save();
        if (item.cantidad) {
          await app.dao.item.calcularPrecioModulo(ID_PROYECTO, ID_ADJUDICACION);
        }

        // console.log(ITEM_ACTUALIZADO,"ITEM_ACTUALIZADO")
        res.json({
          finalizado: true,
          mensaje: 'Item actualizado correctamente.',
          datos: ITEM_ACTUALIZADO
        });
      } catch (e) {
        _t.rollback();
        next(e);
      }
    });
  };

  const _verificarAccionEnModificacion = async (proyecto, item) => {
    if (proyecto.estado_actual.codigo === 'MODIFICADO_FDI') {
      const MODULOS = await app.dao.modulo_historico.obtenerUltimaVersion(proyecto.id_proyecto);
      if (_.find(MODULOS, {id_modulo: item.fid_modulo})) {
        throw new errors.ValidationError(`El item corresponde a un módulo ya aprobado, no puede ejecutar la acción.`);
      }
    }
  };

  const _verificarRelaciones = async (item) => {
    const COMPUTOS = await app.dao.computoMetrico.getSupervisionesItem(item.id_item);
    if (COMPUTOS.length > 0) {
      throw new errors.ValidationError(`El item tiene registros de cómputos métricos`);
    }
  };

  const eliminar = async (req, res, next) => {
    app.dao.common.crearTransaccion(async (_t) => {
      try {
        const ITEM = await app.dao.item.obtenerPorId(req.params.id, {}, false);
        const MODULO = await _app.dao.modulo.obtenerEstadoProyecto(ITEM.fid_modulo);
        _validarEdicion(MODULO.proyecto, 'put');
        await _verificarAccionEnModificacion(MODULO.proyecto, ITEM);
        await _verificarRelaciones(ITEM);
        const ID_PROYECTO = MODULO.proyecto.id_proyecto;
        const ID_ADJUDICACION = MODULO.adjudicacion.id_adjudicacion;
        await _app.dao.item.eliminar(ITEM.id_item);
        await _app.dao.proyecto.eliminarArchivo(ID_PROYECTO, `ET_${ITEM.id_item}`);
        await _app.dao.proyecto.eliminarArchivo(ID_PROYECTO, `APU_${ITEM.id_item}`);
        await app.dao.item.calcularPrecioModulo(ID_PROYECTO, ID_ADJUDICACION);
        await app.dao.item.actualizarNro(ID_PROYECTO);
        res.json({
          finalizado: true,
          mensaje: 'Item eliminado correctamente.',
          datos: null
        });
      } catch (e) {
        _t.rollback();
        next(e);
      }
    });
  };

  itemController.getDocumento = (req, res) => {
    itemModel.findById(req.params.id)
    .then((result) => {
      if (result.dataValues.especificaciones) {
        res.json({
          finalizado: true,
          mensaje: 'Especificaciones recuperadas correctamente.',
          datos: app.dao.proyecto.leerArchivo(result.dataValues.especificaciones)
        });
      } else {
        res.json({
          finalizado: false,
          mensaje: 'El archivo no fue cargado.',
          datos: null
        });
      }
    }).catch((error) => {
      res.status(412).json({
        finalizado: false,
        mensaje: error.message,
        datos: null
      });
    });
  };

  async function getSupervisiones (req, res, next) {
    const ID_ITEM = req.params.id;
    try {
      await app.dao.item.obtenerPorId(ID_ITEM);
      const SUPERVISIONES = await app.dao.computoMetrico.getSupervisionesItem(ID_ITEM);
      res.json({
        finalizado: true,
        mensaje: 'Supervisiones del item obtenidas correctamente.',
        datos: SUPERVISIONES
      });
    } catch (e) {
      next(e);
    }
  }

  async function getResumen (req, res, next) {
    app.dao.item.getResumen(req.params.id, req.query).then((result) => {
      res.json({
        finalizado: true,
        mensaje: 'Item recuperado correctamente.',
        datos: result
      });
    }).catch((error) => {
      res.status(412).json({
        finalizado: false,
        mensaje: error.message,
        datos: null
      });
    });
  }

  async function descargarCsvModelo (req, res, next) {
    try {
      const file = `${_path}/files/${req.query.archivo}.${req.query.tipo || 'pdf'}`;
      try {
        fs.readFileSync(file);
        res.set('Content-Type', 'application/pdf');
        res.set('Content-Disposition', `attachment; filename=${req.query.archivo}`);
        res.download(file);
      } catch (e) {
        throw new errors.NotFoundError('No se encontró el archivo solicitado.');
      }
    } catch (error) {
      next(error);
    }
  }

  itemController.importarCsv = importarCsv;
  itemController.getProyecto = getProyecto;
  itemController.getModulo = getModulo;
  itemController.guardar = guardar;
  itemController.actualizar = actualizar;
  itemController.eliminar = eliminar;
  itemController.getSupervisiones = getSupervisiones;
  itemController.getResumen = getResumen;
  itemController.descargarCsvModelo = descargarCsvModelo;
};
