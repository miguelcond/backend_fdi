import errors from '../../../lib/errors';
import util from '../../../lib/util';
import seqOpt from '../../../lib/sequelize-options';
import shortid from 'shortid';
import Reporte from '../../../lib/reportes';
import logger from '../../../lib/logger';
import _ from 'lodash';

module.exports = (app) => {
  app.dao.adjudicacion = {};
  const models = app.src.db.models;
  const sequelize = app.src.db.sequelize;
  const output = {
    model: 'adjudicacion',
    fields: {
      id_adjudicacion: ['int', 'pk'],
      version: ['int'],
      referencia: ['str', 'fk'],
      monto: ['float', 'fk'],
      doc_adjudicacion: ['str'],
      doc_especificaciones_tecnicas: ['str'],
      estado: ['enum'],
      orden_proceder: ['date'],
      fid_proyecto: ['int', 'fk'],
      _fecha_creacion: ['date'],
      _fecha_modificacion: ['date'],
      tipo_modificacion:['enum'],
      estado_actual: {
        model: 'estado',
        fields: {
          codigo: ['str', 'pk'],
          nombre: ['str'],
          tipo: ['enum'],
          acciones: ['str'],
          atributos: ['str'],
          requeridos: ['str'],
          areas: ['str'],
          fid_rol: ['array']
        }
      },
      tipo_modificacion_adjudicacion: {
        model: 'parametrica',
        fields: {
          nombre: ['str']
        }
      },
      observacion: {
        model: 'transicion',
        fields: {
          observacion: ['str'],
          usuario: {
            model: 'usuario',
            fields: {
              id_usuario: ['int'],
              persona: {
                model: 'persona',
                fields: {
                  nombres: ['str'],
                  primer_apellido: ['str'],
                  segundo_apellido: ['str']
                }
              }
            }
          }
        }
      }
    }
  };

  const outputList = {
    model: 'adjudicacion',
    fields: {
      id_adjudicacion: ['int', 'pk'],
      version: ['int'],
      referencia: ['str', 'fk'],
      monto: ['float', 'fk'],
      orden_proceder: ['date'],
      doc_adjudicacion: ['str'],
      doc_especificaciones_tecnicas: ['str'],
      estado: ['enum'],
      fid_proyecto: ['int', 'fk'],
      _fecha_creacion: ['date'],
      _fecha_modificacion: ['date'],
      tipo_modificacion:['enum'],
      estado_actual: {
        model: 'estado',
        fields: {
          codigo: ['str', 'pk'],
          nombre: ['str'],
          tipo: ['enum'],
          acciones: ['str'],
          atributos: ['str'],
          requeridos: ['str'],
          areas: ['str'],
          fid_rol: ['array']
        }
      },
      tipo_modificacion_adjudicacion: {
        model: 'parametrica',
        fields: {
          nombre: ['str']
        }
      }
    }
  };

  async function crear (datos) {
    return models.adjudicacion.create(datos);
  }

  async function actualizar (datos, idAdjudicacion) {
    return models.adjudicacion.update(datos,{where: {id_adjudicacion: idAdjudicacion}});
  }

  const obtener = async (query = {}, idAdjudicacion) => {
    let options = seqOpt.createOptions(query, models, output);
    if (idAdjudicacion) {
      options.where = { id_adjudicacion: idAdjudicacion };
    }
    Object.assign(options, query);
    let datosAdjudicacion = await models.adjudicacion.findOne(options);
    if (!datosAdjudicacion) {
      throw new errors.NotFoundError(`No existe el registro de la adjudicacion solicitado.`);
    }
    return datosAdjudicacion;
  };

  const obtenerPorId = async (idAdjudicacion) => {
    let datosAdjudicacion = await models.adjudicacion.findOne({
      where: {
        id_adjudicacion: idAdjudicacion
      },
      include:[
        {
          model: models.parametrica,
          as: 'tipo_modificacion_adjudicacion',
          attributes: ['nombre']
        },
      {
        model: models.estado,
        as: 'estado_actual',
        attributes: ['codigo', 'nombre', 'tipo', 'acciones', 'atributos', 'requeridos', 'areas', ['fid_rol', 'roles']]
      }]
    });
    if (!datosAdjudicacion) {
      throw new errors.NotFoundError(`No existe el registro de la adjudicacion solicitado.`);
    }
    return datosAdjudicacion;
  };

  const obtenerPorArray = async (idArray) => {
    // ***SE AUMENTO LA CONDICION ESTADO PARA MOSTRAR SOLO ADJUDICACIONES ACTIVAS***
    let datosAdjudicaciones = await models.adjudicacion.findAll({
      attributes: ['id_adjudicacion', 'referencia', 'monto'],
      where: {
        id_adjudicacion: {$in: idArray},
 	estado: 'ACTIVO'
      }
    });
    if (!datosAdjudicaciones) {
      throw new error.NotFoundError(`No se encontraron adjudicaciones para este supervisor`);
    }
    return datosAdjudicaciones;
  };

  async function listar (query = {}, idProyecto) {
    let options = seqOpt.createOptions(query, models, outputList);
    if (!options.where) {
      options.where = {};
    }
    options.where.fid_proyecto = idProyecto;
    options.where.estado = 'ACTIVO';
    //options.distinct = true;
    options.order = 'id_adjudicacion';
    if (typeof(options.order) === 'string') {
      options.order = sequelize.literal(options.order);
    }
    return models.adjudicacion.findAndCount(options);
  }

  async function eliminar (idAdjudicacion, idUsuario) {
    let datos = {
      estado: 'INACTIVO',
      _usuario_modificacion: idUsuario
    }
    await models.adjudicacion.update(datos, {where: {id_adjudicacion: idAdjudicacion}});
  }

  const obtenerDatosModificacion = async (idProyecto,idAdjudicacion) => {
    let query = {
      where: {
        fid_proyecto: idProyecto,
        id_adjudicacion: idAdjudicacion
      },
      order: [['version', 'ASC']],
      attributes: ['version', 'tipo_modificacion', 'estado_adjudicacion', 'fecha_modificacion'],
      include: [
        {
          model: models.estado,
          as: 'estado_actual',
          attributes: ['nombre']
        }, {
          model: models.parametrica,
          as: 'tipo_modificacion_adjudicacion',
          attributes: ['nombre']
        },
        {
          model: models.transicion,
          as: 'observacion',
          attributes: [['observacion', 'mensaje'], 'fid_usuario'],
          where: {
            fid_usuario_asignado: null,
            codigo_estado: {
              $eq: sequelize.col('adjudicacion.estado_adjudicacion')
            }
          },
          required: false,
          include: [
            {
              attributes: ['id_usuario', 'fid_persona'],
              model: sequelize.models.usuario,
              as: 'usuario',
              include: [
                {
                  attributes: ['nombres', 'primer_apellido', 'segundo_apellido'],
                  model: sequelize.models.persona,
                  as: 'persona'
                }
              ]
            }
          ]
        }
      ]
    };
    return models.adjudicacion.findAll(query);
  };

  const esOrdenTrabajo = (tipoModificacion) => {
    return tipoModificacion === 'TM_ORDEN_TRABAJO';
  };

  const esOrdenCambio = (tipoModificacion) => {
    return tipoModificacion === 'TM_ORDEN_CAMBIO';
  };

  const esContratoModificatorio = (tipoModificacion) => {
    return tipoModificacion === 'TM_CONTRATO_MODIFICATORIO';
  };

  const _validarOrdenTrabajo = async (adjudicacion, nuevoMonto) => {
    if (adjudicacion.monto !== nuevoMonto) {
      throw new errors.ValidationError(`El monto modificado debe ser igual al monto total de la adjudicación.`);
    }
  };

  const _verificarVariaciones = async (adjudicacion, nuevoMonto, porcentaje, idUsuario) => {
    const ESTADO_VARIACIONES = await app.dao.adjudicacion_historico.obtenerVariaciones(adjudicacion.fid_proyecto, adjudicacion.id_adjudicacion, adjudicacion.version - 1);
    const VARIACION = {
      5: _.find(ESTADO_VARIACIONES.variaciones, (e) => { return e.tipo_modificacion === 'TM_ORDEN_CAMBIO'; }) || 0,
      10: _.find(ESTADO_VARIACIONES.variaciones, (e) => { return e.tipo_modificacion === 'TM_CONTRATO_MODIFICATORIO'; }) || 0
    };
    let variacionActual = util.restar(nuevoMonto - ESTADO_VARIACIONES.monto_total_anterior);
    let variacionParcial = util.sumar([variacionActual, VARIACION[porcentaje].variacion]);
    if (util.valorAbsoluto(variacionParcial) > ESTADO_VARIACIONES.porcentajes[porcentaje]) {
      throw new errors.ValidationError(`El monto total modificado sobrepasa al ${porcentaje}% del monto contractual`);
    }
    let variacionTotal = util.sumar([variacionActual, VARIACION['5'].variacion, VARIACION['10'].variacion]);
    if (util.valorAbsoluto(variacionTotal) > ESTADO_VARIACIONES.porcentajes['15']) {
      throw new errors.ValidationError(`El monto total modificado sobrepasa el 15% del monto contractual`);
    }
    adjudicacion.monto = nuevoMonto;
    adjudicacion._usuario_modificacion = idUsuario;
    await adjudicacion.save();
  };

  const _validarModificacion = async (adjudicacion, idUsuario) => {
    const MONTO_MODIFICADO = await app.dao.item.obtenerMontoAdjudicacion(adjudicacion.fid_proyecto,adjudicacion.id_adjudicacion);
    if (esOrdenTrabajo(adjudicacion.tipo_modificacion)) {
      await _validarOrdenTrabajo(adjudicacion, MONTO_MODIFICADO);
    }
    if (esOrdenCambio(adjudicacion.tipo_modificacion)) {
      await _verificarVariaciones(adjudicacion, MONTO_MODIFICADO, '5', idUsuario);
    }
    if (esContratoModificatorio(adjudicacion.tipo_modificacion)) {
      await _verificarVariaciones(adjudicacion, MONTO_MODIFICADO, '10', idUsuario);
      // TODO verificar que los nuevos items tengan sus documento adjuntos
    }
    if (!adjudicacion.doc_respaldo_modificacion) {
      throw new errors.ValidationError(`Debe adjuntar el documento de respaldo para la modificación`);
    }
    if (adjudicacion.fecha_modificacion <= adjudicacion.orden_proceder) {
      throw new errors.ValidationError(`La fecha de modificación debe ser posterior a la fecha de orden de proceder.`);
    }
  };

  async function ejecutarMetodos (adjudicacion, metodos, idUsuario) {
    adjudicacion = await obtenerPorId(util.json(adjudicacion).id_adjudicacion);
    for (let i = 0; i < metodos.length; i++) {
      switch (metodos[i]) {
        case '$validarModificacion':
          await _validarModificacion(adjudicacion, idUsuario);
          break;
        case '$cancelarModificacion':
          await app.dao.adjudicacion_historico.cancelarModificacion(adjudicacion, idUsuario);
          break;
        default:
          console.log(`No existe el método a ejecutar para la adjudicacion.`);
          //logger.warn(`No existe el método a ejecutar para la adjudicacion.`);
          break;
      }
    }
  }

  async function validar (adjudicacion, idRol, idUsuario, t, id) {
    let adjudicacionActual;
    let estadoAdjudicacion;
    let result = {};

    adjudicacionActual = await models.adjudicacion.findById(adjudicacion.id_adjudicacion);
    result = adjudicacionActual;
    if (adjudicacionActual) {
      estadoAdjudicacion = util.json(adjudicacionActual).estado_adjudicacion;
    } else {
      throw new errors.ValidationError(`No se encontró la adjudicación.`);
    }
    //result = adjudicacionActual;

    if (adjudicacion.estado_adjudicacion) {
      result.cambio = {
        origen: estadoAdjudicacion,
        destino: adjudicacion.estado_adjudicacion
      };
      await app.dao.estado.validarDestinos(adjudicacionActual, adjudicacion, 'estado_adjudicacion', estadoAdjudicacion, idRol, app.dao.adjudicacion);
    } else {
      throw new errors.ValidationError(`Especifique el estado de la adjudicación.`);
    }

    const ESTADO_ACTUAL = util.json(await app.dao.estado.getPorCodigo(estadoAdjudicacion));
    await app.dao.estado.verificarPermisos(adjudicacionActual, ESTADO_ACTUAL, idRol, idUsuario, id);

    let ATRIBUTOS_PERMITIDOS = await app.dao.estado.getAtributosPermitidos(ESTADO_ACTUAL, idRol);
    ATRIBUTOS_PERMITIDOS = await app.dao.estado.revisarCondiciones(adjudicacionActual, ATRIBUTOS_PERMITIDOS);
    await app.dao.estado.agregarAtributosPermitidos(result, adjudicacion, ATRIBUTOS_PERMITIDOS);
    await app.dao.estado.ejecutarAutomaticos(result, ESTADO_ACTUAL, app.dao.adjudicacion);

    if(result.cambio.origen != result.cambio.destino){
      result.estado_adjudicacion = result.cambio.destino;
    }
    return result;
  }

  const filtrosPlanilla = (idAdjudicacion) => {
    let query = {
      where: {
        id_adjudicacion: idAdjudicacion
      },
      include: [
      ],
      attributes: ['fid_proyecto', 'referencia', 'monto', 'monto_supervision', 'doc_adjudicacion', 'doc_especificaciones_tecnicas', 'orden_proceder',
        'estado_adjudicacion', 'tipo_modificacion', 'plazo_ampliacion', 'doc_respaldo_modificacion', 'fecha_modificacion', 'version']
    };
    return query;
  };

  const obtenerDatosParaPlanilla = async (idAdjudicacion, idRol, conEstadoActual = false, conHistorial = false) => {
    let consulta = filtrosPlanilla(idAdjudicacion);
    consulta.attributes.push([sequelize.literal(`(CASE WHEN (SELECT version FROM adjudicacion WHERE id_adjudicacion = ${idAdjudicacion}) = 1 THEN
      (SELECT monto FROM adjudicacion WHERE id_adjudicacion = ${idAdjudicacion}) ELSE
      (SELECT monto FROM adjudicacion_historico WHERE id_adjudicacion = ${idAdjudicacion} AND version = 1) END)`), 'monto_contractual']);
    if (conEstadoActual) {
      consulta.include.push({
        model: models.estado,
        as: 'estado_actual',
        attributes: ['codigo', 'nombre', 'tipo', 'acciones', 'atributos', 'requeridos', 'areas', ['fid_rol', 'roles']]
      });
    }
    if (conHistorial) {
      consulta.include.push({
        model: models.parametrica,
        as: 'tipo_modificacion_adjudicacion',
        attributes: ['nombre', [sequelize.literal(`(SELECT COUNT(ah.*)
                FROM adjudicacion_historico ah
                WHERE ah.tipo_modificacion="adjudicacion".tipo_modificacion AND ah.id_adjudicacion=${idAdjudicacion}) + 1`), 'numero']]
      });
      consulta.include.push({
        model: models.adjudicacion_historico,
        as: 'adjudicacion_historico',
        attributes: ['version', 'tipo_modificacion'],
        include: {
          model: models.parametrica,
          as: 'tipo_modificacion_adjudicacion',
          attributes: ['nombre', [sequelize.literal(`(SELECT COUNT(ah.*)
                  FROM adjudicacion_historico ah
                  WHERE ah.tipo_modificacion="adjudicacion_historico".tipo_modificacion AND ah.id_adjudicacion=${idAdjudicacion} AND ah.version <= "adjudicacion_historico".version )`), 'numero']]
        }
      });
      consulta.order = [[{model: models.adjudicacion_historico, as: 'adjudicacion_historico'}, 'version', 'ASC']];
    }

    let adjudicacion = await models.adjudicacion.findOne(consulta);
    if (conEstadoActual) {
      await app.dao.estado.prepararDatosPermitidos(adjudicacion, idRol);
    }
    // proyecto.dataValues.empresa = null; // proyecto.dataValues.contrato[0].empresa;
    // delete proyecto.dataValues.contrato;
    // proyecto.dataValues.dpa = app.dao.municipio.getDpa(util.json(proyecto.municipio));

    // proyecto.dataValues.formularios = proyecto.dataValues.formularios[0] ? proyecto.dataValues.formularios[0] : {};

    // delete proyecto.dataValues.municipio;
    Object.assign(adjudicacion.dataValues, await calcularFechasRecepcion(adjudicacion));
    return adjudicacion;
  };

  const calcularFechasRecepcion = async (adjudicacion, proyecto) => {
    let ORDEN_PROCEDER;
    // let ORDEN_PROCEDER = adjudicacion.orden_proceder;
    if (!proyecto) {
      proyecto = util.json(await app.dao.proyecto.getPorId(adjudicacion.fid_proyecto));
    }else{proyecto = util.json(proyecto)}
    if (!ORDEN_PROCEDER) { ORDEN_PROCEDER = proyecto.orden_proceder };
    if (!ORDEN_PROCEDER) {
      return null;
    }
    const PLAZO_EJECUCION = proyecto.plazo_ejecucion;
    const NRO_DIAS = proyecto.plazo_recepcion_definitiva || 90;
    const AMPLIACION_PLAZO = adjudicacion.plazo_ampliacion ?adjudicacion.plazo_ampliacion.por_volumenes + adjudicacion.plazo_ampliacion.por_compensacion : 0;

    // Obtener configuración calc fecha
    let configCalcFecha = await app.dao.parametrica.obtenerPorCodigoTipo('CFG_CALC_FECHA', 'CONFIG');
    let configRestarDias = configCalcFecha && configCalcFecha.descripcion ? parseInt(configCalcFecha.descripcion) : 0;

    return {
      fecha_recepcion_provisional: util.sumarDiasAFecha(PLAZO_EJECUCION /*+ AMPLIACION_PLAZO */- configRestarDias, ORDEN_PROCEDER),
      fecha_recepcion_definitiva: util.sumarDiasAFecha(PLAZO_EJECUCION + NRO_DIAS /*+ AMPLIACION_PLAZO*/ - configRestarDias, ORDEN_PROCEDER)
    };
  };

  app.dao.adjudicacion.crear = crear;
  app.dao.adjudicacion.actualizar = actualizar;
  app.dao.adjudicacion.obtener = obtener;
  app.dao.adjudicacion.obtenerPorId = obtenerPorId;
  app.dao.adjudicacion.obtenerPorArray = obtenerPorArray;
  app.dao.adjudicacion.obtenerDatosModificacion = obtenerDatosModificacion;
  app.dao.adjudicacion.listar = listar;
  app.dao.adjudicacion.eliminar = eliminar;
  app.dao.adjudicacion.validar = validar;
  app.dao.adjudicacion.ejecutarMetodos = ejecutarMetodos;
  app.dao.adjudicacion.filtrosPlanilla = filtrosPlanilla;
  app.dao.adjudicacion.obtenerDatosParaPlanilla = obtenerDatosParaPlanilla;
  app.dao.adjudicacion.calcularFechasRecepcion = calcularFechasRecepcion;
};
