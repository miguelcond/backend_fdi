import fs from 'fs-extra';
import fileType from 'file-type';
import shortid from 'shortid';
import util from '../../../lib/util';
import errors from '../../../lib/errors';

module.exports = (app) => {
  const _app = app;
  _app.dao.fotoSupervision = {};
  const fotoSupervisionModel = app.src.db.models.foto_supervision;
  const supervisionModel = app.src.db.models.supervision;
  const models = app.src.db.models;
  shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');

  async function guardar (foto, idUsuario, idRol, t) {
    let fotografia;
    if (foto.id_foto_supervision) {
      fotografia = await fotoSupervisionModel.findById(foto.id_foto_supervision, {
        transaction: t
      });
    } else if (foto.uuid) {
      fotografia = await fotoSupervisionModel.findOne({
        where: {
          uuid: foto.uuid
        },
        transaction: t
      });
    }
    if(!fotografia) {
      let cant_fotografia = await fotoSupervisionModel.count({
        where: {
          fid_item: foto.fid_item,
          fid_supervision: foto.fid_supervision,
          path_fotografia: {
            $iLike: `%${foto.path_fotografia}`
          }
        },
        transaction: t
      });
      if(cant_fotografia>=1) {
        throw new errors.ValidationError('Ya se registro una fotografia con el mismo nombre');
      }
    }
    let supervision;
    if (foto.fid_supervision) {
      supervision = await supervisionModel.findOne({
        attributes: ['id_supervision', 'estado_supervision', 'nro_supervision'],
        where: {
          id_supervision: foto.fid_supervision
        },
        include: [{
          model: models.proyecto,
          as: 'proyecto',
          attributes: ['codigo','id_proyecto']
        }],
        transaction: t
      });
      foto.path_fotografia = `${shortid.generate()}_${foto.path_fotografia}`;
    } else {
      supervision = await supervisionModel.findOne({
        attributes: ['id_supervision', 'estado_supervision', 'nro_supervision'],
        where: {
          uuid: foto.fuuid
        },
        include: [{
          model: models.proyecto,
          as: 'proyecto',
          attributes: ['codigo','id_proyecto']
        }],
        transaction: t
      });
      foto.fid_supervision = supervision.id_supervision;
    }
    await validarRegistro(foto, supervision, idRol);
    await escribirBase64(foto.path_fotografia, foto.base64, supervision);
    let res;

    if (fotografia) {
      // if para compatibilidad con la version 1.0.0 (Obsoleto en nuevas versiones)
      // if(fs.existsSync(`${_path}/uploads/${fotografia.path_fotografia}`)) {
      //   fs.unlink(`${_path}/uploads/${fotografia.path_fotografia}`);
      // }
      fs.unlink(`${_path}/uploads/proyectos/${supervision.proyecto.codigo}/fotos/${fotografia.path_fotografia}`,(err)=>{
        if (err) console.log(err);
        console.log(`${_path}/uploads/proyectos/${supervision.proyecto.codigo}/fotos/${fotografia.path_fotografia} fue eliminado`)
      });
      fotografia.path_fotografia = foto.path_fotografia;
      fotografia.coordenadas = foto.coordenadas;
      res = await fotografia.save({
        transaction: t
      });
    } else {
      if (foto.tipo === 'TF_SUPERVISION') {
        await verificarFotosSupervision(foto, t);
      }
      res = await fotoSupervisionModel.create(foto, {
        transaction: t
      });
    }
    return res;
  };

  async function validarRegistro (foto, supervision, idRol) {
    const ESTADO_ACTUAL = util.json(await app.dao.estado.getPorCodigo(supervision.estado_supervision));
    const ATRIBUTOS_PERMITIDOS = await app.dao.estado.getAtributosPermitidos(ESTADO_ACTUAL, idRol);

    switch (foto.tipo) {
      case 'TF_EMPRESA':
        if (ATRIBUTOS_PERMITIDOS.indexOf('fotos_item') === -1) {
          throw new Error(`No puede registrar las fotos, ya que la supervisión se encuentra en un estado diferente al permitido.`);
        }
        break;
      case 'TF_SUPERVISION':
        if (ATRIBUTOS_PERMITIDOS.indexOf('fotos_supervision') === -1) {
          throw new Error(`No puede registrar las fotos, ya que la supervisión se encuentra en un estado diferente al permitido.`);
        }
        break;
    }
    if (supervision.estado_supervision === 'APROBADO_FDI') {
      throw new Error('La supervisión que intenta modificar ya fue aprobada.');
    }
  }

  async function verificarFotosSupervision (foto, t) {
    const FOTOS_SUPERVISION = await fotoSupervisionModel.count({
      where: {
        tipo: 'TF_SUPERVISION',
        fid_supervision: foto.fid_supervision
      },
      transaction: t
    });
    // ***"puede subir max. "+app.src.config.config.supervision.fotos.max+" fotos"***
    if (FOTOS_SUPERVISION >= app.src.config.config.supervision.fotos.max) {
      console.log(`No se pueden agregar mas de ${app.src.config.config.supervision.fotos.max} fotografías a una supervisión.`);
      throw new errors.ValidationError(`No se pueden agregar mas de ${app.src.config.config.supervision.fotos.max} fotografías a una supervisión.`);
    }
  }

  async function escribirBase64 (path, base64, sup) {
    let tipo;
    let decoded;
    const partes = base64.split('base64,');
    if (partes.length === 2) {
      decoded = Buffer.from(partes[1], 'base64');
      tipo = partes[0];
    } else {
      decoded = Buffer.from(partes[0], 'base64');
      let type = fileType(decoded);
      if (type) {
        tipo = type.mime;
      }
    }

    const dir = `${_path}/uploads/proyectos/${sup.proyecto.codigo}/fotos`;
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    fs.writeFileSync(`${dir}/${path}`, decoded);
    return tipo;
  };

  async function leerBase64 (path) {
    const file = fs.readFileSync(path);
    return Buffer.from(file).toString('base64');
  };

  async function obtenetFotos (idSupervision, idItem) {
    const consulta = {
      where: {
        fid_supervision: idSupervision,
        fid_item: idItem || null
      },
      order: [['_fecha_modificacion']]
    };
    const fotos = await fotoSupervisionModel.findAll(consulta);
    return fotos;
  }

  async function obtenetFotoId (idFotoSupervision, idUsuario, t) {
    const consulta = {
      where: {
        id_foto_supervision: idFotoSupervision
      },
      include: [{
        model: models.supervision,
        as: 'supervision',
        attributes: ['id_supervision','nro_supervision'],
        include:[{
          model: models.proyecto,
          as: 'proyecto',
          attributes: ['id_proyecto','codigo'],
        }]
      }]
    };
    const foto = await fotoSupervisionModel.findOne(consulta);
    let base64;
    // if para compatibilidad con la version 1.0.0 (Obsoleto en nuevas versiones)
    // if(fs.existsSync(`${_path}/uploads/${foto.path_fotografia}`)) {
    //   base64 = await leerBase64(`${_path}/uploads/${foto.path_fotografia}`);
    // } else {
    base64 = await leerBase64(`${_path}/uploads/proyectos/${foto.supervision.proyecto.codigo}/fotos/${foto.path_fotografia}`);
    // }
    return {coordenadas:foto.coordenadas, base64};
  }

  async function getFotosSupervision (idSupervision) {
    return fotoSupervisionModel.findAll({
      attributes: ['id_foto_supervision', 'path_fotografia'],
      where: {
        fid_supervision: idSupervision,
        tipo: 'TF_SUPERVISION',
        estado: 'ACTIVO'
      }
    });
  }

  async function getFotosSupervisionItem(idSupervision) {
    return fotoSupervisionModel.findAll({
      attributes: ['id_foto_supervision', 'path_fotografia','fid_item'],
      where: {
        fid_supervision: idSupervision,
        tipo: 'TF_EMPRESA',
        estado: 'ACTIVO'
      }
    });
  }

  async function getId (idFoto) {
    const FOTO = await fotoSupervisionModel.findById(idFoto);
    if (!FOTO) {
      throw new errors.ValidationError(`No existe la foto solicitada.`);
    }
    return FOTO;
  }

  async function borrar (idFoto, idRol, fid_supervision, t) {
    if(idRol != 16) {
      throw new errors.ValidationError(`No puede eliminar la foto.`);
    }
    const supervision = await supervisionModel.findOne({
      attributes: ['id_supervision', 'estado_supervision'],
      where: {
        id_supervision: fid_supervision,
        estado_supervision: 'EN_SUPERVISION_FDI'
      },
      transaction: t
    });
    if(!supervision) {
      throw new errors.ValidationError(`No puede eliminar la foto.`);
    }
    const FOTO = await fotoSupervisionModel.destroy({
      where: {
        id_foto_supervision: idFoto,
        fid_supervision: fid_supervision
      },
      transaction: t
    });
    if (!FOTO) {
      throw new errors.ValidationError(`No existe la foto solicitada.`);
    }
    return FOTO;
  }

  _app.dao.fotoSupervision.guardar = guardar;
  _app.dao.fotoSupervision.obtenetFotos = obtenetFotos;
  _app.dao.fotoSupervision.obtenetFotoId = obtenetFotoId;
  _app.dao.fotoSupervision.getFotosSupervision = getFotosSupervision;
  _app.dao.fotoSupervision.getId = getId;
  _app.dao.fotoSupervision.borrar = borrar;
  _app.dao.fotoSupervision.getFotosSupervisionItem = getFotosSupervisionItem;
};
