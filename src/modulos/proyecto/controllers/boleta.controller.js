// import util from '../../../lib/util';
import errors from '../../../lib/errors';

module.exports = (app) => {
  app.controller.boleta = {};

  async function crear (req, res, next) {
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    return app.dao.common.crearTransaccion(async (t) => {
      const datosProyecto = await app.dao.proyecto.obtenerDatosProyecto(ID_PROYECTO);
      if (!datosProyecto) {
        throw new errors.NotFoundError(`No existe el registro del proyecto solicitado.`);
      }
      const NRO_BOLETA = await app.dao.boleta.getNumero(ID_PROYECTO);
      let datosBoleta = {
        fid_proyecto: ID_PROYECTO,
        tipo: req.body.tipo,
        numero: req.body.numero,
        monto: req.body.monto,
        fecha_inicio_validez: req.body.fecha_inicio_validez,
        fecha_fin_validez: req.body.fecha_fin_validez,
        doc_boleta: await app.dao.proyecto.guardarArchivo(req.body.doc_boleta, ID_PROYECTO, `BA_${NRO_BOLETA}`),
        _usuario_creacion: ID_USUARIO
      };
      datosBoleta = await app.dao.boleta.crear(datosBoleta);
      return { datosBoleta };
    }).then(resultado => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se registró los datos exitosamente.',
        datos: resultado.datosBoleta
      });
    }).catch(error => {
      return next(error);
    });
  }

  async function modificar (req, res, next) {
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    return app.dao.common.crearTransaccion(async (t) => {
      const datosProyecto = await app.dao.proyecto.obtenerDatosProyecto(ID_PROYECTO);
      if (!datosProyecto) {
        throw new errors.NotFoundError(`No existe el registro del proyecto solicitado.`);
      }
      const ID_BOLETA = req.body.id_boleta;
      let datosBoleta = {
        estado: req.body.estado,
        _usuario_modificacion: ID_USUARIO
      };
      datosBoleta = await app.dao.boleta.modificar(ID_BOLETA, datosBoleta);
      return { datosBoleta };
    }).then(resultado => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se modificó los datos exitosamente.',
        datos: resultado.datosBoleta
      });
    }).catch(error => {
      return next(error);
    });
  }

  async function adicionarboletas (req, res, next) {
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    return app.dao.common.crearTransaccion(async (t) => {
      const datosProyecto = await app.dao.proyecto.obtenerDatosProyecto(ID_PROYECTO);
      if (!datosProyecto) {
        throw new errors.NotFoundError(`No existe el registro del proyecto solicitado.`);
      }
      let datosBoleta = null;
      for(let i in req.body.boletas) {
        datosBoleta = req.body.boletas[i];
        datosBoleta._usuario_creacion = ID_USUARIO;
        datosBoleta.fid_proyecto = ID_PROYECTO;
        datosBoleta = await app.dao.boleta.adicionar(datosBoleta);
      }
      return { datosBoleta };
    }).then(resultado => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se registró los datos exitosamente.',
        datos: resultado.datosBoleta
      });
    }).catch(error => {
      return next(error);
    });
  }

  app.controller.boleta.crear = crear;
  app.controller.boleta.modificar = modificar;
  app.controller.boleta.adicionar = adicionarboletas;
};
