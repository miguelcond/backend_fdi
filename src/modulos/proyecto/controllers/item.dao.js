import util from '../../../lib/util';
const fs = require('fs-extra');
const xlsx = require('xlsx');
const copyFrom = require('pg-copy-streams').from;
const seqOpt = require('../../../lib/sequelize-options');
const errors = require('../../../lib/errors');

module.exports = (app) => {
  const _app = app;
  _app.dao.item = {};
  const models = app.src.db.models;
  const itemModel = app.src.db.models.item;
  const sequelize = app.src.db.sequelize;

  const output = {
    model: 'proyecto',
    fields: {
      id_item: ['int', 'pk'],
      fid_modulo: ['int', 'fk'],
      nro_item: ['int'],
      nombre: ['str'],
      unidad_medida: ['str'],
      cantidad: ['float'],
      precio_unitario: ['float'],
      peso_ponderado: ['float'],
      tiempo_ejecucion: ['int'],
      avance_total: ['float'],
      especificaciones: ['str'],
      estado: ['enum'],
      regularizar: ['bool'],
      variacion: ['float'],
      modulo: {
        model: 'modulo',
        fields: {
          nombre: ['str'],
          proyecto: {
            model: 'proyecto',
            fields: {
              nombre: ['str']
            }
          }
        }
      },
      unidad: {
        model: 'parametrica',
        fields: {
          codigo: ['str'],
          nombre: ['str'],
          descripcion: ['str']
        }
      }
    }
  };

  const actualizarNro = async (idProyecto) => {
    await sequelize.query(`UPDATE item i
      SET nro_item = n.nro_item
      FROM (SELECT id_item, row_number() OVER (ORDER BY i.fid_modulo, i.id_item) nro_item
          FROM item i, modulo m
          WHERE i.fid_modulo = m.id_modulo
          AND m.fid_proyecto = ?
          AND i.estado <> 'ELIMINADO') n
      WHERE i.id_item = n.id_item
      AND i.nro_item <> n.nro_item`, {
        replacements: [ idProyecto ],
        type: sequelize.QueryTypes.BULKUPDATE
      }
    );
  };

  const calcularPrecioModulo = async (idProyecto, idAdjudicacion) => {
    await sequelize.query(`update modulo set precio = (SELECT
      COALESCE(SUM(subq.multiplied_column), 0)
      FROM (
        SELECT
          round(precio_unitario * cantidad,2) AS multiplied_column
        FROM
          item
        WHERE modulo.id_modulo = item.fid_modulo
      ) as subq) WHERE fid_proyecto = ? AND fid_adjudicacion = ?;`, {
        replacements: [ idProyecto, idAdjudicacion ],
        type: sequelize.QueryTypes.BULKUPDATE
      }
    );
  };

  const getPesoAcumulado = async (idProyecto, idAdjudicacion) => {
    const pesoAcumulado = await sequelize.query(`SELECT sum(peso_ponderado) AS acumulado
      FROM item WHERE fid_modulo IN (
        SELECT id_modulo FROM modulo WHERE fid_proyecto = ? and fid_adjudicacion = ?
      );`, {
        replacements: [ idProyecto, idAdjudicacion ],
        type: sequelize.QueryTypes.SELECT
      }
    );
    return pesoAcumulado && pesoAcumulado[0] && pesoAcumulado[0].acumulado ? pesoAcumulado[0].acumulado : 0;
  };

  // Funcion para cargar un array de datos a la tabla temporal item_tempo
  async function cargarArchivoJson (idProyecto, idAdjudicacion, datos, idUsuario, t) {
    await models.item_temp.destroy({
      truncate: {cascade: false},
      cascade: false,
      transaction: t
    });
    await sequelize.query(`ALTER SEQUENCE item_temp_id_item_temp_seq RESTART WITH 1;`, {
        type: sequelize.QueryTypes.SELECT,
        transaction: t
      }
    );
    await models.item_temp.bulkCreate(datos, {transaction: t});
    return await verificarItems(idProyecto, idAdjudicacion, idUsuario, t);
  }

  function copy (sql, archivo, t) {
    return new Promise((resolve, reject) => {
      var stream = t.connection.query(copyFrom(sql));
      var fileStream = fs.createReadStream(archivo);
      fileStream.on('error', reject);
      fileStream.pipe(stream).on('finish', resolve).on('error', reject);
    });
  }

  // async function cargarArchivo (idProyecto, archivo, idUsuario, t) {
    // await sequelize.query(`CREATE TEMPORARY TABLE item_temp (
    //     id_item_temp Serial,
    //     componente varchar(255),
    //     modulo varchar(255),
    //     nombre varchar(255),
    //     unidad varchar(255),
    //     scantidad text,
    //     cantidad float,
    //     sprecio_unitario text,
    //     precio_unitario float
    //   ) ON COMMIT DROP;`, {
    //     transaction: t
    //   }
    // );
    // await copy(`COPY item_temp(componente, modulo, nombre, unidad, scantidad, sprecio_unitario) FROM STDIN
    //   DELIMITER E'\t' CSV HEADER ENCODING 'UTF-8';`, archivo, t);
    //
    // const nroRegistros = await sequelize.query('SELECT count(*)::int FROM item_temp;', {
    //   type: sequelize.QueryTypes.SELECT,
    //   plain: true,
    //   transaction: t
    // });
    // if (nroRegistros.count === 0) {
    //   throw new Error('No se encontraron datos en el archivo.');
    // }
  async function verificarItems (idProyecto, idAdjudicacion, idUsuario, t) {
    // Obtener registro de equipo tecnico de supervisor
    // let equipo_tecnico = await app.dao.equipo_tecnico.obtenerPorSupervisor(idProyecto, idUsuario);

    // Obtener adjudicacion
    let adjudicacion = await app.dao.adjudicacion.obtenerPorId(idAdjudicacion);

    // inicio Validacion CSV, generar objeto de errores de validacion
    let listErrors = [];

    // let componentesValidos = adjudicacion.referencia.split(',');
    // // Remover espacios y agregar "'"
    // for (let i in componentesValidos) {
    //   componentesValidos[i] = `'${componentesValidos[i].trim()}'`;
    // }

    // // Verificar que los componentes esten contenidos en la referencia de la adjudicacion
    // let compNombreDesc = await sequelize.query(`SELECT id_item_temp, componente FROM item_temp
    //   WHERE componente NOT LIKE ALL(ARRAY[${componentesValidos}]);`, {
    //     type: sequelize.QueryTypes.SELECT,
    //     transaction: t
    //   }
    // );
    // if (compNombreDesc.length > 0) {
    //   let indice = listErrors.push({ // 0
    //     mensaje: 'Verifique el/los nombres de los componentes, solo se admite aquellos registrados en la referencia de la adjudicacion',
    //     items: []
    //   });
    //   for (let i in compNombreDesc) {
    //     listErrors[indice-1].items.push({
    //       item: compNombreDesc[i].id_item_temp,
    //       texto: compNombreDesc[i].componente
    //     });
    //   }
    // }

    // Verificar si las cantidades son numeros validos
    let unidDesc = await sequelize.query(`SELECT id_item_temp, cantidad FROM item_temp WHERE NOT (cantidad ~ '^([0-9]+[,]?[0-9]*|[0-9]+[.]?[0-9]*|[.][0-9]+)$');`, {
      type: sequelize.QueryTypes.SELECT,
      transaction: t
    });
    if (unidDesc.length > 0) {
      let indice = listErrors.push({ // 0
        mensaje: 'Verifique los valores de la columna de cantidades.',
        items: []
      });
      for (let i in unidDesc) {
        listErrors[indice-1].items.push({
          item: unidDesc[i].id_item_temp,
          texto: unidDesc[i].scantidad,
        });
      }
    } else {
      const nroRegistros2 = await sequelize.query(`UPDATE item_temp SET fcantidad = REPLACE(cantidad,',','.')::FLOAT`, {
        type: sequelize.QueryTypes.SELECT,
        plain: true,
        transaction: t
      });
    }

    // Verificar si los precios unitarios son numeros validos
    unidDesc = await sequelize.query(`SELECT id_item_temp, precio_unitario FROM item_temp WHERE NOT (precio_unitario ~ '^([0-9]+[,]?[0-9]*|[0-9]+[.]?[0-9]*|[.][0-9]+)$');`, {
      type: sequelize.QueryTypes.SELECT,
      transaction: t
    });
    if (unidDesc.length > 0) {
      let indice = listErrors.push({ // 0
        mensaje: 'Verifique los valores de la columna de precios unitarios.',
        items: []
      });
      for (let i in unidDesc) {
        listErrors[indice-1].items.push({
          item: unidDesc[i].id_item_temp,
          texto: unidDesc[i].sprecio_unitario,
        });
      }
    } else {
      const nroRegistros2 = await sequelize.query(`UPDATE item_temp SET fprecio_unitario = REPLACE(precio_unitario,',','.')::FLOAT`, {
        type: sequelize.QueryTypes.SELECT,
        plain: true,
        transaction: t
      });
    }

    // Verificar si los pesos ponderados son numeros validos
    unidDesc = await sequelize.query(`SELECT id_item_temp, peso_ponderado FROM item_temp WHERE NOT (peso_ponderado ~ '^([0-9]+[,]?[0-9]*|[0-9]+[.]?[0-9]*|[.][0-9]+)$');`, {
      type: sequelize.QueryTypes.SELECT,
      transaction: t
    });
    if (unidDesc.length > 0) {
      let indice = listErrors.push({ // 0
        mensaje: 'Verifique los valores de la columna de pesos ponderados.',
        items: []
      });
      for (let i in unidDesc) {
        listErrors[indice-1].items.push({
          item: unidDesc[i].id_item_temp,
          texto: unidDesc[i].sprecio_unitario,
        });
      }
    } else {
      const nroRegistros2 = await sequelize.query(`UPDATE item_temp SET fpeso_ponderado = REPLACE(peso_ponderado,',','.')::FLOAT`, {
        type: sequelize.QueryTypes.SELECT,
        plain: true,
        transaction: t
      });
    }

    // unidades
    unidDesc = await sequelize.query(`SELECT id_item_temp, unidad FROM item_temp
      WHERE unidad NOT IN(SELECT nombre FROM parametrica WHERE tipo = 'UNIDAD');`, {
        type: sequelize.QueryTypes.SELECT,
        transaction: t
      }
    );
    if (unidDesc.length > 0) {
      let indice = listErrors.push({ // 0
        mensaje: 'Verifique las unidades de medida, solo se admite: glb, pza, m, m2 y m3.',
        items: []
      });
      for (let i in unidDesc) {
        listErrors[indice-1].items.push({
          item: unidDesc[i].id_item_temp,
          texto: unidDesc[i].unidad,
        });
      }
    }

    // cantidades glb
    // unidDesc = await sequelize.query(`SELECT id_item_temp, fcantidad FROM item_temp
    //   WHERE unidad IN ('glb') AND fcantidad != 1;`, {
    //     type: sequelize.QueryTypes.SELECT,
    //     transaction: t
    //   }
    // );
    // if (unidDesc.length > 0) {
    //   let indice = listErrors.push({ // 1
    //     mensaje: 'La cantidad debe ser 1 sin decimales, para las unidades \'glb\'.',
    //     items: []
    //   });
    //   for (let i in unidDesc) {
    //     listErrors[indice-1].items.push({
    //       item: unidDesc[i].id_item_temp,
    //       texto: unidDesc[i].cantidad,
    //     });
    //   }
    // }

    // cantidades pza
    unidDesc = await sequelize.query(`SELECT id_item_temp, fcantidad FROM item_temp
      WHERE unidad IN ('pza') AND (fcantidad::INT != fcantidad OR fcantidad < 0);`, {
        type: sequelize.QueryTypes.SELECT,
        transaction: t
      }
    );
    if (unidDesc.length > 0) {
      let indice = listErrors.push({ // 1
        mensaje: 'En la cantidad solo se admiten valores enteros positivos, para las unidades \'pza\'.',
        items: []
      });
      for (let i in unidDesc) {
        listErrors[indice-1].items.push({
          item: unidDesc[i].id_item_temp,
          texto: unidDesc[i].cantidad,
        });
      }
    }

    // cantidades menos glb y pza
    unidDesc = await sequelize.query(`SELECT id_item_temp, fcantidad FROM item_temp
      WHERE unidad NOT IN ('glb','pza') AND ((fcantidad*100)::INT/100::FLOAT != fcantidad OR fcantidad < 0);`, {
        type: sequelize.QueryTypes.SELECT,
        transaction: t
      }
    );
    if (unidDesc.length > 0) {
      let indice = listErrors.push({ // 2
        mensaje: 'Las cantidades solo pueden contener hasta 2 decimales y deben ser valores positivos.',
        items: []
      });
      for (let i in unidDesc) {
        listErrors[indice-1].items.push({
          item: unidDesc[i].id_item_temp,
          texto: unidDesc[i].cantidad,
        });
      }
    }

    if (listErrors.length > 0) {
      throw new errors.ValidationError('Verifique los campos unidad, cantidad y precio unitario en el archivo.', null, null, listErrors);
    }
    // fin Validacion CSV

    // cambiar nombre de unidad por codigo segun parametricas
    await sequelize.query(`UPDATE item_temp i
      SET unidad = p.codigo
      FROM parametrica p
      WHERE i.unidad = p.nombre
      AND p.tipo = 'UNIDAD';`, {
        replacements: [ idProyecto ],
        type: sequelize.QueryTypes.BULKUPDATE,
        transaction: t
      }
    );

    await sequelize.query(`DELETE FROM item
      WHERE fid_modulo IN(SELECT id_modulo FROM modulo WHERE fid_proyecto = ? AND fid_adjudicacion = ?);`, {
        replacements: [ idProyecto, adjudicacion.id_adjudicacion ],
        type: sequelize.QueryTypes.BULKUPDATE,
        transaction: t
      }
    );

    await sequelize.query(`DELETE FROM modulo WHERE fid_proyecto = ? AND fid_adjudicacion = ?;`, {
      replacements: [ idProyecto, adjudicacion.id_adjudicacion ],
      type: sequelize.QueryTypes.BULKUPDATE,
      transaction: t
    });

    await sequelize.query(`INSERT INTO modulo (fid_proyecto, fid_adjudicacion, nombre, _usuario_creacion, _fecha_creacion, _fecha_modificacion)
      SELECT fid_proyecto, fid_adjudicacion, componente||'|'||modulo, _usuario_creacion, _fecha_creacion, _fecha_modificacion
      FROM (SELECT ? fid_proyecto, ? fid_adjudicacion, componente, modulo, ? _usuario_creacion, now() _fecha_creacion, now() _fecha_modificacion, min(id_item_temp) ord
        FROM item_temp
        GROUP BY componente,modulo) i
      ORDER BY ord;`, {
        replacements: [ idProyecto, adjudicacion.id_adjudicacion, idUsuario ],
        type: sequelize.QueryTypes.BULKUPDATE,
        transaction: t
      }
    );

    await sequelize.query(`INSERT INTO item (fid_modulo, nro_item, nombre, unidad_medida, cantidad, precio_unitario, peso_ponderado, _usuario_creacion, _fecha_creacion, _fecha_modificacion)
      SELECT m.id_modulo, 0, i.nombre, i.unidad, i.fcantidad, i.fprecio_unitario, i.fpeso_ponderado, ?, now(), now()
      FROM modulo m, item_temp i
      WHERE m.nombre = i.componente||'|'||i.modulo
      AND m.fid_proyecto = ? and m.fid_adjudicacion = ?
      ORDER BY id_item_temp;`, {
        replacements: [ idUsuario, idProyecto, idAdjudicacion ],
        type: sequelize.QueryTypes.BULKUPDATE,
        transaction: t
      }
    );

    await calcularPrecioModulo(idProyecto, idAdjudicacion, t);
    await actualizarNro(idProyecto, t);
    const res = await app.src.db.models.modulo.findAll({
      attributes: ['id_modulo', 'nombre', 'fid_proyecto'],
      include: {
        attributes: ['id_item', 'nro_item', 'nombre', 'unidad_medida', 'cantidad', 'precio_unitario', 'peso_ponderado', 'fid_modulo'],
        model: itemModel,
        as: 'items'
      },
      where: {
        fid_proyecto: idProyecto
      },
      transaction: t
    });
    return res;
  };

  function sheetToCsv (file) {
    let workbook = xlsx.readFile(file, {});
    if (workbook.SheetNames.length !== 1) {
      throw new Error('El archivo debe contener exactamente una hoja.');
    }
    const sheet = workbook.Sheets[workbook.SheetNames[0]];

    let csv = '';
    const rango = sheet['!ref'].split(':');
    const coli = rango[0].charCodeAt(0);
    const colf = rango[1].charCodeAt(0);
    const rowi = rango[0].substr(1, rango[0].length - 1);
    const rowf = rango[1].substr(1, rango[1].length - 1);

    for (let i = rowi; i <= rowf; i++) {
      for (let j = coli; j <= colf; j++) {
        let cell = sheet[String.fromCharCode(j) + i];
        if (j === colf) {
          if (cell) {
            if (cell.z) {
              csv += cell.w + '\n';
            } else {
              csv += cell.v + '\n';
            }
          } else {
            csv += '\n';
          }
        } else {
          if (cell) {
            if (cell.z) {
              csv += cell.w + '\t';
            } else {
              csv += cell.v + '\t';
            }
          } else {
            csv += '\t';
          }
        }
      }
    }

    fs.writeFileSync(file, csv);
  }

  const obtenerDatosItems = async (idProyecto, consulta = {}) => {
    const datosItems = await models.item.findAndCount({
      include: [
        {
          model: models.modulo,
          as: 'modulo',
          where: {
            $and: [
              consulta.condicionModulo,
              {
                estado: {
                  $ne: 'ELIMINADO',
                  $ne: 'INACTIVO'
                }
              }, {
                fid_proyecto: idProyecto
              }
            ]
          }
        }, {
          attributes: ['nombre'],
          model: models.parametrica,
          as: 'unidad'
        }
      ],
      where: {
        $and: [
          consulta.condicionItem,
          {
            estado: {
              $ne: 'ELIMINADO'
            }
          }
        ]
      },
      order: [['nro_item', 'ASC']]
    });
    return datosItems;
  };

  const obtenerDatosItemsModulo = async (idModulo, consulta = {}) => {
    const datosItems = await models.item.findAndCount({
      include: [
        {
          attributes: ['nombre'],
          model: models.parametrica,
          as: 'unidad'
        }
      ],
      where: {
        $and: [
          consulta.condicionItem,
          {
            estado: {
              $ne: 'ELIMINADO'
            }
          }, {
            fid_modulo: idModulo
          }
        ]
      },
      order: [['nro_item', 'ASC']]
    });

    return datosItems;
  };

  const obtenerDatosItemsAdjudicacion = async (idProyecto, idAdjudicacion, consulta = {}) => {
    const datosItems = await models.item.findAndCount({
      include: [
        {
          model: models.modulo,
          as: 'modulo',
          where: {
            $and: [
              consulta.condicionModulo,
              {
                estado: {
                  $ne: 'ELIMINADO'
                }
              }, {
                fid_proyecto: idProyecto,
                fid_adjudicacion: idAdjudicacion
              }
            ]
          }
        }, {
          attributes: ['nombre'],
          model: models.parametrica,
          as: 'unidad'
        }
      ],
      where: {
        $and: [
          consulta.condicionItem,
          {
            estado: {
              $ne: 'ELIMINADO'
            }
          }
        ]
      },
      order: [['nro_item', 'ASC']]
    });
    return datosItems;
  };

  const obtenerPorId = async (idItem, query = {}, resumido = true) => {
    const options = seqOpt.createOptions(query, models, output);
    options.where = !options.where ? {} : options.where;
    options.where.id_item = idItem;
    const ITEM = await models.item.findOne(options);
    if (!ITEM) {
      throw new errors.NotFoundError(`El item no existe.`);
    }
    if (resumido) {
      return seqOpt.createResult(query, ITEM, output);
    }
    return ITEM;
  };

  async function getResumen (idItem, query = {}) {
    const ITEM = await obtenerPorId(idItem);
    const CANTIDAD_EJECUTADA = await _app.dao.computoMetrico.getCantidadTotalEjecutada(idItem);
    ITEM.completado = !isNaN(CANTIDAD_EJECUTADA) && ITEM.cantidad === CANTIDAD_EJECUTADA;
    return ITEM;
  }

  const actualizar = async (idItem, datos, idUsuario) => {
    datos._usuario_modificacion = idUsuario;
    return models.item.update(datos, {
      where: {
        id_item: idItem
      }
    });
  };

  const obtenerMontoProyecto = async (idProyecto) => {
    const ITEMS = await app.dao.item.obtenerDatosItems(idProyecto);
    let montoTotal = 0;
    for (let i in ITEMS.rows) {
      let total = util.multiplicar([ITEMS.rows[i].precio_unitario, ITEMS.rows[i].cantidad]);
      montoTotal = util.sumar([montoTotal, total]);
    }
    return montoTotal;
  };
  const obtenerAvanceEjecutado = async (idProyecto) => {
    const ITEMS = await app.dao.item.obtenerDatosItems(idProyecto);
    let avance_ej = 0;
    for (let i in ITEMS.rows) {
      let total = util.multiplicar([ITEMS.rows[i].precio_unitario, util.sumar([ITEMS.rows[i].cantidad_anterior_fis , ITEMS.rows[i].cantidad_actual_fis]) ]);
      avance_ej = util.sumar([avance_ej, total]);
    }
    return avance_ej;
  };

  const cerrarItem = async (item, idUsuario) => {
    await actualizar(item.id_item, {estado: 'CERRADO'}, idUsuario);
  };

  const obtenerMontoAdjudicacion = async(id_proyecto,id_adjudicacion) => {
    const ITEMS = await app.dao.item.obtenerDatosItemsAdjudicacion(id_proyecto,id_adjudicacion);
    let montoTotal = 0;
    for (let i in ITEMS.rows) {
      let total = util.multiplicar([ITEMS.rows[i].precio_unitario, ITEMS.rows[i].cantidad]);
      montoTotal = util.sumar([montoTotal, total]);
    }
    return montoTotal;
  };

  const getCalculos = (item, pesoAcumulado) => {
    item.total = util.sumar([util.multiplicar([item.cantidad, item.precio_unitario]),item.variacion]) ;
    // avance fisico
    item.total_acumulado_fis = util.sumar([item.cantidad_anterior_fis, item.cantidad_actual_fis]);
    item.falta_ejecutar_fis = util.restar(item.cantidad, item.total_acumulado_fis);
    // avance financiero
    if(item.cantidad_anterior_fis > 0){
    item.cantidad_anterior_fin = util.sumar([util.multiplicar([item.cantidad_anterior_fis, item.precio_unitario]),item.variacion]);
    item.cantidad_actual_fin = util.sumar([util.multiplicar([item.cantidad_actual_fis, item.precio_unitario]),item.variacion]);
    }else{
    item.cantidad_anterior_fin = util.multiplicar([item.cantidad_anterior_fis, item.precio_unitario]);
    item.cantidad_actual_fin = util.multiplicar([item.cantidad_actual_fis, item.precio_unitario]);
    }
    // Para restar avance financiero actual con variacion
    if(item.cantidad_actual_fis == 0){
     item.cantidad_actual_fin = util.multiplicar([item.cantidad_actual_fis, item.precio_unitario]);
    }else{
     if(item.cantidad_anterior_fin == 0){
       item.cantidad_actual_fin = util.sumar([util.multiplicar([item.cantidad_actual_fis, item.precio_unitario]),item.variacion]);
     }else{
       item.cantidad_actual_fin = util.multiplicar([item.cantidad_actual_fis, item.precio_unitario]);
     }
    }
    // item.cantidad_actual_fin = util.sumar([util.multiplicar([item.cantidad_actual_fis, item.precio_unitario]),item.variacion]);
    item.total_acumulado_fin = util.sumar([item.cantidad_anterior_fin, item.cantidad_actual_fin]);
    item.falta_ejecutar_fin = util.restar(item.total, item.total_acumulado_fin);
    // porcentajes
    item.ejecutado_porcentaje_fis = util.calcularPorcentaje(item.total_acumulado_fis, item.cantidad);
    item.ejecutar_porcentaje_fis = util.restar(100, item.ejecutado_porcentaje_fis);

    item.ejecutado_porcentaje_fin = item.precio_unitario === 0 ? 0 : util.calcularPorcentaje(item.total_acumulado_fin, item.total);
    item.ejecutar_porcentaje_fin = item.precio_unitario === 0 ? 0 : util.restar(100, item.ejecutado_porcentaje_fin);

    item.completado = item.falta_ejecutar_fis === 0;

    // con peso ponderado
    item.relacion_peso = util.dividir(item.peso_ponderado, pesoAcumulado, false, 4);
    item.relacion_peso_sobre_cien = util.multiplicar([item.relacion_peso, 100], false, 4);
    item.porcentaje_ponderado_fis = util.multiplicar([item.ejecutado_porcentaje_fis, item.relacion_peso], false, 4);

    // console.log('---------called calculos------------');
    // console.log(pesoAcumulado);
    // console.log(item.relacion_peso);
    // console.log(item.ejecutado_porcentaje_fis);
    // console.log(item.porcentaje_ponderado_fis);
    // console.log('------------------------------------');

    if (item.completado && item.falta_ejecutar_fin !== 0 && item.estado === 'CERRADO') {
      item.falta_ejecutar_fin = 0;
      item.total_acumulado_fin = item.total;
    }
    return item;
  };

  const getAvanceItems = async (idProyecto, idSupervision , idAdjudicacion) => {
    let consulta = {
      attributes: ['id_item', 'nro_item', 'nombre', 'cantidad', 'estado',
        [sequelize.fn('SUM', (sequelize.fn('COALESCE', (sequelize.col('computo_metrico.cantidad_parcial')), 0))), 'total_avance']
      ],
      include: [
        {
          attributes: [],
          model: models.modulo,
          as: 'modulo',
          where: {
            fid_proyecto: idProyecto
          }
        }, {
          attributes: [],
          model: models.computo_metrico,
          as: 'computo_metrico',
          where: {
            estado: { $ne: 'ELIMINADO' }
          },
          required: false
        }
      ],
      order: ['nro_item'],
      group: ['id_item']
    };
    if (idSupervision) {
      consulta.include[1].where.fid_supervision = idSupervision;
    }
    if (idAdjudicacion) {
      consulta.include[0].where.fid_adjudicacion = idAdjudicacion;
    }
    if (typeof(consulta.order) === 'string') {
      consulta.order = sequelize.literal(consulta.order);
    }
    return models.item.findAll(consulta);
  };

  const estanCompletados = async (idProyecto,idSupervision,idAdjudicacion) => {
    const ITEMS = util.json(await getAvanceItems(idProyecto,idSupervision,idAdjudicacion));
    let completo = true;
    for (var i = 0; i < ITEMS.length; i++) {
      if (ITEMS[i].cantidad !== ITEMS[i].total_avance) {
        completo = false;
      }
    }
    return completo;
  };

  const obtenerDeModulo = (idModulo) => {
    let query = {
      where: {
        fid_modulo: idModulo,
        estado: {
          $ne: 'ELIMINADO'
        }
      }
    };
    return models.item.findAll(query);
  };

  /**
   * Cerrando items, por una modificación aprobada se actualiza el estado de los items que tienen un avance
   * (hasta la última supervisión aprobada) igual a la cantidad (que pudo haberse modificado)
   */
  const cerrarItems = async (proyecto, idUsuario) => {
    const ULTIMA_SUPERVISION = await app.dao.supervision.obtenerUltimaAprobada(proyecto.id_proyecto);
    const ID_SUPERVISION = ULTIMA_SUPERVISION ? ULTIMA_SUPERVISION.id_supervision : null;
    const ITEMS = await getAvanceItems(proyecto.id_proyecto, ID_SUPERVISION);

    for (var i = 0; i < ITEMS.length; i++) {
      let item = util.json(ITEMS[i]);
      if (item.estado !== 'CERRADO' && item.cantidad === item.total_avance) {
        await cerrarItem(item, idUsuario);
      }
    }
  };

  const eliminar = async (idItem) => {
    await models.item.destroy({ where: { id_item: idItem } });
  };

  const refreshCantidadesFis = async (idItem, idSupervision, t) => {
    await sequelize.query(`
      UPDATE item SET cantidad_anterior_fis = COALESCE ((SELECT SUM(cantidad_parcial) FROM computo_metrico WHERE fid_item = ? AND estado <> 'ELIMINADO' AND fid_supervision < ?)::Decimal(12, 2)::float, 0) WHERE id_item = ?;
      UPDATE item SET cantidad_actual_fis = COALESCE ((SELECT SUM(cantidad_parcial) FROM computo_metrico WHERE fid_item = ? AND estado <> 'ELIMINADO' AND fid_supervision = ?)::Decimal(12, 2)::float, 0) WHERE id_item = ?;
      `, {
        replacements: [ idItem, idSupervision, idItem, idItem, idSupervision, idItem ],
        type: sequelize.QueryTypes.BULKUPDATE,
        transaction: t
      }
    );
  };

  // _app.dao.item.cargarArchivo = cargarArchivo;
  _app.dao.item.cargarArchivoJson = cargarArchivoJson;
  _app.dao.item.sheet_to_csv = sheetToCsv;
  _app.dao.item.actualizarNro = actualizarNro;
  _app.dao.item.calcularPrecioModulo = calcularPrecioModulo;
  _app.dao.item.getPesoAcumulado = getPesoAcumulado;
  _app.dao.item.obtenerDatosItems = obtenerDatosItems;
  _app.dao.item.obtenerDatosItemsModulo = obtenerDatosItemsModulo;
  _app.dao.item.obtenerDatosItemsAdjudicacion = obtenerDatosItemsAdjudicacion;
  _app.dao.item.obtenerPorId = obtenerPorId;
  _app.dao.item.getResumen = getResumen;
  _app.dao.item.actualizar = actualizar;
  _app.dao.item.obtenerMontoProyecto = obtenerMontoProyecto;
  _app.dao.item.obtenerAvanceEjecutado = obtenerAvanceEjecutado;
  _app.dao.item.cerrarItem = cerrarItem;
  _app.dao.item.getCalculos = getCalculos;
  _app.dao.item.estanCompletados = estanCompletados;
  _app.dao.item.obtenerDeModulo = obtenerDeModulo;
  _app.dao.item.cerrarItems = cerrarItems;
  _app.dao.item.eliminar = eliminar;
  _app.dao.item.obtenerMontoAdjudicacion = obtenerMontoAdjudicacion;
  _app.dao.item.refreshCantidadesFis = refreshCantidadesFis;
};
