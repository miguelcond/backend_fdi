import errors from '../../../lib/errors';
import util from '../../../lib/util';
import seqOpt from '../../../lib/sequelize-options';
import shortid from 'shortid';
import Reporte from '../../../lib/reportes';
import _ from 'lodash';

module.exports = (app) => {
  app.dao.equipo_tecnico = {};
  const models = app.src.db.models;
  const sequelize = app.src.db.sequelize;
  const output = {
    model: 'equipo_tecnico',
    fields: {
      id_equipo_tecnico: ['int', 'pk'],
      tipo_equipo: ['str', 'fk'],
      cargo: ['str', 'fk'],
      doc_designacion: ['str'],
      estado: ['enum'],
      fid_usuario: ['int', 'fk'],
      fid_proyecto: ['int', 'fk'],
      equipo: {
        model: 'parametrica',
        fields: {
          codigo: ['str', 'pk'],
          tipo: ['str'],
          nombre: ['str']
        }
      },
      cargo_usuario: {
        model: 'parametrica',
        fields: {
          codigo: ['str', 'pk'],
          tipo: ['str'],
          nombre: ['str']
        }
      },
      usuario: {
        model: 'usuario',
        fields: {
          id_usuario: ['int', 'pk'],
          usuario: ['str'],
          fid_persona: ['int', 'fk'],
          persona: {
            model: 'persona',
            fields: {
              id_persona: ['int', 'pk'],
              numero_documento: ['str'],
              tipo_documento: ['str'],
              complemento: ['str'],
              complemento_visible: ['bool'],
              fecha_nacimiento: ['date'],
              nombres: ['str'],
              primer_apellido: ['str'],
              segundo_apellido: ['str'],
              lugar_nacimiento_pais: ['str'],
              correo: ['str'],
              telefono: ['str'],
              direccion: ['str'],
              estado: ['enum']
            }
          }
        }
      },
      _fecha_creacion: ['date'],
      _fecha_modificacion: ['date']
    }
  };

  async function crearUpre (idProyecto, idEnc, idEncRol, idTec, idTecRol, auditUsuario, t) {
    let equipos = [];
    let equipoTecnico = await models.equipo_tecnico.findAll({
      where: {
        fid_proyecto: idProyecto,
        cargo: ['CG_ENCARGADO', 'CG_TECNICO']
      },
      transaction: t
    });
    for (let i = 0; i < equipoTecnico.length; i++) {
      await equipoTecnico[i].destroy({ transaction: t });
    }
    // let roles = {}
    // let rolesDB = await models.usuario_rol.findAll({
    //   attributes: ['fid_usuario','fid_rol'],
    //   where: {
    //     fid_usuario: {
    //       $in: [idEnc, idTec]
    //     },
    //     estado: 'ACTIVO',
    //   },
    // });
    // if (rolesDB) {
    //   for (let i in rolesDB) {
    //     roles[rolesDB[i].fid_usuario] = rolesDB[i].fid_rol;
    //   }
    // }
    // Equipo técnico
    equipos.push({
      fid_proyecto: idProyecto,
      tipo_equipo: 'TE_INSTITUCION',
      fid_usuario: idEnc,
      cargo: 'CG_ENCARGADO',
      fid_rol: idEncRol, //roles[idEnc],
      _usuario_creacion: auditUsuario.id_usuario
    });
    equipos.push({
      fid_proyecto: idProyecto,
      tipo_equipo: 'TE_INSTITUCION',
      fid_usuario: idTec,
      cargo: 'CG_TECNICO',
      fid_rol: idTecRol, //roles[idTec],
      _usuario_creacion: auditUsuario.id_usuario
    });
    return models.equipo_tecnico.bulkCreate(equipos, {
      transaction: t
    });
  }

  async function crear (datos) {
    return models.equipo_tecnico.create(datos);
  }

  async function getMiembroEquipoTecnico (idEquipoTecnico) {
    const options = {
      where: {
        id_equipo_tecnico: idEquipoTecnico
      },
      include: [
        {
          model: models.usuario,
          as: 'usuario',
          include: [
            {
              model: models.persona,
              as: 'persona'
            }
          ]
        }
      ]
    };
    const MIEMBRO_EQUIPOTECNICO = await models.equipo_tecnico.findOne(options);
    if (!MIEMBRO_EQUIPOTECNICO) {
      throw new errors.NotFoundError(`No se encuentra el miembro del equipo técnico solicitado.`);
    }
    return MIEMBRO_EQUIPOTECNICO;
  }

  async function modificar (datosEquipoTecnico, idEquipoTecnico, idUsuario) {
    datosEquipoTecnico._usuario_modificacion = idUsuario;
    await models.equipo_tecnico.update(datosEquipoTecnico, {where: {id_equipo_tecnico: idEquipoTecnico}});
  }

  async function verificarAccesoUsuario (Usuario, idProyecto, idEquipoTecnico, idRol) {
    const USUARIO = await app.dao.usuario.getPorId(Usuario.id_usuario);
    const PROYECTO = await app.dao.proyecto.getPorId(idProyecto);
    let options = {};

    if (USUARIO.fid_persona === PROYECTO.fid_autoridad_beneficiaria) {
      options = {
        where: {
          fid_proyecto: idProyecto
        }
      };
    } else {
      if(idRol == 19 || idRol ==20){
        options = {
          where: {
            fid_proyecto: idProyecto
          },
        };
      }else{
        options = {
          where: {
            fid_proyecto: idProyecto,
            fid_usuario: Usuario.id_usuario,
            fid_rol: {
              $in: [3,4,5,12,13,16,17,18]
            }
          },
        };
      }

    }
    let datosEquipoTecnico = await models.equipo_tecnico.findOne(options);
    if (!datosEquipoTecnico) {
      throw new errors.NotFoundError(`No tiene acceso a los datos del equipo técnico.`);
    }
    return datosEquipoTecnico;
  }

  async function listar (query = {}, idProyecto) {
    let options = seqOpt.createOptions(query, models, output);
    if (!options.where) {
      options.where = {};
    }
    options.where.fid_proyecto = idProyecto;
    options.where.fid_adjudicacion = null;
    options.distinct = true;
    options.order = 'estado, cargo'
    if (typeof(options.order) === 'string') {
      options.order = sequelize.literal(options.order);
    }
    return models.equipo_tecnico.findAndCount(options);
  }

  async function listarPorAdjudicacion (query = {}, idProyecto, idAdjudicacion) {
    let options = seqOpt.createOptions(query, models, output);
    if (!options.where) {
      options.where = {};
    }
    options.where.fid_proyecto = idProyecto;
    options.where.fid_adjudicacion = idAdjudicacion;
    options.distinct = true;
    options.order = 'estado, cargo'
    if (typeof(options.order) === 'string') {
      options.order = sequelize.literal(options.order);
    }
    return models.equipo_tecnico.findAndCount(options);
  }

  const obtener = async (query = {}, idEquipoTecnico) => {
    let options = seqOpt.createOptions(query, models, output);
    if (idEquipoTecnico) {
      options.where = { id_equipo_tecnico: idEquipoTecnico };
    }
    Object.assign(options, query);
    let datosEquipoTecnico = await models.equipo_tecnico.findOne(options);
    if (!datosEquipoTecnico) {
      throw new errors.NotFoundError(`No existe el registro del equipo tecnico solicitado.`);
    }
    return datosEquipoTecnico;
  };

  async function obtenerPorSupervisor (idProyecto, idUsuario) {
    let options = {
      where: {
        fid_proyecto: idProyecto,
        fid_usuario: idUsuario,
        fid_rol: 16
      }
    };
    let datosEquipoTecnico = await models.equipo_tecnico.findAll(options);
    if (!datosEquipoTecnico) {
      throw new errors.NotFoundError(`No se encontró el registro solicitado`);
    }
    return datosEquipoTecnico;
  }

  async function obtenerSupervisores(idProyecto, idAdjudicacion){
    let supervisores = await models.equipo_tecnico.findAll({
      where:{
        fid_proyecto: idProyecto,
        fid_adjudicacion: idAdjudicacion,
        estado: 'ACTIVO',
        cargo: 'CG_SUPERVISOR'
      }
    });
    return supervisores;
  }

  async function eliminar (idEquipoTecnico, idUsuario) {
    let datos = {
      estado: 'INACTIVO',
      _usuario_modificacion: idUsuario
    }
    //await models.equipo_tecnico.destroy({where: {id_equipo_tecnico: idEquipoTecnico}});
    await models.equipo_tecnico.update(datos, {where: {id_equipo_tecnico: idEquipoTecnico}});
  }

  async function enviarCorreo (proyecto, eqTec, idUsuario, t) {
    let pass = shortid.generate();
    await app.dao.usuario.actualizarContrasena(eqTec.usuario.id_usuario, pass, idUsuario);
    const PARA = eqTec.usuario.persona.correo;
    const TITULO = 'Designación - Equipo Técnico';
    const reporte = new Reporte('correo_designacion_equipo_tecnico');
    const reporte_supervisor = new Reporte('correo_designacion_responsable_supervisor');

    const datos = {
      proyecto: proyecto.nombre,
      cargo: eqTec.cargo_usuario.nombre,
      usuario: eqTec.usuario.usuario,
      contrasena: pass,
      urlLogo: app.src.config.config.sistema.urlLogo,
      urlLogin: app.src.config.config.sistema.urlLogin
    };
    if(eqTec.cargo_usuario.codigo == 'CG_SUPERVISOR'){
      const HTML = reporte_supervisor.template(datos);
      await util.enviarEmail(PARA, TITULO, HTML);
    }else{
      const HTML = reporte.template(datos);
      await util.enviarEmail(PARA, TITULO, HTML);
    }
  }

  async function enviarCorreoUsuarioExisente (proyecto, eqTec, idUsuario, t) {
    const PARA = eqTec.usuario.persona.correo;
    const TITULO = 'Designación - Equipo Técnico';
    const reporte = new Reporte('correo_designacion_equipo_tecnico_existente');
    const reporte_supervisor = new Reporte('correo_designacion_responsable_supervisor_existente');
    const datos = {
      proyecto: proyecto.nombre,
      cargo: eqTec.cargo_usuario.nombre,
      usuario: eqTec.usuario.usuario,
      urlLogo: app.src.config.config.sistema.urlLogo,
      urlLogin: app.src.config.config.sistema.urlLogin
    };
    if(eqTec.cargo_usuario.codigo == 'CG_SUPERVISOR'){
      const HTML = reporte_supervisor.template(datos);
      await util.enviarEmail(PARA, TITULO, HTML);
    }else{
      const HTML = reporte.template(datos);
      await util.enviarEmail(PARA, TITULO, HTML);
    }
  }

  async function esParteDelEquipo (idProyecto, idUsuario) {
    const USUARIO = await models.equipo_tecnico.findOne({
      where: {
        fid_proyecto: idProyecto,
        fid_usuario: idUsuario,
        estado: 'ACTIVO'
      }
    });
    return USUARIO !== null;
  }

  async function existe (idProyecto, idUsuario, cargo) {
    let USUARIO = await models.equipo_tecnico.findOne({
      where: {
        fid_proyecto: idProyecto,
        fid_usuario: idUsuario,
        cargo: cargo,
        estado: 'ACTIVO'
      }
    });
    if (USUARIO) {
      return USUARIO.id_equipo_tecnico;
    } else {
      return false;
    }
  }

  async function reemplazar (idEquipo, persona, idUsuario, t) {
    let datos = {
      estado: 'INACTIVO',
      _usuario_modificacion: idUsuario
    }
    let equipoAnterior = await obtener(undefined, idEquipo);

    // await models.transicion.update({
    //   fid_usuario: persona.usuario.id_usuario
    // },{ where: { fid_usuario: equipoAnterior.fid_usuario } },{ transaction: t })
    // await models.transicion.update({
    //   fid_usuario_asignado: persona.usuario.id_usuario
    // },{ where: { fid_usuario_asignado: equipoAnterior.fid_usuario } },{ transaction: t })

    await app.dao.transicion.reemplazarUsuario(idUsuario, equipoAnterior.fid_usuario, persona.usuario.id_usuario, equipoAnterior.fid_proyecto, t);

    await models.equipo_tecnico.update(datos, {where: {id_equipo_tecnico: idEquipo}},{transaction: t});
  }

  app.dao.equipo_tecnico.crearUpre = crearUpre;
  app.dao.equipo_tecnico.crear = crear;
  app.dao.equipo_tecnico.modificar = modificar;
  app.dao.equipo_tecnico.verificarAccesoUsuario = verificarAccesoUsuario;
  app.dao.equipo_tecnico.listar = listar;
  app.dao.equipo_tecnico.listarPorAdjudicacion = listarPorAdjudicacion;
  app.dao.equipo_tecnico.obtener = obtener;
  app.dao.equipo_tecnico.obtenerPorSupervisor = obtenerPorSupervisor;
  app.dao.equipo_tecnico.eliminar = eliminar;
  app.dao.equipo_tecnico.enviarCorreo = enviarCorreo;
  app.dao.equipo_tecnico.enviarCorreoUsuarioExisente = enviarCorreoUsuarioExisente;
  app.dao.equipo_tecnico.getMiembroEquipoTecnico = getMiembroEquipoTecnico;
  app.dao.equipo_tecnico.esParteDelEquipo = esParteDelEquipo;
  app.dao.equipo_tecnico.existe = existe;
  app.dao.equipo_tecnico.reemplazar = reemplazar;
  app.dao.equipo_tecnico.obtenerSupervisores = obtenerSupervisores;
};
