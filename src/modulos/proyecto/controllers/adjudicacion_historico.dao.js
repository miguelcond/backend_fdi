import errors from '../../../lib/errors';
import util from '../../../lib/util';
import seqOpt from '../../../lib/sequelize-options';
import shortid from 'shortid';
import Reporte from '../../../lib/reportes';
import _ from 'lodash';

module.exports = (app) => {
  app.dao.adjudicacion_historico = {};
  const sequelize = app.src.db.sequelize;
  const models = app.src.db.models;
  const output = {
    model: 'adjudicacion_historico',
    fields: {
      id_adjudicacion: ['int', 'pk'],
      version: ['int'],
      referencia: ['str', 'fk'],
      monto: ['float', 'fk'],
      orden_proceder: ['date'],
      doc_adjudicacion: ['str'],
      doc_especificaciones_tecnicas: ['str'],
      estado: ['enum'],
      fid_proyecto: ['int', 'fk'],
      _fecha_creacion: ['date'],
      _fecha_modificacion: ['date'],
      estado_actual: {
        model: 'estado',
        fields: {
          codigo: ['str', 'pk'],
          nombre: ['str'],
          tipo: ['enum'],
          acciones: ['str'],
          atributos: ['str'],
          requeridos: ['str'],
          areas: ['str'],
          fid_rol: ['array']
        }
      },
    }
  };

  async function crear (datos) {
    return models.adjudicacion_historico.create(datos);
  }

  const obtenerHistoricoModificaciones = async (idProyecto, idAdjudicacion, tipoModificacion, version) => {
    let query = {
      where: {
        fid_proyecto: idProyecto,
        id_adjudicacion: idAdjudicacion
      },
      order: [['version', 'ASC']],
      attributes: ['version', 'tipo_modificacion', 'estado_adjudicacion', 'monto', 'fecha_modificacion', 'plazo_ampliacion',
        [sequelize.literal(`COALESCE((monto - (SELECT ph.monto FROM adjudicacion_historico as ph WHERE ph.id_adjudicacion = ${idAdjudicacion} and version = ("adjudicacion_historico".version - 1)))::Decimal(12, 2)::float, 0)`), 'variacion']],
      include: [
        {
          model: models.estado,
          as: 'estado_actual',
          attributes: ['nombre']
        }, {
          model: models.parametrica,
          as: 'tipo_modificacion_adjudicacion',
          attributes: ['nombre']
        }
      ]
    };
    if (version) {
      query.where.version = { $lt: version };
    }
    if (!_.isEmpty(tipoModificacion)) {
      //query.where.tipo_modificacion = tipoModificacion;
      query.where.tipo_modificacion = { $not: null };
    }
    return models.adjudicacion_historico.findAll(query);
  }

  const obtenerDatosModificadosVersion = async (idProyecto, idAdjudicacion, version = 0) => {
    let consulta = {
      include: {
        model: models.parametrica,
        as: 'tipo_modificacion_adjudicacion',
        attributes: ['nombre', [sequelize.literal(`(select count(ph.*)
          from adjudicacion_historico ph
          where ph.tipo_modificacion="adjudicacion_historico".tipo_modificacion and ph.id_adjudicacion=${idAdjudicacion} AND ph.version <= ${version})`), 'numero']]
      },
      where: {
        id_adjudicacion: idAdjudicacion,
        fid_proyecto: idProyecto,
        version: version
      },
      attributes: ['monto', 'plazo_ampliacion', 'doc_respaldo_modificacion', 'fecha_modificacion']
    };
    return models.adjudicacion_historico.findOne(consulta);
  };

  const cancelarModificacion = async (adjudicacionActual) => {
    let adjudicacion = await app.dao.adjudicacion.obtenerPorId(adjudicacionActual.id_adjudicacion);
    await app.dao.modulo_historico.cancelarModificacionAdjudicacion(adjudicacion.fid_proyecto, adjudicacion.id_adjudicacion, adjudicacionActual.version - 1);
    await sequelize.query(`UPDATE adjudicacion p
      SET monto = ph.monto, version = ph.version, plazo_ampliacion = ph.plazo_ampliacion, doc_respaldo_modificacion = ph.doc_respaldo_modificacion,
          tipo_modificacion = ph.tipo_modificacion, fecha_modificacion = ph.fecha_modificacion, estado_adjudicacion = ph.estado_adjudicacion
      FROM adjudicacion_historico ph
      WHERE p.id_adjudicacion = $1 AND ph.id_adjudicacion = $1 AND ph.version = $2`,
      { bind: [adjudicacion.id_adjudicacion, adjudicacionActual.version - 1], type: sequelize.QueryTypes.UPDATE }
    );
    await sequelize.query(`DELETE FROM adjudicacion_historico
      WHERE id_adjudicacion = $1 AND version = $2;`,
      { bind: [adjudicacion.id_adjudicacion, adjudicacionActual.version - 1], type: sequelize.QueryTypes.UPDATE }
    );
  };

  const obtenerVariaciones = async (idProyecto, idAdjudicacion, version) => {
    let consulta = {
      where: {
        fid_proyecto: idProyecto,
        id_adjudicacion: idAdjudicacion,
        version: {
          $ne: 1
        }
      },
      attributes: ['tipo_modificacion', [sequelize.literal(`(SUM (monto - (SELECT ph.monto FROM adjudicacion_historico as ph WHERE ph.fid_proyecto = ${idProyecto} and ph.id_adjudicacion = ${idAdjudicacion} and version = ("adjudicacion_historico".version - 1))))::Decimal(12, 2)::float`), 'variacion']],
      group: 'tipo_modificacion'
    };
    let variaciones = util.json(await models.adjudicacion_historico.findAll(consulta));
    for (var i = 0; i < variaciones.length; i++) {
      variaciones[i].variacion = util.redondear(variaciones[i].variacion);
    }
    const VERSION_INICIAL = await obtenerDatosModificadosVersion(idProyecto,idAdjudicacion, 1);
    const VERSION_ANTERIOR = await obtenerDatosModificadosVersion(idProyecto,idAdjudicacion, version);
    return {
      monto_total_inicial: VERSION_INICIAL.monto,
      monto_total_anterior: VERSION_ANTERIOR.monto,
      porcentajes: {
        5: util.valorPorcentual(VERSION_INICIAL.monto, 5),
        10: util.valorPorcentual(VERSION_INICIAL.monto, 10),
        15: util.valorPorcentual(VERSION_INICIAL.monto, 15)
      },
      variaciones
    };
  };

  const obtenerModificaciones = async(idAdjudicacion,version) =>{
    return models.adjudicacion_historico.findAll({
      where:{
        id_adjudicacion: idAdjudicacion,
        version:{
          $gt: 1,
          $lt: version
        }
      },
      include: [
        {
          model: models.parametrica,
          as: 'tipo_modificacion_adjudicacion',
          attributes: ['nombre']
        }
      ],
      order: [['version', 'ASC']]
    });
  };

  const obtenerDatosParaPlanilla = async (idAdjudicacion, idRol, conEstadoActual = false, conHistorial = false, version) => {
    let consulta = app.dao.adjudicacion.filtrosPlanilla(idAdjudicacion);
    consulta.where.version = version;
    consulta.attributes.push([sequelize.literal(`(CASE WHEN (SELECT version FROM adjudicacion WHERE id_adjudicacion = ${idAdjudicacion}) = 1 THEN
      (SELECT monto FROM adjudicacion WHERE id_adjudicacion = ${idAdjudicacion}) ELSE
      (SELECT monto FROM adjudicacion_historico WHERE id_adjudicacion = ${idAdjudicacion} AND version = 1) END)`), 'monto_contractual']);
    if (conEstadoActual) {
      consulta.include.push({
        model: models.estado,
        as: 'estado_actual',
        attributes: ['codigo', 'nombre', 'tipo', 'acciones', 'atributos', 'requeridos', 'areas', ['fid_rol', 'roles']]
      });
    }
    if (conHistorial) {
      consulta.include.push({
        model: models.parametrica,
        as: 'tipo_modificacion_adjudicacion',
        attributes: ['nombre',
          [sequelize.literal(`(SELECT COUNT(ah.*) FROM adjudicacion_historico ah
            WHERE ah.tipo_modificacion="adjudicacion_historico".tipo_modificacion AND ah.id_adjudicacion=${idAdjudicacion} AND ah.version <= ${version})`), 'numero']
        ]
      });
      consulta.include.push({
        model: models.adjudicacion_historico,
        as: 'historicos',
        where: {
          version: {
            $lt: version
          }
        },
        required: false,
        attributes: ['version', 'tipo_modificacion'],
        include: {
          model: models.parametrica,
          as: 'tipo_modificacion_adjudicacion',
          attributes: ['nombre', [sequelize.literal(`(SELECT COUNT(ah.*)
                  FROM adjudicacion_historico ah
                  WHERE ah.tipo_modificacion="historicos".tipo_modificacion AND ah.id_adjudicacion=${idAdjudicacion} AND ah.version <= "historicos".version )`), 'numero']]
        }
      });
      consulta.order = [[{model: models.adjudicacion_historico, as: 'historicos'}, 'version', 'ASC']];
    }
    if (typeof(consulta.order) === 'string') {
      consulta.order = sequelize.literal(consulta.order);
    }
    let adjudicacion = await models.adjudicacion_historico.findOne(consulta);
    adjudicacion.dataValues.adjudicacion_historico = adjudicacion.historicos;
    delete adjudicacion.dataValues.historicos;
    if (conEstadoActual) {
      await app.dao.estado.prepararDatosPermitidos(adjudicacion, idRol);
    }
    // adjudicacion.dataValues.empresa = adjudicacion.dataValues.contrato[0].empresa;
    // delete adjudicacion.dataValues.contrato;
    // adjudicacion.dataValues.dpa = app.dao.municipio.getDpa(util.json(adjudicacion.municipio));
    // delete adjudicacion.dataValues.municipio;
    Object.assign(adjudicacion.dataValues, await app.dao.adjudicacion.calcularFechasRecepcion(adjudicacion));
    return adjudicacion;
  };

  const obtenerPorId = async (idAdjudicacion,version) => {
    let datosAdjudicacion = await models.adjudicacion_historico.findOne({
      where: {
        id_adjudicacion: idAdjudicacion,
        version: version
      },
      include:[
          {
            model: models.parametrica,
            as: 'tipo_modificacion_adjudicacion',
            attributes: ['nombre']
          },
      {
        model: models.estado,
        as: 'estado_actual',
        attributes: ['codigo', 'nombre', 'tipo', 'acciones', 'atributos', 'requeridos', 'areas', ['fid_rol', 'roles']]
      }]
    });
    if (!datosAdjudicacion) {
      return null;
    }
    return datosAdjudicacion;
  };

  app.dao.adjudicacion_historico.crear = crear;
  app.dao.adjudicacion_historico.obtenerHistoricoModificaciones = obtenerHistoricoModificaciones;
  app.dao.adjudicacion_historico.obtenerDatosModificadosVersion = obtenerDatosModificadosVersion;
  app.dao.adjudicacion_historico.cancelarModificacion = cancelarModificacion;
  app.dao.adjudicacion_historico.obtenerVariaciones = obtenerVariaciones;
  app.dao.adjudicacion_historico.obtenerDatosParaPlanilla = obtenerDatosParaPlanilla;
  app.dao.adjudicacion_historico.obtenerModificaciones = obtenerModificaciones;
  app.dao.adjudicacion_historico.obtenerPorId = obtenerPorId;
};
