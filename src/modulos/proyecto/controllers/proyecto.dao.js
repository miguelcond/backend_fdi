import fs from 'fs-extra';
import util from '../../../lib/util';
import moment from 'moment';
import fileType from 'file-type';
import shortid from 'shortid';
import Reporte from '../../../lib/reportes';
import seqOpt from '../../../lib/sequelize-options';
import errors from '../../../lib/errors';
import logger from '../../../lib/logger';
import _ from 'lodash';

module.exports = (app) => {
  const _app = app;
  const models = app.src.db.models;
  const proyectoModel = app.src.db.models.proyecto;
  const sequelize = app.src.db.sequelize;

  const output = {
    model: 'proyecto',
    fields: {
      id_proyecto: ['int', 'pk'],
      version: ['int'],
      codigo: ['str'],
      tipo: ['str'],
      nombre: ['str'],
      nro_convenio: ['str'],
      monto_fdi: ['float'],
      monto_contraparte: ['float'],
      monto_total_convenio: ['float'],
      monto_total: ['float'],
      plazo_ejecucion: ['int'],
      plazo_ejecucion_convenio: ['int'],
      plazo_recepcion_definitiva: ['int'],
      entidad_beneficiaria: ['str'],
      fecha_suscripcion_convenio: ['date'],
      doc_convenio: ['str'],
      doc_convenio_extendido: ['str'],
      doc_convenio_extendido2: ['str'],
      doc_convenio_extendido3: ['str'],
      doc_convenio_extendido4: ['str'],
      doc_convenio_extendido5: ['str'],
      nro_resmin_cife: ['str'],
      doc_resmin_cife: ['str'],
      doc_cert_presupuestaria: ['str'],
      doc_cert_presupuestaria2: ['str'],
      doc_cert_presupuestaria3: ['str'],
      doc_ev_externa: ['str'],
      desembolso: ['float'],
      porcentaje_desembolso: ['float'],
      fecha_desembolso: ['date'],
      anticipo: ['float'],
      porcentaje_anticipo: ['float'],
      sector: ['str'],
      fid_autoridad_beneficiaria: ['int'],
      cargo_autoridad_beneficiaria: ['str'],
      fax_autoridad_beneficiaria: ['str'],
      fcod_municipio: ['str'],
      coordenadas: ['str'],
      doc_especificaciones_tecnicas: ['str'],
      doc_base_contratacion: ['str'],
      orden_proceder: ['date'],
      fecha_fin_proyecto: ['date'],
      codigo_acceso: ['str'],
      matricula: ['str'],
      cod_documentacion_exp: ['str'],
      documentacion_exp: ['str'],
      motivo_rechazo: ['str'],
      estado_proyecto: ['enum'],
      fid_usuario_asignado: ['int'],
      nro_invitacion: ['str'],
      cartera:['int'],
      org_sociales:['bool'],
      estado: ['enum'],
      tipo_proyecto: {
        model: 'parametrica',
        fields: {
          codigo: ['str'],
          tipo: ['enum'],
          nombre: ['str']
        }
      },
      municipio: {
        model: 'municipio',
        fields: {
          codigo: ['str'],
          nombre: ['str'],
          codigo_provincia: ['str'],
          estado: ['enum'],
          point: ['str'],
          provincia: {
            model: 'provincia',
            fields: {
              codigo: ['str'],
              nombre: ['str'],
              codigo_departamento: ['str'],
              estado: ['enum'],
              departamento: {
                model: 'departamento',
                fields: {
                  codigo: ['str'],
                  nombre: ['str']
                }
              }
            }
          }
        }
      },
      usuario: {
        model: 'usuario',
        fields: {
          nro: ['int', 'pk'],
          persona: {
            fields: {
              nombres: ['str'],
              primer_apellido: ['str'],
              segundo_apellido: ['str']
            }
          }
        }
      },
      usuario_rol: {
        fields: {
          nro: ['int', 'pk'],
          nombre: ['str'],
          descripcion: ['str'],
          estado: ['str']
        }
      },
      contrato: {
        model: 'contrato',
        fields: {
          id_contrato: ['int', 'pk'],
          fid_empresa: ['int', 'fk'],
          fid_proyecto: ['int', 'fk'],
          nro_invitacion: ['str'],
          nro_cuce: ['str'],
          fecha_inscripcion: ['date'],
          nombre_representante_legal: ['str'],
          ci_representante_legal: ['str'],
          numero_testimonio: ['str'],
          lugar_emision: ['str'],
          fecha_expedicion: ['date'],
          fax: ['str'],
          correo: ['str'],
          estado: ['enum']
        }
      },
      boletas: {
        model: 'boleta',
        fields: {
          id_boleta: ['int', 'pk'],
          fid_proyecto: ['int', 'fk'],
          tipo_documento: ['enum'],
          tipo_boleta: ['enum'],
          numero: ['str'],
          monto: ['float'],
          estado: ['str'],
          fecha_inicio_validez: ['date'],
          fecha_fin_validez: ['date']
        }
      },
      equipo_tecnico: {
        model: 'equipo_tecnico',
        fields: {
          id_equipo_tecnico: ['int', 'pk'],
          fid_proyecto: ['int', 'fk'],
          tipo_equipo: ['enum'],
          fid_usuario: ['int', 'fk'],
          cargo: ['str'],
          doc_designacion: ['str'],
          estado: ['enum']
        }
      },
      estado_actual: {
        model: 'estado',
        fields: {
          codigo: ['str', 'pk'],
          nombre: ['str'],
          tipo: ['enum'],
          acciones: ['str'],
          atributos: ['str'],
          requeridos: ['str'],
          areas: ['str'],
          roles: ['array']
        }
      },
      autoridad_beneficiaria: {
        model: 'persona',
        fields: {
          tipo_documento: ['enum'],
          numero_documento: ['str'],
          complemento: ['str'],
          complemento_visible: ['bol'],
          fecha_nacimiento: ['date'],
          nombres: ['str'],
          primer_apellido: ['str'],
          segundo_apellido: ['str'],
          correo: ['str'],
          telefono: ['str'],
          estado: ['str']
        }
      },
      observacion: {
        fields: {
          mensaje: ['str'],
          usuario: {
            fields: {
              persona: {
                fields: {
                  nombres: ['str'],
                  primer_apellido: ['str'],
                  segundo_apellido: ['str']
                }
              }
            }
          }
        }
      },
      dpa: {
        fields: {
          departamento: {
            fields: {
              codigo: ['str'],
              nombre: ['str']
            }
          },
          provincia: {
            fields: {
              codigo: ['str'],
              nombre: ['str'],
              codigo_departamento: ['str']
            }
          },
          municipio: {
            fields: {
              codigo: ['str'],
              nombre: ['str'],
              point: ['str'],
              codigo_provincia: ['str']
            }
          }
        }
      },
      transicion: {
        fields: {
          objeto: ['int'],
          tipo: ['str'],
          observacion: ['str'],
          codigo_estado: ['str'],
          fid_usuario: ['int'],
          fid_usuario_rol: ['int'],
          fid_usuario_asignado: ['int'],
          fid_usuario_asignado_rol: ['int'],
          usuario: {
            fields: {
              nro: ['int','pk'],
              persona: {
                fields: {
                  nombres: ['str'],
                  primer_apellido: ['str'],
                  segundo_apellido: ['str']
                }
              }
            }
          },
          usuario_rol: {
            fields: {
              nro: ['int','pk'],
              nombre: ['str'],
              descripcion: ['str'],
              estado: ['str']
            }
          },
          usuario_asignado: {
            fields: {
              nro: ['int','pk'],
              persona: {
                fields: {
                  nombres: ['str'],
                  primer_apellido: ['str'],
                  segundo_apellido: ['str']
                }
              }
            }
          },
          usuario_asignado_rol: {
            fields: {
              nro: ['int','pk'],
              nombre: ['str'],
              descripcion: ['str'],
              estado: ['str']
            }
          },
        }
      },
      supervisiones: {
        fields: {
          id_supervision: ['int', 'pk'],
          nro_supervision: ['int'],
          estado_supervision: ['enum', 'fk'],
          fid_proyecto: ['int', 'fk'],
          fid_adjudicacion: ['int', 'fk'],
          adjudicacion:{
            fields: {
              id_adjudicacion: ['int'],
              referencia: ['str'],
              version:['int'],
              monto: ['float'],
              estado: ['str']
            }
          },
          observacion_proceso: {
            fields: {
              mensaje: ['str'],
              fid_usuario: ['int', 'fk'],
              usuario: {
                fields: {
                  persona: {
                    fields: {
                      nombres: ['str'],
                      primer_apellido: ['str'],
                      segundo_apellido: ['str']
                    }
                  }
                }
              }
            }
          },
          estado_actual: {
            fields: {
              codigo: ['enum', 'pk'],
              codigo_proceso: ['enum'],
              nombre: ['str'],
              tipo: ['enum'],
              fid_rol: ['array']
            }
          }
        }
      },
      formularios: {
        fields: {
          fid_proyecto: ['int'],
          fid_plantilla: ['int'],
          codigo_estado: ['str'],
          version: ['int'],
          nombre: ['str'],
          form_model: ['str'],
          estado: ['str'],
        }
      },
      _fecha_creacion: ['date'],
      _fecha_modificacion: ['date']
    }
  };
  const outputProcesados = {
    model: 'proyecto',
    fields: {
      id_proyecto: ['int', 'pk'],
      codigo: ['str'],
      nombre: ['str'],
      tipo: ['enum', 'fk'],
      matricula: ['str'],
      tipo_proyecto: {
        model: 'parametrica',
        fields: {
          codigo: ['enum', 'pk'],
          nombre: ['str']
        }
      },
      monto_total: ['float'],
      plazo_ejecucion: ['int'],
      plazo_recepcion_definitiva: ['int'],
      codigo_acceso: ['str'],
      _fecha_modificacion: ['date'],
      estado: ['str'],
      estado_proyecto: ['enum', 'fk'],
      estado_actual: {
        model: 'estado',
        fields: {
          codigo: ['enum', 'pk'],
          codigo_proceso: ['enum'],
          nombre: ['str'],
          tipo: ['enum'],
          fid_rol: ['array']
        }
      },
      transicion: {
        model: 'transicion',
        fields: {
          objeto_transicion: ['int'],
          observacion: ['str'],
          codigo_estado: ['str'],
          fid_usuario: ['int']
        }
      },
      supervisiones: {
        model: 'supervision',
        fields: {
          id_supervision: ['int', 'pk'],
          nro_supervision: ['int'],
          estado_supervision: ['enum'],
          fid_proyecto: ['int', 'fk'],
          estado_actual: {
            model: 'estado',
            fields: {
              codigo: ['enum', 'pk'],
              codigo_proceso: ['enum'],
              nombre: ['str'],
              tipo: ['enum'],
              fid_rol: ['array'],
              areas: ['array']
            }
          }
        }
      }
    }
  };

  const _ordernarSupervisiones = (supervisiones) => {
    return _.orderBy(supervisiones, ['nro_supervision'], ['desc']);
  };

  const _filtrarEstado = async (datos, idRol) => {
    const proyectos = datos;
    for (let i = 0; i < proyectos.rows.length; i++) {
      let estado = proyectos.rows[i].estado_actual;
      if (!Array.isArray(estado.acciones)) {
        estado.acciones = estado.acciones[idRol];
      }
      const acciones = [];
      if(estado.acciones){
        for (let j = 0; j < estado.acciones.length; j++) {
          if (!estado.acciones[j].condicion || await app.dao.estado.evaluar(estado.acciones[j].condicion, proyectos.rows[i]) === 1) {
            acciones.push(estado.acciones[j]);
          }
        }
      }
      estado.acciones = acciones;
      estado.atributos = app.dao.estado.getDatosPermitidos(estado.atributos, idRol);
      estado.requeridos = app.dao.estado.getDatosPermitidos(estado.requeridos, idRol);
      estado.areas = app.dao.estado.getDatosPermitidos(estado.areas, idRol);
      estado.areas = await app.dao.estado.revisarCondiciones(proyectos.rows[i], estado.areas);
      proyectos.rows[i].dataValues.dpa = app.dao.municipio.getDpa(util.json(proyectos.rows[i].municipio));
      delete proyectos.rows[i].dataValues.municipio;
      proyectos.rows[i].dataValues.usuario = undefined;
      proyectos.rows[i].dataValues.supervisiones = _ordernarSupervisiones(proyectos.rows[i].supervisiones);
    }
    return proyectos;
  };

  const adminObtener = async (consulta, idRol, idUsuario, query = {}) => {
    let resultOptimized;
    if (query.page < 1) {
      query.page = 1;
    }
    consulta.where = {
      $and: [consulta.condicionProyecto],
    };
    if (typeof(consulta.order) === 'string') {
      consulta.order = sequelize.literal(consulta.order);
    }
    const proyectos = await proyectoModel.findAndCountAll(consulta);
    if (proyectos.rows.length > 0) {
      resultOptimized = {
        count: proyectos.count,
        rows: seqOpt.createResult(query, util.json(proyectos.rows), output)
      };
    } else {
      resultOptimized = {
        count: 0,
        rows: []
      };
    }
    return resultOptimized;
  };

  const adminObtenerUsuario = async (codigo, consulta, idRol, idUsuario, query = {}) => {
    let resultOptimized;
    const proyectos = await proyectoModel.findOne({
      attributes: ['id_proyecto','nombre','tipo','estado_proyecto','nro_convenio','estado','fecha_suscripcion_convenio'],
      distinct: true,
      where: {
        id_proyecto: codigo
      },
      include: [
        {
          model: models.transicion,
          as: 'transicion',
          required: false,
          distinct: true,
          attributes: [['objeto_transicion','objeto'],['tipo_transicion','tipo']],
          where: {
            tipo_transicion: 'proyecto',
            fid_usuario_asignado: {
              $not: null
            },
            estado: 'ACTIVO'
          },
          include: [
            // {
            //   model: models.usuario,
            //   as: 'usuario',
            //   attributes: ['id_usuario'],
            //   include: [{
            //     model: models.persona,
            //     as: 'persona',
            //     attributes: ['nombres','primer_apellido','segundo_apellido']
            //   }]
            // },
            // {
            //   model: models.rol,
            //   as: 'usuario_rol',
            //   attributes: ['nombre','descripcion','estado'],
            // },
            {
              model: models.usuario,
              as: 'usuario_asignado',
              attributes: [['id_usuario','nro']],
              include: [{
                model: models.persona,
                as: 'persona',
                attributes: ['nombres','primer_apellido','segundo_apellido']
              }]
            },
            {
              model: models.rol,
              as: 'usuario_asignado_rol',
              attributes: [['id_rol','nro'],'nombre','descripcion','estado'],
            },
          ]
        },
        {
          model: models.usuario,
          as: 'usuario',
          attributes: [['id_usuario','nro']],
          include: [{
            model: models.persona,
            as: 'persona',
            attributes: ['nombres','primer_apellido','segundo_apellido']
          }]
        },
        {
          model: models.rol,
          as: 'usuario_rol',
          attributes: [['id_rol','nro'],'nombre','descripcion','estado'],
        },
        {
          model: models.municipio,
          as: 'municipio',
          attributes: ['nombre'],
          include: {
            model: models.provincia,
            as: 'provincia',
            attributes: ['nombre'],
            include: {
              model: models.departamento,
              as: 'departamento',
              attributes: ['nombre'],
            }
          }
        },
      ],
      order: [[{model: models.transicion, as: 'transicion'}, '_fecha_creacion', 'ASC']],
    });
    if (proyectos) {
      resultOptimized =  seqOpt.createResult(query, util.json(proyectos), output);
      // Eliminar transiciones realizadas con un mismo usuario y rol
      let transicion = [];
      for (let i in resultOptimized.transicion) {
        if ( ['EMPRESA','SUPERVISOR'].indexOf(resultOptimized.transicion[i].usuario_asignado_rol.nombre)<0 &&
             !transicion.find((t)=>{
               return t.usuario_asignado.nro==resultOptimized.transicion[i].usuario_asignado.nro &&
                     t.usuario_asignado_rol.nro==resultOptimized.transicion[i].usuario_asignado_rol.nro;
               })
            ) {
          transicion.push(resultOptimized.transicion[i]);
        }
      }
      resultOptimized.transicion = transicion;
    } else {
      resultOptimized = null;
    }
    return resultOptimized;
  };

  const adminModificarUsuario = async (codigo, consulta, idRol, idUsuario, body = {}, t) => {
    if (body.nuevo_nro_rol == 4 || body.nuevo_nro_rol == 5 || body.nuevo_nro_rol === 13) {
      const proyecto = await models.proyecto.update({
        fid_usuario_asignado: body.nuevo_nro_usuario,
        fid_usuario_asignado_rol: body.nuevo_nro_rol,
      },{
        where: {
          id_proyecto: codigo,
          fid_usuario_asignado: body.nro_usuario,
          fid_usuario_asignado_rol: body.nro_rol,
        },
        transaction: t,
      });
    }

    // Cambiar transiciones
    const transicion = await app.dao.transicion.reemplazarUsuario(idUsuario, body.nro_usuario, body.nuevo_nro_usuario, codigo, t);

    // const transicion = await models.transicion.update({
    //   fid_usuario_asignado: body.nuevo_nro_usuario,
    //   fid_usuario_asignado_rol: body.nuevo_nro_rol,
    //   _usuario_modificacion: idUsuario,
    // },{
    //   where: {
    //     fid_usuario_asignado: body.nro_usuario,
    //     fid_usuario_asignado_rol: body.nro_rol,
    //     objeto_transicion: codigo,
    //     // tipo_transicion: 'proyecto',
    //   },
    //   transaction: t,
    // });
    // const transicion2 = await models.transicion.update({
    //   fid_usuario: body.nuevo_nro_usuario,
    //   fid_usuario_rol: body.nuevo_nro_rol,
    //   _usuario_modificacion: idUsuario,
    // },{
    //   where: {
    //     fid_usuario: body.nro_usuario,
    //     fid_usuario_rol: body.nro_rol,
    //     objeto_transicion: codigo,
    //     // tipo_transicion: 'proyecto',
    //   },
    //   transaction: t,
    // });

    if (body.nuevo_nro_rol == 12 || body.nuevo_nro_rol == 13 || body.nuevo_nro_rol === 16 || body.nuevo_nro_rol === 17) {
      try {
        const equipo = await models.equipo_tecnico.update({
          fid_usuario: body.nuevo_nro_usuario,
          fid_rol: body.nuevo_nro_rol,
          _usuario_modificacion: idUsuario,
        },{
          where: {
            fid_usuario: body.nro_usuario,
            fid_rol: body.nro_rol,
            fid_proyecto: codigo,
            // tipo_transicion: 'proyecto',
          },
          transaction: t,
        });
      } catch (e) {
        throw new errors.ValidationError('Ya es o ha sido parte del equipo técnico en este proyecto.');
      }
    }

    return transicion;
  };
  const getPendientes = async (consulta, idRol, idUsuario, query = {}, flagAdjudicacion = false) => {
    const outputList = {
      model: 'proyecto',
      fields: {
        id_proyecto: ['int', 'pk'],
        version: ['int'],
        codigo: ['str'],
        tipo: ['str'],
        nombre: ['str'],
        nro_convenio: ['str'],
        monto_fdi: ['float'],
        monto_contraparte: ['float'],
        monto_total_convenio: ['float'],
        monto_total: ['float'],
        plazo_ejecucion: ['int'],
        plazo_ejecucion_convenio: ['int'],
        plazo_recepcion_definitiva: ['int'],
        entidad_beneficiaria: ['str'],
        fecha_suscripcion_convenio: ['date'],
        coordenadas: ['str'],
        motivo_rechazo: ['str'],
        orden_proceder: ['date'],
        fecha_fin_proyecto: ['date'],
        codigo_acceso: ['str'],
        matricula: ['str'],
        estado_proyecto: ['enum'],
        fid_usuario_asignado: ['int'],
        nro_invitacion: ['str'],
        estado: ['enum'],
        cartera:['int'],
        org_sociales:['bool'],
        tipo_proyecto: {
          model: 'parametrica',
          fields: {
            codigo: ['str'],
            tipo: ['enum'],
            nombre: ['str']
          }
        },
        estado_actual: {
          model: 'estado',
          fields: {
            codigo: ['str', 'pk'],
            nombre: ['str'],
            tipo: ['enum'],
            acciones: ['str'],
            atributos: ['str'],
            requeridos: ['str'],
            areas: ['str'],
            roles: ['array']
          }
        },
        observacion: {
          fields: {
            mensaje: ['str'],
            usuario: {
              fields: {
                persona: {
                  fields: {
                    nombres: ['str'],
                    primer_apellido: ['str'],
                    segundo_apellido: ['str']
                  }
                }
              }
            }
          }
        },
        dpa: {
          fields: {
            departamento: {
              fields: {
                codigo: ['str'],
                nombre: ['str']
              }
            },
            provincia: {
              fields: {
                codigo: ['str'],
                nombre: ['str'],
                codigo_departamento: ['str']
              }
            },
            municipio: {
              fields: {
                codigo: ['str'],
                nombre: ['str'],
                point: ['str'],
                codigo_provincia: ['str']
              }
            }
          }
        },
        adjudicaciones: {
          fields: {
            id_adjudicacion: ['int'],
            referencia: ['str'],
            version:['int'],
            monto: ['float'],
            estado: ['str'],
            supervision: {
              fields: {
                id_supervision: ['int', 'pk'],
                nro_supervision: ['int'],
                estado_supervision: ['enum', 'fk'],
                fid_proyecto: ['int', 'fk'],
                fid_adjudicacion: ['int'],
                observacion_proceso: {
                  fields: {
                    mensaje: ['str'],
                    fid_usuario: ['int', 'fk'],
                    usuario: {
                      fields: {
                        persona: {
                          fields: {
                            nombres: ['str'],
                            primer_apellido: ['str'],
                            segundo_apellido: ['str']
                          }
                        }
                      }
                    }
                  }
                },
                estado_actual: {
                  fields: {
                    codigo: ['enum', 'pk'],
                    codigo_proceso: ['enum'],
                    nombre: ['str'],
                    tipo: ['enum'],
                    fid_rol: ['array']
                  }
                }
              }
            }
          }
        },
        supervisiones: {
          fields: {
            id_supervision: ['int', 'pk'],
            nro_supervision: ['int'],
            estado_supervision: ['enum', 'fk'],
            fid_proyecto: ['int', 'fk'],
            fid_adjudicacion: ['int', 'fk'],
            adjudicacion:{
              fields: {
                id_adjudicacion: ['int'],
                referencia: ['str'],
                version:['int'],
                monto: ['float'],
                estado: ['str']
              }
            },
            observacion_proceso: {
              fields: {
                mensaje: ['str'],
                fid_usuario: ['int', 'fk'],
                usuario: {
                  fields: {
                    persona: {
                      fields: {
                        nombres: ['str'],
                        primer_apellido: ['str'],
                        segundo_apellido: ['str']
                      }
                    }
                  }
                }
              }
            },
            estado_actual: {
              fields: {
                codigo: ['enum', 'pk'],
                codigo_proceso: ['enum'],
                nombre: ['str'],
                tipo: ['enum'],
                fid_rol: ['array']
              }
            }
          }
        },
        formularios: {
          fields: {
            fid_proyecto: ['int'],
            fid_plantilla: ['int'],
            codigo_estado: ['str'],
            version: ['int'],
            nombre: ['str'],
            form_model: ['str'],
            estado: ['str'],
          }
        },
        _fecha_creacion: ['date'],
        _fecha_modificacion: ['date']
      }
    };

    const esSupervisorOFiscal = await app.dao.rol.esSupervisorOFiscal(idRol);
    const esLegal = await app.dao.rol.esLegal(idRol);
    const esResponsableGam = await app.dao.rol.esResponsableGam(idRol);

    let idAutoridad = 0;
    if (esResponsableGam) {
      const usuarioAutoridad = await app.src.db.models.usuario.findOne({
        attributes: [
          'fid_persona'
        ],
        where: {
          id_usuario: idUsuario
        }
      });

      idAutoridad = usuarioAutoridad && usuarioAutoridad.fid_persona ? usuarioAutoridad.fid_persona : 0;
    }
    // const usuariosAsignados = await app.dao.transicion.obtenerUsuariosAsignados(idUsuario);
    // console.log('usuariosAsignados',usuariosAsignados);

    const datosEstadosProyecto = await app.dao.estado.obtenerEstadosPorRol(idRol);
    const estadosRol = [];
    for (let i in datosEstadosProyecto) {
      if (datosEstadosProyecto[i].tipo !== 'FIN') {
        estadosRol.push(datosEstadosProyecto[i].codigo);
      }
    }
    const proyectos = await proyectoModel.proyectoIncluyePendiente(consulta, idRol, idUsuario, idAutoridad, esSupervisorOFiscal, esLegal, estadosRol, query.matricula);
    const result = await _filtrarEstado(proyectos, idRol);
    let resultOptimized = {
      count: result.count,
    };
    if (consulta.condicionProyecto) {
      resultOptimized.rows = seqOpt.createResult(query, util.json(result.rows), output)
    } else {
      resultOptimized.rows = seqOpt.createResult(query, util.json(result.rows), outputList)
    }
    // Parsear datos de los formularios obtenidos
    for (let i in resultOptimized.rows) {
      let duplicados = await proyectoModel.findAll({ attributes: ['nro_convenio', 'nombre'], where: { nro_convenio: resultOptimized.rows[i].nro_convenio, $not: { estado_proyecto: 'RECHAZADO_FDI' } } });
      if (duplicados.length>1) {
        resultOptimized.rows[i].duplicados = [];
        for(let j in duplicados) {
          resultOptimized.rows[i].duplicados.push(duplicados[j].nombre);
        }
      }
      let forms = resultOptimized.rows[i].formularios;
      for (let j in forms) {
        resultOptimized.rows[i][forms[j].nombre] = forms[j].form_model;
      }
      delete resultOptimized.rows[i].formularios;
    }

    return resultOptimized;
  };


  const get = async (consulta, idRol, idUsuario, query = {}, flagAdjudicacion = false) => {
    const outputList = {
      model: 'proyecto',
      fields: {
        id_proyecto: ['int', 'pk'],
        version: ['int'],
        codigo: ['str'],
        tipo: ['str'],
        nombre: ['str'],
        nro_convenio: ['str'],
        monto_fdi: ['float'],
        monto_contraparte: ['float'],
        monto_total_convenio: ['float'],
        monto_total: ['float'],
        plazo_ejecucion: ['int'],
        plazo_ejecucion_convenio: ['int'],
        plazo_recepcion_definitiva: ['int'],
        entidad_beneficiaria: ['str'],
        fecha_suscripcion_convenio: ['date'],
        coordenadas: ['str'],
        motivo_rechazo: ['str'],
        orden_proceder: ['date'],
        fecha_fin_proyecto: ['date'],
        codigo_acceso: ['str'],
        matricula: ['str'],
        estado_proyecto: ['enum'],
        fid_usuario_asignado: ['int'],
        nro_invitacion: ['str'],
        estado: ['enum'],
        cartera:['int'],
        org_sociales:['bool'],
        tipo_proyecto: {
          model: 'parametrica',
          fields: {
            codigo: ['str'],
            tipo: ['enum'],
            nombre: ['str']
          }
        },
        estado_actual: {
          model: 'estado',
          fields: {
            codigo: ['str', 'pk'],
            nombre: ['str'],
            tipo: ['enum'],
            acciones: ['str'],
            atributos: ['str'],
            requeridos: ['str'],
            areas: ['str'],
            roles: ['array']
          }
        },
        observacion: {
          fields: {
            mensaje: ['str'],
            usuario: {
              fields: {
                persona: {
                  fields: {
                    nombres: ['str'],
                    primer_apellido: ['str'],
                    segundo_apellido: ['str']
                  }
                }
              }
            }
          }
        },
        dpa: {
          fields: {
            departamento: {
              fields: {
                codigo: ['str'],
                nombre: ['str']
              }
            },
            provincia: {
              fields: {
                codigo: ['str'],
                nombre: ['str'],
                codigo_departamento: ['str']
              }
            },
            municipio: {
              fields: {
                codigo: ['str'],
                nombre: ['str'],
                point: ['str'],
                codigo_provincia: ['str']
              }
            }
          }
        },
        adjudicaciones: {
          fields: {
            id_adjudicacion: ['int'],
            referencia: ['str'],
            version:['int'],
            monto: ['float'],
            estado: ['str'],
            supervision: {
              fields: {
                id_supervision: ['int', 'pk'],
                nro_supervision: ['int'],
                estado_supervision: ['enum', 'fk'],
                fid_proyecto: ['int', 'fk'],
                fid_adjudicacion: ['int'],
                observacion_proceso: {
                  fields: {
                    mensaje: ['str'],
                    fid_usuario: ['int', 'fk'],
                    usuario: {
                      fields: {
                        persona: {
                          fields: {
                            nombres: ['str'],
                            primer_apellido: ['str'],
                            segundo_apellido: ['str']
                          }
                        }
                      }
                    }
                  }
                },
                estado_actual: {
                  fields: {
                    codigo: ['enum', 'pk'],
                    codigo_proceso: ['enum'],
                    nombre: ['str'],
                    tipo: ['enum'],
                    fid_rol: ['array']
                  }
                }
              }
            }
          }
        },
        supervisiones: {
          fields: {
            id_supervision: ['int', 'pk'],
            nro_supervision: ['int'],
            estado_supervision: ['enum', 'fk'],
            fid_proyecto: ['int', 'fk'],
            fid_adjudicacion: ['int', 'fk'],
            adjudicacion:{
              fields: {
                id_adjudicacion: ['int'],
                referencia: ['str'],
                version:['int'],
                monto: ['float'],
                estado: ['str']
              }
            },
            observacion_proceso: {
              fields: {
                mensaje: ['str'],
                fid_usuario: ['int', 'fk'],
                usuario: {
                  fields: {
                    persona: {
                      fields: {
                        nombres: ['str'],
                        primer_apellido: ['str'],
                        segundo_apellido: ['str']
                      }
                    }
                  }
                }
              }
            },
            estado_actual: {
              fields: {
                codigo: ['enum', 'pk'],
                codigo_proceso: ['enum'],
                nombre: ['str'],
                tipo: ['enum'],
                fid_rol: ['array']
              }
            }
          }
        },
        formularios: {
          fields: {
            fid_proyecto: ['int'],
            fid_plantilla: ['int'],
            codigo_estado: ['str'],
            version: ['int'],
            nombre: ['str'],
            form_model: ['str'],
            estado: ['str'],
          }
        },
        _fecha_creacion: ['date'],
        _fecha_modificacion: ['date']
      }
    };

    const esSupervisorOFiscal = await app.dao.rol.esSupervisorOFiscal(idRol);
    const esLegal = await app.dao.rol.esLegal(idRol);
    const esResponsableGam = await app.dao.rol.esResponsableGam(idRol);

    let idAutoridad = 0;
    if (esResponsableGam) {
      const usuarioAutoridad = await app.src.db.models.usuario.findOne({
        attributes: [
          'fid_persona'
        ],
        where: {
          id_usuario: idUsuario
        }
      });

      idAutoridad = usuarioAutoridad && usuarioAutoridad.fid_persona ? usuarioAutoridad.fid_persona : 0;
    }
    // const usuariosAsignados = await app.dao.transicion.obtenerUsuariosAsignados(idUsuario);
    // console.log('usuariosAsignados',usuariosAsignados);

    const datosEstadosProyecto = await app.dao.estado.obtenerEstadosPorRol(idRol);
    const estadosRol = [];
    for (let i in datosEstadosProyecto) {
      if (datosEstadosProyecto[i].tipo !== 'FIN') {
        estadosRol.push(datosEstadosProyecto[i].codigo);
      }
    }

    const proyectos = await proyectoModel.proyectoIncluye(consulta, idRol, idUsuario, idAutoridad, esSupervisorOFiscal, esLegal, estadosRol, query.matricula);
    const result = await _filtrarEstado(proyectos, idRol);

    let resultOptimized = {
      count: result.count,
    };
    if (consulta.condicionProyecto) {
      resultOptimized.rows = seqOpt.createResult(query, util.json(result.rows), output)
    } else {
      resultOptimized.rows = seqOpt.createResult(query, util.json(result.rows), outputList)
    }
    // Parsear datos de los formularios obtenidos
    for (let i in resultOptimized.rows) {
      let duplicados = await proyectoModel.findAll({ attributes: ['nro_convenio', 'nombre'], where: { nro_convenio: resultOptimized.rows[i].nro_convenio, $not: { estado_proyecto: 'RECHAZADO_FDI' } } });
      if (duplicados.length>1) {
        resultOptimized.rows[i].duplicados = [];
        for(let j in duplicados) {
          resultOptimized.rows[i].duplicados.push(duplicados[j].nombre);
        }
      }
      let forms = resultOptimized.rows[i].formularios;
      for (let j in forms) {
        resultOptimized.rows[i][forms[j].nombre] = forms[j].form_model;
      }
      delete resultOptimized.rows[i].formularios;
    }

    return resultOptimized;
  };

  const getMatricula = async (consulta, idRol) => {
    const proyectos = await proyectoModel.proyectoIncluyeMatricula(consulta, idRol);
    return _filtrarEstado(proyectos, idRol);
  };

  const getPorId = async (idProyecto) => {
    const PROYECTO = await proyectoModel.findOne({
      attributes: {
        exclude: ['documentacion_exp']
      },
      where: {
        id_proyecto: idProyecto
      }
    });
    if (!PROYECTO) {
      throw new errors.NotFoundError('El proyecto indicado no existe.');
    }
    return PROYECTO;
  };

  const guardarArchivo = async (archivo, idProyecto, codigoDocumento,nombreExtra) => {
    const CODIGO_PROYECTO = await app.dao.proyecto.obtenerCodigoProyecto(idProyecto);
    console.log("*************************************");
    console.log(CODIGO_PROYECTO);

    let { nombreArchivo, carpeta } = await app.dao.reporte.obtenerNombreArchivoYCarpeta(codigoDocumento, nombreExtra);
    await app.dao.reporte.crearObtenerDirectorioProyecto(CODIGO_PROYECTO);
    const path = `proyectos/${CODIGO_PROYECTO}/${carpeta}/${nombreArchivo}`;
    const base64 = archivo.base64;
    await app.dao.proyecto.escribirBase64(path, base64);
    return nombreArchivo;
  };

  const _procesarArchivos = async (id, proyecto, atributos, result, auditUsuario, proyectoActual, t) => {
    for (let i = 0; i < atributos.length; i++) {
      const atributo = atributos[i];
      if (proyecto[atributo]) {
        if (typeof proyecto[atributo] === 'object' && proyecto[atributo].path) {
          let codigoDocumento;
          if (atributo === 'doc_convenio') { codigoDocumento = 'C'; }
          if (atributo === 'doc_convenio_extendido') { codigoDocumento = 'CXT'; }
          if (atributo === 'doc_convenio_extendido2') { codigoDocumento = 'CXT2'; }
          if (atributo === 'doc_convenio_extendido3') { codigoDocumento = 'CXT3'; }
          if (atributo === 'doc_convenio_extendido4') { codigoDocumento = 'CXT4'; }
          if (atributo === 'doc_convenio_extendido5') { codigoDocumento = 'CXT5'; }
          if (atributo === 'doc_resmin_cife') { codigoDocumento = 'RM'; }
          if (atributo === 'doc_cert_presupuestaria') { codigoDocumento = 'CIP'; }
          if (atributo === 'doc_cert_presupuestaria2') { codigoDocumento = 'CIP2'; }
          if (atributo === 'doc_cert_presupuestaria3') { codigoDocumento = 'CIP3'; }
          if (atributo === 'doc_ev_externa') { codigoDocumento = 'EVE'; }
          if (atributo === 'doc_especificaciones_tecnicas') { codigoDocumento = 'ET'; }
          if (atributo === 'doc_base_contratacion') { codigoDocumento = 'DBC'; }
          if (atributo === 'doc_boleta_garantia') { codigoDocumento = 'BG'; }
          if (atributo === 'doc_respaldo_modificacion') { codigoDocumento = `RMP_v${proyectoActual.version}`; }
          result[atributo] = (id) ? (await guardarArchivo(proyecto[atributo], id, codigoDocumento))
            : (await app.dao.reporte.obtenerNombreArchivoYCarpeta(codigoDocumento)).nombreArchivo;
        } else {
          result[atributo] = proyecto[atributo];
          if (atributo === 'fid_usuario_asignado' && proyecto.fid_usuario_asignado) {
            await app.dao.equipo_tecnico.crearUpre(id, auditUsuario.id_usuario, auditUsuario.id_rol, proyecto.fid_usuario_asignado, proyecto.fid_usuario_asignado_rol, auditUsuario, t);
          }
        }
      }
    }
  };

  const validar = async (proyecto, auditUsuario, t) => {
    let proyectoActual;
    let estadoProyecto;
    let result = {};
    if (proyecto.id_proyecto) {
      proyectoActual = await getPorId(proyecto.id_proyecto);
      estadoProyecto = util.json(proyectoActual).estado_proyecto;
      result = proyectoActual;
    } else {
      let res = await app.src.db.models.estado.estadoInicial('PROYECTO-FDI', t);
      estadoProyecto = util.json(res).codigo;
    }
    if (proyecto.estado_proyecto) {
      result.cambio = {
        origen: estadoProyecto,
        destino: proyecto.estado_proyecto
      };
      if (proyecto.estado_proyecto !== estadoProyecto) {
        await app.dao.estado.validarDestinos(proyectoActual, proyecto, 'estado_proyecto', estadoProyecto, auditUsuario.id_rol, app.dao.proyecto);
        //if (proyectoActual.dataValues.estado_proyecto === 'MODIFICADO_FDI') {
        if (proyecto.estado_proyecto === 'MODIFICADO_FDI') {
          result.version = proyectoActual.dataValues.version + 1;
        }
      }
    }
    const ESTADO_ACTUAL = util.json(await app.dao.estado.getPorCodigo(estadoProyecto));
    await app.dao.estado.verificarPermisos(proyectoActual, ESTADO_ACTUAL, auditUsuario.id_rol, auditUsuario.id_usuario, proyecto.id_proyecto);
    let atributosPermitidos = await app.dao.estado.getAtributosPermitidos(ESTADO_ACTUAL, auditUsuario.id_rol);
    atributosPermitidos = await app.dao.estado.revisarCondiciones(proyectoActual, atributosPermitidos);
    await app.dao.estado.agregarAtributosPermitidos(result, proyecto, atributosPermitidos);
    await _procesarArchivos(proyecto.id_proyecto, proyecto, atributosPermitidos, result, auditUsuario, proyectoActual, t);
    await app.dao.estado.ejecutarAutomaticos(result, ESTADO_ACTUAL, app.dao.proyecto);
    return result;
  };

  const getConDatos = async (idProyecto) => {
    return proyectoModel.findOne({
      include: [
        {
          model: app.src.db.sequelize.models.modulo,
          as: 'modulos',
          where: {
            estado: 'ACTIVO'
          },
          required: false,
          include: [
            {
              model: app.src.db.sequelize.models.item,
              as: 'items',
              where: {
                estado: {
                  $in: ['ACTIVO','CERRADO']
                }
              },
              required: false
            }
          ]
        }, {
          model: app.src.db.sequelize.models.equipo_tecnico,
          as: 'equipo_tecnico'
        }, {
          model: app.src.db.sequelize.models.persona,
          as: 'autoridad_beneficiaria'
        }, {
          model: app.src.db.sequelize.models.contrato,
          as: 'contrato'
        }, {
          model: app.src.db.sequelize.models.boleta,
          as: 'boletas'
        }
      ],
      where: {
        id_proyecto: idProyecto
      }
    });
  };

  const siguiente = async (id, idRol, t) => {
    let estadoProyecto;
    let proyectoActual = await proyectoModel.findOne({
      include: [
        {
          model: app.src.db.sequelize.models.modulo,
          as: 'modulos',
          include: [
            {
              model: app.src.db.sequelize.models.item,
              as: 'items'
            }
          ]
        }, {
          model: app.src.db.sequelize.models.equipo_tecnico,
          as: 'equipo_tecnico'
        }, {
          model: app.src.db.sequelize.models.contrato,
          as: 'contrato'
        }
      ],
      where: {
        id_proyecto: id
      },
      transaction: t
    });
    estadoProyecto = proyectoActual.dataValues.estado_proyecto;
    if (estadoProyecto === 'EN_EJECUCION_FDI') {
      proyectoActual.version = proyectoActual.dataValues.version + 1;
    }
    await app.dao.estado.validarSiguiente(proyectoActual, {origen: estadoProyecto}, idRol);
    let nuevoEstado = await app.dao.estado.siguiente(estadoProyecto, idRol, proyectoActual, t);
    if (nuevoEstado) {
      proyectoActual.estado_proyecto = nuevoEstado;
      return proyectoActual.save({ transaction: t });
    } else {
      return proyectoActual;
    }
  };

  const obtenerEstado = async (idProyecto, t) => {
    let proyecto = await proyectoModel.findOne({
      attributes: ['id_proyecto'],
      include: [
        {
          attributes: ['codigo', 'nombre', 'atributos_detalle'],
          model: sequelize.models.estado,
          as: 'estado_actual'
        }
      ],
      where: {
        id_proyecto: idProyecto
      },
      transaction: t
    });
    if (proyecto) {
      return {
        id_proyecto: proyecto.id_proyecto,
        estado_actual: JSON.parse(JSON.stringify(proyecto.estado_actual))
      };
    } else {
      throw new Error('No se encontró el proyecto.');
    }
  };

  const eliminarArchivo = async (idProyecto, codigoDocumento, codigoProyecto) => {
    const CODIGO_PROYECTO = codigoProyecto ? codigoProyecto : await app.dao.proyecto.obtenerCodigoProyecto(idProyecto);
    const { nombreArchivo, carpeta } = await app.dao.reporte.obtenerNombreArchivoYCarpeta(codigoDocumento);
    await app.dao.reporte.crearObtenerDirectorioProyecto(CODIGO_PROYECTO);
    const path = `${_path}/uploads/proyectos/${CODIGO_PROYECTO}/${carpeta}/${nombreArchivo}`;
    try { fs.unlinkSync(path); } catch (error) { }
  };

  const escribirBase64 = (path, base64) => {
    let tipo;
    let decoded;
    const partes = base64.split('base64,');
    if (partes.length === 2) {
      decoded = Buffer.from(partes[1], 'base64');
      tipo = partes[0];
    } else {
      decoded = Buffer.from(partes[0], 'base64');
      let type = fileType(decoded);
      if (type) {
        tipo = type.mime;
      }
    }
    const dir = `${_path}/uploads`;
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    fs.writeFileSync(`${dir}/${path}`, decoded);
    return tipo;
  };

  const leerArchivo = (path) => {
    const dir = `${_path}/uploads`;
    try{
      const buffer = fs.readFileSync(`${dir}/${path}`);
      console.log("buffer>>",buffer)
      if (!buffer){
        console.log("NO SE EXISTE!!")
      }
      let type = fileType(buffer);
      if (type) {
        type = type.mime;
      } else {
        type = 'text/plain';
      }
      return {
        tipo: type,
        base64: buffer.toString('base64')
      };
    } catch (err){
      console.log("no se encontro el archivo")
      return null;
    }
  };

  const getMontoConvenio = async (proyecto) => {
    return util.sumar([proyecto.monto_fdi, proyecto.monto_contraparte]);
  };

  const calcularDesembolso = async (proyecto) => {
    return util.valorPorcentual(proyecto.monto_fdi, 20);
  };

  const getValoresAutomaticos = async (cola, elemento, proyecto) => {
    switch (elemento) {
      case '$calcularMontoConvenio':
        cola.push(await getMontoConvenio(proyecto));
        break;
      case '$calcularDesembolso':
        cola.push(await calcularDesembolso(proyecto));
        break;
      case '$shortid':
        cola.push(shortid.generate());
        break;
      case '$experiencia':
        cola.push(await app.dao.empresa.obtenerDocumentacionExp(cola.pop(), cola.pop()));
        break;
      case '$contrato':
        cola.push(app.dao.empresa.contrato(cola.pop(), cola.pop()));
        break;
      default:
        throw new errors.ValidationError(`No se encontró ${elemento}.`);
    }
    return cola;
  };

  const obtenerCodigoProyecto = async (idProyecto) => {
    const datosProyecto = await models.proyecto.findOne({
      attributes: ['codigo'],
      where: {
        id_proyecto: idProyecto
      }
    });
    return datosProyecto ? datosProyecto.codigo : null;
  };

  const obtenerDatosProyecto = async (idProyecto) => {
    return models.proyecto.findOne({
      where: {
        id_proyecto: idProyecto
      },
      include: [{
        model: models.municipio,
        as: 'municipio',
        attributes: ['codigo', 'nombre'],
        include: [{
          model: models.provincia,
          as: 'provincia',
          attributes: ['codigo', 'nombre', 'codigo_departamento'],
          include: [{
            model: models.departamento,
            as: 'departamento',
            attributes: ['codigo', 'nombre'],
          }]
        }]
      }]
    });
  };

  const obtenerDatosProyectoParaPlanilla = async (idProyecto) => {
    let datosProyecto = models.proyecto.findOne({
      where: {
        id_proyecto: idProyecto
      },
      include: [{
        model: models.municipio,
        as: 'municipio',
        attributes: ['codigo', 'nombre'],
        include: [{
          model: models.provincia,
          as: 'provincia',
          attributes: ['codigo', 'nombre', 'codigo_departamento'],
          include: [{
            model: models.departamento,
            as: 'departamento',
            attributes: ['codigo', 'nombre'],
          }]
        }]
      }],
      include: [{
        attributes: ['nombre', 'form_model'],
        model: models.formulario,
        as: 'formularios',
        where: {
          nombre: 'financiamiento'
        }
      }]
    });
    return datosProyecto;
  };

  // Version 2
  const listarProyectosProcesados_v2 = async (query, idRol, idUsuario, geom) => {
    let data = {
      options: {},     // Opciones de consulta
      result: null,    // resultado
      proyectos: [],   // proyectos procesados
      supervisa: [],   // participa en supervision
      adjudicacion: [],   // participa en supervision
    };

    // Departamentos
    data.options = {
      attributes: ['fcod_departamento'],
      where: {
        id_usuario: idUsuario
      },
      include: [{
        model: models.usuario_rol,
        as: 'usuarios_roles',
        attributes: ['fid_rol','fid_usuario'],
        required: true,
        where: {
          fid_rol: idRol,
          estado: 'ACTIVO'
        }
      }]
    };
    let departamentos = util.json(await models.usuario.findOne(data.options));

    // Supervisiones
    data.options = {
      attributes: ['fid_proyecto','id_supervision'],
      distinct: true,
      include: [
        {
          model: models.transicion,
          as: 'transicion',
          attributes: [],
          where: {
            tipo_transicion: 'supervision',
            fid_usuario_asignado: idUsuario,
            fid_usuario_asignado_rol: idRol,
          },
          required: true,
        },
        {
          model: models.estado,
          as: 'estado_actual',
          attributes: [],
          where: {
            $or: {
              $not: {
                $or: {
                  fid_rol: {
                    $contains: [idRol]
                  },
                  fid_rol_asignado: {
                    $contains: [idRol]
                  }
                }
              },
              codigo: {$in:['CERRADO_FDI','RECHAZADO_FDI']}
            }
          },
          required: true,
        },
      ],
    };
    data.result = util.json(await models.supervision.findAll(data.options));
    for (let i in data.result) {
      if (data.proyectos.indexOf(data.result[i].fid_proyecto)<0) {
        data.proyectos.push(data.result[i].fid_proyecto);
      }
      if (data.supervisa.indexOf(data.result[i].id_supervision)<0) {
        data.supervisa.push(data.result[i].id_supervision);
      }
    }
    // adjudicaciones
    data.options = {
      attributes: ['fid_proyecto','id_adjudicacion'],
      distinct: true,
      include: [
        {
          model: models.transicion,
          as: 'transicion',
          attributes: [],
          where: {
            tipo_transicion: 'adjudicacion',
            fid_usuario_asignado: idUsuario,
            fid_usuario_asignado_rol: idRol,
          },
          required: true,
        },
        {
          model: models.estado,
          as: 'estado_actual',
          attributes: [],
          where: {
            $or: {
              $not: {
                $or: {
                  fid_rol: {
                    $contains: [idRol]
                  },
                  fid_rol_asignado: {
                    $contains: [idRol]
                  }
                }
              },
              codigo: {$in:['REVISION_MODIFICADO_FDI']}
            }
          },
          required: true,
        },
      ],
    };
    data.result = util.json(await models.adjudicacion.findAll(data.options));
    for (let i in data.result) {
      if (data.proyectos.indexOf(data.result[i].fid_proyecto)<0) {
        data.proyectos.push(data.result[i].fid_proyecto);
      }
      if (data.adjudicacion.indexOf(data.result[i].id_adjudicacion)<0) {
        data.adjudicacion.push(data.result[i].id_adjudicacion);
      }
    }
    // Proyectos
    if(idRol == 12){ //solo los roles 12 ver todos los proyectos procesados
    data.options = {
      attributes: ['id_proyecto'],
      distinct: true,
      include: [
        {
          model: models.transicion,
          as: 'transicion',
          attributes: [],
          where: {
            tipo_transicion: 'proyecto',
            fid_usuario_asignado_rol: idRol,
          },
          required: true,
        },
        {
          model: models.estado,
          as: 'estado_actual',
          attributes: [],
          where: {
            $or: {
              $not: {
                $or: {
                  fid_rol: {
                    $contains: [idRol]
                  },
                  fid_rol_asignado: {
                    $contains: [idRol]
                  }
                }
              },
              codigo: {$in:['CERRADO_FDI','RECHAZADO_FDI']}
            }
          },
          required: true,
        },
      ],
    };
    }
    else {
     data.options = {
        attributes: ['id_proyecto'],
        distinct: true,
        include: [
          {
            model: models.transicion,
            as: 'transicion',
            attributes: [],
            where: {
              tipo_transicion: 'proyecto',
              fid_usuario_asignado: idUsuario,
              fid_usuario_asignado_rol: idRol,
            },
            required: true,
          },
          {
            model: models.estado,
            as: 'estado_actual',
            attributes: [],
            where: {
              $or: {
                $not: {
                  $or: {
                    fid_rol: {
                      $contains: [idRol]
                    },
                    fid_rol_asignado: {
                      $contains: [idRol]
                    }
                  }
                },
                codigo: {$in:['CERRADO_FDI','RECHAZADO_FDI']}
              }
            },
            required: true,
          },
        ],
      };
    }
    data.result = util.json(await models.proyecto.findAll(data.options));
    for (let i in data.result) {
      if (data.proyectos.indexOf(data.result[i].id_proyecto)<0) {
        data.proyectos.push(data.result[i].id_proyecto);
      }
    }

    // Roles especiales para ver las supervisiones
    // TODO se debe obtener de los parametros de configuracion.
    if ([3,4,5,8,12,13,16,17,18].indexOf(idRol) >= 0) {
      data.verSupervision = { $ne: null }
    } else {
      data.verSupervision = { $eq: null }
    }

    let estadosProyecto = ['ACTIVO'];
    if(idRol==1) estadosProyecto.push('INACTIVO');

    data.options = {
      attributes: ['id_proyecto','nro_convenio','codigo','nombre','monto_total','tipo','codigo_acceso','plazo_ejecucion','orden_proceder','estado_proyecto','_fecha_modificacion','fcod_municipio','motivo_rechazo','cartera'],
      distinct: true,
      where: {
        $or: [{
          id_proyecto: { $in: data.proyectos }
          
      }],
          $and: [
            sequelize.where(sequelize.fn('SUBSTRING', sequelize.col('proyecto.fcod_municipio'),1,2), sequelize.fn('ANY',departamentos.fcod_departamento)),
            (idRol===19) // Rol que pueden ver todo los proyectos por departamento
          ]
        ,
        estado: {
          $in: estadosProyecto
        }
      },
      include: [
        {
          model: models.parametrica,
          as: 'tipo_proyecto',
          attributes: ['nombre'],
          required: true,
        },
        {
          model: models.estado,
          as: 'estado_actual',
          attributes: ['codigo','nombre'],
          required: true,
        },
        {
          model: sequelize.models.adjudicacion,
          as: 'adjudicaciones',
          include:[
            {
              attributes: ['id_supervision','fid_adjudicacion', 'fid_proyecto', 'nro_supervision', 'estado_supervision'],
              model: sequelize.models.supervision,
              as: 'supervision',
              required: false,
              include:[
                {
                  attributes: ['codigo', 'codigo_proceso', 'nombre', 'tipo', 'fid_rol'],
                  model: sequelize.models.estado,
                  as: 'estado_actual'
                }
              ],
              where: sequelize.literal(`estado_supervision NOT IN ('EN_ARCHIVO_FDI','EN_FISCAL_FDI')`)
            }
          ],
          required: false,
          where: sequelize.literal(`(id_adjudicacion IN (SELECT fid_adjudicacion FROM equipo_tecnico WHERE fid_rol = ${idRol} and fid_usuario = ${idUsuario})
                                        OR ( ${idRol} = 12 AND adjudicaciones.version > 1)
                                        OR ( ${idRol} = 13 AND adjudicaciones.version > 1)
                                        OR ( ${idRol} = 17 AND adjudicaciones.version > 1)
                                        OR ( ${idRol} = 19 AND adjudicaciones.version > 1)
                                        OR ( (estado_adjudicacion IN('REVISION_MODIFICADO_FDI','MODIFICADO_FDI') or adjudicaciones.version > 1) AND ${idRol} = 18)) `),
          order: sequelize.literal(`id_adjudicacion ASC`)
        },
        {
          model: models.supervision,
          as: 'supervisiones',
          attributes: ['id_supervision','fid_adjudicacion','nro_supervision','estado_supervision'],
          where: {
            $or: {
              id_supervision: {
                $in: data.supervisa
              },
              nro_supervision: data.verSupervision
            }
          },
          include: [
            {
              attributes: ['id_adjudicacion','referencia','monto'],
              model: sequelize.models.adjudicacion,
              as: 'adjudicacion'
            },
            {
              model: models.estado,
              as: 'estado_actual',
              attributes: ['codigo','nombre'],
            },
          ],
          required: false,
        },
        {
          model: models.formulario,
          as: 'formularios',
          attributes: ['fid_plantilla','nombre','form_model'],
        },
        {
          model: models.proyecto,
          as: 'proyectos',
          attributes: ['id_proyecto','nro_convenio'],
          where: {
            $not:{
              estado: 'INACTIVO'
            }
          },
          required: false,
        },
        {
            attributes: ['codigo', 'nombre', 'codigo_provincia', geom? 'geom':'point'],
            model: sequelize.models.municipio,
            as: 'municipio',
            include: {
              attributes: ['codigo', 'nombre', 'codigo_departamento'],
              model: sequelize.models.provincia,
              as: 'provincia',
              include: {
                attributes: ['codigo', 'nombre'],
                model: sequelize.models.departamento,
                as: 'departamento'
              }
            }
          },
      ],
      order: [
        ['_fecha_modificacion','DESC'],
        // [{model:models.supervision,as:'supervisiones'},'nro_supervision','DESC']
      ],
    };
    data.options = seqOpt.createQuery(data.options, query, models, 'proyecto');
    // Parsear datos
    data.result = util.json(await models.proyecto.findAndCountAll(data.options));
    for (let i in data.result.rows) {
      // Ordenar supervisiones
      data.result.rows[i].supervisiones = _.sortBy(data.result.rows[i].supervisiones,['nro_supervision']).reverse();
      // Obtener datos de formularios dinamicos
      let forms = data.result.rows[i].formularios;
      for (let j in forms) {
        data.result.rows[i][forms[j].nombre] = forms[j].form_model;
      }
      delete data.result.rows[i].formularios;
    }

    return data.result;
  }

  const listarProyectosProcesados_v3 = async (query, idRol, idUsuario, geom) => {
    let data = {
      options: {},     // Opciones de consulta
      result: null,    // resultado
      proyectos: [],   // proyectos procesados
      supervisa: [],   // participa en supervision
      adjudicacion: [],   // participa en supervision
    };

    // Departamentos
    data.options = {
      attributes: ['fcod_departamento'],
      where: {
        id_usuario: idUsuario
      },
      include: [{
        model: models.usuario_rol,
        as: 'usuarios_roles',
        attributes: ['fid_rol','fid_usuario'],
        required: true,
        where: {
          fid_rol: idRol,
          estado: 'ACTIVO'
        }
      }]
    };
    let departamentos = util.json(await models.usuario.findOne(data.options));
    // console.log(departamentos.fcod_departamento, query)
    if(query['departamento.nombre']){
      // console.log(query['departamento.nombre'])
    let deps = {'01':'chuquisaca','02':'la paz', '03':'cochabamba','04':'oruro','05':'potosi','06':'tarija','07':'santa cruz','08':'beni','09':'pando'};

    departamentos.fcod_departamento = departamentos.fcod_departamento.filter(function(cod){console.log(deps[cod].indexOf(query['departamento.nombre']) >= 5); return deps[cod].indexOf(query['departamento.nombre']) >= 0;})
    // console.log('departamento', departamentos)
    }
    // Supervisiones
    data.options = {
      attributes: ['fid_proyecto','id_supervision'],
      distinct: true,
      include: [
        {
          model: models.transicion,
          as: 'transicion',
          attributes: [],
          where: {
            tipo_transicion: 'supervision',
            
          },
          required: true,
        },
        {
          model: models.estado,
          as: 'estado_actual',
          attributes: [],
          where: {
            $or: {
              $not: {
                $or: {
                  fid_rol: {
                    $contains: [idRol]
                  },
                  fid_rol_asignado: {
                    $contains: [idRol]
                  }
                }
              },
              codigo: {$in:['CERRADO_FDI','RECHAZADO_FDI']}
            }
          },
          required: true,
        },
      ],
    };
    data.result = util.json(await models.supervision.findAll(data.options));
    for (let i in data.result) {
      if (data.proyectos.indexOf(data.result[i].fid_proyecto)<0) {
        data.proyectos.push(data.result[i].fid_proyecto);
      }
      if (data.supervisa.indexOf(data.result[i].id_supervision)<0) {
        data.supervisa.push(data.result[i].id_supervision);
      }
    }
    // adjudicaciones
    data.options = {
      attributes: ['fid_proyecto','id_adjudicacion'],
      distinct: true,
      include: [
        {
          model: models.transicion,
          as: 'transicion',
          attributes: [],
          where: {
            tipo_transicion: 'adjudicacion',
           
          },
          required: true,
        },
        {
          model: models.estado,
          as: 'estado_actual',
          attributes: [],
          where: {
            $or: {
              $not: {
                $or: {
                  fid_rol: {
                    $contains: [idRol]
                  },
                  fid_rol_asignado: {
                    $contains: [idRol]
                  }
                }
              },
              codigo: {$in:['REVISION_MODIFICADO_FDI']}
            }
          },
          required: true,
        },
      ],
    };
    data.result = util.json(await models.adjudicacion.findAll(data.options));
    for (let i in data.result) {
      if (data.proyectos.indexOf(data.result[i].fid_proyecto)<0) {
        data.proyectos.push(data.result[i].fid_proyecto);
      }
      if (data.adjudicacion.indexOf(data.result[i].id_adjudicacion)<0) {
        data.adjudicacion.push(data.result[i].id_adjudicacion);
      }
    }
    // Proyectos
    data.options = {
      attributes: ['id_proyecto'],
      distinct: true,
      include: [
        {
          model: models.transicion,
          as: 'transicion',
          attributes: [],
          where: {
            tipo_transicion: 'proyecto',
            
          },
          required: true,
        },
        {
          model: models.estado,
          as: 'estado_actual',
          attributes: [],
          where: {
            $or: {
              $not: {
                $or: {
                  fid_rol: {
                    $contains: [idRol]
                  },
                  fid_rol_asignado: {
                    $contains: [idRol]
                  }
                }
              },
              codigo: {$in:['CERRADO_FDI','RECHAZADO_FDI']}
            }
          },
          required: true,
        },
      ],
    };
    data.result = util.json(await models.proyecto.findAll(data.options));
    for (let i in data.result) {
      if (data.proyectos.indexOf(data.result[i].id_proyecto)<0) {
        data.proyectos.push(data.result[i].id_proyecto);
      }
    }

    // Roles especiales para ver las supervisiones
    // TODO se debe obtener de los parametros de configuracion.
    if ([3,4,5,8,12,13,16,17,18].indexOf(idRol) >= 0) {
      data.verSupervision = { $ne: null }
    } else {
      data.verSupervision = { $eq: null }
    }

    let estadosProyecto = ['ACTIVO'];
    if(idRol==1) estadosProyecto.push('INACTIVO');

    data.options = {
      attributes: ['id_proyecto','nro_convenio','codigo','nombre','monto_total','tipo','codigo_acceso','plazo_ejecucion','orden_proceder','estado_proyecto','_fecha_modificacion','fcod_municipio','motivo_rechazo','cartera'],
      distinct: true,
      where: {
        $or: [{
          id_proyecto: { $in: data.proyectos }
          
      }],
          $and: [
            sequelize.where(sequelize.fn('SUBSTRING', sequelize.col('proyecto.fcod_municipio'),1,2), sequelize.fn('ANY',departamentos.fcod_departamento)),
            (idRol===19) // Rol que pueden ver todo los proyectos por departamento
          ]
        ,
        estado: {
          $in: estadosProyecto
        }
      },
      include: [
        {
          model: models.parametrica,
          as: 'tipo_proyecto',
          attributes: ['nombre'],
          required: true,
        },
        {
          model: models.estado,
          as: 'estado_actual',
          attributes: ['codigo','nombre'],
          required: true,
        },
        {
          model: sequelize.models.adjudicacion,
          as: 'adjudicaciones',
          include:[
            {
              attributes: ['id_supervision','fid_adjudicacion', 'fid_proyecto', 'nro_supervision', 'estado_supervision'],
              model: sequelize.models.supervision,
              as: 'supervision',
              required: false,
              include:[
                {
                  attributes: ['codigo', 'codigo_proceso', 'nombre', 'tipo', 'fid_rol'],
                  model: sequelize.models.estado,
                  as: 'estado_actual'
                }
              ],
              where: sequelize.literal(`estado_supervision NOT IN ('EN_ARCHIVO_FDI','EN_FISCAL_FDI')`)
            }
          ],
          required: false,
          where: sequelize.literal(`(id_adjudicacion IN (SELECT fid_adjudicacion FROM equipo_tecnico WHERE fid_rol = ${idRol} and fid_usuario = ${idUsuario})
                                        OR ( ${idRol} = 12 AND adjudicaciones.version > 1)
                                        OR ( ${idRol} = 13 AND adjudicaciones.version > 1)
                                        OR ( ${idRol} = 17 AND adjudicaciones.version > 1)
                                        OR ( ${idRol} = 19 AND adjudicaciones.version > 1)
                                        OR ( ${idRol} = 20 AND adjudicaciones.version > 1)
                                        OR ( (estado_adjudicacion IN('REVISION_MODIFICADO_FDI','MODIFICADO_FDI') or adjudicaciones.version > 1) AND ${idRol} = 18)) `),
          order: sequelize.literal(`id_adjudicacion ASC`)
        },
        {
          model: models.supervision,
          as: 'supervisiones',
          attributes: ['id_supervision','fid_adjudicacion','nro_supervision','estado_supervision'],
          where: {
            $or: {
              id_supervision: {
                $in: data.supervisa
              },
              nro_supervision: data.verSupervision
            }
          },
          include: [
            {
              attributes: ['id_adjudicacion','referencia','monto'],
              model: sequelize.models.adjudicacion,
              as: 'adjudicacion'
            },
            {
              model: models.estado,
              as: 'estado_actual',
              attributes: ['codigo','nombre'],
            },
          ],
          required: false,
        },
        {
          model: models.formulario,
          as: 'formularios',
          attributes: ['fid_plantilla','nombre','form_model'],
        },
        {
          model: models.proyecto,
          as: 'proyectos',
          attributes: ['id_proyecto','nro_convenio'],
          where: {
            $not:{
              estado: 'INACTIVO'
            }
          },
          required: false,
        },
        {
            attributes: ['codigo', 'nombre', 'codigo_provincia', geom? 'geom':'point'],
            model: sequelize.models.municipio,
            as: 'municipio',
            include: {
              attributes: ['codigo', 'nombre', 'codigo_departamento'],
              model: sequelize.models.provincia,
              as: 'provincia',
              include: {
                attributes: ['codigo', 'nombre'],
                model: sequelize.models.departamento,
                as: 'departamento'
              }
            }
          },
      ],
      order: [
        ['_fecha_modificacion','DESC'],
        // [{model:models.supervision,as:'supervisiones'},'nro_supervision','DESC']
      ],
    };
    data.options = seqOpt.createQuery(data.options, query, models, 'proyecto');
    // Parsear datos
    data.result = util.json(await models.proyecto.findAndCountAll(data.options));
    for (let i in data.result.rows) {
      // Ordenar supervisiones
      data.result.rows[i].supervisiones = _.sortBy(data.result.rows[i].supervisiones,['nro_supervision']).reverse();
      // Obtener datos de formularios dinamicos
      let forms = data.result.rows[i].formularios;
      for (let j in forms) {
        data.result.rows[i][forms[j].nombre] = forms[j].form_model;
      }
      delete data.result.rows[i].formularios;
    }

    return data.result;

  }

  const listarProyectosProcesados_v2Movil = async (query, idRol, idUsuario) => {
    let data = {
      options: {},     // Opciones de consulta
      result: null,    // resultado
      proyectos: [],   // proyectos procesados
      supervisa: [],   // participa en supervision
    };
    // Supervisiones
    data.options = {
      attributes: ['fid_proyecto','id_supervision'],
      distinct: true,
      include: [
        {
          model: models.transicion,
          as: 'transicion',
          attributes: [],
          where: {
            tipo_transicion: 'supervision',
            fid_usuario_asignado: idUsuario,
            fid_usuario_asignado_rol: idRol,
          },
          required: true,
        },
        {
          model: models.estado,
          as: 'estado_actual',
          attributes: [],
          where: {
            $or: {
              $not: {
                $or: {
                  fid_rol: {
                    $contains: [idRol]
                  },
                  fid_rol_asignado: {
                    $contains: [idRol]
                  }
                }
              },
              codigo: 'CERRADO_FDI',
            }
          },
          required: true,
        },
      ],
    };
    data.result = util.json(await models.supervision.findAll(data.options));
    for (let i in data.result) {
      if (data.proyectos.indexOf(data.result[i].fid_proyecto)<0) {
        data.proyectos.push(data.result[i].fid_proyecto);
      }
      if (data.supervisa.indexOf(data.result[i].id_supervision)<0) {
        data.supervisa.push(data.result[i].id_supervision);
      }
    }
    // Proyectos
    data.options = {
      attributes: ['id_proyecto','fcod_municipio'],
      distinct: true,
      include: [
        {
          model: models.transicion,
          as: 'transicion',
          attributes: [],
          where: {
            tipo_transicion: 'proyecto',
            fid_usuario_asignado: idUsuario,
            fid_usuario_asignado_rol: idRol,
          },
          required: true,
        },
        {
          model: models.estado,
          as: 'estado_actual',
          attributes: [],
          where: {
            $or: {
              $not: {
                $or: {
                  fid_rol: {
                    $contains: [idRol]
                  },
                  fid_rol_asignado: {
                    $contains: [idRol]
                  }
                }
              },
              codigo: 'CERRADO_FDI',
            }
          },
          required: true,
        },
      ],
    };
    data.result = util.json(await models.proyecto.findAll(data.options));
    for (let i in data.result) {
      if (data.proyectos.indexOf(data.result[i].id_proyecto)<0) {
        data.proyectos.push(data.result[i].id_proyecto);
      }
    }

    // Roles especiales para ver las supervisiones
    // TODO se debe obtener de los parametros de configuracion.
    if ([3,4,5,8,12,13,16,17].indexOf(idRol) >= 0) {
      data.verSupervision = { $ne: null }
    } else {
      data.verSupervision = { $eq: null }
    }

    data.options = {
      attributes: ['id_proyecto','nro_convenio','codigo','nombre','monto_total','tipo','codigo_acceso','plazo_ejecucion','estado_proyecto','_fecha_modificacion','fcod_municipio','motivo_rechazo'],
      distinct: true,
      where: {
        id_proyecto: {
          $in: data.proyectos
        }
      },
      include: [
        {
          model: models.parametrica,
          as: 'tipo_proyecto',
          attributes: ['nombre'],
          required: true,
        },
        {
          model: models.estado,
          as: 'estado_actual',
          attributes: ['codigo','nombre'],
          required: true,
        }, {
            attributes: ['codigo', 'nombre','geom','codigo_provincia'],
            model: sequelize.models.municipio,
            as: 'municipio',
            include: {
              attributes: ['codigo', 'nombre', 'codigo_departamento'],
              model: sequelize.models.provincia,
              as: 'provincia',
              include: {
                attributes: ['codigo', 'nombre'],
                model: sequelize.models.departamento,
                as: 'departamento'
              }
            }
          },
      ],
      order: [
        ['_fecha_modificacion','DESC'],
        // [{model:models.supervision,as:'supervisiones'},'nro_supervision','DESC']
      ],
    };
    data.options = seqOpt.createQuery(data.options, query, models, 'proyecto');
    // Parsear datos
    data.result = util.json(await models.proyecto.findAndCountAll(data.options));
    for (let i in data.result.rows) {
      // Ordenar supervisiones
      data.result.rows[i].supervisiones = _.sortBy(data.result.rows[i].supervisiones,['nro_supervision']).reverse();
      // Obtener datos de formularios dinamicos
      let forms = data.result.rows[i].formularios;
      for (let j in forms) {
        data.result.rows[i][forms[j].nombre] = forms[j].form_model;
      }
      delete data.result.rows[i].formularios;
    }

    return data.result;
  }

  // Version 1
  const listarProyectosProcesados_v1 = async (query, idRol, idUsuario) => {
    const options = seqOpt.createOptions(query, models, outputProcesados);
    const matricula = query.matricula;
    const ES_EMPRESA = await app.dao.rol.esEmpresa(idRol);
    const ROLES_GENERALES = util.listaACadena(app.dao.rol.getRolesGenerales());
    if (!options.include) {
      options.include = [];
    }
    const datosEstadosProyecto = await app.dao.estado.obtenerEstadosPorRol(idRol);
    const estadosRol = [];
    for (let i in datosEstadosProyecto) {
      if (datosEstadosProyecto[i].tipo !== 'FIN') {
        estadosRol.push(datosEstadosProyecto[i].codigo);
      }
    }
    let estadosRolSTR = util.listaACadena(estadosRol) || `'DESCONOCIDO'`;
    for (let i in options.include) {
      if (options.include[i].as === 'transicion') {
        if (!options.include[i].where) {
          options.include[i].where = {};
        }
        options.include[i].required = true;
        options.include[i].where.$and = [
          { fid_usuario: idUsuario, fid_usuario_rol: idRol },
          sequelize.literal(`(
            (
              (
                SELECT COUNT(*)
                FROM equipo_tecnico eq
                WHERE ("proyecto".id_proyecto = eq.fid_proyecto)
                AND ( eq.fid_usuario = ${idUsuario} AND eq.fid_rol = ${idRol} AND estado='ACTIVO')
              ) > 0
              OR (
                SELECT COUNT(*)
                FROM rol
                WHERE id_rol = ${idRol} AND  rol.nombre IN (${ROLES_GENERALES})
              ) > 0
              OR (
                ${ES_EMPRESA}
              )
            )
          )`)
        ];
      }
      if (options.include[i].as === 'estado_actual') {
        options.include[i].required = true;
        options.include[i].where = sequelize.literal(`(
              (
                (estado_proyecto <> 'APROBADO_FDI')
              )
              OR
              (
                ( (
                    SELECT COUNT(*)
                    FROM supervision sup
                    WHERE ("proyecto".id_proyecto = sup.fid_proyecto)
                    AND (
                      ( sup.estado_supervision IN(${estadosRolSTR}) AND (sup.estado_supervision <> 'EN_TECNICO_FDI') )
                      OR ( (sup.estado_supervision = 'EN_TECNICO_FDI') AND (fid_usuario_asignado = ${idUsuario})  )
                    )
                    AND (
                      (${ES_EMPRESA} AND ("proyecto".matricula = '${matricula}'))
                      OR (${!ES_EMPRESA})
                    )
                  ) = 0)
              )
         )`);
      };
    }
    if (!options.where) {
      options.where = {};
    }
    options.distinct = true;
    const filtroEstadoProyecto = options.where.estado_proyecto;
    options.where.estado_proyecto = { '$notIn': estadosRol };
    if (filtroEstadoProyecto) {
      options.where.estado_proyecto.$in = [filtroEstadoProyecto];
    }
    options.order = [['estado','ASC'], ['_fecha_modificacion','DESC']];
    const PROYECTOS = util.json(await models.proyecto.findAndCount(options));

    const USUARIO_SUPERVISION = await app.dao.rol.formaParteDeSupervision(idRol);
    PROYECTOS.rows.map((proyecto) => {
      if (!USUARIO_SUPERVISION) {
        delete proyecto.supervisiones;
      } else {
        proyecto.supervisiones = _ordernarSupervisiones(proyecto.supervisiones);
      }
    });
    return PROYECTOS;
  };
  const getProyectoQR = async (codigoPro) => {
    const PROYECTO = await models.proyecto.sequelize.query(`select p.id_proyecto, p.codigo, p.nombre,p.cartera, f.fid_proyecto, f.form_model->'nro_convenio' as numero_convenio,p.fcod_municipio,m.nombre as mun,d.nombre as dep
from proyecto p, formulario f, municipio m, departamento d
where p.codigo = ? and f.nombre ='convenio'  and f.fid_proyecto = p.id_proyecto and m.codigo =p.fcod_municipio and d.codigo = substring(p.fcod_municipio,1,2)` ,{
      replacements:[codigoPro],
      type: sequelize.QueryTypes.SELECT
    })
    if (!PROYECTO) {
      throw new errors.NotFoundError(`No existe el proyecto solicitado.`);
    }
    return PROYECTO;
  };
  const getProyectoId = async (idProyecto) => {
    const PROYECTO = await models.proyecto.findOne({
      where: {
        id_proyecto: idProyecto
      }
    });
    if (!PROYECTO) {
      throw new errors.NotFoundError(`No existe el proyecto solicitado.`);
    }
    return PROYECTO;
  };

  const getUsuarioTransicion = async (idProyecto,estado) => {
    return models.transicion.findOne({
      where: {
        tipo_transicion:'proyecto',
        objeto_transicion:idProyecto,
        codigo_estado: estado
      },
      order: sequelize.literal('id_transicion ASC')
    });
  }

  const getParametrica = async (_tipo,_codigo) => {
    return models.parametrica.findOne({
      where: {
        tipo:_tipo,
        codigo:_codigo
      }
    });
  }

  const getPorCodigoAcceso = async (codigoAcceso) => {
    return models.proyecto.findOne({
      where: {
        $and: [
          {
            codigo_acceso: {
              $ne: null
            }
          }, {
            codigo_acceso: codigoAcceso,
            matricula: null,
            estado_proyecto: 'ADJUDICACION'
          }
        ]
      }
    });
  };

  const calcularFechasRecepcion = async (proyecto) => {
    if (!proyecto.orden_proceder) {
      return null;
    }
    const ORDEN_PROCEDER = proyecto.orden_proceder;
    const PLAZO_EJECUCION = proyecto.plazo_ejecucion;
    const NRO_DIAS = proyecto.plazo_recepcion_definitiva || 90;
    const AMPLIACION_PLAZO = proyecto.plazo_ampliacion ? proyecto.plazo_ampliacion.por_volumenes + proyecto.plazo_ampliacion.por_compensacion : 0;

    // Obtener configuración calc fecha
    let configCalcFecha = await app.dao.parametrica.obtenerPorCodigoTipo('CFG_CALC_FECHA', 'CONFIG');
    let configRestarDias = configCalcFecha && configCalcFecha.descripcion ? parseInt(configCalcFecha.descripcion) : 0;

    return {
      fecha_recepcion_provisional: util.sumarDiasAFecha(PLAZO_EJECUCION + AMPLIACION_PLAZO - configRestarDias, ORDEN_PROCEDER),
      fecha_recepcion_definitiva: util.sumarDiasAFecha(PLAZO_EJECUCION + NRO_DIAS + AMPLIACION_PLAZO - configRestarDias, ORDEN_PROCEDER)
    };
  };

  const filtrosPlanilla = (idProyecto) => {
    let query = {
      where: {
        id_proyecto: idProyecto
      },
      include: [
        {
          model: models.municipio,
          as: 'municipio',
          attributes: ['codigo', 'nombre', 'codigo_provincia'],
          include: {
            model: models.provincia,
            as: 'provincia',
            attributes: ['codigo', 'nombre', 'codigo_departamento'],
            include: {
              model: models.departamento,
              as: 'departamento',
              attributes: ['codigo', 'nombre']
            }
          }
        }
        // {
        //   model: models.contrato,
        //   as: 'contrato',
        //   where: {
        //     estado: 'ACTIVO'
        //   },
        //   include: {
        //     model: models.empresa,
        //     as: 'empresa',
        //     attributes: ['nombre', 'nit', 'matricula']
        //   }
        // }, {
        //   model: models.boleta,
        //   as: 'boletas',
        //   attributes: ['monto', 'tipo_boleta', 'tipo_documento', 'fecha_fin_validez', 'estado']
        // }
      ],
      // attributes: ['codigo', 'version', 'tipo', 'nombre', 'desembolso', 'monto_total', 'monto_total_convenio', 'monto_fdi', 'monto_contraparte', 'orden_proceder', 'plazo_ejecucion',
      //   'entidad_beneficiaria', 'anticipo', 'porcentaje_anticipo', 'doc_especificaciones_tecnicas', 'tipo_modificacion', 'plazo_ampliacion', 'doc_respaldo_modificacion', 'fecha_modificacion']
    };
    return query;
  };

  const obtenerDatosParaPlanilla = async (idProyecto, idRol, conEstadoActual = false, conHistorial = false) => {
    let consulta = filtrosPlanilla(idProyecto);
    consulta.attributes = { include: [[sequelize.literal(`(CASE WHEN (SELECT version FROM proyecto WHERE id_proyecto = ${idProyecto}) = 1 THEN
    (SELECT monto_total FROM proyecto WHERE id_proyecto = ${idProyecto}) ELSE
    (SELECT monto_total FROM proyecto_historico WHERE id_proyecto = ${idProyecto} AND version = 1) END)`), 'monto_contractual']] };
    // consulta.attributes.push([sequelize.literal(`(CASE WHEN (SELECT version FROM proyecto WHERE id_proyecto = ${idProyecto}) = 1 THEN
    //   (SELECT monto_total FROM proyecto WHERE id_proyecto = ${idProyecto}) ELSE
    //   (SELECT monto_total FROM proyecto_historico WHERE id_proyecto = ${idProyecto} AND version = 1) END)`), 'monto_contractual']);
    if (conEstadoActual) {
      consulta.include.push({
        model: models.estado,
        as: 'estado_actual',
        attributes: ['codigo', 'nombre', 'tipo', 'acciones', 'atributos', 'requeridos', 'areas', ['fid_rol', 'roles']]
      });
    }
    if (conHistorial) {
      consulta.include.push({
        model: models.parametrica,
        as: 'tipo_modificacion_proyecto',
        attributes: ['nombre', [sequelize.literal(`(SELECT COUNT(ph.*)
                FROM proyecto_historico ph
                WHERE ph.tipo_modificacion="proyecto".tipo_modificacion AND ph.id_proyecto=${idProyecto}) + 1`), 'numero']]
      });
      consulta.include.push({
        model: models.proyecto_historico,
        as: 'proyecto_historico',
        attributes: ['version', 'tipo_modificacion'],
        include: {
          model: models.parametrica,
          as: 'tipo_modificacion_proyecto',
          attributes: ['nombre', [sequelize.literal(`(SELECT COUNT(ph.*)
                  FROM proyecto_historico ph
                  WHERE ph.tipo_modificacion="proyecto_historico".tipo_modificacion AND ph.id_proyecto=${idProyecto} AND ph.version <= "proyecto_historico".version )`), 'numero']]
        }
      });
      consulta.order = [[{model: models.proyecto_historico, as: 'proyecto_historico'}, 'version', 'ASC']];
    }

    consulta.include.push({
      attributes: ['nombre', 'form_model'],
      model: models.formulario,
      as: 'formularios',
      where: {
        nombre: {$in:['financiamiento','convenio']} //obtengo convenio para la suma de las adendas al plazo ejecucion
      }
    });

    let proyecto = await models.proyecto.findOne(consulta);
    for(let a in proyecto.dataValues.formularios){
      if(proyecto.dataValues.formularios[a].nombre == 'convenio'){
        proyecto.dataValues.convenio = proyecto.dataValues.formularios[a].form_model;
        proyecto.dataValues.formularios.pop(); //elimina el ultimo elemento del array formularios
      }
    }
    if (conEstadoActual) {
      await app.dao.estado.prepararDatosPermitidos(proyecto, idRol);
    }
    proyecto.dataValues.empresa = null; // proyecto.dataValues.contrato[0].empresa;
    // delete proyecto.dataValues.contrato;
    proyecto.dataValues.dpa = app.dao.municipio.getDpa(util.json(proyecto.municipio));

    // proyecto.dataValues.formularios = proyecto.dataValues.formularios[0] ? proyecto.dataValues.formularios[0] : {};

    delete proyecto.dataValues.municipio;
    let fechasAjustadas = await calcularFechasRecepcion(proyecto);
    Object.assign(proyecto.dataValues, fechasAjustadas);
    return proyecto;
  };

  const actualizar = async (idProyecto, datos, idUsuario) => {
    datos._usuario_modificacion = idUsuario;
    return models.proyecto.update(datos, {where: {id_proyecto: idProyecto}});
  };

  const _getBoletaMasAntiguaPorFechaInicio = (proyecto) => {
    return util.getListaOrdenada(proyecto.boletas, 'fecha_inicio_validez')[0];
  };

  const validarFechaDesembolso = async (proyecto) => {
    if (proyecto.fecha_desembolso <= proyecto.fecha_suscripcion_convenio) {
      throw new errors.ValidationError(`La fecha de desembolso debe ser mayor a la fecha de suscripción del convenio.`);
    }
  };

  //codigoProyectoReferencia
  const codigoProyectoReferencia = async (idProyecto) => {
    let proyecto = await models.proyecto.findOne({
      attributes: ['nro_convenio'],
      where: {
        id_proyecto: idProyecto
      }
    });
    let codigo_generado = proyecto.nro_convenio;
    let subcodigos = await models.proyecto.findAll({
      attributes: ['nro_convenio'],
      where: sequelize.literal('nro_convenio like \''+codigo_generado+'-%\''),
      order: sequelize.literal('nro_convenio DESC')
    });

    codigo_generado = codigo_generado+"-"+(subcodigos.length+1);

    return codigo_generado;
  };

  // Funcion para obtener codigo correlativo FDI/DTP/ARTP/0012-2018
  const verNroCodigo = async (nroConvenio) => {
    let proyecto = await models.proyecto.findOne({
      attributes: ['nro_convenio'],
      where: {
        nro_convenio: nroConvenio
      }
    });
    if (!proyecto) {
      return nroConvenio
    }

    // let anio = moment(new Date()).format('YYYY');
    let anio = nroConvenio.substr(-4);
    proyecto = await models.proyecto.findOne({
      attributes: ['nro_convenio'],
      // where: sequelize.literal('extract(YEAR FROM fecha_suscripcion_convenio) = '+anio+' and nro_convenio like \'FDI/DTP/ARTP/%\' '),
      where: sequelize.literal(`nro_convenio LIKE '%/____-${anio}' `),
      order: sequelize.literal('nro_convenio DESC')
    });
    // if(codigo!=null)
    proyecto = util.json(proyecto);
    let nro_convenio = proyecto.nro_convenio;
    nro_convenio = nro_convenio.replace('ETP/FDI/DTP/','');

    let str = "" + (parseInt(nro_convenio.split('-')[0]) + 1);
    let pad = "0000";
    let ans = pad.substring(0, pad.length - str.length) + str;

    return 'ETP/FDI/DTP/'+ans+'-'+nro_convenio.split('-')[1];
  };

  const verificarBoletas = async (proyecto) => {
    if (proyecto.porcentaje_anticipo < 0 || proyecto.porcentaje_anticipo > 20) {
      throw new errors.ValidationError(`El porcentaje del anticipo debe ser mayor o igual a 0 o menor o igual a 20.`);
    }
    const CALCULO_ANTICIPO = util.valorPorcentual(proyecto.monto_total, proyecto.porcentaje_anticipo);
    if (proyecto.anticipo !== CALCULO_ANTICIPO) {
      throw new errors.ValidationError(`El monto del anticipo no corresponde al porcentaje señalado.`);
    }
    if (proyecto.anticipo === 0) {
      return;
    }
    if (!proyecto.boletas || proyecto.boletas.length === 0) {
      throw new errors.ValidationError(`Debe registrar la(s) boleta(s) que sume(n) la totalidad del anticipo.`);
    }
    const SUMA_MONTOS_BOLETAS = _.sumBy(proyecto.boletas, (boleta) => {
      return boleta.monto;
    });
    if (SUMA_MONTOS_BOLETAS < proyecto.anticipo) {
      throw new errors.ValidationError(`La suma de los montos de las boletas debe ser igual al monto del anticipo solicitado.`);
    }
    let FECHA_INI_PRIMERA_BOLETA = _getBoletaMasAntiguaPorFechaInicio(proyecto).fecha_inicio_validez;
    if (proyecto.tipo === 'TP_CIF' && FECHA_INI_PRIMERA_BOLETA <= proyecto.fecha_desembolso) {
      throw new errors.ValidationError(`La(s) fecha(s) de inicio de la(s) boleta(s) debe(n) ser mayor a la fecha del desembolso.`);
    } else if (proyecto.tipo === 'TP_CIFE' && FECHA_INI_PRIMERA_BOLETA <= proyecto.fecha_suscripcion_convenio) {
      throw new errors.ValidationError(`La(s) fecha(s) de inicio de la(s) boleta(s) debe(n) ser mayor a la fecha de suscripción del convenio.`);
    }
  };

  const validarOrdenProceder = async (proyecto) => {
    let mensajeError = 'La fecha del orden de proceder debe ser mayor a la fecha';
    if (proyecto.anticipo === 0 || proyecto.boletas.length === 0) {
      return;
    }
    if (proyecto.orden_proceder <= _getBoletaMasAntiguaPorFechaInicio(proyecto).fecha_inicio_validez) {
      throw new errors.ValidationError(`${mensajeError} de inicio de la boleta de anticipo más antigua.`);
    } else if (proyecto.tipo === 'TP_CIF' && proyecto.orden_proceder <= proyecto.fecha_desembolso) {
      throw new errors.ValidationError(`${mensajeError} del desembolso.`);
    } else if (proyecto.tipo === 'TP_CIFE' && proyecto.orden_proceder <= proyecto.fecha_suscripcion_convenio) {
      throw new errors.ValidationError(`${mensajeError} del convenio.`);
    }
  };

  const esOrdenTrabajo = (tipoModificacion) => {
    return tipoModificacion === 'TM_ORDEN_TRABAJO';
  };

  const esOrdenCambio = (tipoModificacion) => {
    return tipoModificacion === 'TM_ORDEN_CAMBIO';
  };

  const esContratoModificatorio = (tipoModificacion) => {
    return tipoModificacion === 'TM_CONTRATO_MODIFICATORIO';
  };

  const _validarOrdenTrabajo = async (proyecto, nuevoMonto) => {
    if (proyecto.monto_total !== nuevoMonto) {
      throw new errors.ValidationError(`El monto modificado debe ser igual al monto total del proyecto.`);
    }
  };

  const _verificarVariaciones = async (proyecto, nuevoMonto, porcentaje, idUsuario) => {
    const ESTADO_VARIACIONES = await app.dao.proyecto_historico.obtenerVariaciones(proyecto.id_proyecto, proyecto.version - 1);
    const VARIACION = {
      5: _.find(ESTADO_VARIACIONES.variaciones, (e) => { return e.tipo_modificacion === 'TM_ORDEN_CAMBIO'; }) || 0,
      10: _.find(ESTADO_VARIACIONES.variaciones, (e) => { return e.tipo_modificacion === 'TM_CONTRATO_MODIFICATORIO'; }) || 0
    };
    let variacionActual = util.restar(nuevoMonto - ESTADO_VARIACIONES.monto_total_anterior);
    let variacionParcial = util.sumar([variacionActual, VARIACION[porcentaje].variacion]);
    if (util.valorAbsoluto(variacionParcial) > ESTADO_VARIACIONES.porcentajes[porcentaje]) {
      throw new errors.ValidationError(`El monto total modificado sobrepasa al ${porcentaje}% del monto contractual`);
    }
    let variacionTotal = util.sumar([variacionActual, VARIACION['5'].variacion, VARIACION['10'].variacion]);
    if (util.valorAbsoluto(variacionTotal) > ESTADO_VARIACIONES.porcentajes['15']) {
      throw new errors.ValidationError(`El monto total modificado sobrepasa el 15% del monto contractual`);
    }
    proyecto.monto_total = nuevoMonto;
    proyecto._usuario_modificacion = idUsuario;
    await proyecto.save();
  };

  const _validarModificacion = async (proyecto, idUsuario) => {
    const MONTO_MODIFICADO = await app.dao.item.obtenerMontoProyecto(proyecto.id_proyecto);
    if (esOrdenTrabajo(proyecto.tipo_modificacion)) {
      await _validarOrdenTrabajo(proyecto, MONTO_MODIFICADO);
    }
    if (esOrdenCambio(proyecto.tipo_modificacion)) {
      await _verificarVariaciones(proyecto, MONTO_MODIFICADO, '5', idUsuario);
    }
    if (esContratoModificatorio(proyecto.tipo_modificacion)) {
      await _verificarVariaciones(proyecto, MONTO_MODIFICADO, '10', idUsuario);
      // TODO verificar que los nuevos items tengan sus documento adjuntos
    }
    if (!proyecto.doc_respaldo_modificacion) {
      throw new errors.ValidationError(`Debe adjuntar el documento de respaldo para la modificación`);
    }
    if (proyecto.fecha_modificacion <= proyecto.orden_proceder) {
      throw new errors.ValidationError(`La fecha de modificación debe ser posterior a la fecha de orden de proceder.`);
    }
  };

  const ejecutarMetodos = async (proyecto, metodos, idUsuario) => {
    for (let i = 0; i < metodos.length; i++) {
      switch (metodos[i]) {
        case '$validarDesembolso':
          await validarFechaDesembolso(proyecto);
          break;
        case '$verificarBoletas':
          await verificarBoletas(proyecto);
          break;
        case '$validarOrdenProceder':
          await validarOrdenProceder(proyecto);
          break;
        case '$validarModificacion':
          await _validarModificacion(proyecto, idUsuario);
          break;
        case '$cerrarItems':
          await app.dao.item.cerrarItems(proyecto, idUsuario);
          break;
        case '$cancelarModificacion':
          await app.dao.proyecto_historico.cancelarModificacion(proyecto, idUsuario);
          break;
        default:
          logger.warn(`No existe el método a ejecutar para el proyecto.`);
      }
    }
    return true;
  };

  /**
   * cerrarProyecto - Para cerrar un proyecto se verifica que los items tengan concluído su avance físico,
   * si es que no han sido validados previamente (atributo itemsVerificados)
   */
  const cerrarProyecto = async (idProyecto, idRol, idUsuario, itemsVerificados) => {
    const ESTADO_FIN_PROYECTO = util.json(await app.dao.estado.getEstadoFinal('PROYECTO-FDI'));
    await app.dao.estado.verificarPermisos(null, ESTADO_FIN_PROYECTO, idRol, idUsuario, null);
    if (!itemsVerificados) {
      if (!await app.dao.item.estanCompletados(idProyecto)) {
        throw new errors.ValidationError(`Existen items que no están completos.`);
      }
    }
    await actualizar(idProyecto, {estado_proyecto: ESTADO_FIN_PROYECTO.codigo}, idUsuario);
    await app.dao.transicion.registrarTransicion(idProyecto, 'proyecto', ESTADO_FIN_PROYECTO.codigo, null, idUsuario, idRol);
  };

  const getProcesado = (consulta, idUsuario) => {
    const query = {
      attributes: {
        exclude: ['documentacion_exp']
      },
      include: [
        {
          model: models.equipo_tecnico,
          as: 'equipo_tecnico',
          required: false,
          where: {
            fid_usuario: idUsuario
          }
        }, {
          model: models.contrato,
          as: 'contrato',
          include: [
            {
              model: models.empresa,
              as: 'empresa'
            }
          ],
          required: false
        }, {
          attributes: ['tipo_documento', 'tipo_boleta', 'numero', 'monto', 'fecha_inicio_validez', 'fecha_fin_validez', 'estado', 'id_boleta'],
          model: models.boleta,
          as: 'boletas',
          require: false
        }, {
          attributes: ['codigo', 'nombre', 'areas'],
          model: models.estado,
          as: 'estado_actual'
        }, {
          attributes: ['nombre'],
          model: models.parametrica,
          as: 'tipo_proyecto'
        }, {
          attributes: ['codigo', 'nombre', 'codigo_provincia', 'point'],
          model: models.municipio,
          as: 'municipio',
          include: {
            attributes: ['codigo', 'nombre', 'codigo_departamento'],
            model: models.provincia,
            as: 'provincia',
            include: {
              attributes: ['codigo', 'nombre'],
              model: models.departamento,
              as: 'departamento'
            }
          }
        }, {
          attributes: ['tipo_documento', 'numero_documento', 'fecha_nacimiento', 'nombres', 'primer_apellido', 'segundo_apellido', 'correo', 'telefono'],
          model: models.persona,
          as: 'autoridad_beneficiaria'
        }, {
          model: sequelize.models.formulario,
          attributes: ['form_model','codigo_estado','fid_plantilla','nombre'],
          as: 'formularios'
        },{
          model: sequelize.models.transicion,
          as: 'observacion',
          attributes: [['observacion', 'mensaje'], 'fid_usuario'],
          where: {
            fid_usuario_asignado: null,
            tipo_transicion: 'proyecto',
            codigo_estado: {
              $eq: sequelize.col('proyecto.estado_proyecto')
            }
          },
          required: false,
          // required: esLegal,
          include: [
            {
              attributes: ['id_usuario', 'fid_persona'],
              model: sequelize.models.usuario,
              as: 'usuario',
              include: [
                {
                  attributes: ['nombres', 'primer_apellido', 'segundo_apellido'],
                  model: sequelize.models.persona,
                  as: 'persona'
                }
              ]
            }
          ]
        }
      ],
      where: consulta.condicionProyecto,
      offset: consulta.offset,
      limit: consulta.limit,
      order: consulta.order,
      distinct: true
    };
    if (typeof(query.order) === 'string') {
      query.order = sequelize.literal(query.order);
    }
    return proyectoModel.findAndCountAll(query);
  };

  const obtenerDatosModificacion = async (idProyecto) => {
    let query = {
      where: {
        id_proyecto: idProyecto
      },
      order: [['version', 'ASC']],
      attributes: ['version', 'tipo_modificacion', 'estado_proyecto', 'fecha_modificacion'],
      include: [
        {
          model: models.estado,
          as: 'estado_actual',
          attributes: ['nombre']
        }, {
          model: models.parametrica,
          as: 'tipo_modificacion_proyecto',
          attributes: ['nombre']
        }
      ]
    };
    return models.proyecto.findAll(query);
  };

  const paralizarProyecto = async (USUARIO, ID_PROYECTO, t) => {
    let proyecto = await models.proyecto.findOne({
      where: {
        id_proyecto: ID_PROYECTO
      },
      transaction: t
    });
    if (proyecto.estado_proyecto == 'CERRADO_FDI' || proyecto.estado_proyecto == 'PARALIZADO_FDI') {
      throw new errors.ValidationError('No se puede modificar un proyecto cerrado o paralizado.');
    }
    if (await app.dao.supervision.existeSupervisionEnCurso(proyecto)) {
      throw new errors.ValidationError('No se puede paralizar mientras exista una supervisión en curso.');
    }
    if (await app.dao.supervision.existeSupervisionComputos(proyecto, t)) {
      throw new errors.ValidationError('No se puede paralizar mientras la supervisión iniciada tenga computos métricos registrados.');
    }
    {
      // Datos del proyecto para la nueva empresa
      let nProyecto = util.json(proyecto);
      nProyecto.id_proyecto = null;
      nProyecto.version = 1;
      nProyecto.codigo = shortid.generate();
      //nProyecto.monto_total = null;  // Mantener monto total por si hubo algun contrato modificatorio
      nProyecto.plazo_ejecucion = null;
      nProyecto.desembolso = null;
      nProyecto.porcentaje_desembolso = null;
      nProyecto.fecha_desembolso = null;
      nProyecto.anticipo = null;
      nProyecto.porcentaje_anticipo = null;
      nProyecto.orden_proceder = null;
      nProyecto.codigo_acceso = shortid.generate();
      nProyecto.matricula = null;
      nProyecto.documentacion_exp = null;
      nProyecto.cod_documentacion_exp = null;
      nProyecto.estado_proyecto = 'ADJUDICACION';
      nProyecto.observacion = null;
      nProyecto.nro_invitacion = null;
      nProyecto.tipo_modificacion = null;
      nProyecto.plazo_ampliacion = null;
      nProyecto.doc_respaldo_modificacion = null;
      nProyecto.fecha_modificacion = null;
      nProyecto._usuario_modificacion = USUARIO.id_usuario;
      nProyecto = await models.proyecto.create(nProyecto, {transaction:t});

      // Modulos no cerrrados
      let modulos = await models.modulo.findAll({
        where: {
          fid_proyecto: ID_PROYECTO,
        },
        include: [{
          model: models.item,
          as: 'items',
          include: [{
            model: models.computo_metrico,
            as: 'computo_metrico'
          }]
        }],
        order: [
          ['id_modulo', 'ASC'],
          [{model:models.item,as:'items'}, 'nro_item', 'ASC'],
          [{model:models.item,as:'items'}, {model:models.computo_metrico,as:'computo_metrico'}, 'fid_supervision', 'DESC']
        ],
        transaction: t
      });

      let montoTotalAvance = 0;
      modulos = util.json(modulos);
      await modulos.map(async (modulo, i)=>{
        modulo.id_modulo = null;
        modulo.version = 1;
        // Renombrar si hubo Contrato Modificatorio
        let ni = modulo.nombre.indexOf('(Contrato Modificatorio');
        if (ni>=0) {
          modulo.nombre = modulo.nombre.substring(0,ni);
        }
        modulo.fid_proyecto = nProyecto.id_proyecto;
        modulo._usuario_creacion = USUARIO.id_usuario;
        modulo._usuario_modificacion = null;
        modulo._fecha_creacion = new Date();
        modulo._fecha_modificacion = new Date();
        let nModulo = await models.modulo.create(modulo, {transaction:t});
        let items = [];
        await modulo.items.map(async (item, j)=>{
          item.id_item = null;
          item.version = 1;
          item.fid_modulo = nModulo.id_modulo;
          item._usuario_creacion = USUARIO.id_usuario;
          item._usuario_modificacion = null;
          item._fecha_creacion = new Date();
          item._fecha_modificacion = new Date();
          let cantAvance = 0;
          // let idSup = item.computo_metrico[0]? item.computo_metrico[0].fid_supervision: 0; // Ultima supervision
          for (let i in item.computo_metrico) {
            // if(idSup == item.computo_metrico[i].fid_supervision) {
              cantAvance = util.sumar([item.computo_metrico[i].cantidad_parcial,cantAvance]);
            // }
          }
          montoTotalAvance = util.sumar([montoTotalAvance,util.multiplicar([cantAvance,item.precio_unitario])]);
          item.cantidad = util.restar(item.cantidad,cantAvance);
          item.precio_unitario = null;
          delete item.computo_metrico;
          if (item.cantidad!=0) {
            items.push(item);
          }
        });
        if (items.length>0) {
          await models.item.bulkCreate(items, {transaction:t});
        } else {
          await nModulo.destroy({transaction: t});
        }
      });

      // Rescindir Proyecto
      proyecto.estado_proyecto = 'PARALIZADO_FDI';
      proyecto.estado = 'INACTIVO';
      proyecto.monto_total_ejecutado = montoTotalAvance;
      await proyecto.save({transaction: t});
      await app.dao.log.crearLog(ID_PROYECTO,'proyecto',USUARIO,'PARALIZADO_FDI',`Rescisión de Contrato.`);

      // Transiciones necesarias
      let transicion = util.json(await models.transicion.findAll({
        where: {
          objeto_transicion: ID_PROYECTO,
          tipo_transicion: 'proyecto',
          codigo_estado: {
            $in: ['REGISTRO_CONVENIO','ASIGNACION_TECNICO','ASIGNACION_PROYECTO_REGISTRO_FINANCIERO_CIFE','REGISTRO_FINANCIERO_CIFE','ASIGNACION_PROYECTO_REGISTRO_FINANCIERO','REGISTRO_FINANCIERO','ADJUDICACION']
          }
        },
        order: [['id_transicion','ASC']]
      }));
      for (let i in transicion) {
        transicion[i].id_transicion = null;
        transicion[i].objeto_transicion = nProyecto.id_proyecto;
      }
      transicion[transicion.length-1].fid_usuario_asignado = null;
      transicion[transicion.length-1].fid_usuario_asignado_rol = null;
      await models.transicion.bulkCreate(transicion, {transaction:t});
      // Equipo tecnico
      let equipo = util.json(await models.equipo_tecnico.findAll({
        where: {
          fid_proyecto: ID_PROYECTO,
        }
      }));
      for (let i in equipo) {
        equipo[i].id_equipo_tecnico = null;
        equipo[i].fid_proyecto = nProyecto.id_proyecto;
      }
      await models.equipo_tecnico.bulkCreate(equipo, {transaction:t});
      // Monto total ejecutado por la anterior empresa
      await models.proyecto.update({
        monto_total_ejecutado: montoTotalAvance
      },{
        where: {
          id_proyecto: nProyecto.id_proyecto
        },
        transaction: t
      });

      // Tabla rescision
      let rescision = util.json(await models.rescision.findOne({
        attributes: ['id_rescision','proyectos'],
        where: {
          proyectos: {
            $contains: [ID_PROYECTO]
          }
        },
        transaction: t,
      }));
      if (rescision) {
        rescision._usuario_modificacion = USUARIO.id_usuario;
        rescision._fecha_modificacion = new Date();
        rescision.proyectos.push(nProyecto.id_proyecto);
        await models.rescision.update(rescision,{where:{id_rescision:rescision.id_rescision},transaction:t});
      } else {
        let datos = {
          proyectos: [ID_PROYECTO, nProyecto.id_proyecto],
          _usuario_creacion: USUARIO.id_usuario,
          _usuario_modificacion: USUARIO.id_usuario,
          _fecha_creacion: new Date(),
          _fecha_modificacion: new Date(),
        };
        rescision = await models.rescision.create(datos, {transaction:t});
      }

      // Copiar archivos
      let archivos = ['documentos'];
      for (let i in archivos) {
        if(fs.existsSync(`${_path}/uploads/proyectos/${proyecto.codigo}/${archivos[i]}`)) {
          fs.copy(`${_path}/uploads/proyectos/${proyecto.codigo}/${archivos[i]}`, `${_path}/uploads/proyectos/${nProyecto.codigo}/${archivos[i]}`, err => {
            if (err) {
              console.error('Error al copiar archivo...');
              throw new errors.NotFoundError('Error al generar los archivos de respaldo.');
            }
            console.log('OK!');
          })
        } else {
          throw new errors.NotFoundError('No se puede encontrar los archivos necesarios.');
        }
      }

      // Guardar registro log.
      await app.dao.log.crearLog(nProyecto.id_proyecto,'proyecto',USUARIO,'ADJUDICACION',`Continuando proyecto paralizado.`);
    }
    return true;
  }

  const empresasAvance = async (USUARIO, ID_PROYECTO, t) => {
    let rescision;
    let proyectos = [];
    let proyecto = await models.proyecto.findOne({
      where: {
        id_proyecto: ID_PROYECTO
      },
      transaction: t
    });
    if(!proyecto) {
      throw new errors.NotFoundError('El proyecto no existe.');
    }
    if (proyecto.estado_proyecto == 'PARALIZADO_FDI') {
      throw new errors.ValidationError('Rescisión de contrato.');
    }
    // Verificar usuarios tecnico y/o departamanetal
    if ([4,5].indexOf(USUARIO.id_rol)<0) {
      //throw new errors.ValidationError('No tiene permisos para obtener los datos.');
      return {};
    }
    let monto_total_proyecto = 0;
    let monto_total_acumulado = 0;
    let porcentaje_total_fis = 0;
    let porcentaje_total_fin = 0;
    {
      // Obtener avances
      rescision = await models.rescision.findOne({
        where: {
          proyectos: {
            $contains: [ID_PROYECTO],
          }
        },
        transaction: t
      });
      if (rescision) {
        for (let i in rescision.proyectos) {
          let proyecto = await models.proyecto.findOne({
            attributes: ['id_proyecto', 'monto_total', 'desembolso', 'documentacion_exp'],
            where: {
              id_proyecto: rescision.proyectos[i]
            },
            include: [{
              model: models.supervision,
              as: 'supervisiones',
            }],
            order: [
              [{model:models.supervision,as:'supervisiones'}, 'nro_supervision', 'DESC']
            ],
            transaction: t,
          });
          proyecto = util.json(proyecto);
          proyecto.monto_avance = 0;
          proyecto.total_acumulado_fis = 0;
          if (proyecto.supervisiones&&proyecto.supervisiones[0]) {
            for(let c in proyecto.supervisiones) {
              proyecto.modulos = await app.dao.modulo.obtenerDatosProyecto(proyecto.supervisiones[c].fid_proyecto, proyecto.supervisiones[c].id_supervision, true);
              for (let j in proyecto.modulos) {
                let items = util.json(proyecto.modulos[j].items);
                for (let k in items) {
                  proyecto.total_acumulado_fis += items[k].cantidad_actual_fis? items[k].cantidad_actual_fis * items[k].precio_unitario: 0;
                }
              }
              proyecto.monto_avance += proyecto.supervisiones[c].desembolso;
              // if (i<rescision.proyectos.length-1) { // (*) Tomando en cuenta el avance financiero pa calcular el monto total del proyecto
              //   monto_total_proyecto += proyecto.supervisiones[c].desembolso;
              // }
            }
            if (i<rescision.proyectos.length-1) { // (*) Tomando en cuenta el avance fisico pa calcular el monto total del proyecto
              monto_total_proyecto += proyecto.total_acumulado_fis;
            }
            // Si es la ultima empresa, tiene supervisiones y no se ha desembolsado entonces se toma la cantidad contratada
            // if (!proyecto.supervisiones[0].desembolso) {
            if (i>=rescision.proyectos.length-1) {
              monto_total_proyecto += proyecto.monto_total;
            }
          }
          proyecto.empresa = proyecto.documentacion_exp.datos_empresa.razon_social;
          delete proyecto.modulos;
          delete proyecto.documentacion_exp;
          // delete proyecto.supervisiones;
          proyectos.push(proyecto);
        }
        // Calcular porcentajes totales.
        for (let i in proyectos) {
          porcentaje_total_fis += proyectos[i].total_acumulado_fis/monto_total_proyecto*100;
          porcentaje_total_fin += proyectos[i].monto_avance/monto_total_proyecto*100;
        }
      }
    }
    return {monto_total_proyecto ,monto_total_acumulado, porcentaje_total_fis, porcentaje_total_fin, proyectos};
  }

  async function notificarRegistroEquipoTecnico (proyecto, usuario, persona, idUsuario, t) {
    const reporte = new Reporte('correo_registro_equipo_tecnico');
    const PARA = persona.correo;
    const TITULO = `Registro de Equipo Tecnico - ${proyecto.nombre}`;
    let pass = 'pass';

    let _usuario = util.json(await app.dao.usuario.getPorId(usuario.id_usuario));
    // lo quite esta condicion para regenerar contrasena aunque tengo otra contrasena
      pass = shortid.generate();
      await app.dao.usuario.actualizarContrasena(usuario.id_usuario, pass, idUsuario);

    // console.log(pass, 'password nuevo!!')
    const datos = {
      proyecto: proyecto.nombre,
      cargo: 'Responsable de GAM/GAIOC',
      usuario: usuario.usuario,
      contrasena: pass,
      urlLogo: app.src.config.config.sistema.urlLogo,
      urlLogin: app.src.config.config.sistema.urlLogin
    };
    const HTML = reporte.template(datos);
    await util.enviarEmail(PARA, TITULO, HTML);
  }

  async function obtenerMontoFDI (idProyecto) {
    // const montoFdi = await models.proyecto.findOne({
    //   attributes: ['monto_fdi'],
    //   where: {
    //     id_proyecto: idProyecto
    //   }
    // });
    const proyecto = await proyectoModel.findOne({
      attributes: ['id_proyecto','monto_fdi','nro_convenio'],
      where: {
        id_proyecto: idProyecto
      },
      include: [{
        model: models.formulario,
        as: 'formularios',
        attributes: ['nombre', 'form_model'],
        where: {
          nombre: 'financiamiento'
        }
      }]
    });

    let datos = {
      monto_fdi: 0,
      nro_convenio: proyecto.nro_convenio
    };

    if (proyecto) {
      if (proyecto.formularios.length > 0) {
        let formulario = proyecto.formularios[0];
        let financiamiento = util.json(formulario.form_model);
        datos.monto_fdi = proyecto.monto_fdi !== financiamiento.monto_fdi ? financiamiento.monto_fdi : proyecto.monto_fdi;
      } else {
        datos.monto_fdi = proyecto.monto_fdi;
      }
    }

    return datos;
  }

  async function obtenerCodigoMunicipio (idProyecto) {
    const codigoMunicipio = await models.proyecto.findOne({
      attributes: ['fcod_municipio','nro_convenio'],
      where: {
        id_proyecto: idProyecto
      }
    });
    return codigoMunicipio;
  }

  async function tienePlanillaAbierta (idProyecto) {
    // Consultar supervisiones abiertas
    let options = {
      attributes: ['fid_proyecto','id_supervision','estado_supervision'],
      distinct: true,
      where: {
        fid_proyecto: idProyecto,
        estado_supervision: {$notIn: ['EN_ARCHIVO_FDI','EN_FISCAL_FDI']}
      }
    };
    let planillas = util.json(await models.supervision.findAll(options));
    if (planillas && planillas.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  async function tieneAdjudicacionesSinModulos (idProyecto) {
    let tieneAdjudicacionesSinModulos = false;
    let opcionesAdjudicacion = {
      attributes: ['id_adjudicacion', 'fid_proyecto', 'estado'],
      distinct: true,
      where: {
        fid_proyecto: idProyecto,
        estado: 'ACTIVO'
      }
    };
    let adjudicaciones = util.json(await models.adjudicacion.findAll(opcionesAdjudicacion));
    if (adjudicaciones && adjudicaciones.length > 0) {
      let opcionesModulo = {
        attributes: ['id_modulo', 'fid_proyecto', 'fid_adjudicacion', 'estado'],
        distinct: true,
        where: {
          fid_proyecto: idProyecto,
          estado: 'ACTIVO'
        }
      }
      for (let i in adjudicaciones) {
        opcionesModulo.where.fid_adjudicacion = adjudicaciones[i].id_adjudicacion;
        let modulos = util.json(await models.modulo.findAll(opcionesModulo));
        if (!modulos || modulos.length === 0) {
          tieneAdjudicacionesSinModulos = true;
          break;
        }
      }
    }
    return tieneAdjudicacionesSinModulos;
  }

  _app.dao.proyecto = {
    adminObtener,
    adminObtenerUsuario,
    adminModificarUsuario,
    get,
    getPorId,
    getMatricula,
    validar,
    verNroCodigo,
    codigoProyectoReferencia,
    obtenerEstado,
    siguiente,
    guardarArchivo,
    eliminarArchivo,
    leerArchivo,
    obtenerCodigoProyecto,
    obtenerDatosProyecto,
    obtenerDatosProyectoParaPlanilla,
    obtenerDatosParaPlanilla,
    escribirBase64,
    getProyectoId,
    getProyectoQR,
    getPorCodigoAcceso,
    calcularFechasRecepcion,
    listarProyectosProcesados_v1,
    listarProyectosProcesados_v2,
    listarProyectosProcesados_v3,
    listarProyectosProcesados_v2Movil,
    getValoresAutomaticos,
    getConDatos,
    actualizar,
    ejecutarMetodos,
    cerrarProyecto,
    getProcesado,
    filtrosPlanilla,
    esOrdenTrabajo,
    esOrdenCambio,
    esContratoModificatorio,
    obtenerDatosModificacion,
    paralizarProyecto,
    empresasAvance,
    getUsuarioTransicion,
    getParametrica,
    notificarRegistroEquipoTecnico,
    obtenerMontoFDI,
    obtenerCodigoMunicipio,
    tienePlanillaAbierta,
    tieneAdjudicacionesSinModulos,
    getPendientes
  };
};
