import util from '../../../lib/util';
import shortid from 'shortid';
import errors from '../../../lib/errors';
import _ from 'lodash';

module.exports = (app) => {
  app.controller.equipo_tecnico = {};

  async function crear (req, res, next) {
    const ID_INST = req.body.audit_usuario.institucion.id;
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    return app.dao.common.crearTransaccion(async (t) => {
      let {datosEquipoTecnico, datosUsuario, idRol, datosProyecto, EXISTE_USUARIO} = await prepararEquipoTecnico(req, ID_PROYECTO, ID_USUARIO, ID_INST);
      // if(!req.body.doc_designacion) {
      //   throw new errors.ValidationError(`No se ha especificado el archivo de designación.`);
      // }
      // const datosProyecto = await app.dao.proyecto.getProyectoId(ID_PROYECTO);
      // let datos = util.crearObjeto(req.body, [
      //   'numero_documento',
      //   'lugar_expedicion_documento',
      //   'fecha_nacimiento',
      //   'tipo_documento',
      //   'nombres',
      //   'primer_apellido',
      //   'segundo_apellido',
      //   'direccion',
      //   'correo',
      //   'telefono',
      //   'estado'
      // ]);
      // const EXISTE_USUARIO = await app.dao.usuario.existe(datos.numero_documento, datos.fecha_nacimiento);
      // let codigoDocumento = 'DET_' + shortid.generate();
      // let {datosUsuario, idRol} = await registrarUsuarioEquipoTecnico(datos, req.body.cargo, ID_USUARIO, ID_INST);
      // let datosEquipoTecnico = {
      //   tipo_equipo: req.body.tipo_equipo,
      //   cargo: req.body.cargo,
      //   doc_designacion: await app.dao.proyecto.guardarArchivo(req.body.doc_designacion, ID_PROYECTO, codigoDocumento),
      //   fid_proyecto: ID_PROYECTO,
      //   fid_usuario: datosUsuario.id_usuario,
      //   fid_rol: idRol,
      //   _usuario_creacion: ID_USUARIO
      // };
      if(req.body.fid_adjudicacion){
        datosEquipoTecnico.fid_adjudicacion = req.body.fid_adjudicacion;
      }
      datosEquipoTecnico = await app.dao.equipo_tecnico.crear(datosEquipoTecnico);
      datosEquipoTecnico = await app.dao.equipo_tecnico.obtener(undefined, datosEquipoTecnico.id_equipo_tecnico);
      console.log(EXISTE_USUARIO,"EXISTE_USUARIO")
      if (EXISTE_USUARIO) {
        // await app.dao.equipo_tecnico.enviarCorreoUsuarioExisente(datosProyecto, datosEquipoTecnico, ID_USUARIO, t);
        console.log("Existe Usuario")
      } else {
        await app.dao.equipo_tecnico.enviarCorreo(datosProyecto, datosEquipoTecnico, ID_USUARIO, t);
      }
      return { datosEquipoTecnico };
    }).then(resultado => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se registró los datos exitosamente.',
        datos: resultado.datosEquipoTecnico
      });
    }).catch(error => {
      return next(error);
    });
  }

  async function registrarUsuarioEquipoTecnico (datosPersona, cargo, idUsuario, idInstitucion) {
    await app.dao.persona.validarDatosNacionalExtranjero(datosPersona);
    let persona = util.json(await app.dao.persona.crearObtener(datosPersona, idUsuario));
    persona.noVerificar = datosPersona.noVerificar;
    let ID_ROL;
    switch (idInstitucion) {
      case 1:
        ID_ROL = cargo === 'CG_SUPERVISOR' ? 9 : cargo === 'CG_FISCAL' ? 10 : null;
        break;
      case 2:
      default:
        ID_ROL = cargo === 'CG_SUPERVISOR' ? 16 : cargo === 'CG_FISCAL' ? 17 : cargo === 'CG_RESP_GAM' ? 18 : null;
        break;
    }
    // const DATOS_USUARIO = await app.dao.usuario.crearRecuperar(persona, ID_ROL, 'TODOS', idUsuario);
    const DATOS_USUARIO = await app.dao.usuario.crearRecuperar(persona, {idRol:ID_ROL, cod:'TODOS', idUsuario, idInstitucion});
    return {datosUsuario: DATOS_USUARIO, idRol: ID_ROL};
  }

  async function listar (req, res, next) {
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    const ID_ROL = req.query.rol;
    try {
      await app.dao.equipo_tecnico.verificarAccesoUsuario(req.body.audit_usuario, ID_PROYECTO,"", ID_ROL);
      if(req.params.id_adjudicacion){
        const ID_ADJUDICACION = req.params.id_adjudicacion;
        const datosEquipoTecnico = await app.dao.equipo_tecnico.listarPorAdjudicacion(req.query, ID_PROYECTO, ID_ADJUDICACION);
        return res.status(200).json({
          finalizado: true,
          mensaje: 'Se obtuvo la lista exitosamente.',
          datos: datosEquipoTecnico
        });
      }else{
        const datosEquipoTecnico = await app.dao.equipo_tecnico.listar(req.query, ID_PROYECTO);
        return res.status(200).json({
          finalizado: true,
          mensaje: 'Se obtuvo la lista exitosamente.',
          datos: datosEquipoTecnico
        });
      }

    } catch (error) {
      return next(error);
    }
  }

  async function obtener (req, res, next) {
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    const ID_EQUIPO_TECNICO = req.params.id;
    try {//adicionando parametro rol para mostrar datos del equipo tecnico en el rol visor
      await app.dao.equipo_tecnico.verificarAccesoUsuario(req.body.audit_usuario, ID_PROYECTO, ID_EQUIPO_TECNICO,req.query.rol);
      const datosEquipoTecnico = await app.dao.equipo_tecnico.obtener(req.query, ID_EQUIPO_TECNICO);
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se obtuvo los datos exitosamente.',
        datos: datosEquipoTecnico
      });
    } catch (error) {
      return next(error);
    }
  }

  async function obtenerPorSupervisor (req, res, next) {
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    const ID_EQUIPO_TECNICO = req.params.id;
    try {
      await app.dao.equipo_tecnico.verificarAccesoUsuario(req.body.audit_usuario, ID_PROYECTO, ID_EQUIPO_TECNICO);
      const datosEquipoTecnico = await app.dao.equipo_tecnico.obtenerPorSupervisor(ID_PROYECTO, ID_USUARIO);
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se obtuvo los datos exitosamente.',
        datos: datosEquipoTecnico
      });
    } catch (error) {
      return next(error);
    }
  }

  async function modificar (req, res, next) {
    const ID_INST = req.body.audit_usuario.institucion.id;
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    const ID_EQUIPO_TECNICO = req.params.id;
    return app.dao.common.crearTransaccion(async (t) => {
      await app.dao.equipo_tecnico.verificarAccesoUsuario(req.body.audit_usuario, ID_PROYECTO, ID_EQUIPO_TECNICO);
      let datosPersona = util.crearObjeto(req.body, [
        'id_persona',
        'tipo_documento',
        'numero_documento',
        'fecha_nacimiento',
        'correo',
        'telefono'
      ]);
      let datosEquipoTecnico = util.crearObjeto(req.body, [
        'doc_designacion',
        'cargo'
      ]);
      datosPersona._usuario_modificacion = ID_USUARIO;
      let datosEqTecnicoModificar = {};
      const MIEMBRO_EQUIPO_TECNICO = await app.dao.equipo_tecnico.getMiembroEquipoTecnico(ID_EQUIPO_TECNICO);
      const ES_CORREO_ACTUALIZADO = MIEMBRO_EQUIPO_TECNICO.usuario.persona.correo !== datosPersona.correo;
      if (MIEMBRO_EQUIPO_TECNICO.usuario.persona.id_persona !== datosPersona.id_persona ||
        MIEMBRO_EQUIPO_TECNICO.usuario.persona.numero_documento !== datosPersona.numero_documento) {
        let {datosUsuario, idRol} = await registrarUsuarioEquipoTecnico(datosPersona, datosEquipoTecnico.cargo, ID_USUARIO, ID_INST);
        datosEqTecnicoModificar.fid_usuario = datosUsuario.id_usuario;
        datosEqTecnicoModificar.fid_rol = idRol;
      } else {
        const tipoDocumento = MIEMBRO_EQUIPO_TECNICO.usuario.persona.tipo_documento;
        const idPersona = MIEMBRO_EQUIPO_TECNICO.usuario.persona.id_persona;
        await app.dao.persona.modificar(datosPersona, tipoDocumento, idPersona, ID_USUARIO);
      }
      if (datosEquipoTecnico.cargo !== MIEMBRO_EQUIPO_TECNICO.cargo) {
        datosEqTecnicoModificar.cargo = datosEquipoTecnico.cargo;
      }
      if (!_.isEmpty(datosEqTecnicoModificar)) {
        await app.dao.equipo_tecnico.modificar(datosEqTecnicoModificar, ID_EQUIPO_TECNICO, ID_USUARIO);
      }
      if (datosEquipoTecnico.doc_designacion && typeof datosEquipoTecnico.doc_designacion === 'object') {
        await actualizarArchivoDesignacion(MIEMBRO_EQUIPO_TECNICO, datosEquipoTecnico);
      }
      const EQUIPO_TECNICO = await app.dao.equipo_tecnico.obtener({}, ID_EQUIPO_TECNICO);
      if (ES_CORREO_ACTUALIZADO) {
        const datosProyecto = await app.dao.proyecto.obtenerDatosProyecto(ID_PROYECTO);
        const EXISTE_USUARIO = await app.dao.usuario.existe(datosPersona.numero_documento, datosPersona.fecha_nacimiento);
        if (EXISTE_USUARIO) {
          await app.dao.equipo_tecnico.enviarCorreoUsuarioExisente(datosProyecto, EQUIPO_TECNICO, ID_USUARIO, t);
        } else {
          await app.dao.equipo_tecnico.enviarCorreo(datosProyecto, EQUIPO_TECNICO, ID_USUARIO, t);
        }
      }
      return { datosEquipoTecnico: EQUIPO_TECNICO };
    }).then(resultado => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se actualizaron los datos exitosamente.',
        datos: resultado.datosEquipoTecnico
      });
    }).catch(error => {
      return next(error);
    });
  }

  async function actualizarArchivoDesignacion (miembroEquipoTecnico, datosEquipoTecnico) {
    let codigoDocumento = miembroEquipoTecnico.doc_designacion.substring(1, miembroEquipoTecnico.doc_designacion.indexOf(']'));
    await app.dao.proyecto.eliminarArchivo(miembroEquipoTecnico.fid_proyecto, codigoDocumento);
    await app.dao.proyecto.guardarArchivo(datosEquipoTecnico.doc_designacion, miembroEquipoTecnico.fid_proyecto, codigoDocumento);
  }

  async function eliminar (req, res, next) {
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    const ID_EQUIPO_TECNICO = req.params.id;
    return app.dao.common.crearTransaccion(async (t) => {
      await app.dao.equipo_tecnico.verificarAccesoUsuario(req.body.audit_usuario, ID_PROYECTO, ID_EQUIPO_TECNICO);
      await app.dao.equipo_tecnico.eliminar(ID_EQUIPO_TECNICO, ID_USUARIO);
    }).then(() => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se eliminó los datos exitosamente.',
        datos: null
      });
    }).catch(error => {
      return next(error);
    });
  }

  async function notificar (req, res, next) {
    const CORREO = req.body.correo;
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    const ID_EQUIPO_TECNICO = req.params.id;
    return app.dao.common.crearTransaccion(async (t) => {
      await app.dao.equipo_tecnico.verificarAccesoUsuario(req.body.audit_usuario, ID_PROYECTO, ID_EQUIPO_TECNICO);
      const datosProyecto = await app.dao.proyecto.obtenerDatosProyecto(ID_PROYECTO);
      const datosEquipoTecnico = await app.dao.equipo_tecnico.obtener({}, ID_EQUIPO_TECNICO);
      if (CORREO !== datosEquipoTecnico.usuario.persona.correo) {
        await app.dao.persona.actualizarDatos(datosEquipoTecnico.usuario.persona.id_persona, {correo: CORREO}, ID_USUARIO);
        datosEquipoTecnico.usuario.persona.correo = CORREO;
        await app.dao.equipo_tecnico.enviarCorreo(datosProyecto, datosEquipoTecnico, ID_USUARIO, t);
      } else {
        // await app.dao.equipo_tecnico.enviarCorreoUsuarioExisente(datosProyecto, datosEquipoTecnico, ID_USUARIO, t);
        await app.dao.equipo_tecnico.enviarCorreo(datosProyecto, datosEquipoTecnico, ID_USUARIO, t);
      }
    }).then(() => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se reenvió una notificación por correo exitosamente.',
        datos: null
      });
    }).catch(error => {
      return next(error);
    });
  }

  async function reemplazar (req, res, next) {
    const ID_INST = req.body.audit_usuario.institucion.id;
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    const ID_EQUIPO_TECNICO = req.params.id;
    return app.dao.common.crearTransaccion(async (t) => {
      await app.dao.equipo_tecnico.verificarAccesoUsuario(req.body.audit_usuario, ID_PROYECTO, ID_EQUIPO_TECNICO);
      // codigo que se repite separado a una nueva función
      let {datosEquipoTecnico, datosUsuario, idRol, datosProyecto, EXISTE_USUARIO} = await prepararEquipoTecnico(req, ID_PROYECTO, ID_USUARIO, ID_INST);

      // if(!req.body.doc_designacion) {
      //   throw new errors.ValidationError(`No se ha especificado el archivo de designación.`);
      // }

      // const datosProyecto = await app.dao.proyecto.getProyectoId(ID_PROYECTO);

      // let datos = util.crearObjeto(req.body, [
      //   'numero_documento',
      //   'lugar_expedicion_documento',
      //   'fecha_nacimiento',
      //   'tipo_documento',
      //   'nombres',
      //   'primer_apellido',
      //   'segundo_apellido',
      //   'direccion',
      //   'correo',
      //   'telefono',
      //   'estado'
      // ]);
      // const EXISTE_USUARIO = await app.dao.usuario.existe(datos.numero_documento, datos.fecha_nacimiento);
      // let codigoDocumento = 'DET_' + shortid.generate();
      // let {datosUsuario, idRol} = await registrarUsuarioEquipoTecnico(datos, req.body.cargo, ID_USUARIO, ID_INST);
      // let datosEquipoTecnico = {
      //   tipo_equipo: req.body.tipo_equipo,
      //   cargo: req.body.cargo,
      //   doc_designacion: await app.dao.proyecto.guardarArchivo(req.body.doc_designacion, ID_PROYECTO, codigoDocumento),
      //   fid_proyecto: ID_PROYECTO,
      //   fid_usuario: datosUsuario.id_usuario,
      //   fid_rol: idRol,
      //   _usuario_creacion: ID_USUARIO
      // };

      const USUARIO_EN_EQUIPO_TECNICO = await app.dao.equipo_tecnico.existe(ID_PROYECTO, datosUsuario.id_usuario, req.body.cargo);

      if (USUARIO_EN_EQUIPO_TECNICO) {
        datosEquipoTecnico = await app.dao.equipo_tecnico.obtener(undefined, USUARIO_EN_EQUIPO_TECNICO /*datosEquipoTecnico.id_equipo_tecnico*/);
      } else {
        datosEquipoTecnico = await app.dao.equipo_tecnico.crear(datosEquipoTecnico);
      }

      if (EXISTE_USUARIO) {
        await app.dao.equipo_tecnico.enviarCorreoUsuarioExisente(datosProyecto, datosEquipoTecnico, ID_USUARIO, t);
      } else {
        await app.dao.equipo_tecnico.enviarCorreo(datosProyecto, datosEquipoTecnico, ID_USUARIO, t);
      }

      await app.dao.equipo_tecnico.reemplazar(ID_EQUIPO_TECNICO, datosEquipoTecnico, ID_USUARIO, t);
      // Guardar registro log
      let proyecto = await app.dao.proyecto.obtenerEstado(ID_PROYECTO);
      let rol = await app.dao.rol.getId(idRol);
      datosEquipoTecnico = await app.dao.equipo_tecnico.obtener(undefined, ID_EQUIPO_TECNICO);
      let us1 = await app.dao.usuario.getUsuarioPersona(datosEquipoTecnico.fid_usuario);
      await app.dao.log.crearLog(ID_PROYECTO, 'proyecto', req.body.audit_usuario, proyecto.estado_actual.codigo,
        `Reasignación de ${rol.descripcion}. ${us1.persona.nombres||''} ${us1.persona.primer_apellido||''} -> ${req.body.nombres||''} ${req.body.primer_apellido||''}`);

      return { datosEquipoTecnico };
    }).then(() => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se reemplazó los datos exitosamente.',
        datos: null
      });
    }).catch(error => {
      return next(error);
    });
  }

  async function prepararEquipoTecnico (req, ID_PROYECTO, ID_USUARIO, ID_INST) {
    if(!req.body.doc_designacion) {
      throw new errors.ValidationError(`No se ha especificado el archivo de designación.`);
    }
    const datosProyecto = await app.dao.proyecto.getProyectoId(ID_PROYECTO);
    let datos = util.crearObjeto(req.body, [
      'numero_documento',
      'complemento',
      'lugar_expedicion_documento',
      'fecha_nacimiento',
      'tipo_documento',
      'nombres',
      'primer_apellido',
      'segundo_apellido',
      'direccion',
      'correo',
      'telefono',
      'noVerificar',
      'estado'
    ]);
    const EXISTE_USUARIO = await app.dao.usuario.existe(datos.numero_documento, datos.fecha_nacimiento);
    let codigoDocumento = 'DET_' + shortid.generate();
    let {datosUsuario, idRol} = await registrarUsuarioEquipoTecnico(datos, req.body.cargo, ID_USUARIO, ID_INST);
    let datosEquipoTecnico = {
      tipo_equipo: req.body.tipo_equipo,
      cargo: req.body.cargo,
      doc_designacion: await app.dao.proyecto.guardarArchivo(req.body.doc_designacion, ID_PROYECTO, codigoDocumento),
      fid_proyecto: ID_PROYECTO,
      fid_usuario: datosUsuario.id_usuario,
      fid_rol: idRol,
      _usuario_creacion: ID_USUARIO
    };
    return {
      datosEquipoTecnico: datosEquipoTecnico,
      datosUsuario: datosUsuario,
      idRol: idRol,
      datosProyecto: datosProyecto,
      EXISTE_USUARIO: EXISTE_USUARIO,
    };
  }

  app.controller.equipo_tecnico.crear = crear;
  app.controller.equipo_tecnico.listar = listar;
  app.controller.equipo_tecnico.obtener = obtener;
  app.controller.equipo_tecnico.obtenerPorSupervisor = obtenerPorSupervisor;
  app.controller.equipo_tecnico.modificar = modificar;
  app.controller.equipo_tecnico.eliminar = eliminar;
  app.controller.equipo_tecnico.notificar = notificar;
  app.controller.equipo_tecnico.reemplazar = reemplazar;
};
