import util from '../../../lib/util';
import shortid from 'shortid';
import errors from '../../../lib/errors';
import moment from 'moment';
import _ from 'lodash';

module.exports = (app) => {
  app.controller.adjudicacion = {};

  async function crear (req, res, next) {
    const ID_INST = req.body.audit_usuario.institucion.id;
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    return app.dao.common.crearTransaccion(async (t) => {
      if(!req.body.doc_adjudicacion || typeof req.body.doc_adjudicacion !== 'object') {
        throw new errors.ValidationError(`No se ha especificado el archivo de adjudicación.`);
      }
      const datosProyecto = await app.dao.proyecto.getProyectoId(ID_PROYECTO);

      let codigoDocumento = 'ADJ_' + shortid.generate();
      let datosAdjudicacion = {
        version: 1,
        monto: req.body.monto,
        referencia: req.body.referencia,
        doc_adjudicacion: await app.dao.proyecto.guardarArchivo(req.body.doc_adjudicacion, ID_PROYECTO, codigoDocumento),
        fid_proyecto: ID_PROYECTO,
        _usuario_creacion: ID_USUARIO
      };
      datosAdjudicacion = await app.dao.adjudicacion.crear(datosAdjudicacion);
      datosAdjudicacion = await app.dao.adjudicacion.obtener(undefined, datosAdjudicacion.id_adjudicacion);
      await app.dao.proyecto.actualizar(datosAdjudicacion.fid_proyecto, { 'req_extension_convenio': true }, ID_USUARIO);

      return { datosAdjudicacion };
    }).then(resultado => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se registró los datos exitosamente.',
        datos: resultado.datosAdjudicacion
      });
    }).catch(error => {
      return next(error);
    });
  }

  async function actualizar (req, res, next) {
    const ID_INST = req.body.audit_usuario.institucion.id;
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    const ID_ADJUDICACION = req.params.id_adjudicacion;

    return app.dao.common.crearTransaccion(async (t) => {
      if(!req.body.doc_adjudicacion) {
        throw new errors.ValidationError(`No se ha especificado el archivo de adjudicación.`);
      }
      const datosProyecto = await app.dao.proyecto.getProyectoId(ID_PROYECTO);

      //calcular monto de los items
      let adjudicacion = await app.dao.adjudicacion.obtenerPorId(ID_ADJUDICACION);
      let monto_items = await app.dao.item.obtenerMontoAdjudicacion(ID_PROYECTO,ID_ADJUDICACION);

      if(monto_items <= adjudicacion.monto){
        if(monto_items > req.body.monto){
          throw new errors.ValidationError(`El monto mínimo que puede ingresar es: ${util.convertirNumeroAString(monto_items)} .`);
        }
      }

      let monto_maximo = datosProyecto.monto_total;
      let _adjudicaciones_existentes = await app.dao.adjudicacion.listar(undefined,ID_PROYECTO);
      for(let i = 0;i<_adjudicaciones_existentes.rows.length;i++){
        if(_adjudicaciones_existentes.rows[i].id_adjudicacion!= ID_ADJUDICACION){
          monto_maximo -= _adjudicaciones_existentes.rows[i].monto;
        }
      }
      if(monto_maximo < req.body.monto){
        throw new errors.ValidationError(`El monto máximo que puede ingresar es: ${util.convertirNumeroAString(monto_maximo)} .`);
      }

      //let codigoDocumento = 'ADJ_' + shortid.generate();
      let datosAdjudicacion = {
        monto: req.body.monto,
        referencia: req.body.referencia,
        //doc_adjudicacion: await app.dao.proyecto.guardarArchivo(req.body.doc_adjudicacion, ID_PROYECTO, codigoDocumento),
        _usuario_creacion: ID_USUARIO
      };
      if(req.body.doc_adjudicacion && typeof req.body.doc_adjudicacion === "object"){
        let datosAdjudicacionRegistro = await app.dao.adjudicacion.obtener(undefined,ID_ADJUDICACION);
        let codigoDocumento = datosAdjudicacionRegistro.doc_adjudicacion.substring(1, datosAdjudicacionRegistro.doc_adjudicacion.indexOf(']'));
        await app.dao.proyecto.eliminarArchivo(datosAdjudicacionRegistro.fid_proyecto, codigoDocumento);
        datosAdjudicacion.doc_adjudicacion = await app.dao.proyecto.guardarArchivo(req.body.doc_adjudicacion, datosAdjudicacionRegistro.fid_proyecto, codigoDocumento);
      }

      datosAdjudicacion = await app.dao.adjudicacion.actualizar(datosAdjudicacion,ID_ADJUDICACION);
      datosAdjudicacion = await app.dao.adjudicacion.obtener(undefined,ID_ADJUDICACION);
      return { datosAdjudicacion };
    }).then(resultado => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se registró los datos exitosamente.',
        datos: resultado.datosAdjudicacion
      });
    }).catch(error => {
      return next(error);
    });
  }

  async function procesar (req, res, next) {
    const ID_INST = req.body.audit_usuario.institucion.id;
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    const ID_ADJUDICACION = req.params.id_adjudicacion;
    const idRol = req.body.audit_usuario.id_rol;

    let adjudicacion = req.body;

    return app.dao.common.crearTransaccion(async (t) => {
      let datosAdjudicacion = {};
      adjudicacion.id_adjudicacion = ID_ADJUDICACION;
      //let datosAdjudicacionRegistro = await app.dao.adjudicacion.obtenerPorId(ID_ADJUDICACION);
      //datosAdjudicacionRegistro = util.json(datosAdjudicacionRegistro);
      if(adjudicacion.fecha_modificacion){
        adjudicacion.fecha_modificacion = moment(adjudicacion.fecha_modificacion,'YYYY-MM-DD');
      }
      if(adjudicacion.estado_proyecto){
        adjudicacion.estado_adjudicacion = adjudicacion.estado_proyecto;
        delete adjudicacion.estado_proyecto;
      }
      /*if(req.body.fecha_modificacion){
        datosAdjudicacionRegistro.fecha_modificacion = req.body.fecha_modificacion;
      }*/
      console.log("**********************AAAAAAA*************************");
      console.log(adjudicacion);

      const ADJUDICACION_VALIDA = await app.dao.adjudicacion.validar(adjudicacion, idRol, ID_USUARIO, t, ID_ADJUDICACION);
      console.log(ADJUDICACION_VALIDA.fecha_modificacion);

      let estadoActual = ADJUDICACION_VALIDA.estado_adjudicacion;
      const CAMBIO_ESTADO = ADJUDICACION_VALIDA.cambio;

      console.log("**********************BBBBBBB*************************");
      console.log(ADJUDICACION_VALIDA);
      //throw new errors.ValidationError(`NO SE PUDO ACTUALIZAR LA ADJUDICACION_VALIDA`);

      //datosAdjudicacion = await app.dao.adjudicacion.actualizar(ADJUDICACION_VALIDA,ID_ADJUDICACION);
      await ADJUDICACION_VALIDA.save();
      if (CAMBIO_ESTADO && CAMBIO_ESTADO.origen === CAMBIO_ESTADO.destino ) {
        ADJUDICACION_VALIDA.estado_adjudicacion = CAMBIO_ESTADO.destino;
        ADJUDICACION_VALIDA.save();
      }

      // Cargado de extensión de convenio cuando se amplia el plazo en una modificación
      if (adjudicacion && adjudicacion.plazo_ampliacion && ((parseInt(adjudicacion.plazo_ampliacion.por_compensacion) + parseInt(adjudicacion.plazo_ampliacion.por_volumenes)) > 0) && ADJUDICACION_VALIDA.fid_proyecto > 0) {
        await app.dao.proyecto.actualizar(ADJUDICACION_VALIDA.fid_proyecto, { 'req_extension_convenio': true }, ID_USUARIO);
      }

      let datosAdjudicacionRegistro = await app.dao.adjudicacion.obtenerPorId(ID_ADJUDICACION);
      await app.dao.estado.validarSiguiente(datosAdjudicacionRegistro, CAMBIO_ESTADO, idRol);
      await app.dao.estado.ejecutarMetodos(datosAdjudicacionRegistro, CAMBIO_ESTADO, idRol, app.dao.adjudicacion, ID_USUARIO);
      await app.dao.transicion.registrarTransicion(ID_ADJUDICACION, 'adjudicacion', datosAdjudicacionRegistro.estado_adjudicacion, ADJUDICACION_VALIDA.observacion, ID_USUARIO, idRol, '');

      datosAdjudicacion = await app.dao.adjudicacion.obtenerPorId(ID_ADJUDICACION);
      return { datosAdjudicacion };
    }).then(resultado => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se registró los datos exitosamente.',
        datos: resultado.datosAdjudicacion
      });
    }).catch(error => {
      return next(error);
    });
  }

  async function guardar (req, res, next) {
    const ID_INST = req.body.audit_usuario.institucion.id;
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    const ID_ADJUDICACION = req.params.id_adjudicacion;
    const idRol = req.body.audit_usuario.id_rol;

    return app.dao.common.crearTransaccion(async (t) => {
      let datosAdjudicacion = {};
      let datosAdjudicacionRegistro = await app.dao.adjudicacion.obtenerPorId(ID_ADJUDICACION);
      //throw new errors.ValidationError(`No procesar cambios en la adjudicacion`);
      let codigoDocumento = 'RMP_v' + datosAdjudicacionRegistro.version+"-"+datosAdjudicacionRegistro.id_adjudicacion;
      if(req.body.doc_respaldo_modificacion && typeof req.body.doc_respaldo_modificacion === "object"){
        datosAdjudicacion.doc_respaldo_modificacion = await app.dao.proyecto.guardarArchivo(req.body.doc_respaldo_modificacion, datosAdjudicacionRegistro.fid_proyecto, codigoDocumento);
      }
      if(req.body.documento_tecnico && typeof req.body.documento_tecnico === "object") {
        codigoDocumento = req.body.documento_tecnico.datos.codigo;
        if (!codigoDocumento || codigoDocumento.indexOf(datosAdjudicacionRegistro.version+"-"+datosAdjudicacionRegistro.id_adjudicacion)<0)
          codigoDocumento = 'RMT_v' + datosAdjudicacionRegistro.version+"-"+datosAdjudicacionRegistro.id_adjudicacion;
        if (!datosAdjudicacionRegistro.documentos) datosAdjudicacionRegistro.documentos = [];
        req.body.documento_tecnico.datos.archivo = await app.dao.proyecto.guardarArchivo(req.body.documento_tecnico, datosAdjudicacionRegistro.fid_proyecto, codigoDocumento, req.body.documento_tecnico.datos.name);
        let documento = datosAdjudicacionRegistro.documentos.find((d)=>{return d.name==req.body.documento_tecnico.datos.name});
        if (!documento) {
          req.body.documento_tecnico.datos.codigo = codigoDocumento;
          datosAdjudicacionRegistro.documentos.push(req.body.documento_tecnico.datos);
        }
        datosAdjudicacion.documentos = datosAdjudicacionRegistro.documentos;
      }
      if(req.body.estado_proyecto){
        datosAdjudicacion.estado_adjudicacion = req.body.estado_proyecto;
      }

      datosAdjudicacion = await app.dao.adjudicacion.actualizar(datosAdjudicacion,ID_ADJUDICACION);
      datosAdjudicacion = await app.dao.adjudicacion.obtenerPorId(ID_ADJUDICACION);
      return { datosAdjudicacion };
    }).then(resultado => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se guardó los datos exitosamente.',
        datos: resultado.datosAdjudicacion
      });
    }).catch(error => {
      return next(error);
    });
  }

  async function eliminarRespaldo (req, res, next) {
    const ID_ADJUDICACION = req.params.id_adjudicacion;

    return app.dao.common.crearTransaccion(async (t) => {
      let datosAdjudicacion = {};
      let datosAdjudicacionRegistro = await app.dao.adjudicacion.obtenerPorId(ID_ADJUDICACION);
      let eliminado = false;
      let i = 0;
      if(datosAdjudicacionRegistro.documentos && req.body.datos && typeof req.body.datos === "object") {
        let documento = datosAdjudicacionRegistro.documentos.find((d)=>{return d.name==req.body.datos.name});
        while (!eliminado && datosAdjudicacionRegistro.documentos[i]) {
          if (datosAdjudicacionRegistro.documentos[i].name == req.body.datos.name) {
            datosAdjudicacionRegistro.documentos.splice(i,1);
            eliminado = true;
            datosAdjudicacion.documentos = datosAdjudicacionRegistro.documentos;
            break;
          }
          i++;
        }
      }
      datosAdjudicacion = await app.dao.adjudicacion.actualizar(datosAdjudicacion,ID_ADJUDICACION);
      // datosAdjudicacion = await app.dao.adjudicacion.obtenerPorId(ID_ADJUDICACION);
      return { datosAdjudicacion };
    }).then(resultado => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se modifico la lista de documentos adjuntos.',
        datos: resultado.datosAdjudicacion
      });
    }).catch(error => {
      return next(error);
    });
  }


  async function obtener (req, res, next) {
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    const ID_ADJUDICACION = req.params.id;
    try {
      //await app.dao.adjudicacion.verificarAccesoUsuario(req.body.audit_usuario, ID_PROYECTO, ID_ADJUDICACION);
      const datosAdjudicacion = await app.dao.adjudicacion.obtener(req.query, ID_ADJUDICACION);
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se obtuvo los datos exitosamente.',
        datos: datosAdjudicacion
      });
    } catch (error) {
      return next(error);
    }
  }

  async function getPorId (req, res ,next) {
    const ID_ADJUDICACION = req.params.id_adjudicacion;
    try {
      const datosAdjudicacion = await app.dao.adjudicacion.obtenerPorId(ID_ADJUDICACION);
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se obtuvo el registro exitosamente',
        datos: datosAdjudicacion
      });
    } catch (error) {
      return next(error);
    }
  }

  async function obtenerPorSupervisor (req, res ,next) {
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    try {
      const equiposTecnicosUsuario = await app.dao.equipo_tecnico.obtenerPorSupervisor(ID_PROYECTO, ID_USUARIO);
      let adjudicacionIds = [];
      for (let i in equiposTecnicosUsuario) {
        const equipo_tecnico = equiposTecnicosUsuario[i];
        adjudicacionIds.push(equipo_tecnico.fid_adjudicacion);
      }
      const adjudicaciones = await app.dao.adjudicacion.obtenerPorArray(adjudicacionIds);
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se obtuvieron las adjudicaciones del supervisor exitosamente',
        datos: adjudicaciones
      });
    } catch (error) {
      return next(error);
    }
  }

  async function listar (req, res, next) {
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    try {
      //await app.dao.adjudicacion.verificarAccesoUsuario(req.body.audit_usuario, ID_PROYECTO);
      const datosAdjudicacion = await app.dao.adjudicacion.listar(req.query, ID_PROYECTO);
      for (var i = datosAdjudicacion.rows.length - 1; i >= 0; i--) {
        let datosAdjudicacionHistorico = await app.dao.adjudicacion_historico.obtenerPorId(datosAdjudicacion.rows[i].dataValues.id_adjudicacion,1);
        if(!datosAdjudicacionHistorico){
          datosAdjudicacion.rows[i].dataValues.montoHistorico = null;
        }else{
          datosAdjudicacion.rows[i].dataValues.montoHistorico = datosAdjudicacionHistorico.monto;
        }
      }
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se obtuvo la lista exitosamente.',
        datos: datosAdjudicacion
      });
    } catch (error) {
      return next(error);
    }
  }

  async function eliminar (req, res, next) {
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    const ID_ADJUDICACION = req.params.id_adjudicacion;

    if (!app.dao.rol.esResponsableGam(req.body.audit_usuario.id_rol)) {
      throw new errors.NotFoundError(`No tiene acceso a la ruta especificada.`);
    }
    try{
      let supervisiones = await app.dao.supervision.lista(ID_PROYECTO,ID_ADJUDICACION);
      if(supervisiones.length >0){
        throw new errors.ValidationError(`No se puede eliminar debido a que tiene Planillas creadas.`);
      }
    }catch(error){
      return next(error);
    };

    return app.dao.common.crearTransaccion(async (t) => {
      //await app.dao.adjudicacion.verificarAccesoUsuario(req.body.audit_usuario, ID_PROYECTO, ID_ADJUDICACION);
      await app.dao.adjudicacion.eliminar(ID_ADJUDICACION, ID_USUARIO);
    }).then(() => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se eliminó el registro exitosamente.',
        datos: null
      });
    }).catch(error => {
      return next(error);
    });
  }

  async function cargarEspTecnicas (req, res, next) {
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_PROYECTO = req.params.id_proyecto;
    const ID_ADJUDICACION = req.params.id_adjudicacion;

    return app.dao.common.crearTransaccion(async (t) => {
      if(!req.body.doc_especificaciones_tecnicas || typeof req.body.doc_especificaciones_tecnicas !== "object") {
        throw new errors.ValidationError(`No se ha especificado el archivo de especificaciones tecnicas.`);
      }
      // const datosProyecto = await app.dao.proyecto.getProyectoId(ID_PROYECTO);
      // ETA (Especificaciones tecnicas de adjudicacion)
      let codigoDocumento = 'ETA_' + shortid.generate();
      let datosAdjudicacionRegistro = await app.dao.adjudicacion.obtener(undefined,ID_ADJUDICACION);
      if (datosAdjudicacionRegistro.doc_especificaciones_tecnicas) {
        codigoDocumento = datosAdjudicacionRegistro.doc_especificaciones_tecnicas.substring(1, datosAdjudicacionRegistro.doc_especificaciones_tecnicas.indexOf(']'));
      }
      await app.dao.proyecto.eliminarArchivo(datosAdjudicacionRegistro.fid_proyecto, codigoDocumento);
      let datosAdjudicacion = {
        doc_especificaciones_tecnicas: await app.dao.proyecto.guardarArchivo(req.body.doc_especificaciones_tecnicas, datosAdjudicacionRegistro.fid_proyecto, codigoDocumento)
      }

      console.log("ACTUALIZAR => ");
      console.log(datosAdjudicacion);
      datosAdjudicacion = await app.dao.adjudicacion.actualizar(datosAdjudicacion,ID_ADJUDICACION);
      datosAdjudicacion = await app.dao.adjudicacion.obtener(undefined,ID_ADJUDICACION);
      return { datosAdjudicacion };
    }).then(resultado => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se registró los datos exitosamente.',
        datos: resultado.datosAdjudicacion
      });
    }).catch(error => {
      return next(error);
    });
  }

  app.controller.adjudicacion.crear = crear;
  app.controller.adjudicacion.actualizar = actualizar;
  app.controller.adjudicacion.guardar = guardar;
  app.controller.adjudicacion.eliminarRespaldo = eliminarRespaldo;
  app.controller.adjudicacion.procesar = procesar;
  app.controller.adjudicacion.obtener = obtener;
  app.controller.adjudicacion.getPorId = getPorId;
  app.controller.adjudicacion.obtenerPorSupervisor = obtenerPorSupervisor;
  app.controller.adjudicacion.listar = listar;
  app.controller.adjudicacion.eliminar = eliminar;
  app.controller.adjudicacion.cargarEspTecnicas = cargarEspTecnicas;
};
