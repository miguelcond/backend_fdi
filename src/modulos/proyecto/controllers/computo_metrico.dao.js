import errors from './../../../lib/errors';
import util from '../../../lib/util';
import _ from 'lodash';

module.exports = (app) => {
  const _app = app;
  _app.dao.computoMetrico = {};

  const computoMetricoModel = app.src.db.models.computo_metrico;
  const supervisionModel = app.src.db.models.supervision;
  const sequelize = app.src.db.sequelize;
  const models = app.src.db.models;

  async function crearRecuperar (computoMetrico, idSupervision, idUsuario, t, conValidacion = true) {
    let result;
    const supervision = await supervisionModel.findById(idSupervision);
    // Se esta comentando porque cuando reasignar a otro supervisor no pueden editar computos metricos
   /* if (supervision._usuario_creacion !== idUsuario) {
      throw new Error(`Usted no esta autorizado para modificar este cómputo`);
    }*/

    if (computoMetrico.id_computo_metrico) {
      result = await computoMetricoModel.findById(computoMetrico.id_computo_metrico, {
        transaction: t
      });
      if (!result) {
        throw new errors.NotFoundError(`No existe el cómputo métrico a actualizar.`);
      }
      await _app.dao.supervision.verificarEdicionPermitida(result.fid_supervision);
    } else {
      await _app.dao.supervision.verificarEdicionPermitida(idSupervision);
      if (computoMetrico.uuid) {
        result = await computoMetricoModel.findOne({
          where: {
            uuid: computoMetrico.uuid
          }
        }, {
          transaction: t
        });
      }
    }
    if (result) {
      if (result._usuario_creacion !== idUsuario) {
        throw new Error(`Usted no esta autorizado para modificar este cómputo`);
      }
      computoMetrico._usuario_modificacion = idUsuario;
      computoMetrico.fid_item = result.fid_item;
      computoMetrico.fid_supervision = result.fid_supervision;
      await computoMetricoModel.update(conValidacion ? await calcular(computoMetrico, t) : computoMetrico, {
        where: {
          id_computo_metrico: result.id_computo_metrico
        },
        transaction: t
      });
      result = await computoMetricoModel.findById(result.id_computo_metrico);
    } else {
      computoMetrico.fid_supervision = idSupervision;
      computoMetrico._usuario_creacion = idUsuario;
      result = await computoMetricoModel.create(conValidacion ? await calcular(computoMetrico, t) : computoMetrico, {
        transaction: t
      });
    }
    let computo = JSON.parse(JSON.stringify(result));
    await _app.dao.item.refreshCantidadesFis(computo.fid_item, computo.fid_supervision, t);
    return computo;
  };

  async function calcular (computoMetrico, t) {
    const ITEM = await app.src.db.models.item.findOne({
      attributes: ['nombre', 'unidad_medida', 'cantidad'],
      where: {
        id_item: computoMetrico.fid_item
      },
      transaction: t
    });
    const resp = await calcularDatosComputos(computoMetrico, ITEM);
    await validarCantidadEjecutada(ITEM, resp);
    return resp;
  }

  const _calcularArea = (computoMetrico) => {
    // Con redondeos intermedios
    // return util.multiplicar([util.multiplicar([computoMetrico.largo, computoMetrico.alto]), computoMetrico.factor_forma]);
    // Sin redondeos intermedios
    let area = 0;
    if (computoMetrico.alto && !computoMetrico.ancho) {
      area = util.multiplicar([computoMetrico.largo, computoMetrico.alto, computoMetrico.factor_forma]);
    }
    if (computoMetrico.ancho && !computoMetrico.alto) {
      area = util.multiplicar([computoMetrico.largo, computoMetrico.ancho, computoMetrico.factor_forma]);
    }
    return area;
  };

  const _calcularVolumen = (computoMetrico) => {
    // Con redondeos intermedios
    // return util.multiplicar([util.multiplicar([computoMetrico.largo, computoMetrico.ancho, computoMetrico.alto]), computoMetrico.factor_forma]);
    // Sin redondeos intermedios
    return util.multiplicar([computoMetrico.largo, computoMetrico.ancho, computoMetrico.alto, computoMetrico.factor_forma]);
  };

  const _calcularCantidadParcial = (unidad, computoMetrico) => {
    switch (unidad) {
      case 'UN_METRO_CUB':
        // Con redondeos intermedios
          //return util.multiplicar([computoMetrico.cantidad, _calcularVolumen(computoMetrico)]);
        // Sin redondeos intermedios
        return util.multiplicar([computoMetrico.cantidad, computoMetrico.largo, computoMetrico.ancho, computoMetrico.alto, computoMetrico.factor_forma]);
      case 'UN_METRO_CUAD':
        // Con redondeos intermedios
          //return util.multiplicar([computoMetrico.cantidad, _calcularArea(computoMetrico)]);
        // Sin redondeos intermedios
        if (computoMetrico.alto && !computoMetrico.ancho) {
          return util.multiplicar([computoMetrico.cantidad, computoMetrico.largo, computoMetrico.alto, computoMetrico.factor_forma]);
        } else if (computoMetrico.ancho && !computoMetrico.alto) {
          return util.multiplicar([computoMetrico.cantidad, computoMetrico.largo, computoMetrico.ancho, computoMetrico.factor_forma]);
        } else {
          return util.multiplicar([computoMetrico.cantidad, computoMetrico.largo, 0, computoMetrico.factor_forma]);
        }
      case 'UN_METRO':
        return util.multiplicar([computoMetrico.cantidad, computoMetrico.largo]);
      case 'UN_PIEZA':
      case 'UN_PUNTO':
      case 'UN_GLOBAL':
      default:
        return computoMetrico.cantidad;
    }
  };

  async function calcularDatosComputos (computo, item) {
    switch (item.unidad_medida) {
      // case 'UN_GLOBAL':
      //   if (computo.id_computo_metrico) {
      //     validarGlobal(computo, item, null);
      //   } else {
      //     const computoRegistrado = await app.src.db.models.computo_metrico.findOne({
      //       attributes: ['id_computo_metrico'],
      //       where: {
      //         fid_item: computo.fid_item,
      //         fid_supervision: computo.fid_supervision,
      //         estado: {$ne: 'ELIMINADO'}
      //       }
      //     });
      //     validarGlobal(computo, item, computoRegistrado);
      //   }
      //   break;
      case 'UN_METRO_CUB':
        validarMetroCubico(computo, item);
        computo.area = null;
        computo.volumen = _calcularVolumen(computo);
        break;
      case 'UN_METRO_CUAD':
        validarMetroCuadrado(computo, item);
        computo.area = _calcularArea(computo);
        computo.volumen = null;
        break;
      case 'UN_METRO':
        validarMetroLineal(computo, item);
        break;
      case 'UN_PIEZA':
      case 'UN_PUNTO':
      case 'UN_GLOBAL':
      default:
        validarPieza(computo, item);
        break;
    }
    computo.factor_forma = computo.factor_forma ? util.redondear(computo.factor_forma, 6) : null;
    computo.largo = computo.largo ? util.redondear(computo.largo) : null;
    computo.ancho = computo.ancho ? util.redondear(computo.ancho) : null;
    computo.alto = computo.alto ? util.redondear(computo.alto) : null;
    computo.cantidad_parcial = _calcularCantidadParcial(item.unidad_medida, computo);
    return computo;
  }

  async function eliminar (idComputo, idUsuario, t) {
    const COMPUTO = await computoMetricoModel.findById(idComputo, {
      transaction: t
    });
    if (!COMPUTO) {
      throw new errors.ValidationError(`El cómputo métrico indicado no existe.`);
    }
    await _app.dao.supervision.verificarEdicionPermitida(COMPUTO.fid_supervision);
    // return computoMetricoModel.destroy({ where: { id_computo_metrico: idComputo } });
    COMPUTO._usuario_modificacion = idUsuario;
    COMPUTO.estado = 'ELIMINADO';
    let response = await computoMetricoModel.update(COMPUTO.dataValues, {
      where: {
        id_computo_metrico: COMPUTO.id_computo_metrico
      }
    });
    await _app.dao.item.refreshCantidadesFis(COMPUTO.fid_item, COMPUTO.fid_supervision, t);

    return response;
  }

  async function eliminarTodos (idSupervision, idItem, idUsuario, t) {
    await _app.dao.supervision.verificarEdicionPermitida(idSupervision);
    const computos = await computoMetricoModel.findAll({
      where: {
        fid_supervision: idSupervision,
        fid_item: idItem,
        estado: 'ACTIVO'
      }
    });
    if (computos.length === 0) {
      throw new errors.ValidationError(`No existen cómputos métricos para el item señalado.`);
    }
    let result = {
      _usuario_modificacion: idUsuario,
      estado: 'ELIMINADO'
    };
    let response = await computoMetricoModel.update(result, {
      where: {
        fid_supervision: idSupervision,
        fid_item: idItem,
        estado: 'ACTIVO'
      },
      transaction: t
    });
    await _app.dao.item.refreshCantidadesFis(idItem, idSupervision, t);

    return response;
  }

  function validarGlobal (computoMetrico, result, computoRegistrado) {
    if (computoRegistrado !== null) {
      throw new errors.ValidationError(`${result.nombre}: ya existe un cómputo métrico registrado para este item con unidad GLOBAL.`);
    }
    if (!computoMetrico.cantidad || computoMetrico.largo || computoMetrico.ancho || computoMetrico.alto || computoMetrico.factor_forma) {
      throw new errors.ValidationError(`${result.nombre}: debe especificar sólo el número de veces.`);
    }
    if (computoMetrico.cantidad !== 1) {
      throw new errors.ValidationError(`${result.nombre}: el número de veces debe ser igual a uno.`);
    }
  }

  function validarPieza (computoMetrico, result) {
    if (!computoMetrico.cantidad || computoMetrico.largo || computoMetrico.ancho || computoMetrico.alto || computoMetrico.factor_forma) {
      throw new errors.ValidationError(`${result.nombre}: debe especificar sólo el número de veces o cantidad.`);
    }
    validarNroDeVeces(computoMetrico, result.nombre);
  }

  function validarMetroLineal (computoMetrico, result) {
    if (!computoMetrico.cantidad || !computoMetrico.largo || computoMetrico.ancho || computoMetrico.alto || computoMetrico.factor_forma) {
      throw new errors.ValidationError(`${result.nombre}: debe especificar sólo el número de veces y el largo.`);
    }
    validarNroDeVeces(computoMetrico, result.nombre);
    if (computoMetrico.largo <= 0) {
      throw new errors.ValidationError(`${result.nombre}: el largo debe ser un número positivo.`);
    }
  }

  function validarMetroCuadrado (computoMetrico, result) {
    if (!computoMetrico.cantidad || !computoMetrico.largo || (!computoMetrico.ancho && !computoMetrico.alto) || (computoMetrico.ancho && computoMetrico.alto) || !computoMetrico.factor_forma) {
      throw new errors.ValidationError(`${result.nombre}: debe especificar sólo el número de veces, factor de forma, largo y alto o ancho.`);
    }
    validarNroDeVeces(computoMetrico, result.nombre);
    if (computoMetrico.largo <= 0) {
      throw new errors.ValidationError(`${result.nombre}: el largo debe ser un número positivo.`);
    }
    if (computoMetrico.alto && computoMetrico.alto <= 0) {
      throw new errors.ValidationError(`${result.nombre}: el alto debe ser un número positivo.`);
    }
    if (computoMetrico.ancho && computoMetrico.ancho <= 0) {
      throw new errors.ValidationError(`${result.nombre}: el ancho debe ser un número positivo.`);
    }
  }

  function validarMetroCubico (computoMetrico, result) {
    if (!computoMetrico.cantidad || !computoMetrico.largo || !computoMetrico.ancho || !computoMetrico.alto || !computoMetrico.factor_forma) {
      throw new errors.ValidationError(`${result.nombre}: debe especificar el número de veces, largo, ancho, alto y factor de forma.`);
    }
    validarNroDeVeces(computoMetrico, result.nombre);
    if (computoMetrico.largo <= 0) {
      throw new errors.ValidationError(`${result.nombre}: el largo debe ser un número positivo.`);
    }
    if (computoMetrico.ancho <= 0) {
      throw new errors.ValidationError(`${result.nombre}: el ancho debe ser un número positivo.`);
    }
    if (computoMetrico.alto <= 0) {
      throw new errors.ValidationError(`${result.nombre}: el alto debe ser un número positivo.`);
    }
  }

  function validarNroDeVeces (computoMetrico, nombreComputo) {
    // if (computoMetrico.cantidad !== parseInt(computoMetrico.cantidad) || computoMetrico.cantidad <= 0) {
    //   throw new errors.ValidationError(`${nombreComputo}: El número de veces de ser entero positivo.`);
    // }
    if (computoMetrico.cantidad <= 0) {
      throw new errors.ValidationError(`${nombreComputo}: El número de veces de ser positivo.`);
    }
  }

  const calculoCantidadAvance = async (idItem, eq, idSupervision, idProyecto) => {
    const consulta = {
      attributes: [[sequelize.fn('sum', sequelize.col('cantidad_parcial')), 'total']],
      where: {
        estado: { $ne: 'ELIMINADO' }
      }
    };
    switch (eq) {
      case 'eq':
        consulta.where.fid_supervision = { $eq: idSupervision };
        break;
      case 'lt':
        consulta.where.fid_supervision = { $lt: idSupervision };
        break;
      case 'lte':
        consulta.where.fid_supervision = { $lte: idSupervision };
        break;
    }
    if (idItem) {
      consulta.where.fid_item = idItem;
    }
    if (idProyecto) {
      consulta.where.fid_proyecto = idProyecto;
    }
    const result = await computoMetricoModel.findOne(consulta);
    return result.dataValues.total;
  };

  async function validarCantidadEjecutada (item, computoMetrico) {
    if (computoMetrico.cantidad_parcial > item.cantidad) {
      throw new errors.ValidationError(`La cantidad parcial sobrepasa a la cantidad contractual.`);
    }
    let cantidadEjecutada = await computoMetricoModel.getSumCantidadParcial(computoMetrico.fid_item, 'eq', computoMetrico.fid_supervision, computoMetrico.id_computo_metrico);
    cantidadEjecutada = isNaN(cantidadEjecutada) ? 0 : cantidadEjecutada;
    if (util.sumar([computoMetrico.cantidad_parcial, cantidadEjecutada]) > item.cantidad) {
      throw new errors.ValidationError(`La cantidad ejecutada sobrepasa a la cantidad contractual.`);
    }
  }

  async function getPorItemSupervision (idItem, idSupervision) {
    return computoMetricoModel.findAll({
      attributes: ['id_computo_metrico', 'fid_item', 'fid_supervision', 'orden', 'descripcion', 'cantidad',
        'cantidad_parcial', 'subtitulo', 'uuid', 'estado'].concat(await getParametrosSegunUnidad(idItem)),
      where: {
        fid_item: idItem,
        estado: { $ne: 'ELIMINADO' },
        fid_supervision: idSupervision
      },
      order: [['orden']]
    });
  }

  async function getPorItemSupervisionAnterior (idItem, idSupervision) {
    return computoMetricoModel.findAll({
      attributes: ['id_computo_metrico', 'fid_item', 'fid_supervision', 'orden', 'descripcion', 'cantidad',
        'cantidad_parcial', 'subtitulo', 'uuid', 'estado'].concat(await getParametrosSegunUnidad(idItem)),
      where: {
        fid_item: idItem,
        estado: { $ne: 'ELIMINADO' },
        fid_supervision: {
          $lt : idSupervision
        }
      },
      order: [['orden']]
    });
  }

  async function getParametrosSegunUnidad (idItem) {
    const unidad = util.json(await models.item.findById(idItem)).unidad_medida;
    switch (unidad) {
      case 'UN_METRO':
        return ['largo'];
      case 'UN_METRO_CUAD':
        return ['largo', 'ancho', 'alto', 'factor_forma', 'area'];
      case 'UN_METRO_CUB':
        return ['largo', 'ancho', 'alto', 'factor_forma', 'volumen'];
      default:
        return [];
    }
  }

  async function getSupervisionesItem (idItem) {
    let supervisionesItem = await computoMetricoModel.findAll({
      attributes: ['fid_supervision'],
      where: {
        fid_item: idItem,
        estado: {$ne: 'ELIMINADO'}
      },
      group: 'fid_supervision'
    });
    return supervisionModel.findAll({
      attributes: ['id_supervision', 'nro_supervision', 'estado_supervision'],
      where: {
        id_supervision: {$in: _.map(util.json(supervisionesItem), 'fid_supervision')}
      }
    });
  }

  async function getCantidadTotalEjecutada (idItem) {
    const consulta = {
      where: {
        fid_item: idItem,
        estado: { $ne: 'ELIMINADO' }
      }
    };
    return sequelize.models.computo_metrico.sum('cantidad_parcial', consulta);
  }

  async function getItemsSupervision (idSupervision) {
    const ITEMS = await computoMetricoModel.findAll({
      attributes: ['fid_item'],
      where: {
        fid_supervision: idSupervision
      },
      group: 'fid_item'
    });
    return models.item.findAll({
      attributes: ['id_item', 'nombre', 'unidad_medida', 'cantidad', 'precio_unitario'],
      where: {
        id_item: { $in: _.map(util.json(ITEMS), 'fid_item') }
      }
    });
  }

  async function getSupervision(idSupervision) {
    return computoMetricoModel.findAll({
      //attributes: ['id_computo_metrico','fid_item', 'descripcion'],
      where: {
        fid_supervision: idSupervision
      }
    });
  }

  _app.dao.computoMetrico.crearRecuperar = crearRecuperar;
  _app.dao.computoMetrico.eliminar = eliminar;
  _app.dao.computoMetrico.eliminarTodos = eliminarTodos;
  _app.dao.computoMetrico.calculoCantidadAvance = calculoCantidadAvance;
  _app.dao.computoMetrico.getPorItemSupervision = getPorItemSupervision;
  _app.dao.computoMetrico.getSupervisionesItem = getSupervisionesItem;
  _app.dao.computoMetrico.getCantidadTotalEjecutada = getCantidadTotalEjecutada;
  _app.dao.computoMetrico.getItemsSupervision = getItemsSupervision;
  _app.dao.computoMetrico.calcularDatosComputos = calcularDatosComputos;
  _app.dao.computoMetrico.getSupervision = getSupervision;
  _app.dao.computoMetrico.getPorItemSupervisionAnterior = getPorItemSupervisionAnterior;
};
