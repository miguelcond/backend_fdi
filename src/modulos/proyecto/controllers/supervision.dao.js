import errors from '../../../lib/errors';
import logger from '../../../lib/logger';
import util from '../../../lib/util';
import moment from 'moment';
import _ from 'lodash';

module.exports = (app) => {
  const _app = app;
  _app.dao.supervision = {};
  const models = app.src.db.models;
  const sequelize = app.src.db.sequelize;

  async function crearRecuperar (supervision, idAdjudicacion, idUsuario, t) {
    let result;
    if (supervision.id_supervision && supervision.id_supervision > 0) {
      result = await models.supervision.findById(supervision.id_supervision, {
        transaction: t
      });
    } else {
      if (supervision.uuid) {
        result = await models.supervision.findOne({
          where: {
            uuid: supervision.uuid
          }
        }, {
          transaction: t
        });
      }
      if (!result) {
        let supervisiones = await models.supervision.findAll({
          where: {
            fid_proyecto: supervision.fid_proyecto,
            fid_adjudicacion: idAdjudicacion,
            estado_supervision: { $in: ['REGISTRO_PLANILLA_FDI', 'EN_SUPERVISION_FDI', 'PENDIENTE_FDI', 'EN_TECNICO_FDI'] }
          }
        });
        let modulos = await models.modulo.findAll({
          where:{
            fid_adjudicacion: idAdjudicacion,
            estado:{ $in:['ACTIVO'] }
          }
        });
        if (modulos.length <= 0) {
          throw new errors.ValidationError('Necesita registrar Items antes de crear una planilla.');
        }
        if (supervisiones.length > 0) {
          throw new errors.ValidationError('No puede crear otra planilla, hasta que le aprueben las que tiene pendientes.');
        }
        let nro = await models.supervision.maxSupervision(supervision.fid_proyecto, idAdjudicacion, t)
        .then((result) => {
          if (result && result.dataValues.nro) {
            return result.dataValues.nro + 1;
          } else {
            return 1;
          }
        });
        supervision.fid_adjudicacion = idAdjudicacion;
        supervision.nro_supervision = nro;
        supervision.cantidad_avance = 0;
        supervision._usuario_creacion = idUsuario;
        supervision.estado_supervision = util.json(await _app.dao.estado.getEstadoInicial('SUPERV-FDI')).codigo;
        supervision.fecha_inicio = await getFechaInicioSupervision(supervision.fid_proyecto,idAdjudicacion);
        supervision.fecha_creacion = new Date();
        result = await models.supervision.create(supervision, {
          transaction: t
        });
      }
    }
    result = JSON.parse(JSON.stringify(result));
    if (supervision.computo_metrico) {
      result.computo_metrico = [];
      for (let i = 0; i < supervision.computo_metrico.length; i++) {
        result.computo_metrico.push(await _app.dao.computoMetrico.crearRecuperar(supervision.computo_metrico[i], result.id_supervision, idUsuario, t));
      }
      await models.supervision.update({
        cantidad_avance: result.cantidad_avance
      }, {
        where: {
          id_supervision: result.id_supervision
        },
        transaction: t
      });
    }
    return result;
  };

  async function getFechaInicioSupervision (idProyecto,idAdjudicacion) {
    const ULTIMA_SUPERVISION = await getUltimaSupervisionProyecto(idProyecto,idAdjudicacion);
    if (!ULTIMA_SUPERVISION) {
      const PROYECTO = await app.dao.proyecto.getProyectoId(idProyecto);
      if (PROYECTO.orden_proceder) {
        return PROYECTO.orden_proceder;
      } else {
        throw new errors.ValidationError(`El proyecto no cuenta con la fecha del orden de proceder.`);
      }
    } else {
      if (ULTIMA_SUPERVISION.fecha_fin) {
        const FECHA_INICIO = moment(ULTIMA_SUPERVISION.fecha_fin, 'YYYY-MM-DD').add(1, 'days');
        return FECHA_INICIO;
      } else {
        throw new errors.ValidationError(`La supervisión anterior no ha concluido.`);
      }
    }
  }

  async function getUltimaSupervisionProyecto (idProyecto,idAdjudicacion) {
    return models.supervision.findOne({
      where: {
        fid_proyecto: idProyecto,
        fid_adjudicacion: idAdjudicacion
      },
      order: [['nro_supervision', 'DESC']],
      limit: 1
    });
  }

  const getSupervisionesProyecto = async (idProyecto, idAdjudicacion, nroSupervision) => {
    const consulta = {
      attributes: ['nro_supervision', 'desembolso', 'descuento_anticipo', 'tipo_descuento_anticipo', 'estado_supervision'],
      where: {
        fid_proyecto: idProyecto,
        estado_supervision: 'EN_ARCHIVO_FDI'
      },
      order: [['nro_supervision']]
    };
    if (idAdjudicacion) {
      consulta.where.fid_adjudicacion = idAdjudicacion;
    }
    if (nroSupervision) {
      consulta.where.nro_supervision = {
        $lte: nroSupervision,
        $gt: 0,
      };
    }
    return models.supervision.findAll(consulta);
  };

  const _getDesembolsoTotal = (supervision) => {
    return util.sumarPor(supervision.supervisiones_anteriores, (supervision) => {
      return supervision.desembolso;
    });
  };

  const _getPorcentajeAvFisico = (supervision) => {
    return util.calcularPorcentaje(supervision.monto_total_acumulado, supervision.monto_total_contractual);
  };

  const _getPorcentajeAvFinanciero = (supervision) => {
    return util.calcularPorcentaje(supervision.monto_desembolso_total, supervision.monto_total_contractual);
  };

  const getDatos = async (id, idRol, query) => {
    let consulta = {
      where: {
        id_supervision: id
      },
      include: [
        {
          attributes: ['referencia','monto','version'],
          model: models.adjudicacion,
          as: 'adjudicacion'
        },
        {
          attributes: ['codigo', 'acciones', 'atributos', 'requeridos', 'areas', ['fid_rol', 'roles']],
          model: models.estado,
          as: 'estado_actual'
        },
        {
          model: models.transicion,
          as: 'observacion_proceso',
          attributes: [['observacion', 'mensaje'], 'fid_usuario'],
          where: {
            fid_usuario_asignado: null,
            codigo_estado: {
              $eq: app.src.db.Sequelize.col('supervision.estado_supervision')
            }
          },
          required: false,
          include: [
            {
              attributes: ['id_usuario', 'fid_persona'],
              model: models.usuario,
              as: 'usuario',
              include: [
                {
                  attributes: ['nombres', 'primer_apellido', 'segundo_apellido'],
                  model: models.persona,
                  as: 'persona'
                }
              ]
            }
          ]
        }
      ]
    };
    let supervision = await models.supervision.findOne(consulta);
    if (supervision) {
      supervision = util.json(supervision);
    } else {
      throw new errors.ValidationError('No se encontró la supervisión.');
    }

    supervision = await procesarSupervision(supervision, idRol);

    // // let proyecto = await app.dao.proyecto.getPorId(supervision.fid_proyecto);

    // // if (supervision.version_proyecto && supervision.version_proyecto < proyecto.version) {
    // //   supervision.modulos = await app.dao.modulo_historico.obtenerDatosProyecto(supervision.fid_proyecto, supervision.id_supervision, supervision.fid_adjudicacion, true, supervision.version_proyecto);
    // //   supervision.proyecto = await app.dao.proyecto_historico.obtenerDatosParaPlanilla(supervision.fid_proyecto, idRol, false, true, supervision.version_proyecto);
    // // } else {
    // //   supervision.modulos = await app.dao.modulo.obtenerDatosProyecto(supervision.fid_proyecto, supervision.id_supervision, supervision.fid_adjudicacion, true);
    // //   supervision.proyecto = await app.dao.proyecto.obtenerDatosParaPlanilla(supervision.fid_proyecto, idRol, false, true);
    // // }

    // let adjudicacion = await app.dao.adjudicacion.obtenerPorId(supervision.fid_adjudicacion);

    // if (supervision.version_adjudicacion && supervision.version_adjudicacion < adjudicacion.version) {
    //   // supervision.proyecto = await app.dao.proyecto_historico.obtenerDatosParaPlanilla(supervision.fid_proyecto, idRol, false, true, supervision.version_proyecto);
    //   supervision.modulos = await app.dao.modulo_historico.obtenerDatosProyecto(supervision.fid_proyecto, supervision.id_supervision, supervision.fid_adjudicacion, true, supervision.version_adjudicacion);
    //   supervision.adjudicacion = await app.dao.adjudicacion_historico.obtenerDatosParaPlanilla(supervision.fid_adjudicacion, idRol, false, true, supervision.version_adjudicacion);
    // } else {
    //   // supervision.proyecto = await app.dao.proyecto.obtenerDatosParaPlanilla(supervision.fid_proyecto, idRol, false, true);
    //   supervision.modulos = await app.dao.modulo.obtenerDatosProyecto(supervision.fid_proyecto, supervision.id_supervision, supervision.fid_adjudicacion, true);
    //   supervision.adjudicacion = await app.dao.adjudicacion.obtenerDatosParaPlanilla(supervision.fid_adjudicacion, idRol, false, true);
    // }

    // supervision.proyecto = await app.dao.proyecto.obtenerDatosParaPlanilla(supervision.fid_proyecto, idRol, false, true);

    // supervision.monto_desembolso_acumulado = 0;
    // supervision.monto_desembolso_actual = 0;
    // supervision.monto_desembolso_total = 0;
    // supervision.monto_total_actual_ejecutado = 0;
    // supervision.monto_total_acumulado = 0;
    // supervision.monto_total_contractual = 0;
    // supervision.monto_total_por_ejecutar = 0;
    // supervision.modulos = JSON.parse(JSON.stringify(supervision.modulos));
    // supervision.modulos.forEach(modulo => {
    //   modulo.items.forEach(item => {
    //     app.dao.item.getCalculos(item);
    //     // para datos de proyecto
    //     supervision.monto_desembolso_acumulado = util.sumar([supervision.monto_desembolso_acumulado, item.cantidad_anterior_fin]);
    //     supervision.monto_desembolso_actual = util.sumar([supervision.monto_desembolso_actual, item.cantidad_actual_fin]);
    //     // datos para resumen de finales
    //     supervision.monto_total_actual_ejecutado = util.sumar([supervision.monto_total_actual_ejecutado, item.cantidad_actual_fin]);
    //     supervision.monto_total_acumulado = util.sumar([supervision.monto_total_acumulado, item.total_acumulado_fin]);
    //     supervision.monto_total_por_ejecutar = util.sumar([supervision.monto_total_por_ejecutar, item.falta_ejecutar_fin]);
    //     supervision.monto_total_contractual = util.sumar([supervision.monto_total_contractual, item.total]);
    //   });
    // });

    // supervision.fotos_supervision = await app.dao.fotoSupervision.getFotosSupervision(supervision.id_supervision);
    // supervision.supervisiones_anteriores = await getSupervisionesProyecto(supervision.fid_proyecto, supervision.fid_adjudicacion, supervision.nro_supervision);
    // supervision.monto_desembolso_total = util.redondear(_getDesembolsoTotal(supervision));
    // supervision.porcentaje_avance_fis = _getPorcentajeAvFisico(supervision);

    // supervision.porcentaje_avance_fin = supervision.porcentaje_avance_fis === 100 && supervision.estado_supervision === 'EN_ARCHIVO_FDI' ? 100 : _getPorcentajeAvFinanciero(supervision);
    return supervision;
  };

  // Codigo repetido con función getDatos, optimizado mediante función procesarSupervision
  const getDatosPorProyectoyNumero = async (idProyecto, idAdjudicacion, nroSupervision, idRol, query) => {
    let consulta = {
      where: {
        fid_proyecto: idProyecto,
        fid_adjudicacion: idAdjudicacion,
        nro_supervision: nroSupervision
      },
      include: [
        {
          attributes: ['referencia','monto','version'],
          model: models.adjudicacion,
          as: 'adjudicacion'
        },
        {
          attributes: ['codigo', 'acciones', 'atributos', 'requeridos', 'areas', ['fid_rol', 'roles']],
          model: models.estado,
          as: 'estado_actual'
        },
        {
          model: models.transicion,
          as: 'observacion_proceso',
          attributes: [['observacion', 'mensaje'], 'fid_usuario'],
          where: {
            fid_usuario_asignado: null,
            codigo_estado: {
              $eq: app.src.db.Sequelize.col('supervision.estado_supervision')
            }
          },
          required: false,
          include: [
            {
              attributes: ['id_usuario', 'fid_persona'],
              model: models.usuario,
              as: 'usuario',
              include: [
                {
                  attributes: ['nombres', 'primer_apellido', 'segundo_apellido'],
                  model: models.persona,
                  as: 'persona'
                }
              ]
            }
          ]
        }
      ]
    };
    let supervision = await models.supervision.findOne(consulta);

    if (supervision) {
      supervision = util.json(supervision);
    } else {
      throw new errors.ValidationError('No se encontró la supervisión.');
    }

    supervision = await procesarSupervision(supervision, idRol);

    // let proyecto = await app.dao.proyecto.getPorId(supervision.fid_proyecto);
    // let adjudicacion = await app.dao.adjudicacion.obtenerPorId(supervision.fid_adjudicacion);

    // // if (supervision.version_proyecto && supervision.version_proyecto < proyecto.version) {
    // //   supervision.modulos = await app.dao.modulo_historico.obtenerDatosProyecto(supervision.fid_proyecto, supervision.id_supervision, supervision.fid_adjudicacion, true, supervision.version_proyecto);
    // //   supervision.proyecto = await app.dao.proyecto_historico.obtenerDatosParaPlanilla(supervision.fid_proyecto, idRol, false, true, supervision.version_proyecto);
    // // } else {
    // //   supervision.modulos = await app.dao.modulo.obtenerDatosProyecto(supervision.fid_proyecto, supervision.id_supervision, supervision.fid_adjudicacion, true);
    // //   supervision.proyecto = await app.dao.proyecto.obtenerDatosParaPlanilla(supervision.fid_proyecto, idRol, false, true);
    // // }

    // if (supervision.version_adjudicacion && supervision.version_adjudicacion < adjudicacion.version) {
    //   supervision.modulos = await app.dao.modulo_historico.obtenerDatosProyecto(supervision.fid_proyecto, supervision.id_supervision, supervision.fid_adjudicacion, true, supervision.version_adjudicacion);
    //   // supervision.proyecto = await app.dao.proyecto_historico.obtenerDatosParaPlanilla(supervision.fid_proyecto, idRol, false, true, supervision.version_proyecto);
    //   supervision.adjudicacion = await app.dao.adjudicacion_historico.obtenerDatosParaPlanilla(supervision.fid_adjudicacion, idRol, false, true, supervision.version_adjudicacion);
    // } else {
    //   supervision.modulos = await app.dao.modulo.obtenerDatosProyecto(supervision.fid_proyecto, supervision.id_supervision, supervision.fid_adjudicacion, true);
    //   // supervision.proyecto = await app.dao.proyecto.obtenerDatosParaPlanilla(supervision.fid_proyecto, idRol, false, true);
    //   supervision.adjudicacion = await app.dao.adjudicacion.obtenerDatosParaPlanilla(supervision.fid_adjudicacion, idRol, false, true);
    // }

    // supervision.proyecto = await app.dao.proyecto.obtenerDatosParaPlanilla(supervision.fid_proyecto, idRol, false, true);

    // supervision.monto_desembolso_acumulado = 0;
    // supervision.monto_desembolso_actual = 0;
    // supervision.monto_desembolso_total = 0;
    // supervision.monto_total_actual_ejecutado = 0;
    // supervision.monto_total_acumulado = 0;
    // supervision.monto_total_contractual = 0;
    // supervision.monto_total_por_ejecutar = 0;
    // supervision.modulos = JSON.parse(JSON.stringify(supervision.modulos));
    // supervision.modulos.forEach(modulo => {
    //   modulo.items.forEach(item => {
    //     app.dao.item.getCalculos(item);
    //     // para datos de proyecto
    //     supervision.monto_desembolso_acumulado = util.sumar([supervision.monto_desembolso_acumulado, item.cantidad_anterior_fin]);
    //     supervision.monto_desembolso_actual = util.sumar([supervision.monto_desembolso_actual, item.cantidad_actual_fin]);
    //     // datos para resumen de finales
    //     supervision.monto_total_actual_ejecutado = util.sumar([supervision.monto_total_actual_ejecutado, item.cantidad_actual_fin]);
    //     supervision.monto_total_acumulado = util.sumar([supervision.monto_total_acumulado, item.total_acumulado_fin]);
    //     supervision.monto_total_por_ejecutar = util.sumar([supervision.monto_total_por_ejecutar, item.falta_ejecutar_fin]);
    //     supervision.monto_total_contractual = util.sumar([supervision.monto_total_contractual, item.total]);
    //   });
    // });

    // supervision.fotos_supervision = await app.dao.fotoSupervision.getFotosSupervision(supervision.id_supervision);
    // supervision.supervisiones_anteriores = await getSupervisionesProyecto(supervision.fid_proyecto, supervision.fid_adjudicacion, supervision.nro_supervision);
    // supervision.monto_desembolso_total = util.redondear(_getDesembolsoTotal(supervision));
    // supervision.porcentaje_avance_fis = _getPorcentajeAvFisico(supervision);
    // supervision.porcentaje_avance_fin = supervision.porcentaje_avance_fis === 100 && supervision.estado_supervision === 'EN_ARCHIVO_FDI' ? 100 : _getPorcentajeAvFinanciero(supervision);
    return supervision;
  };

  async function sumaAdendas(plazo_ejecucion,ampliado1,ampliado2,ampliado3,ampliado4,ampliado5){//funcion temporal para suma de adendas

    let plazo_ejecucion_ampliado;
    if(plazo_ejecucion){
      plazo_ejecucion_ampliado = plazo_ejecucion;
    }
    if(ampliado1){
      plazo_ejecucion_ampliado = parseInt(plazo_ejecucion) + parseInt(ampliado1);
    }
    if(ampliado1 && ampliado2){
      plazo_ejecucion_ampliado = parseInt(plazo_ejecucion) + parseInt(ampliado1) + parseInt(ampliado2);
    }
    if(ampliado1 && ampliado2 && ampliado3){
      plazo_ejecucion_ampliado = parseInt(plazo_ejecucion) + parseInt(ampliado1) + parseInt(ampliado2) + parseInt(ampliado3);
    }
    if(ampliado1 && ampliado2 && ampliado3 && ampliado4){
      plazo_ejecucion_ampliado = parseInt(plazo_ejecucion) + parseInt(ampliado1) + parseInt(ampliado2) + parseInt(ampliado3) + parseInt(ampliado4);
    }
    if(ampliado1 && ampliado2 && ampliado3 && ampliado4 && ampliado5){
      plazo_ejecucion_ampliado = parseInt(plazo_ejecucion) + parseInt(ampliado1) + parseInt(ampliado2) + parseInt(ampliado3) + parseInt(ampliado4) + parseInt(ampliado5);
    }
    return plazo_ejecucion_ampliado;
  }

  async function procesarSupervision (supervision, idRol) {
    // let proyecto = await app.dao.proyecto.getPorId(supervision.fid_proyecto);
    // if (supervision.version_proyecto && supervision.version_proyecto < proyecto.version) {
    //   supervision.modulos = await app.dao.modulo_historico.obtenerDatosProyecto(supervision.fid_proyecto, supervision.id_supervision, supervision.fid_adjudicacion, true, supervision.version_proyecto);
    //   supervision.proyecto = await app.dao.proyecto_historico.obtenerDatosParaPlanilla(supervision.fid_proyecto, idRol, false, true, supervision.version_proyecto);
    // } else {
    //   supervision.modulos = await app.dao.modulo.obtenerDatosProyecto(supervision.fid_proyecto, supervision.id_supervision, supervision.fid_adjudicacion, true);
    //   supervision.proyecto = await app.dao.proyecto.obtenerDatosParaPlanilla(supervision.fid_proyecto, idRol, false, true);
    // }

    // let adjudicacion = await app.dao.adjudicacion.obtenerPorId(supervision.fid_adjudicacion);
    let adjudicacion = supervision.adjudicacion;

    if (supervision.version_adjudicacion && supervision.version_adjudicacion < adjudicacion.version) {
      // supervision.proyecto = await app.dao.proyecto_historico.obtenerDatosParaPlanilla(supervision.fid_proyecto, idRol, false, true, supervision.version_proyecto);
      supervision.modulos = await app.dao.modulo_historico.obtenerDatosProyecto(supervision.fid_proyecto, supervision.id_supervision, supervision.fid_adjudicacion, true, supervision.version_adjudicacion);
      supervision.adjudicacion = await app.dao.adjudicacion_historico.obtenerDatosParaPlanilla(supervision.fid_adjudicacion, idRol, false, true, supervision.version_adjudicacion);
    } else {
      // supervision.proyecto = await app.dao.proyecto.obtenerDatosParaPlanilla(supervision.fid_proyecto, idRol, false, true);
      supervision.modulos = await app.dao.modulo.obtenerDatosProyecto(supervision.fid_proyecto, supervision.id_supervision, supervision.fid_adjudicacion, true);
      supervision.adjudicacion = await app.dao.adjudicacion.obtenerDatosParaPlanilla(supervision.fid_adjudicacion, idRol, false, true);
    }

    supervision.adjudicacion = util.json(supervision.adjudicacion);
    supervision.adjudicacion.monto_inicial = (supervision.adjudicacion.version == 1) ? supervision.adjudicacion.monto : (await app.dao.adjudicacion_historico.obtenerPorId(supervision.fid_adjudicacion,1)).monto;
    supervision.modificaciones = await app.dao.adjudicacion_historico.obtenerModificaciones(supervision.fid_adjudicacion, supervision.adjudicacion.version);
    supervision.proyecto = await app.dao.proyecto.obtenerDatosParaPlanilla(supervision.fid_proyecto, idRol, false, true);
    let amp1 = supervision.proyecto.dataValues.convenio.plazo_convenio_ampliado;
    let amp2 = supervision.proyecto.dataValues.convenio.plazo_convenio_ampliado2;
    let amp3 = supervision.proyecto.dataValues.convenio.plazo_convenio_ampliado3;
    let amp4 = supervision.proyecto.dataValues.convenio.plazo_convenio_ampliado4;
    let amp5 = supervision.proyecto.dataValues.convenio.plazo_convenio_ampliado5;
    supervision.proyecto.dataValues.plazo_ejecucion = await sumaAdendas(supervision.proyecto.dataValues.plazo_ejecucion,amp1,amp2,amp3,amp4,amp5);
    if (!supervision.adjudicacion.orden_proceder) supervision.adjudicacion.orden_proceder = supervision.proyecto.orden_proceder;
    let fechasRecepcionAdjudicacion = await app.dao.adjudicacion.calcularFechasRecepcion(supervision.adjudicacion, supervision.proyecto);
    if (fechasRecepcionAdjudicacion) {
      supervision.proyecto.dataValues.fecha_recepcion_provisional = fechasRecepcionAdjudicacion.fecha_recepcion_provisional;
      supervision.proyecto.dataValues.fecha_recepcion_definitiva = fechasRecepcionAdjudicacion.fecha_recepcion_definitiva;
    }

    supervision.monto_desembolso_acumulado = 0;
    supervision.monto_desembolso_actual = 0;
    supervision.monto_desembolso_total = 0;
    supervision.monto_total_actual_ejecutado = 0;
    supervision.monto_total_acumulado = 0;
    supervision.monto_total_contractual = 0;
    supervision.monto_total_por_ejecutar = 0;
    supervision.pesoAcumulado = await app.dao.item.getPesoAcumulado(supervision.fid_proyecto, supervision.fid_adjudicacion);
    supervision.completado_fisico = true;

    supervision.modulos = JSON.parse(JSON.stringify(supervision.modulos));
    // supervision.modulos.forEach(modulo => {
      for (let modulo of supervision.modulos){
      for(let item of modulo.items){


      // modulo.items.forEach(async item => {
        // console.log('item', item);
        let temporal = await app.dao.computoMetrico.getPorItemSupervisionAnterior(item.id_item, supervision.id_supervision)
        temporal = temporal.map(t=> t.cantidad_parcial)
        temporal = util.sumar(temporal)
        item.cantidad_anterior_fis = temporal;
        temporal = await app.dao.computoMetrico.getPorItemSupervision(item.id_item, supervision.id_supervision)
        temporal = temporal.map(t=> t.cantidad_parcial)
        temporal = util.sumar(temporal)
        item.cantidad_actual_fis = temporal ;
       // console.log(temporal, "TEMPORALLLLLLS",item)
        app.dao.item.getCalculos(item, supervision.pesoAcumulado);
        // para datos de proyecto
        supervision.monto_desembolso_acumulado = util.sumar([supervision.monto_desembolso_acumulado, item.cantidad_anterior_fin]);
        supervision.monto_desembolso_actual = util.sumar([supervision.monto_desembolso_actual, item.cantidad_actual_fin]);
        // datos para resumen de finales
        supervision.monto_total_actual_ejecutado = util.sumar([supervision.monto_total_actual_ejecutado, item.cantidad_actual_fin]);
        supervision.monto_total_acumulado = util.sumar([supervision.monto_total_acumulado, item.total_acumulado_fin]);
        supervision.monto_total_por_ejecutar = util.sumar([supervision.monto_total_por_ejecutar, item.falta_ejecutar_fin]);
        supervision.monto_total_contractual = util.sumar([supervision.monto_total_contractual, item.total]);
        supervision.avance_fisico_ponderado = util.sumar([supervision.avance_fisico_ponderado, item.porcentaje_ponderado_fis], false, 4);
        supervision.maximo_esperado = util.sumar([supervision.maximo_esperado, item.relacion_peso], false, 4);
        supervision.completado_fisico = supervision.completado_fisico && item.completado;
      }
    };

    // Si todos los items están en 100% el avance fisico es 100% (evita perdida de decimales por redondeos)
    supervision.avance_fisico_ponderado = supervision.completado_fisico ? 100 : supervision.avance_fisico_ponderado;

    // console.log('-------------maximo esperado--------');
    // console.log(supervision.maximo_esperado);
    // console.log('------------------------------------');

    supervision.fotos_supervision = await app.dao.fotoSupervision.getFotosSupervision(supervision.id_supervision);
    supervision.supervisiones_anteriores = await getSupervisionesProyecto(supervision.fid_proyecto, supervision.fid_adjudicacion, supervision.nro_supervision);
    supervision.monto_desembolso_total = util.redondear(_getDesembolsoTotal(supervision));
    supervision.porcentaje_avance_fis = _getPorcentajeAvFisico(supervision);
    supervision.porcentaje_avance_fin = supervision.porcentaje_avance_fis === 100 && supervision.estado_supervision === 'EN_ARCHIVO_FDI' ? 100 : _getPorcentajeAvFinanciero(supervision);

    return supervision;
  };

  async function validar (supervision, idRol, idUsuario, t, id) {
    let supervisionActual;
    let estadoSupervision;
    let result = {};
    if (id) {
      supervisionActual = await models.supervision.findById(id);

      if (supervisionActual) {
        estadoSupervision = util.json(supervisionActual).estado_supervision;
      } else {
        throw new errors.ValidationError(`No se encontró la supervisión.`);
      }
      result = supervisionActual;
    } else {
      let res = await app.srcdb.models.estado.estadoInicial('SUPERV-FDI', t);
      estadoSupervision = util.json(res).codigo;
    }
    if (supervision.estado_supervision) {
      result.cambio = {
        origen: estadoSupervision,
        destino: supervision.estado_supervision
      };
      await app.dao.estado.validarDestinos(supervisionActual, supervision, 'estado_supervision', estadoSupervision, idRol, app.dao.supervision);
    } else {
      throw new errors.ValidationError(`Especifique el estado de la supervisión.`);
    }
    const ESTADO_ACTUAL = util.json(await app.dao.estado.getPorCodigo(estadoSupervision));
    await app.dao.estado.verificarPermisos(supervisionActual, ESTADO_ACTUAL, idRol, idUsuario, id);

    const ATRIBUTOS_PERMITIDOS = await app.dao.estado.getAtributosPermitidos(ESTADO_ACTUAL, idRol);
    await app.dao.estado.agregarAtributosPermitidos(result, supervision, ATRIBUTOS_PERMITIDOS);
    await app.dao.estado.ejecutarAutomaticos(result, ESTADO_ACTUAL, app.dao.supervision);

    return result;
  }

  async function enviarObservacion (usuario, observador, supervision, proyecto, persona) {
    var correo = require('./../../../lib/correo');
    // console.log(correo);
    console.log(`enviando observacion a ${persona}`);
    const datos = {
      urlLogoUpre: 'http://localhost:4000/imagen/logoUpre.jpg', // deberia entrar en config.js
      urlSistemaFrontend: 'http://localhost:8080/#!/login',
      usuario: {
        nombre_completo: 'Usuario que recibe observacion'
      },
      observador: {
        nombre_completo: 'Usuario que envia observacion'
      },
      supervision: {
        nro_supervision: 4,
        observacion: 'Los computos del item nro 4, no se completaron como lo especifica en la planilla. Por favor ingresar los datos reales.',
        fecha_observacion: '10/12/2017'
      },
      proyecto: {
        nombre: 'Proyecto de prueba del sistema'
      }
    };
    const rutaFile = `${_path}/src/templates/correo_observacion_avance.html`;
    let html = require('fs').readFileSync(rutaFile).toString();
    const email = require('handlebars').compile(html)(datos);

    correo.enviar({
      para: 'jloza@agetic.gob.bo',
      titulo: 'Observación de planilla',
      html: email
    });
  }

  async function verificarMontos (supervision) {
    if (supervision.monto_total_actual_ejecutado === 0) {
      throw new errors.ValidationError('Debe realizar al menos un computo métrico para continuar');
    }

    // verificamos que el porcentaje que falta ejecutar sea 0  y que la cantidad que falta ejecutar no sea  mayor a 0
    for (var i = 0; i < supervision.modulos.length; i++) {
      var modulo = supervision.modulos[i];
      for (var j = 0; j < modulo.items.length; j++) {
        var item = modulo.items[j];
        // TODO la validación falla cuando el porcentaje se redondea a dos decimales 0,0026478376 a 0
        // if (item.ejecutar_porcentaje_fis === 0 && item.falta_ejecutar_fis > 0) {
        //   throw new errors.ValidationError(`El item nro ${item.nro_item} - ${item.nombre} tiene un error por favor corríjalo`);
        // }
        if (item.cantidad_actual_fis > item.cantidad) {
          throw new errors.ValidationError(`La cantidad ejecutada del item nro ${item.nro_item} - ${item.nombre} sobrepasa la cantidad contractual.`);
        }
        if (item.total_acumulado_fis > item.cantidad) {
          throw new errors.ValidationError(`El total ejecutado supera a la cantidad del item nro ${item.nro_item} - ${item.nombre}`);
        }
      }
    }
  }

  async function verificarFotosItems (supervision) {
    let fotos_requeridas = 2;
    if (app.src.config.config.supervision && app.src.config.config.supervision.items && app.src.config.config.supervision.items.fotos) {
      fotos_requeridas = app.src.config.config.supervision.items.fotos.requerido || 0;
    }

    const fotos = await models.foto_supervision.findAll({
      where: {
        fid_supervision: supervision.id_supervision
      }
    });
    const fotosItemObj = {};

    fotos.forEach(foto => {
      if (!fotosItemObj[foto.fid_item]) {
        fotosItemObj[foto.fid_item] = [];
      }
      fotosItemObj[foto.fid_item].push(foto);
    });

    for (var i = 0; i < supervision.modulos.length; i++) {
      var modulo = supervision.modulos[i];
      for (var j = 0; j < modulo.items.length; j++) {
        var item = modulo.items[j];
        if (item.cantidad_actual_fis > 0) {
          if (fotos_requeridas > 0) {
            if (!fotosItemObj[item.id_item] || !fotosItemObj[item.id_item].length === 0) {
              throw new errors.ValidationError(`Su item nro ${item.nro_item} no tiene fotos. ${fotos_requeridas === 1 ? '1 foto es requerida' : fotos_requeridas + ' fotos son requeridas'}, por favor complete las fotografías.`);
            } else if (fotosItemObj[item.id_item].length < fotos_requeridas) {
              throw new errors.ValidationError(`Su item nro ${item.nro_item} tiene ${fotosItemObj[item.id_item].length} de ${fotos_requeridas} fotos, por favor complete las fotografías.`);
            }
          }
        }
      }
    }
  }

  async function verificarFotosSupervision (idSupervision) {
    let fotos_requeridas = 3;
    if (app.src.config.config.supervision && app.src.config.config.supervision.items.fotos) {
      fotos_requeridas = app.src.config.config.supervision.fotos.requerido || 0;
    }
    if (fotos_requeridas > 0) {
      const fotos = await models.foto_supervision.findAll({
        where: {
          fid_supervision: idSupervision,
          fid_item: null,
          tipo: 'TF_SUPERVISION'
        }
      });

      if (fotos.length < fotos_requeridas) {
        let mensaje = `El supervisor debe subir las respectivas fotos de supervisión antes de enviar la planilla de avance a la siguiente instancia.<br><strong>(mín. ${fotos_requeridas == 1 ? '1 fotografía' : fotos_requeridas + ' fotografías'})</strong>`;
        throw new errors.ValidationError(mensaje);
        // throw new errors.ValidationError(`Debe subir 3 fotos de la supervisión, antes de enviar a la siguiente instancia.`);
      }
    }
  }

  async function existe (idSupervision) {
    if (await models.supervision.findById(idSupervision)) {
      return true;
    }
    return false;
  }

  const verificarModificacionEnCurso = async (supervision) => {
    const PROYECTO = await app.dao.proyecto.getPorId(supervision.fid_proyecto);
    if (['APROBADO_FDI','EN_EJECUCION_FDI'].indexOf(PROYECTO.estado_proyecto) < 0) {
      throw new errors.ValidationError(`Existe una modificación en curso, no es posible enviar la planilla.`);
    }
    /*if (PROYECTO.estado_proyecto !== 'APROBADO_FDI' || PROYECTO.estado_proyecto !== 'EN_EJECUCION_FDI') {
      throw new errors.ValidationError(`Existe una modificación en curso, no es posible enviar la planilla.`);
    }*/
  };

  async function ejecutarMetodos (supervision, metodos, idUsuario) {
    supervision = await getDatos(util.json(supervision).id_supervision);
    for (let i = 0; i < metodos.length; i++) {
      switch (metodos[i]) {
        case '$verificarModificacionEnCurso':
          await verificarModificacionEnCurso(supervision);
          break;
        case '$verificarMontos':
          await verificarMontos(supervision);
          break;
        case '$verificarFotosItems':
          await verificarFotosItems(supervision);
          break;
        case '$verificarFotosSupervision':
          await verificarFotosSupervision(supervision.id_supervision);
          break;
        case '$registrarCoordenadas':
          await registrarCoordenadas(supervision, idUsuario);
          break;
        case '$cerrarItemsFinalizados':
          await cerrarItemsFinalizados(supervision, idUsuario);
          break;
        case '$revisarComprobante':
          await revisarComprobante(supervision, idUsuario);
          break;
        case '$generarReportePlanilla':
          await generarReportePlanilla(supervision.id_supervision,supervision.fid_adjudicacion,supervision.nro_supervision,supervision.fid_proyecto);
          break;
        default:
          logger.warn(`No existe el método a ejecutar para la supervisión.`);
      }
    }
  }

  async function revisarComprobante (supervision, idUsuario) {
    if (!supervision.fecha_pago) {
      throw new errors.ValidationError(`Debe registrar la fecha de pago.`);
    }
    if (!supervision.path_comprobante) {
      throw new errors.ValidationError(`Debe adjuntar el comprobante de pago.`);
    }
  }

  async function registrarCoordenadas (supervision, idUsuario) {
    if (supervision.nro_supervision !== 1) {
      return;
    }
    if (supervision.fotos_supervision.length > 0) {
      const COORDENADAS = await app.dao.fotoSupervision.getId(supervision.fotos_supervision[0].id_foto_supervision);
      await app.dao.proyecto.actualizar(supervision.fid_proyecto, {coordenadas: COORDENADAS.coordenadas}, idUsuario);
    }
  }

  async function cerrarItemsFinalizados (supervision, idUsuario) {
    const ITEMS = getItemsSupervision(supervision);
    for (let i = 0; i < ITEMS.length; i++) {
      if (ITEMS[i].falta_ejecutar_fis === 0) {
        await app.dao.item.cerrarItem(ITEMS[i], idUsuario);
      }
    }
  }

  function getItemsSupervision (supervision) {
    let items = [];
    supervision.modulos.map(modulo => {
      items = _.unionWith(items, modulo.items);
    });
    return items;
  }

  async function generarReportePlanilla (idSupervision,idAdjudicacion,nroSupervision,idProyecto) {
    await app.dao.reporte.crearPlanilla(idProyecto,idAdjudicacion,nroSupervision);
  }

  const getVersionProyecto = async (supervision) => {
    const PROYECTO = await app.dao.proyecto.getPorId(supervision.fid_proyecto);
    return PROYECTO.version;
  };

  const getVersionAdjudicacion = async (supervision) => {
    const ADJUDICACION = await app.dao.adjudicacion.obtenerPorId(supervision.fid_adjudicacion);
    return ADJUDICACION.version;
  };

  async function getValoresAutomaticos (cola, elemento, supervision) {
    switch (elemento) {
      case '$getFechaFin':
        cola.push(getFechaFinSupervision());
        break;
      case '$calculoDesembolso':
        cola.push(await getCalculoDesembolso(supervision));
        break;
      case '$calculoDescuentoPorAnticipo':
        cola.push(await getCalculoDescuentoPorAnticipo(supervision));
        break;
      case '$versionProyecto':
        cola.push(await getVersionProyecto(supervision));
        break;
      case '$versionAdjudicacion':
        cola.push(await getVersionAdjudicacion(supervision));
        break;
      default:
        throw new errors.ValidationError(`No se encontró ${elemento}.`);
    }
    return cola;
  }

  async function getFechaFinSupervision () {
    return moment();
  }

  async function getCalculoDesembolso (supervision) {
    const MONTO_EJECUTADO = await getMontoEjecutado(supervision);
    const DESCUENTO_ANTICIPO = await getCalculoDescuentoPorAnticipo(supervision, MONTO_EJECUTADO);
    return util.restar(MONTO_EJECUTADO, DESCUENTO_ANTICIPO);
  }

  async function getCalculoDescuentoPorAnticipo (supervision, montoEjecutado) {
    const PROYECTO = await app.dao.proyecto.getProyectoId(supervision.fid_proyecto);
    if (PROYECTO.porcentaje_anticipo === 0) {
      return 0;
    }
    const SUPERVISIONES_ANTERIORES = util.json(await getSupervisionesProyecto(PROYECTO.id_proyecto));
    const TOTAL_DESCUENTOS_PAGADOS = util.sumarPor(SUPERVISIONES_ANTERIORES, (supervision) => { return supervision.descuento_anticipo; });
    const MONTO_EJECUTADO = !montoEjecutado ? await getMontoEjecutado(supervision) : montoEjecutado;
    const DEUDA_ANTICIPO = util.restar(PROYECTO.anticipo, TOTAL_DESCUENTOS_PAGADOS);
    if (DEUDA_ANTICIPO === 0) {
      return 0;
    }
    switch (supervision.tipo_descuento_anticipo) {
      case 'TOTAL':
        if (MONTO_EJECUTADO < DEUDA_ANTICIPO) {
          throw new errors.ValidationError(`El monto total ejecutado es menor al monto del anticipo pendiente por pagar, el descuento debe ser PROGRESIVO.`);
        }
        return DEUDA_ANTICIPO;
      case 'PROGRESIVO':
        const MONTO_A_DESCONTAR = util.valorPorcentual(MONTO_EJECUTADO, PROYECTO.porcentaje_anticipo);
        if (MONTO_A_DESCONTAR > DEUDA_ANTICIPO) {
          throw new errors.ValidationError(`El monto calculado para el descuento progresivo supera la cantidad que adeuda la empresa, el descuento debe ser por el TOTAL.`);
        }
        return MONTO_A_DESCONTAR;
    }
  }

  async function getMontoEjecutado (supervision) {
    const ITEMS_SUPERVISION = util.json(await app.dao.computoMetrico.getItemsSupervision(supervision.id_supervision));
    let monto = 0;
    for (let i = 0; i < ITEMS_SUPERVISION.length; i++) {
      const CANTIDAD_FIS_EJECUTADA = await models.computo_metrico.getSumCantidadParcial(ITEMS_SUPERVISION[i].id_item, 'eq', supervision.id_supervision);
      monto = util.sumar([monto, util.multiplicar([CANTIDAD_FIS_EJECUTADA, ITEMS_SUPERVISION[i].precio_unitario])]);
    }
    return monto;
  }

  async function obtenerPorId (idSupervision) {
    const sup = await models.supervision.findOne({
      where: {
        id_supervision: idSupervision
      }
    });
    if (sup) {
      return getDatos(sup.id_supervision);
    }
  }

  async function obtener (idProyecto, idAdjudicacion, nroSupervision) {
    const sup = await models.supervision.findOne({
      where: {
        nro_supervision: nroSupervision,
        fid_adjudicacion: idAdjudicacion,
        fid_proyecto: idProyecto
      }
    });
    if (sup) {
      return getDatos(sup.id_supervision);
    }
  }

  async function lista(idProyecto, idAdjudicacion) {
    return await models.supervision.findAll({
      where: {
        fid_adjudicacion: idAdjudicacion,
        fid_proyecto: idProyecto
      }
    });
  }

  async function verificarEdicionPermitida (idSupervision) {
    const SUPERVISION = util.json(await models.supervision.findById(idSupervision));
    if (!SUPERVISION) {
      throw new errors.NotFoundError(`No existe la supervisión indicada.`);
    }
    const ESTADO_INICIAL = util.json(await _app.dao.estado.getEstadoInicial('SUPERV-FDI'));
    if (SUPERVISION.estado_supervision !== ESTADO_INICIAL.codigo) {
      throw new errors.ValidationError(`No puede modificar los datos de una supervisión en proceso o concluida.`);
    }
    return true;
  }

  /**
   * verificarPermisos - Verificando si el usuario tiene permisos para acceder a un recurso de supervisión solicitado.
   * Si el usuario es una empresa entonces se verifica que el nit (de su usuario) sea igual al nit de la empresa
   * adjudicada a ese proyecto, o si el usuario forma parte del equipo técnico del proyecto.
   *
   * @param  {integer} idSupervision Id de la supervisión
   * @param  {integer} idUsuario     Id del usuario que consume el recurso
   * @param  {integer} idRol         Id del rol que consume el recurso
   */
  async function verificarPermisos (idSupervision, idUsuario, idRol) {
    const SUPERVISION = await getId(idSupervision);
    if (await app.dao.rol.esEmpresa(idRol)) {
      const EMPRESA_CONTRATADA = util.json(await app.dao.contrato.getEmpresa(SUPERVISION.fid_proyecto));
      if (util.json(await app.dao.usuario.getPorId(idUsuario)).nit !== EMPRESA_CONTRATADA.nit) {
        throw new errors.UnauthorizedError(`No tiene acceso a los datos de la supervisión.`);
      }
    } else {
      if (!await app.dao.equipo_tecnico.esParteDelEquipo(SUPERVISION.fid_proyecto, idUsuario) && !await app.dao.rol.esResponsableGam(idRol) && !await app.dao.rol.esMae(idRol) && !await app.dao.rol.esVisorReporte(idRol)) {
        throw new errors.UnauthorizedError(`No tiene acceso a los datos de la supervisión.`);
      }
    }
  }

  async function getId (idSupervision) {
    const SUPERVISION = await models.supervision.findById(idSupervision);
    if (!SUPERVISION) {
      throw new errors.NotFoundError(`No existe la supervision`);
    }
    return SUPERVISION;
  }

  const obtenerUltimaAprobada = async (idProyecto,idAdjudicacion) => {
    const ESTADO_FINAL = await app.dao.estado.getEstadoFinal('SUPERV-FDI');
    let consulta = {
      where: {
        fid_proyecto: idProyecto,
        fid_adjudicacion: idAdjudicacion,
        estado_supervision: ESTADO_FINAL.codigo
      },
      order: [['nro_supervision', 'DESC']]
    };
    return models.supervision.findOne(consulta);
  };

  const existeSupervisionEnCurso = async (proyecto) => {
    let consulta = {
      where: {
        fid_proyecto: proyecto.id_proyecto,
        estado: {
          $ne: 'ELIMINADO'
        }
      },
      order: [['nro_supervision', 'DESC']],
      limit: 1
    };
    const ULTIMA_SUPERVISION = await models.supervision.findOne(consulta);
    if (ULTIMA_SUPERVISION) {
      return ['REGISTRO_PLANILLA_FDI','EN_FISCAL_FDI','EN_ARCHIVO_FDI'].indexOf(ULTIMA_SUPERVISION.estado_supervision) == -1;
      //return ULTIMA_SUPERVISION.estado_supervision !== 'REGISTRO_PLANILLA_FDI' && ULTIMA_SUPERVISION.estado_supervision !== 'EN_ARCHIVO_FDI';
    } return false;
  };

  const existeAdjudicacionSupervisionEnCurso = async (proyecto,id_adjudicacion) => {
    let consulta = {
      where: {
        fid_adjudicacion: id_adjudicacion,
        fid_proyecto: proyecto.id_proyecto,
        estado: {
          $ne: 'ELIMINADO'
        }
      },
      order: [['nro_supervision', 'DESC']],
      limit: 1
    };
    const ULTIMA_SUPERVISION = await models.supervision.findOne(consulta);
    if (ULTIMA_SUPERVISION) {
      return ['REGISTRO_PLANILLA_FDI','EN_FISCAL_FDI','EN_ARCHIVO_FDI'].indexOf(ULTIMA_SUPERVISION.estado_supervision) == -1;
      //return ULTIMA_SUPERVISION.estado_supervision !== 'REGISTRO_PLANILLA_FDI' && ULTIMA_SUPERVISION.estado_supervision !== 'EN_ARCHIVO_FDI';
    } return false;
  };

  const existeSupervisionComputos = async (proyecto, t) => {
    let supervision = await models.supervision.findOne({
      where: {
        fid_proyecto: proyecto.id_proyecto,
        estado_supervision: {
          $in: ['REGISTRO_PLANILLA_FDI']
        }
      },
      include: [{
        model: models.computo_metrico,
        as: 'computo_metrico',
        where: {
          estado: 'ACTIVO'
        },
        required: false
      }],
      order: [['nro_supervision', 'DESC']],
      transaction: t
    });
    return supervision && supervision.computo_metrico && supervision.computo_metrico.length
  };

  const obtenerPorVersion = async (idProyecto, version) => {
    const consulta = {
      where: {
        fid_proyecto: idProyecto,
        version_proyecto: version
      }
    };
    const SUPERVISION = models.supervision.findOne(consulta);
    if (!SUPERVISION) {
      throw new errors.ValidationError(`No existe la supervisión del proyecto y versión especificados.`);
    }
    return SUPERVISION;
  };

  const obtenerPorVersionAdjudicacion = async (idAdjudicacion, version) => {
    const consulta = {
      where: {
        fid_adjudicacion: idAdjudicacion,
        version_adjudicacion: version
      }
    };
    const SUPERVISION = models.supervision.findOne(consulta);
    if (!SUPERVISION) {
      throw new error.ValidationError(`No existe la supervisión de la adjudicación y versión especificados`);
    }
    return SUPERVISION;
  };

  app.dao.supervision = {
    obtenerPorId,
    crearRecuperar,
    getDatos,
    getDatosPorProyectoyNumero,
    existe,
    validar,
    ejecutarMetodos,
    getValoresAutomaticos,
    obtener,
    lista,
    enviarObservacion,
    verificarEdicionPermitida,
    verificarPermisos,
    obtenerUltimaAprobada,
    existeSupervisionEnCurso,
    existeSupervisionComputos,
    obtenerPorVersion,
    obtenerPorVersionAdjudicacion,
    existeAdjudicacionSupervisionEnCurso,
    sumaAdendas
  };
};
