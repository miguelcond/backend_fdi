import BaseJoi from '../../../lib/joi';
import Extension from 'joi-date-extensions';
const Joi = BaseJoi.extend(Extension);

module.exports = {
  listar: {
    params: {
      id_proyecto: Joi.number().required()
    }
  },
  obtener: {
    params: {
      id_proyecto: Joi.number().required(),
      id: Joi.number().required()
    }
  },
  crear: {
    params: {
      id_proyecto: Joi.number().required()
    },
    body: {
      tipo_equipo: Joi.string().max(50).required(),
      cargo: Joi.string().max(50).required(),
      doc_designacion: Joi.object({
        path: Joi.string().required(),
        base64: Joi.string().required()
      }),
      numero_documento: Joi.string().max(20).required(),
      fecha_nacimiento: Joi.date().format('YYYY-MM-DD').required(),
      tipo_documento: Joi.string().valid('TD_CI', 'TD_PASS').required(),
      lugar_expedicion_documento: Joi.string().valid('LP', 'OR', 'PT', 'CB', 'SC', 'BN', 'PA', 'TJ', 'CH').allow(null),
      nombres: Joi.string().max(100).uppercase(),
      primer_apellido: Joi.string().max(100).uppercase().allow(''),
      segundo_apellido: Joi.string().max(100).uppercase().allow(''),
      nacionalidad: Joi.string().max(100).uppercase(),
      direccion: Joi.string().max(100),
      correo: Joi.string().max(100).email(),
      telefono: Joi.string().max(20),
      estado: Joi.string().max(100)
    }
  },
  modificar: {
    params: {
      id_proyecto: Joi.number().required(),
      id: Joi.number().required()
    },
    body: {
      cargo: Joi.string().max(50).required(),
      doc_designacion: Joi.object({
        path: Joi.string().optional(),
        base64: Joi.string().optional()
      }),
      numero_documento: Joi.string().max(20),
      fecha_nacimiento: Joi.date().format('YYYY-MM-DD'),
      tipo_documento: Joi.string().valid('TD_CI', 'TD_PASS'),
      correo: Joi.string().max(100).email(),
      telefono: Joi.string().max(20)
    }
  },
  reemplazar: {
    params: {
      id_proyecto: Joi.number().required(),
      id: Joi.number().required()
    },
    body: {
      cargo: Joi.string().max(50).required(),
      doc_designacion: Joi.object({
        path: Joi.string().optional(),
        base64: Joi.string().optional()
      }),
      numero_documento: Joi.string().max(20),
      fecha_nacimiento: Joi.date().format('YYYY-MM-DD'),
      tipo_documento: Joi.string().valid('TD_CI', 'TD_PASS'),
      correo: Joi.string().max(100).email(),
      telefono: Joi.string().max(20)
    }
  },
  eliminar: {
    params: {
      id_proyecto: Joi.number().required(),
      id: Joi.number().required()
    }
  },
  notificar: {
    params: {
      id_proyecto: Joi.number().required(),
      id: Joi.number().required()
    },
    body: {
      correo: Joi.string().max(100).email().required()
    }
  }
};
