const BaseJoi = require('./../../../lib/joi');
const Extension = require('joi-date-extensions');
const Joi = BaseJoi.extend(Extension);

module.exports = {
  getId: {
    params: {
      id: Joi.number().required()
    }
  },
  get: {
    params: { }
  },
  create: {
    body: {
      fid_proyecto: Joi.number().required(),
      computo_metrico: Joi.array().items({
        fid_item: Joi.number().required(),
        descripcion: Joi.string().required(),
        cantidad: Joi.number().allow('').required(),
        largo: Joi.number().allow('').optional(),
        ancho: Joi.number().allow('').optional(),
        alto: Joi.number().allow('').optional(),
        factor_forma: Joi.number().allow('').optional(),
        uuid: Joi.string().allow('').optional()
      })
    }
  },
  update: {
    query: {
      rol: Joi.number()
    },
    params: {
      id: Joi.number().required()
    }
  },
  updateEstado: {
    params: {
      id: Joi.number().required()
    },
    body: {
      /*
      id_supervision: Joi.number().required(),
      computo_metrico: Joi.array().items({
        id_computo_metrico: Joi.number().allow('').optional(),
        descripcion: Joi.string().required(),
        cantidad: Joi.number().allow('').required(),
        largo: Joi.number().allow('').optional(),
        ancho: Joi.number().allow('').optional(),
        alto: Joi.number().allow('').optional(),
        factor_forma: Joi.number().allow('').optional(),
        uuid: Joi.string().allow('').optional()
      })
      */
    }
  }
};
