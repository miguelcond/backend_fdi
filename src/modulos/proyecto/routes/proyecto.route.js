import sequelizeFormly from 'sequelize-formly';
import validate from 'express-validation';
import paramValidation from './proyecto.validation';

module.exports = (app) => {

  app.get('/proyect', app.controller.proyecto.obtenerPorQr);
  app.post('/proyect/scan', app.controller.proyecto.scanqr);
  /**
   * @apiVersion 1.0.0
   * @apiGroup Proyectos
   * @apiName Get proyectos adjudicados a una matricula
   * @api {get} /api/v1/proyectos/empresas/:matricula Obtiene los proyectos adjudicados a una matricula
   *
   * @apiDescription Get proyectos, Obtiene los proyectos procesados
   *
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
   *{
      "finalizado": true,
      "mensaje": "Proyectos recuperados correctamente.",
      "datos": {
        "count": 1,
        "rows": [
          {
            "id_proyecto": 2,
            "version": 1,
            "codigo": "B1xDOdITb",
            "tipo": "TP_CIFE",
            "nombre": "Proyecto de Prueba",
            "nro_convenio": "12/2017",
            "monto_convenio": 10000,
            "monto_total": 10000,
            "plazo_ejecucion": 180,
            "entidad_beneficiaria": "Gobierno Municipal de La Paz",
            "fecha_suscripcion_convenio": "2017-12-31T04:00:00.000Z",
            "doc_convenio": "S1exw__8p-_convenio.txt",
            "nro_resmin_cife": null,
            "doc_resmin_cife": "r1-ewOdI6W_resolucion.txt",
            "doc_cert_presupuestaria": "ryBocdI6b_presupuestaria.txt",
            "desembolso": 2000,
            "porcentaje_desembolso": 20,
            "fecha_desembolso": "2017-10-31T04:00:00.000Z",
            "anticipo": 1000,
            "porcentaje_anticipo": 10,
            "sector": "Sector",
            "fid_autoridad_beneficiaria": 9,
            "cargo_autoridad_beneficiaria": "Alcalde",
            "fax_autoridad_beneficiaria": null,
            "fcod_municipio": "020101",
            "coordenadas": null,
            "doc_especificaciones_tecnicas": "H1XqOuLTb_especificaciones.txt",
            "doc_base_contratacion": "r1gQcOOUTb_contratacion.txt",
            "orden_proceder": null,
            "fecha_fin_proyecto": null,
            "estado_proyecto": "BOLETA_GARANTIA",
            "fid_usuario_asignado": 5,
            "estado": "ACTIVO",
            "_usuario_creacion": 2,
            "_usuario_modificacion": 1022579028,
            "_fecha_creacion": "2017-10-19T19:09:11.962Z",
            "_fecha_modificacion": "2017-10-19T19:19:11.331Z",
            "contrato": [
              {
                "id_contrato": 1,
                "fid_empresa": 1,
                "fid_proyecto": 2,
                "nro_invitacion": "123456",
                "nro_cuce": "9514",
                "fecha_inscripcion": null,
                "nombre_representante_legal": "TEODOVICH CHAVEZ ANA HILDA",
                "ci_representante_legal": null,
                "numero_testimonio": null,
                "lugar_emision": null,
                "fecha_expedicion": null,
                "fax": null,
                "correo": null,
                "estado": "ACTIVO",
                "_usuario_creacion": 1022579028,
                "_usuario_modificacion": null,
                "_fecha_creacion": "2017-10-19T19:19:11.298Z",
                "_fecha_modificacion": "2017-10-19T19:19:11.298Z",
                "empresa": {
                  "id_empresa": 1,
                  "matricula": "00101952",
                  "nit": "03275482018",
                  "nombre": "MR. CAFE",
                  "tipo": "NACIONAL",
                  "otro": null,
                  "pais": "BOLIVIA",
                  "ciudad": "SANTA CRUZ",
                  "direccion": "AEROPUERTO VIRU VIRU- EMBARQUE NACIONAL, PISO 2",
                  "telefono": "72159590",
                  "estado": "ACTIVO",
                  "_usuario_creacion": 1022579028,
                  "_usuario_modificacion": null,
                  "_fecha_creacion": "2017-10-19T19:19:11.287Z",
                  "_fecha_modificacion": "2017-10-19T19:19:11.287Z",
                  "fid_representante_legal": null
                }
              }
            ]
          }
        ]
      }
    }
   */
  app.api.get('/proyectos/empresas/:matricula', app.controller.proyecto.obtenerMatricula);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Proyectos
   * @apiName Get datos proyecto adjudicado a una matrícula
   * @api {get} /api/v1/proyectos/empresas/:matricula/:id Obtiene los datos del proyecto adjudicado a una matrícula
   *
   * @apiDescription Get proyectos, Obtiene los proyectos procesados
   *
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
   *{
      "finalizado": true,
      "mensaje": "Proyecto recuperado correctamente.",
      "datos": {
        "id_proyecto": 2,
        "version": 1,
        "codigo": "HyZWTvC2pZ",
        "tipo": "TP_CIFE",
        "nombre": "Proyecto de Prueba",
        "nro_convenio": "12/2017",
        "monto_convenio": 10000,
        "monto_total": 10000,
        "plazo_ejecucion": 180,
        "entidad_beneficiaria": "Gobierno Municipal de La Paz",
        "fecha_suscripcion_convenio": "2017-12-31T04:00:00.000Z",
        "doc_convenio": "S1ZaP0nTb_convenio.txt",
        "nro_resmin_cife": null,
        "doc_resmin_cife": "Hkx-pPC2p-_resolucion.txt",
        "doc_cert_presupuestaria": "H1dADA2ab_presupuestaria.txt",
        "desembolso": 2000,
        "porcentaje_desembolso": 20,
        "fecha_desembolso": "2017-10-31T04:00:00.000Z",
        "anticipo": 1000,
        "porcentaje_anticipo": 10,
        "sector": "Sector",
        "fid_autoridad_beneficiaria": 9,
        "cargo_autoridad_beneficiaria": "Alcalde",
        "fax_autoridad_beneficiaria": null,
        "fcod_municipio": "020101",
        "coordenadas": null,
        "doc_especificaciones_tecnicas": "S1W7_R3T-_especificaciones.txt",
        "doc_base_contratacion": "rkx-XdR2TZ_contratacion.txt",
        "orden_proceder": null,
        "fecha_fin_proyecto": null,
        "codigo_acceso": "HJWbQOAh6-",
        "matricula": "00101952",
        "cod_documentacion_exp": "123456",
        "documentacion_exp": {},
        "estado_proyecto": "REGISTRO_EMPRESA",
        "fid_usuario_asignado": 5,
        "estado": "ACTIVO",
        "_usuario_creacion": 2,
        "_usuario_modificacion": 1022579028,
        "_fecha_creacion": "2017-10-24T15:09:44.928Z",
        "_fecha_modificacion": "2017-10-24T16:11:17.662Z",
        "contrato": [
          {
            "id_contrato": 1,
            "fid_empresa": 1,
            "fid_proyecto": 2,
            "nro_invitacion": "123456",
            "nro_cuce": "9514",
            "fecha_inscripcion": null,
            "nombre_representante_legal": "TEODOVICH CHAVEZ ANA HILDA",
            "ci_representante_legal": null,
            "numero_testimonio": null,
            "lugar_emision": null,
            "fecha_expedicion": null,
            "fax": null,
            "correo": null,
            "estado": "ACTIVO",
            "_usuario_creacion": 1022579028,
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-10-24T16:06:25.945Z",
            "_fecha_modificacion": "2017-10-24T16:06:25.945Z",
            "empresa": {
              "id_empresa": 1,
              "matricula": "00101952",
              "nit": "03275482018",
              "nombre": "MR. CAFE",
              "tipo": "NACIONAL",
              "otro": null,
              "pais": "BOLIVIA",
              "ciudad": "SANTA CRUZ",
              "direccion": "AEROPUERTO VIRU VIRU- EMBARQUE NACIONAL, PISO 2",
              "telefono": "72159590",
              "estado": "ACTIVO",
              "_usuario_creacion": 1022579028,
              "_usuario_modificacion": null,
              "_fecha_creacion": "2017-10-24T16:06:25.935Z",
              "_fecha_modificacion": "2017-10-24T16:06:25.935Z",
              "fid_representante_legal": null
            }
          }
        ],
        "equipo_tecnico": [
          {
            "id_equipo_tecnico": 1,
            "fid_proyecto": 2,
            "tipo_equipo": "TE_UPRE",
            "fid_usuario": 7,
            "cargo": "CG_ENCARGADO",
            "estado": "ACTIVO",
            "_usuario_creacion": 7,
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-10-24T15:09:49.066Z",
            "_fecha_modificacion": "2017-10-24T15:09:49.066Z"
          },
          {
            "id_equipo_tecnico": 2,
            "fid_proyecto": 2,
            "tipo_equipo": "TE_UPRE",
            "fid_usuario": 5,
            "cargo": "CG_TECNICO",
            "estado": "ACTIVO",
            "_usuario_creacion": 7,
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-10-24T15:09:49.066Z",
            "_fecha_modificacion": "2017-10-24T15:09:49.066Z"
          }
        ],
        "estado_actual": {
          "codigo": "REGISTRO_EMPRESA",
          "acciones": [
            {
              "ruta": "/temporal",
              "tipo": "ventana"
            },
            {
              "tipo": "boton",
              "label": "Enviar",
              "estado": "BOLETA_GARANTIA",
              "condicion": "tipo TP_CIFE =="
            }
          ],
          "atributos": [
            "cod_documentacion_exp",
            "contrato",
            "estado_proyecto"
          ],
          "requeridos": [
            "matricula",
            "cod_documentacion_exp",
            "documentacion_exp",
            "contrato",
            "modulos.items.precio_unitario"
          ],
          "areas": []
        },
        "autoridad_beneficiaria": {
          "tipo_documento": "TD_CI",
          "numero_documento": "10783821",
          "fecha_nacimiento": "1967-10-26T04:00:00.000Z",
          "nombres": "JESSENIA",
          "primer_apellido": "ARREDONDO",
          "segundo_apellido": "",
          "correo": "jarredondo@mail.com",
          "telefono": "79848948"
        },
        "dpa": {
          "departamento": {
            "codigo": "02",
            "nombre": "La Paz"
          },
          "provincia": {
            "codigo": "0201",
            "nombre": "Murillo",
            "codigo_departamento": "02"
          },
          "municipio": {
            "codigo": "020101",
            "nombre": "Nuestra Señora de La Paz",
            "codigo_provincia": "0201"
          }
        },
        "formularios": [
          {
            "nombre": "Formulario A1",
            "nombre_archivo": "Formulario_A1.pdf"
          },
          {
            "nombre": "Formulario A2a",
            "nombre_archivo": "Formulario_A2a.pdf"
          },
          {
            "nombre": "Formulario A3",
            "nombre_archivo": "Formulario_A3.pdf"
          },
          {
            "nombre": "Formulario A4",
            "nombre_archivo": "Formulario_A4.pdf"
          },
          {
            "nombre": "Formulario A5 1",
            "nombre_archivo": "Formulario_A5_1.pdf"
          },
          {
            "nombre": "Formulario A6 1",
            "nombre_archivo": "Formulario_A6_1.pdf"
          },
          {
            "nombre": "Formulario A7",
            "nombre_archivo": "Formulario_A7.pdf"
          },
          {
            "nombre": "Formulario A8",
            "nombre_archivo": "Formulario_A8.pdf"
          },
          {
            "nombre": "Formulario B1",
            "nombre_archivo": "Formulario_B1.pdf"
          },
          {
            "nombre": "Formulario B2",
            "nombre_archivo": "Formulario_B2.pdf"
          },
          {
            "nombre": "Formulario B5",
            "nombre_archivo": "Formulario_B5.pdf"
          }
        ]
      }
    }
   */
  app.api.get('/proyectos/empresas/:matricula/:id', app.controller.proyecto.obtenerMatriculaId);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Proyectos
   * @apiName Adjudicar proyectos a matrícula de empresa
   * @api {post} /api/v1/adjudicar Adjudica un proyecto a una matrícula de una empresa
   *
   * @apiDescription Post para adjudicar proyecto a una matrícula de empresa
   *
   * @apiParamExample {json} Ejemplo para enviar:
   *{
      "matricula": "00101952",
      "codigo_acceso": "HyfxYhi8a-"
    }
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
   *{
      "finalizado": true,
      "mensaje": "Proyectos recuperados correctamente.",
      "datos": {
        "id_proyecto": 2,
        "version": 1,
        "codigo": "ByEF-nLaZ",
        "tipo": "TP_CIFE",
        "nombre": "Proyecto de Prueba",
        "nro_convenio": "12/2017",
        "monto_convenio": 10000,
        "monto_total": 10000,
        "plazo_ejecucion": 180,
        "entidad_beneficiaria": "Gobierno Municipal de La Paz",
        "fecha_suscripcion_convenio": "2017-12-31T04:00:00.000Z",
        "doc_convenio": "S1xVtZnLaZ_convenio.txt",
        "nro_resmin_cife": null,
        "doc_resmin_cife": "HJZ4YZ28Tb_resolucion.txt",
        "doc_cert_presupuestaria": "BJMcW286-_presupuestaria.txt",
        "desembolso": 2000,
        "porcentaje_desembolso": 20,
        "fecha_desembolso": "2017-10-31T04:00:00.000Z",
        "anticipo": 1000,
        "porcentaje_anticipo": 10,
        "sector": "Sector",
        "fid_autoridad_beneficiaria": 9,
        "cargo_autoridad_beneficiaria": "Alcalde",
        "fax_autoridad_beneficiaria": null,
        "fcod_municipio": "020101",
        "coordenadas": null,
        "doc_especificaciones_tecnicas": "H1ht-28aZ_especificaciones.txt",
        "doc_base_contratacion": "HJghKZnL6Z_contratacion.txt",
        "orden_proceder": null,
        "fecha_fin_proyecto": null,
        "codigo_acceso": "rkMNFb28TW",
        "matricula": "00101952",
        "estado_proyecto": "REGISTRO_EMPRESA",
        "fid_usuario_asignado": 5,
        "estado": "ACTIVO",
        "_usuario_creacion": 2,
        "_usuario_modificacion": 3,
        "_fecha_creacion": "2017-10-19T23:12:59.820Z",
        "_fecha_modificacion": "2017-10-19T23:15:48.476Z"
      }
    }
   * @apiErrorExample {json} Error-Response:
   * HTTP/1.1 412 Petición incorrecta
   *{
      "finalizado": false,
      "mensaje": "No se encontró el código indicado.",
      "datos": null
    }
   */
  app.api.post('/adjudicar', app.controller.proyecto.adjudicar);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Proyectos
   * @apiName Get proyectos procesados
   * @api {get} /api/v1/proyectos/procesados Obtiene los proyectos procesados
   *
   * @apiDescription Get proyectos, Obtiene los proyectos procesados
   *
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
   *{
      "finalizado": true,
      "mensaje": "Proyectos recuperados correctamente.",
      "datos": {
        "count": 1,
        "rows": [
          {
            "id_proyecto": 2,
            "version": 1,
            "codigo": "r1iCtrtnW",
            "tipo": "TP_CIFE",
            "nombre": "Proyecto de Prueba",
            "nro_convenio": "12/2017",
            "monto_total": 10000,
            "plazo_ejecucion": 180,
            "entidad_beneficiaria": "Gobierno Municipal de La Paz",
            "fecha_suscripcion_convenio": "2017-12-31T04:00:00.000Z",
            "doc_convenio": "BJloCFrFn-_convenio.txt",
            "nro_resmin_cife": null,
            "doc_resmin_cife": "S1-sAFHFhZ_resolucion.txt",
            "doc_cert_presupuestaria": "rJ7dsStn-_presupuestaria.txt",
            "desembolso": 1000,
            "porcentaje_desembolso": 10,
            "fecha_desembolso": "2017-10-31T04:00:00.000Z",
            "anticipo": "1000",
            "porcentaje_anticipo": 10,
            "sector": "Sector",
            "fcod_municipio": "020101",
            "coordenadas": null,
            "doc_base_contratacion": "Sk2riSth-_contratacion.txt",
            "orden_proceder": null,
            "fecha_fin_proyecto": null,
            "estado_proyecto": "REGISTRO_LICITACION",
            "fid_usuario_asignado": 5,
            "estado": "ACTIVO",
            "_usuario_creacion": 1,
            "_usuario_modificacion": 1,
            "_fecha_creacion": "2017-10-09T19:11:15.269Z",
            "_fecha_modificacion": "2017-10-09T19:18:03.034Z",
            "transicion": [
              {
                "id_transicion": 1,
                "codigo_estado": "REGISTRO_CONVENIO"
              },
              {
                "id_transicion": 2,
                "codigo_estado": "ASIGNACION_TECNICO"
              },
              {
                "id_transicion": 3,
                "codigo_estado": "ASIGNACION_PROYECTO_REGISTRO_FINANCIERO"
              },
              {
                "id_transicion": 4,
                "codigo_estado": "REGISTRO_FINANCIERO"
              }
            ],
            "usuario": {
              "id_usuario": 1
            }
          }
        ]
      }
    }
   */
  // app.api.get('/proyectos/procesados', app.controller.proyecto.getProcesado);
  app.api.get('/proyectos/procesados', app.controller.proyecto.listarProcesados);

  // Ruta para mostrar proyectos al usuarios con rol 19 o 20
  app.api.get('/proyectos/procesados/mae', app.controller.proyecto.listarProcesadosv2)
  //app.api.get('/proyectos/procesados/movil', app.controller.proyecto.obtenerProcesadoMovil);
  app.api.get('/proyectos/procesados/movil', app.controller.proyecto.listarProcesadosMovil);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Proyectos
   * @apiName Get proyecto procesado
   * @api {get} /api/v1/proyectos/:id/procesados Obtiene un proyecto procesado
   *
   * @apiParam id {string} Identificador del proyecto
   *
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
   {
       "finalizado": true,
       "mensaje": "Proyecto recuperado correctamente.",
       "datos": {
           "id_proyecto": 2,
           "version": 1,
           "codigo": "rkUcUJjDf",
           "tipo": "TP_CIFE",
           "nombre": "TINGLADO DE ESCUELA - CIFE",
           "nro_convenio": "65656",
           "monto_fdi": 1500420,
           "monto_contraparte": 50000,
           "monto_total_convenio": 1550420,
           "monto_total": null,
           "plazo_ejecucion": null,
           "plazo_ejecucion_convenio": 432,
           "entidad_beneficiaria": "GAM CARANAVI DE LA CIUDAD DE LA PAZ - BOLIVIA",
           "fecha_suscripcion_convenio": "2017-12-16T04:00:00.000Z",
           "doc_convenio": "[C] Convenio.pdf",
           "nro_resmin_cife": null,
           "doc_resmin_cife": "[RM] Resolución ministerial.pdf",
           "doc_cert_presupuestaria": "[CIP] Certificado de inscripción presupuestaria.pdf",
           "desembolso": null,
           "porcentaje_desembolso": null,
           "fecha_desembolso": null,
           "anticipo": null,
           "porcentaje_anticipo": null,
           "sector": "ST_SOCIAL",
           "fid_autoridad_beneficiaria": 31,
           "cargo_autoridad_beneficiaria": "Gobernador del municipio",
           "fax_autoridad_beneficiaria": null,
           "fcod_municipio": "022001",
           "coordenadas": null,
           "doc_especificaciones_tecnicas": "[ET] Especificaciones técnicas.pdf",
           "doc_base_contratacion": "[DBC] Documento base de contratación.pdf",
           "orden_proceder": null,
           "fecha_fin_proyecto": null,
           "codigo_acceso": "BJlVcliDf",
           "matricula": null,
           "cod_documentacion_exp": null,
           "documentacion_exp": null,
           "estado_proyecto": "ADJUDICACION",
           "fid_usuario_asignado": 21,
           "nro_invitacion": null,
           "estado": "ACTIVO",
           "_usuario_creacion": 2,
           "_usuario_modificacion": 21,
           "_fecha_creacion": "2018-02-21T12:31:10.135Z",
           "_fecha_modificacion": "2018-02-21T13:54:48.346Z",
           "equipo_tecnico": [
               {
                   "id_equipo_tecnico": 4,
                   "fid_proyecto": 2,
                   "tipo_equipo": "TE_UPRE",
                   "fid_usuario": 21,
                   "fid_rol": 5,
                   "cargo": "CG_TECNICO",
                   "doc_designacion": null,
                   "estado": "ACTIVO",
                   "_usuario_creacion": 8,
                   "_usuario_modificacion": null,
                   "_fecha_creacion": "2018-02-21T12:32:45.434Z",
                   "_fecha_modificacion": "2018-02-21T12:32:45.434Z"
               }
           ],
           "contrato": [],
           "boletas": [],
           "estado_actual": {
               "codigo": "ADJUDICACION",
               "nombre": "En proceso de adjudicación",
               "areas": [
                   "DATOS_GENERALES",
                   "CONVENIO",
                   "DESEMBOLSO",
                   "AUTORIDAD_BENEFICIARIA",
                   "MODULOS_ITEMS",
                   "DOCUMENTO_BASE_CONTRATACION"
               ]
           },
           "tipo_proyecto": {
               "nombre": "CIFE"
           },
           "autoridad_beneficiaria": {
               "tipo_documento": "TD_CI",
               "numero_documento": "4192688",
               "fecha_nacimiento": "2010-04-27T04:00:00.000Z",
               "nombres": "REMMY",
               "primer_apellido": "CALLAU",
               "segundo_apellido": "ALVAREZ",
               "correo": null,
               "telefono": "74564465"
           },
           "dpa": {
               "departamento": {
                   "codigo": "02",
                   "nombre": "La Paz"
               },
               "provincia": {
                   "codigo": "0220",
                   "nombre": "Caranavi",
                   "codigo_departamento": "02"
               },
               "municipio": {
                   "codigo": "022001",
                   "nombre": "Caranavi",
                   "codigo_provincia": "0220"
               }
           }
       }
    }
   */
  app.api.get('/proyectos/:id/procesados', app.controller.proyecto.obtenerProcesado);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Proyectos
   * @apiName Get proyectos
   * @api {get} /api/v1/proyectos Obtiene los proyectos
   *
   * @apiDescription Get proyectos, Obtiene los proyectos y opcionalmente se puede indicar el estado
   *
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
   *{
      "finalizado": true,
      "mensaje": "Proyectos recuperados correctamente.",
      "datos": {
        "count": 16,
        "rows": [
          {
            "id_proyecto": 2,
            "version": 1,
            "codigo": "S1l83dSnZ",
            "tipo": "TP_CIFE",
            "nombre": "Proyecto de Prueba",
            "nro_convenio": "12/2017",
            "monto_total": 10000,
            "plazo_ejecucion": 180,
            "entidad_beneficiaria": "Gobierno Municipal de La Paz",
            "fecha_suscripcion_convenio": "2017-12-31T04:00:00.000Z",
            "doc_convenio": "r1xlL2dH2b_convenio.txt",
            "nro_resmin_cife": null,
            "doc_resmin_cife": "ByZlL3OBhb_resolucion.txt",
            "doc_cert_presupuestaria": "HyNa2OH2Z_presupuestaria.txt",
            "desembolso": 1000,
            "fecha_desembolso": "2017-10-31T04:00:00.000Z",
            "porcentaje_desembolso": 10,
            "anticipo": 1000,
            "porcentaje_anticipo": 10,
            "sector": "Sector",
            "fcod_municipio": "020101",
            "coordenadas": null,
            "doc_base_contratacion": "H112hur3Z_contratacion.txt",
            "orden_proceder": null,
            "fecha_fin_proyecto": null,
            "estado_proyecto": "REGISTRO_LICITACION",
            "fid_usuario_asignado": 5,
            "estado": "ACTIVO",
            "_usuario_creacion": 1,
            "_usuario_modificacion": 1022579028,
            "_fecha_creacion": "2017-10-06T21:57:27.715Z",
            "_fecha_modificacion": "2017-10-06T22:17:09.075Z",
            "contrato": [
              {
                "id_contrato": 2,
                "fid_empresa": 2,
                "fid_proyecto": 2,
                "nro_invitacion": "123456",
                "nro_cuce": "9514",
                "matricula": "00101952",
                "fecha_inscripcion": null,
                "nombre_representante_legal": "TEODOVICH CHAVEZ ANA HILDA",
                "ci_representante_legal": null,
                "numero_testimonio": null,
                "lugar_emision": null,
                "fecha_expedicion": null,
                "fax": null,
                "correo": null,
                "estado": "ACTIVO",
                "_usuario_creacion": 1022579028,
                "_usuario_modificacion": null,
                "_fecha_creacion": "2017-10-06T22:00:48.725Z",
                "_fecha_modificacion": "2017-10-06T22:00:48.725Z",
                "empresa": {
                  "id_empresa": 2,
                  "nit": "03275482018",
                  "nombre": "MR. CAFE",
                  "tipo": "NACIONAL",
                  "otro": null,
                  "pais": "BOLIVIA",
                  "ciudad": "SANTA CRUZ",
                  "direccion": "AEROPUERTO VIRU VIRU- EMBARQUE NACIONAL, PISO 2",
                  "telefono": "72159590",
                  "estado": "ACTIVO",
                  "_usuario_creacion": 1022579028,
                  "_usuario_modificacion": null,
                  "_fecha_creacion": "2017-10-06T22:00:48.718Z",
                  "_fecha_modificacion": "2017-10-06T22:00:48.718Z",
                  "fid_representante_legal": null
                }
              },
              {
                "id_contrato": 3,
                "fid_empresa": 2,
                "fid_proyecto": 2,
                "nro_invitacion": "123456",
                "nro_cuce": "9514",
                "matricula": "00101952",
                "fecha_inscripcion": null,
                "nombre_representante_legal": "TEODOVICH CHAVEZ ANA HILDA",
                "ci_representante_legal": null,
                "numero_testimonio": null,
                "lugar_emision": null,
                "fecha_expedicion": null,
                "fax": null,
                "correo": null,
                "estado": "ACTIVO",
                "_usuario_creacion": 1022579028,
                "_usuario_modificacion": null,
                "_fecha_creacion": "2017-10-06T22:13:37.805Z",
                "_fecha_modificacion": "2017-10-06T22:13:37.805Z",
                "empresa": {
                  "id_empresa": 2,
                  "nit": "03275482018",
                  "nombre": "MR. CAFE",
                  "tipo": "NACIONAL",
                  "otro": null,
                  "pais": "BOLIVIA",
                  "ciudad": "SANTA CRUZ",
                  "direccion": "AEROPUERTO VIRU VIRU- EMBARQUE NACIONAL, PISO 2",
                  "telefono": "72159590",
                  "estado": "ACTIVO",
                  "_usuario_creacion": 1022579028,
                  "_usuario_modificacion": null,
                  "_fecha_creacion": "2017-10-06T22:00:48.718Z",
                  "_fecha_modificacion": "2017-10-06T22:00:48.718Z",
                  "fid_representante_legal": null
                }
              },
              {
                "id_contrato": 4,
                "fid_empresa": 2,
                "fid_proyecto": 2,
                "nro_invitacion": "123456",
                "nro_cuce": "9514",
                "matricula": "00101952",
                "fecha_inscripcion": null,
                "nombre_representante_legal": "TEODOVICH CHAVEZ ANA HILDA",
                "ci_representante_legal": null,
                "numero_testimonio": null,
                "lugar_emision": null,
                "fecha_expedicion": null,
                "fax": null,
                "correo": null,
                "estado": "ACTIVO",
                "_usuario_creacion": 1022579028,
                "_usuario_modificacion": null,
                "_fecha_creacion": "2017-10-06T22:14:06.253Z",
                "_fecha_modificacion": "2017-10-06T22:14:06.253Z",
                "empresa": {
                  "id_empresa": 2,
                  "nit": "03275482018",
                  "nombre": "MR. CAFE",
                  "tipo": "NACIONAL",
                  "otro": null,
                  "pais": "BOLIVIA",
                  "ciudad": "SANTA CRUZ",
                  "direccion": "AEROPUERTO VIRU VIRU- EMBARQUE NACIONAL, PISO 2",
                  "telefono": "72159590",
                  "estado": "ACTIVO",
                  "_usuario_creacion": 1022579028,
                  "_usuario_modificacion": null,
                  "_fecha_creacion": "2017-10-06T22:00:48.718Z",
                  "_fecha_modificacion": "2017-10-06T22:00:48.718Z",
                  "fid_representante_legal": null
                }
              },
              {
                "id_contrato": 5,
                "fid_empresa": 2,
                "fid_proyecto": 2,
                "nro_invitacion": "123456",
                "nro_cuce": "9514",
                "matricula": "00101952",
                "fecha_inscripcion": null,
                "nombre_representante_legal": "TEODOVICH CHAVEZ ANA HILDA",
                "ci_representante_legal": null,
                "numero_testimonio": null,
                "lugar_emision": null,
                "fecha_expedicion": null,
                "fax": null,
                "correo": null,
                "estado": "ACTIVO",
                "_usuario_creacion": 1022579028,
                "_usuario_modificacion": null,
                "_fecha_creacion": "2017-10-06T22:17:09.084Z",
                "_fecha_modificacion": "2017-10-06T22:17:09.084Z",
                "empresa": {
                  "id_empresa": 2,
                  "nit": "03275482018",
                  "nombre": "MR. CAFE",
                  "tipo": "NACIONAL",
                  "otro": null,
                  "pais": "BOLIVIA",
                  "ciudad": "SANTA CRUZ",
                  "direccion": "AEROPUERTO VIRU VIRU- EMBARQUE NACIONAL, PISO 2",
                  "telefono": "72159590",
                  "estado": "ACTIVO",
                  "_usuario_creacion": 1022579028,
                  "_usuario_modificacion": null,
                  "_fecha_creacion": "2017-10-06T22:00:48.718Z",
                  "_fecha_modificacion": "2017-10-06T22:00:48.718Z",
                  "fid_representante_legal": null
                }
              }
            ],
            "equipo_tecnico": [
              {
                "id_equipo_tecnico": 1,
                "fid_proyecto": 2,
                "tipo_equipo": "TE_UPRE",
                "fid_usuario": 1,
                "cargo": "CG_ENCARGADO",
                "estado": "ACTIVO",
                "_usuario_creacion": 1,
                "_usuario_modificacion": null,
                "_fecha_creacion": "2017-10-06T21:57:53.719Z",
                "_fecha_modificacion": "2017-10-06T21:57:53.719Z"
              },
              {
                "id_equipo_tecnico": 4,
                "fid_proyecto": 2,
                "tipo_equipo": "TE_BENEFICIARIO",
                "fid_usuario": 9,
                "cargo": "CG_FISCAL",
                "estado": "ACTIVO",
                "_usuario_creacion": 1,
                "_usuario_modificacion": null,
                "_fecha_creacion": "2017-10-06T21:59:02.921Z",
                "_fecha_modificacion": "2017-10-06T21:59:02.921Z"
              },
              {
                "id_equipo_tecnico": 3,
                "fid_proyecto": 2,
                "tipo_equipo": "TE_BENEFICIARIO",
                "fid_usuario": 8,
                "cargo": "CG_SUPERVISOR",
                "estado": "ACTIVO",
                "_usuario_creacion": 1,
                "_usuario_modificacion": null,
                "_fecha_creacion": "2017-10-06T21:59:02.921Z",
                "_fecha_modificacion": "2017-10-06T21:59:02.921Z"
              },
              {
                "id_equipo_tecnico": 2,
                "fid_proyecto": 2,
                "tipo_equipo": "TE_UPRE",
                "fid_usuario": 5,
                "cargo": "CG_TECNICO",
                "estado": "ACTIVO",
                "_usuario_creacion": 1,
                "_usuario_modificacion": null,
                "_fecha_creacion": "2017-10-06T21:57:53.719Z",
                "_fecha_modificacion": "2017-10-06T21:57:53.719Z"
              }
            ],
            "estado_actual": {
              "codigo": "REGISTRO_LICITACION",
              "codigo_proceso": "PROYECTO",
              "nombre": "Registro empresa",
              "tipo": "FLUJO",
              "fid_rol": [
                1,
                8
              ],
              "fid_rol_asignado": [],
              "acciones": [
                {
                  "ruta": "/temporal",
                  "tipo": "ventana"
                },
                {
                  "tipo": "boton",
                  "label": "Enviar",
                  "estado": "REVISION"
                }
              ],
              "atributos": [
                "contrato",
                "estado_proyecto",
                "_usuario_modificacion"
              ],
              "requeridos": [
                "contrato"
              ],
              "estado": "ACTIVO",
              "_usuario_creacion": 1,
              "_usuario_modificacion": null,
              "_fecha_creacion": "2017-10-06T21:56:33.845Z",
              "_fecha_modificacion": "2017-10-06T21:56:33.845Z"
            }
          }
        ]
      }
    }
  */
  app.api.get('/proyectos', validate(paramValidation.get), app.controller.proyecto.obtener); //lista pendientes
  app.api.options('/proyectos', sequelizeFormly.formly(app.src.db.models.proyecto, app.src.db.models));

  /**
   * @apiVersion 1.0.0
   * @apiGroup Proyectos
   * @apiName Admin obtener
   * @api {get} /api/v1/adminproyectos Obtiene una lista de todos los proyectos
   *
   * @apiDescription Get Admin proyectos, Obtiene una lista de todos los proyectos registrados en el sistema
   *
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
   *{
      "finalizado": true
      "mensaje": "Proyectos recuperados correctamente."
      "datos": {
        "count": 11, //Total de registros en BD, se recuperan menos según paginación
        "rows": [
          {
            "anticipo": null,
            "cargo_autoridad_beneficiaria": "Responsable GAM/GAIOC Aprobación Proyectos",
            "cod_documentacion_exp": null,
            "codigo": "BJYtqmnMm",
            "codigo_acceso": null,
            "coordenadas": null,
            "desembolso": null,
            "doc_base_contratacion": null,
            "doc_cert_presupuestaria": null,
            "doc_convenio": null,
            "doc_especificaciones_tecnicas": null,
            "doc_ev_externa": "[EVE] Evaluacion externa.pdf",
            "doc_resmin_cife": null,
            "documentacion_exp": null,
            "entidad_beneficiaria": "test",
            "estado": "ACTIVO",
            "estado_proyecto": "REGISTRO_CONVENIO_FDI",
            "fax_autoridad_beneficiaria": null,
            "fcod_municipio": "020101",
            "fecha_desembolso": null,
            "fecha_fin_proyecto": null,
            "fecha_suscripcion_convenio": "2018-07-05T04:00:00.000Z",
            "fid_autoridad_beneficiaria": 37,
            "fid_usuario_asignado": 32,
            "id_proyecto": 1,
            "matricula": null,
            "monto_contraparte": null,
            "monto_fdi": 450217,
            "monto_total": 492085,
            "monto_total_convenio": null,
            "nombre": "Test",
            "nro_convenio": "FDI/DTP/ARTP/0001-2018",
            "nro_invitacion": null,
            "nro_resmin_cife": null,
            "orden_proceder": null,
            "plazo_ejecucion": 234,
            "plazo_ejecucion_convenio": null,
            "porcentaje_anticipo": null,
            "porcentaje_desembolso": null,
            "sector": null,
            "tipo": "TP_CIFE",
            "version": 1,
            "_fecha_creacion": "2018-07-05T23:32:17.441Z",
            "_fecha_modificacion": "2018-07-05T23:46:49.188Z"
          },
          {
            "anticipo": null,
            "cargo_autoridad_beneficiaria": "Responsable GAM/GAIOC Aprobación Proyectos",
            "cod_documentacion_exp": null,
            "codigo": "B10h372z7",
            "codigo_acceso": null,
            "coordenadas": null,
            "desembolso": null,
            "doc_base_contratacion": null,
            "doc_cert_presupuestaria": null,
            "doc_convenio": null,
            "doc_especificaciones_tecnicas": null,
            "doc_ev_externa": null,
            "doc_resmin_cife": null,
            "documentacion_exp": null,
            "entidad_beneficiaria": "prueba",
            "estado": "ACTIVO",
            "estado_proyecto": "REGISTRO_CONVENIO_FDI",
            "fax_autoridad_beneficiaria": null,
            "fcod_municipio": "020101",
            "fecha_desembolso": null,
            "fecha_fin_proyecto": null,
            "fecha_suscripcion_convenio": "2018-07-05T04:00:00.000Z",
            "fid_autoridad_beneficiaria": 39,
            "fid_usuario_asignado": 32,
            "id_proyecto": 3,
            "matricula": null,
            "monto_contraparte": null,
            "monto_fdi": 512204.68,
            "monto_total": 857894.68,
            "monto_total_convenio": null,
            "nombre": "prueba",
            "nro_convenio": "FDI/DTP/ARTP/0002-2018"
            "nro_invitacion": null,
            "nro_resmin_cife": null,
            "orden_proceder": null,
            "plazo_ejecucion": 34,
            "plazo_ejecucion_convenio": null,
            "porcentaje_anticipo": null,
            "porcentaje_desembolso": null,
            "sector": null,
            "tipo": "TP_CIFE",
            "version": 1,
            "_fecha_creacion": "2018-07-05T23:41:42.385Z",
            "_fecha_modificacion": "2018-07-05T23:52:15.787Z"
          }
        ]
      }
    }
   */
  app.api.get('/adminproyectos', validate(paramValidation.getAdmin), app.controller.proyecto.adminObtener);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Proyectos
   * @apiName Admin obtener usuario
   * @api {get} /api/v1/adminproyectos/:codigo/usuarios Obtiene los datos de un proyecto junto con una lista de usuarios
   *
   * @apiDescription Get Admin obtener usuario, Obtener datos de los usuarios asignados a un proyecto.
   *
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
   *{
      "finalizado": true,
      "mensaje": "Proyecto recuperado correctamente",
      "datos": {
        "estado_proyecto": "REGISTRO_CONVENIO_FDI",
        id_proyecto: 1,
        "municipio": {
          "nombre": "Nuestra Señora de La Paz",
          "provincia": {
            "nombre": "Murillo",
            "departamento": {
              "nombre": "La Paz"
            }
          }
        },
        "nombre": "Test",
        "tipo": "TP_CIFE",
        "transicion": [
          {
            "objeto": 1,
            "tipo": "proyecto",
            "usuario_asignado": {
              "nro": 32,
              "persona": {
                "nombres": "TECNICO_FDI",
                "primer_apellido": null,
                "segundo_apellido": null
              }
            },
            "usuario_asignado_rol": {
              "descripcion": "Técnico Responsable",
              "estado": "ACTIVO",
              "nombre": "TECNICO_FDI",
              "nro": 13
            }
          },
          {
            "objeto": 1,
            "tipo": "proyecto",
            "usuario_asignado": {
              "nro": 31,
              "persona": {
                "nombres": "JEFE_REVISION",
                "primer_apellido": null,
                "segundo_apellido": null
              }
            },
            "usuario_asignado_rol": {
              "descripcion": "Jefe de la Unidad de Revisión",
              "estado": "ACTIVO",
              "nombre": "JEFE_REVISION_FDI",
              "nro": 12
            }
          },
          {
            "objeto": 1,
            "tipo": "proyecto",
            "usuario_asignado": {
              "nro": 34,
              "persona": {
                "nombres": "LEGAL_FDI",
                "primer_apellido": null,
                "segundo_apellido": null
              }
            },
            "usuario_asignado_rol": {
              "descripcion": "Técnico Legal",
              "estado": "ACTIVO",
              "nombre": "LEGAL_FDI",
              "nro": 14
            }
          }
        ],
        "usuario": {
          "nro": 32,
          "persona": {
            "nombres": "TECNICO FDI",
            "primer_apellido": null,
            "segundo_apellido": null
          }
        },
        "usuario_rol": {
          "descripcion": "Técnico Responsable",
          "estado": "ACTIVO",
          "nombre": "TECNICO_FDI",
          "nro": 13
        }
      }
    }
   */
  app.api.get('/adminproyectos/:codigo/usuarios', validate(paramValidation.getAdminUsuario), app.controller.proyecto.adminObtenerUsuario);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Proyectos
   * @apiName Admin Modificar Usuario
   * @api {put} /api/v1/adminproyectos/:codigo/usuarios Modificar usuario
   *
   * @apiDescription Put para modificar usuario asignado a un proyecto.
   *
   * @apiParam (codigo) {numeric} Identificador de usuario
   *
   * @apiParamExample {json} Ejemplo para enviar:
   *{
      "nro_rol": 13,
      "nro_usuario": 32,
      "nuevo_nro_rol": 13,
      "nuevo_nro_usuario": 33
    }
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
   *{
      "finalizado": true,
      "mensaje": "Usuarios reasignados correctamente.",
      "datos": [3]
    }
   */
  app.api.put('/adminproyectos/:codigo/usuarios', validate(paramValidation.putAdminUsuario), app.controller.proyecto.adminModificarUsuario);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Proyectos
   * @apiName Get proyecto
   * @api {get} /api/v1/proyectos/:id Obtiene el proyecto
   *
   * @apiParam id {string} Identificador del proyecto
   *
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Proyectos recuperados correctamente.",
      "datos": {
        "id_proyecto": 2,
        "version": 1,
        "codigo": "S1l83dSnZ",
        "tipo": "TP_CIFE",
        "nombre": "Proyecto de Prueba",
        "nro_convenio": "12/2017",
        "monto_total": 10000,
        "plazo_ejecucion": 180,
        "entidad_beneficiaria": "Gobierno Municipal de La Paz",
        "fecha_suscripcion_convenio": "2017-12-31T04:00:00.000Z",
        "doc_convenio": "r1xlL2dH2b_convenio.txt",
        "nro_resmin_cife": null,
        "doc_resmin_cife": "ByZlL3OBhb_resolucion.txt",
        "doc_cert_presupuestaria": "HyNa2OH2Z_presupuestaria.txt",
        "desembolso": 1000,
        "fecha_desembolso": "2017-10-31T04:00:00.000Z",
        "porcentaje_desembolso": 10,
        "anticipo": 1000,
        "porcentaje_anticipo": 10,
        "sector": "Sector",
        "fcod_municipio": "020101",
        "coordenadas": null,
        "doc_base_contratacion": "H112hur3Z_contratacion.txt",
        "orden_proceder": null,
        "fecha_fin_proyecto": null,
        "estado_proyecto": "REGISTRO_LICITACION",
        "fid_usuario_asignado": 5,
        "estado": "ACTIVO",
        "_usuario_creacion": 1,
        "_usuario_modificacion": 1022579028,
        "_fecha_creacion": "2017-10-06T21:57:27.715Z",
        "_fecha_modificacion": "2017-10-06T22:17:09.075Z",
        "contrato": [
          {
            "id_contrato": 2,
            "fid_empresa": 2,
            "fid_proyecto": 2,
            "nro_invitacion": "123456",
            "nro_cuce": "9514",
            "matricula": "00101952",
            "fecha_inscripcion": null,
            "nombre_representante_legal": "TEODOVICH CHAVEZ ANA HILDA",
            "ci_representante_legal": null,
            "numero_testimonio": null,
            "lugar_emision": null,
            "fecha_expedicion": null,
            "fax": null,
            "correo": null,
            "estado": "ACTIVO",
            "_usuario_creacion": 1022579028,
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-10-06T22:00:48.725Z",
            "_fecha_modificacion": "2017-10-06T22:00:48.725Z",
            "empresa": {
              "id_empresa": 2,
              "nit": "03275482018",
              "nombre": "MR. CAFE",
              "tipo": "NACIONAL",
              "otro": null,
              "pais": "BOLIVIA",
              "ciudad": "SANTA CRUZ",
              "direccion": "AEROPUERTO VIRU VIRU- EMBARQUE NACIONAL, PISO 2",
              "telefono": "72159590",
              "estado": "ACTIVO",
              "_usuario_creacion": 1022579028,
              "_usuario_modificacion": null,
              "_fecha_creacion": "2017-10-06T22:00:48.718Z",
              "_fecha_modificacion": "2017-10-06T22:00:48.718Z",
              "fid_representante_legal": null
            }
          },
          {
            "id_contrato": 3,
            "fid_empresa": 2,
            "fid_proyecto": 2,
            "nro_invitacion": "123456",
            "nro_cuce": "9514",
            "matricula": "00101952",
            "fecha_inscripcion": null,
            "nombre_representante_legal": "TEODOVICH CHAVEZ ANA HILDA",
            "ci_representante_legal": null,
            "numero_testimonio": null,
            "lugar_emision": null,
            "fecha_expedicion": null,
            "fax": null,
            "correo": null,
            "estado": "ACTIVO",
            "_usuario_creacion": 1022579028,
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-10-06T22:13:37.805Z",
            "_fecha_modificacion": "2017-10-06T22:13:37.805Z",
            "empresa": {
              "id_empresa": 2,
              "nit": "03275482018",
              "nombre": "MR. CAFE",
              "tipo": "NACIONAL",
              "otro": null,
              "pais": "BOLIVIA",
              "ciudad": "SANTA CRUZ",
              "direccion": "AEROPUERTO VIRU VIRU- EMBARQUE NACIONAL, PISO 2",
              "telefono": "72159590",
              "estado": "ACTIVO",
              "_usuario_creacion": 1022579028,
              "_usuario_modificacion": null,
              "_fecha_creacion": "2017-10-06T22:00:48.718Z",
              "_fecha_modificacion": "2017-10-06T22:00:48.718Z",
              "fid_representante_legal": null
            }
          },
          {
            "id_contrato": 4,
            "fid_empresa": 2,
            "fid_proyecto": 2,
            "nro_invitacion": "123456",
            "nro_cuce": "9514",
            "matricula": "00101952",
            "fecha_inscripcion": null,
            "nombre_representante_legal": "TEODOVICH CHAVEZ ANA HILDA",
            "ci_representante_legal": null,
            "numero_testimonio": null,
            "lugar_emision": null,
            "fecha_expedicion": null,
            "fax": null,
            "correo": null,
            "estado": "ACTIVO",
            "_usuario_creacion": 1022579028,
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-10-06T22:14:06.253Z",
            "_fecha_modificacion": "2017-10-06T22:14:06.253Z",
            "empresa": {
              "id_empresa": 2,
              "nit": "03275482018",
              "nombre": "MR. CAFE",
              "tipo": "NACIONAL",
              "otro": null,
              "pais": "BOLIVIA",
              "ciudad": "SANTA CRUZ",
              "direccion": "AEROPUERTO VIRU VIRU- EMBARQUE NACIONAL, PISO 2",
              "telefono": "72159590",
              "estado": "ACTIVO",
              "_usuario_creacion": 1022579028,
              "_usuario_modificacion": null,
              "_fecha_creacion": "2017-10-06T22:00:48.718Z",
              "_fecha_modificacion": "2017-10-06T22:00:48.718Z",
              "fid_representante_legal": null
            }
          },
          {
            "id_contrato": 5,
            "fid_empresa": 2,
            "fid_proyecto": 2,
            "nro_invitacion": "123456",
            "nro_cuce": "9514",
            "matricula": "00101952",
            "fecha_inscripcion": null,
            "nombre_representante_legal": "TEODOVICH CHAVEZ ANA HILDA",
            "ci_representante_legal": null,
            "numero_testimonio": null,
            "lugar_emision": null,
            "fecha_expedicion": null,
            "fax": null,
            "correo": null,
            "estado": "ACTIVO",
            "_usuario_creacion": 1022579028,
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-10-06T22:17:09.084Z",
            "_fecha_modificacion": "2017-10-06T22:17:09.084Z",
            "empresa": {
              "id_empresa": 2,
              "nit": "03275482018",
              "nombre": "MR. CAFE",
              "tipo": "NACIONAL",
              "otro": null,
              "pais": "BOLIVIA",
              "ciudad": "SANTA CRUZ",
              "direccion": "AEROPUERTO VIRU VIRU- EMBARQUE NACIONAL, PISO 2",
              "telefono": "72159590",
              "estado": "ACTIVO",
              "_usuario_creacion": 1022579028,
              "_usuario_modificacion": null,
              "_fecha_creacion": "2017-10-06T22:00:48.718Z",
              "_fecha_modificacion": "2017-10-06T22:00:48.718Z",
              "fid_representante_legal": null
            }
          }
        ],
        "equipo_tecnico": [
          {
            "id_equipo_tecnico": 1,
            "fid_proyecto": 2,
            "tipo_equipo": "TE_UPRE",
            "fid_usuario": 1,
            "cargo": "CG_ENCARGADO",
            "estado": "ACTIVO",
            "_usuario_creacion": 1,
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-10-06T21:57:53.719Z",
            "_fecha_modificacion": "2017-10-06T21:57:53.719Z"
          },
          {
            "id_equipo_tecnico": 4,
            "fid_proyecto": 2,
            "tipo_equipo": "TE_BENEFICIARIO",
            "fid_usuario": 9,
            "cargo": "CG_FISCAL",
            "estado": "ACTIVO",
            "_usuario_creacion": 1,
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-10-06T21:59:02.921Z",
            "_fecha_modificacion": "2017-10-06T21:59:02.921Z"
          },
          {
            "id_equipo_tecnico": 3,
            "fid_proyecto": 2,
            "tipo_equipo": "TE_BENEFICIARIO",
            "fid_usuario": 8,
            "cargo": "CG_SUPERVISOR",
            "estado": "ACTIVO",
            "_usuario_creacion": 1,
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-10-06T21:59:02.921Z",
            "_fecha_modificacion": "2017-10-06T21:59:02.921Z"
          },
          {
            "id_equipo_tecnico": 2,
            "fid_proyecto": 2,
            "tipo_equipo": "TE_UPRE",
            "fid_usuario": 5,
            "cargo": "CG_TECNICO",
            "estado": "ACTIVO",
            "_usuario_creacion": 1,
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-10-06T21:57:53.719Z",
            "_fecha_modificacion": "2017-10-06T21:57:53.719Z"
          }
        ],
        "estado_actual": {
          "codigo": "REGISTRO_LICITACION",
          "codigo_proceso": "PROYECTO",
          "nombre": "Registro empresa",
          "tipo": "FLUJO",
          "fid_rol": [
            1,
            8
          ],
          "fid_rol_asignado": [],
          "acciones": [
            {
              "ruta": "/temporal",
              "tipo": "ventana"
            },
            {
              "tipo": "boton",
              "label": "Enviar",
              "estado": "REVISION"
            }
          ],
          "atributos": [
            "contrato",
            "estado_proyecto",
            "_usuario_modificacion"
          ],
          "requeridos": [
            "contrato"
          ],
          "estado": "ACTIVO",
          "_usuario_creacion": 1,
          "_usuario_modificacion": null,
          "_fecha_creacion": "2017-10-06T21:56:33.845Z",
          "_fecha_modificacion": "2017-10-06T21:56:33.845Z"
        }
      }
    }
   */
  app.api.get('/proyectos/:id', validate(paramValidation.getId), app.controller.proyecto.obtenerPorId);//solo proyecto
  
  app.api.get('/proyectos/:id/duplicados', validate(paramValidation.getId), app.controller.proyecto.obtenerDuplicados);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Proyectos
   * @apiName Post proyectos
   * @api {post} /api/v1/proyectos Crear proyecto
   *
   * @apiDescription Post para proyecto
   *
   * @apiParamExample {json} Ejemplo para enviar:
   * LEGAL
   *{
      "tipo": "TP_CIFE",
      "nombre": "Proyecto de Prueba",
      "nro_convenio": "12/2017",
      "monto_total_convenio": 10000,
      "plazo_ejecucion_convenio": 180,
      "entidad_beneficiaria": "Gobierno Municipal de La Paz",
      "fecha_suscripcion_convenio": "31-12-2017",
      "doc_convenio": {
        "path": "convenio.txt",
        "base64": "cHJvYmFuZG8="
      },
      "doc_resmin_cife": {
        "path": "resolucion.txt",
        "base64": "cHJvYmFuZG8="
      },
      "fcod_municipio": "020101"
    }
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Proyecto registrado correctamente.",
      "datos": {
        "estado": "ACTIVO",
        "id_proyecto": 3,
        "version": 1,
        "codigo": "ryWA3dSnb",
        "tipo": "TP_CIFE",
        "nombre": "Proyecto de Prueba",
        "nro_convenio": "12/2017",
        "monto_total": 10000,
        "plazo_ejecucion": 180,
        "entidad_beneficiaria": "Gobierno Municipal de La Paz",
        "fecha_suscripcion_convenio": "2017-12-31T04:00:00.000Z",
        "doc_convenio": "BkeZCnur2-_convenio.txt",
        "doc_resmin_cife": "S1-ZAndSnb_resolucion.txt",
        "fcod_municipio": "020101",
        "estado_proyecto": "ASIGNACION_TECNICO",
        "_usuario_creacion": 1,
        "_fecha_modificacion": "2017-10-06T21:59:36.940Z",
        "_fecha_creacion": "2017-10-06T21:59:36.898Z",
        "nro_resmin_cife": null,
        "doc_cert_presupuestaria": null,
        "desembolso": null,
        "fecha_desembolso": null,
        "porcentaje_desembolso": null,
        "anticipo": null,
        "porcentaje_anticipo": null,
        "sector": null,
        "coordenadas": null,
        "doc_base_contratacion": null,
        "orden_proceder": null,
        "fecha_fin_proyecto": null,
        "fid_usuario_asignado": null,
        "_usuario_modificacion": null,
        "modulos": [],
        "equipo_tecnico": []
      }
    }
   * @apiErrorExample {json} Error-Response:
   * HTTP/1.1 412 Petición incorrecta
    {
      "finalizado": false,
      "mensaje": "No tiene los privilegios requeridos para realizar esta acción.",
      "datos": null
    }
   */
  app.api.post('/proyectos', validate(paramValidation.create), app.controller.proyecto.crear);
  app.api.post('/proyectos/reg', validate(paramValidation.regCreate), app.controller.proyecto.crear);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Proyectos
   * @apiName Put proyectos
   * @api {put} /api/v1/proyectos/:id Modificar proyecto
   *
   * @apiDescription Put para proyecto
   *
   * @apiParam (id) {numeric} Identificador del proyecto a modificar
   *
   * @apiParamExample {json} Ejemplo para enviar:
   * ASIGNACIÖN DE TECNICO RESPONSABLE
    {
      "estado_proyecto": "REGISTRO_PROYECTO_FDI",
      "fid_usuario_asignado": 32,
      "fid_usuario_asignado_rol": 13,
      "rol": 12
    }
   * ASIGNACIÓN DE USUARIO
    {
      "fid_usuario_asignado": 5,
      "estado_proyecto": "ASIGNACION_PROYECTO_REGISTRO_FINANCIERO"
    }
   * TECNICO
    {
      "sector": "Sector",
      "doc_base_contratacion": {
        "path": "contratacion.txt",
        "base64": "cHJvYmFuZG8="
      },
      "doc_especificaciones_tecnicas": {
        "path": "especificaciones.txt",
        "base64": "cHJvYmFuZG8="
      },
      "autoridad_beneficiaria": {
        "tipo_documento": "TD_CI",
        "numero_documento": "10783821",
        "fecha_nacimiento": "26-10-1967",
        "nombres": "JESSENIA",
        "primer_apellido": "ARREDONDO",
        "segundo_apellido": "",
        "telefono": "79848948",
        "correo": "jarredondo@mail.com"
      },
      "cargo_autoridad_beneficiaria": "Alcalde",
      "fax_autoridad_beneficiaria": "",
      "estado_proyecto": "REGISTRO_FINANCIERO",
      "equipo_tecnico": [
        {
          "cargo": "CG_SUPERVISOR",
          "persona": {
            "tipo_documento": "TD_CI",
            "numero_documento": "4175208",
            "fecha_nacimiento": "10-02-1966",
            "nombres": "EDDY",
            "primer_apellido": "ALVAREZ",
            "segundo_apellido": "ARRAZOLA",
            "telefono": "789456123",
            "correo": "ealvarez@mail.com"
          }
        }, {
          "cargo": "CG_FISCAL",
          "persona": {
            "tipo_documento": "TD_CI",
            "numero_documento": "5602708",
            "fecha_nacimiento": "21-07-1983",
            "nombres": "RICARDO",
            "primer_apellido": "AGUILERA",
            "segundo_apellido": "JIMENEZ",
            "telefono": "789456123",
            "correo": "raguilera@mail.com"
          }
        }
      ]
    }
   * FINANCIERA
    {
      "desembolso": 1000,
      "fecha_desembolso": "31-10-2017",
      "porcentaje_desembolso": 10,
      "doc_cert_presupuestaria": {
        "path": "presupuestaria.txt",
        "base64": "cHJvYmFuZG8="
      },
      "anticipo": 1000
      "porcentaje_anticipo" 10,
      "boletas": [
        {
          "fid_proyecto": 1,
          "tipo": "TB_CORRECTA_INVERSION",
          "numero": "456",
          "monto": 500,
          "fecha_inicio_validez": "31-10-2017",
          "fecha_fin_validez": "31-12-2017",
          "doc_boleta_garantia": {
            "path": "boleta.txt",
            "base64": "cHJvYmFuZG8="
          }
        },
        {
          "fid_proyecto": 1,
          "tipo": "TB_CORRECTA_INVERSION",
          "numero": "789",
          "monto": 500,
          "fecha_inicio_validez": "31-10-2017",
          "fecha_fin_validez": "31-12-2017",
          "doc_boleta_garantia": {
            "path": "boleta.txt",
            "base64": "cHJvYmFuZG8="
          }
        }
      ]
    }
   * EMPRESA
    {
      "contrato": {
        "nro_invitacion": "123456",
        "nro_cuce": "9514",
        "nombre_representante_legal": "TEODOVICH CHAVEZ ANA HILDA",
        "numero_testimonio": "123456",
        "lugar_emision": "LA PAZ",
        "fecha_expedicion": "16-07-2017",
        "fax": "25248954",
        "correo": "usuario@mail.com",
        "empresa": {
          "matricula": "00101952",
          "nit": "03275482018",
          "nombre": "MR. CAFE",
          "tipo": "NACIONAL",
          "pais": "BOLIVIA",
          "ciudad": "SANTA CRUZ",
          "direccion": "AEROPUERTO VIRU VIRU- EMBARQUE NACIONAL, PISO 2",
          "telefono": "72159590"
        }
      }
    }
   * ANTICIPOS
    {
      "anticipo": 1000
      "porcentaje_anticipo" 10,
      "boletas": [
        {
          "fid_proyecto": 1,
          "tipo": "TB_CORRECTA_INVERSION",
          "numero": "456",
          "monto": 500,
          "fecha_inicio_validez": "31-10-2017",
          "fecha_fin_validez": "31-12-2017",
          "doc_boleta_garantia": {
            "path": "boleta1.txt",
            "base64": "cHJvYmFuZG8="
          }
        },
        {
          "fid_proyecto": 1,
          "tipo": "TB_CORRECTA_INVERSION",
          "numero": "789",
          "monto": 500,
          "fecha_inicio_validez": "31-10-2017",
          "fecha_fin_validez": "31-12-2017",
          "doc_boleta_garantia": {
            "path": "boleta2.txt",
            "base64": "cHJvYmFuZG8="
          }
        }
      ]
    }

   * EQUIPO_TECNICO
    {
      "equipo_tecnico": [
        {
          "cargo": "CG_SUPERVISOR",
          "doc_designacion": {
            "path": "boleta.txt",
            "base64": "cHJvYmFuZG8="
          },
          "persona": {
            "tipo_documento": "TD_CI",
            "numero_documento": "4175208",
            "fecha_nacimiento": "10-02-1966",
            "nombres": "EDDY",
            "primer_apellido": "ALVAREZ",
            "segundo_apellido": "ARRAZOLA",
            "telefono": "789456123",
            "correo": "ealvarez@mail.com"
          }
        }, {
          "cargo": "CG_FISCAL",
          "persona": {
            "tipo_documento": "TD_CI",
            "numero_documento": "5602708",
            "fecha_nacimiento": "21-07-1983",
            "nombres": "RICARDO",
            "primer_apellido": "AGUILERA",
            "segundo_apellido": "JIMENEZ",
            "telefono": "789456123",
            "correo": "raguilera@mail.com"
          }
        }
      ]
    }
   * ORDERN_PROCEDER
    {
      "orden_proceder": "01-01-2018"
    }
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK

   * @apiErrorExample {json} Error-Response:
   * HTTP/1.1 412 Petición incorrecta
    {
      "finalizado": false,
      "mensaje": "No tiene los privilegios requeridos para realizar esta acción.",
      "datos": null
    }
   */
  app.api.put('/proyectos/:id', validate(paramValidation.update), app.controller.proyecto.actualizar);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Proyectos usuarios
   * @apiName Get usuarios asignables a proyectos
   * @api {get} /api/v1/proyectos/:id/usuarios Obtiene los usuarios asignables al proyecto
   *
   * @apiParam id {string} Identificador del proyecto
   *
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Usuarios recuperados correctamente.",
      "datos": [
        {
          "id_usuario": 4,
          "concat": "Técnico Uno Upre",
          "rol": "TECNICO_UPRE"
        },
        {
          "id_usuario": 5,
          "concat": "Técnico Dos Upre",
          "rol": "TECNICO_UPRE"
        },
        {
          "id_usuario": 6,
          "concat": "Responsable Departamental Upre",
          "rol": "RESP_DEPARTAMENTAL_UPRE"
        }
      ]
    }
   */
  app.api.get('/proyectos/:id/usuarios', validate(paramValidation.getId), app.controller.proyecto.obtenerUsuarios);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Proyectos documentos
   * @apiName Get documentos
   * @api {get} /api/v1/proyectos/:id/documentos/:documento Obtiene los documentos del proyecto
   *
   * @apiParam id {number} Identificador del proyecto
   * @apiParam documento {string} Nombre del proyecto
   *
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Documento recuperado correctamente.",
      "datos": {
        "tipo": "text/plain",
        "base64": "cHJvYmFuZG8="
      }
    }
   */
  app.api.get('/proyectos/:id/documentos/:documento', app.controller.proyecto.obtenerDocumento);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Proyectos
   * @apiName Put datos de la modificación de un proyecto
   * @api {put} /proyectos/:id/modificaciones?tipo=TM_ORDEN_TRABAJO MODIFICACIONES - Crear una modificación a un proyecto APROBADO
   *
   * @apiDescription Crea una modificación a un proyecto en estado de aprobación, se debe enviar el tipo de modificación que se desee.
   *
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
   *{
        "finalizado": true,
        "mensaje": "Modificación iniciada correctamente.",
        "datos": {
            "codigo": "H12YmkzOM",
            "version": 8,
            "nombre": "CONSTRUCCIÓN DE ESCUELAS PARA EL MUNICIPIO DE COPACABANA",
            "desembolso": 200500,
            "monto_total": 1363368.21,
            "monto_total_convenio": 1502500,
            "orden_proceder": "2018-01-22T04:00:00.000Z",
            "plazo_ejecucion": 700,
            "entidad_beneficiaria": "GAM CARANAVI DE LA CIUDAD DE LA PAZ - BOLIVIA",
            "anticipo": 136336.82,
            "porcentaje_anticipo": 10,
            "doc_especificaciones_tecnicas": "[ET] Especificaciones técnicas.pdf",
            "tipo_modificacion": "TM_ORDEN_TRABAJO",
            "boletas": [
                {
                    "monto": 137000,
                    "tipo_boleta": "TB_CORRECTA_INVERSION",
                    "tipo_documento": "POLIZA",
                    "fecha_fin_validez": "2018-08-16T04:00:00.000Z"
                },
                {
                    "monto": 96000,
                    "tipo_boleta": "TB_CUMPLIMIENTO_CONTRATO",
                    "tipo_documento": "BOLETA",
                    "fecha_fin_validez": "2018-06-16T04:00:00.000Z"
                }
            ],
            "empresa": {
                "nombre": "MR. CAFE",
                "nit": "3275482018",
                "matricula": "00101952"
            },
            "dpa": {
                "departamento": {
                    "codigo": "02",
                    "nombre": "La Paz"
                },
                "provincia": {
                    "codigo": "0220",
                    "nombre": "Caranavi",
                    "codigo_departamento": "02"
                },
                "municipio": {
                    "codigo": "022001",
                    "nombre": "Caranavi",
                    "codigo_provincia": "0220"
                }
            },
            "fecha_recepcion_provisional": "2019-12-22T00:00:00-04:00",
            "fecha_recepcion_definitiva": "2020-03-21T00:00:00-04:00",
            "modulos": [
                {
                    "id_modulo": 1,
                    "nombre": "M01 - OBRAS COMPLEMENTARIAS",
                    "items": [
                        {
                            "id_item": 1,
                            "nro_item": 1,
                            "nombre": "REPLANTEO Y TRAZADO",
                            "unidad_medida": "UN_METRO_CUAD",
                            "cantidad": 4000,
                            "precio_unitario": 56,
                            "especificaciones": null,
                            "analisis_precios": null,
                            "unidad": {
                                "nombre": "m2",
                                "descripcion": "Metro cuadrado - M2"
                            }
                        }
                    ]
                },
                ...
            ],
            "modificaciones": {
                "nombre": "Orden de Trabajo N° 6",
                "historial": [
                    {
                        "version": 2,
                        "tipo_modificacion": "TM_ORDEN_TRABAJO"
                    },
                    {
                        "version": 3,
                        "tipo_modificacion": "TM_ORDEN_TRABAJO"
                    },
                    {
                        "version": 4,
                        "tipo_modificacion": "TM_ORDEN_TRABAJO"
                    },
                    {
                        "version": 6,
                        "tipo_modificacion": "TM_ORDEN_TRABAJO"
                    },
                    {
                        "version": 7,
                        "tipo_modificacion": "TM_ORDEN_TRABAJO"
                    }
                ]
            }
        }
    }
   */

  app.api.put('/proyectos/:id/modificaciones/:id_adjudicacion', app.controller.proyecto.crearModificacionAdjudicacion);
  app.api.put('/proyectos/:id/modificaciones', app.controller.proyecto.crearModificacion);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Proyectos
   * @apiName Get datos de la modificación de un proyecto
   * @api {get} /proyectos/:id/modificaciones?version=5 MODIFICACIONES - Obtener datos de la modificación de un proyecto
   *
   * @apiDescription Obtiene datos generales de un proyecto y una lista de modificaciones históricas, de la versión actual.
   * Opcionalmente se puede enviar el dato de la versión que se desea obtener (para versiones anteriores).
   *
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
   *{
    "finalizado": true,
    "mensaje": "Datos del proyecto obtenidos correctamente.",
    "datos": {
          "codigo": "H12YmkzOM",
          "version": 5,
          "nombre": "CONSTRUCCIÓN DE ESCUELAS PARA EL MUNICIPIO DE COPACABANA",
          "desembolso": 200500,
          "monto_total": 1363368.21,
          "monto_total_convenio": 1502500,
          "orden_proceder": "2018-01-22T04:00:00.000Z",
          "plazo_ejecucion": 1240,
          "entidad_beneficiaria": "GAM CARANAVI DE LA CIUDAD DE LA PAZ - BOLIVIA",
          "anticipo": 136336.82,
          "porcentaje_anticipo": 10,
          "doc_especificaciones_tecnicas": "[ET] Especificaciones técnicas.pdf",
          "tipo_modificacion": "TM_ORDEN_CAMBIO",
          "plazo_ampliacion": {
            "por_volumenes": 890,
            "por_compensacion": 350
        },
        "doc_respaldo_modificacion": "[RMP_v5] Respaldo de la modificacion.pdf",
          "boletas": [
              {
                  "monto": 137000,
                  "tipo_boleta": "TB_CORRECTA_INVERSION",
                  "tipo_documento": "POLIZA",
                  "fecha_fin_validez": "2018-08-16T04:00:00.000Z"
              },
              {
                  "monto": 96000,
                  "tipo_boleta": "TB_CUMPLIMIENTO_CONTRATO",
                  "tipo_documento": "BOLETA",
                  "fecha_fin_validez": "2018-06-16T04:00:00.000Z"
              }
          ],
          "empresa": {
              "nombre": "MR. CAFE",
              "nit": "3275482018",
              "matricula": "00101952"
          },
          "dpa": {
              "departamento": {
                  "codigo": "02",
                  "nombre": "La Paz"
              },
              "provincia": {
                  "codigo": "0220",
                  "nombre": "Caranavi",
                  "codigo_departamento": "02"
              },
              "municipio": {
                  "codigo": "022001",
                  "nombre": "Caranavi",
                  "codigo_provincia": "0220"
              }
          },
          "fecha_recepcion_provisional": "2019-12-22T00:00:00-04:00",
          "fecha_recepcion_definitiva": "2020-03-21T00:00:00-04:00",
          "modulos": [
            {
                "id_modulo": 1,
                "nombre": "M01 - OBRAS COMPLEMENTARIAS",
                "items": [
                    {
                        "id_item": 1,
                        "nro_item": 1,
                        "nombre": "LETRERO EN OBRA (LONA PVC)",
                        "unidad_medida": "UN_PIEZA",
                        "cantidad": 44,
                        "precio_unitario": 3952,
                        "especificaciones": null,
                        "analisis_precios": null,
                        "estado": "ACTIVO",
                        "unidad": {
                            "nombre": "pza",
                            "descripcion": "Pieza - PZA"
                        },
                        "item_anterior": {
                            "cantidad": 33
                        }
                    }
                ]
            },
            ...
          ],
          "version_anterior": {
              "monto_total": 1000357.44,
              "plazo_ejecucion": 1200,
              "plazo_ampliacion": {
                  "por_volumenes": 500,
                  "por_compensacion": 700
              },
              "doc_respaldo_modificacion": "[RMP_v4] Respaldo de la modificacion.pdf"
          },
          "modificaciones": {
              "nombre": "Orden de Cambio N° 1",
              "historial": []
          }
      }
    }
   */
  app.api.get('/proyectos/:id/modificaciones', app.controller.proyecto.obtenerModificacion);
  app.api.get('/proyectos/:id/modificaciones/:id_adjudicacion', app.controller.proyecto.obtenerModificacionAdjudicacion);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Proyectos
   * @apiName Get historial de modificaciones de un proyecto
   * @api {get} /proyectos/1/modificaciones/historial MODIFICACIONES - Obtener historial de modificaciones de un proyecto
   *
   * @apiDescription Obtiene una lista de modificaciones de un proyecto.
   *
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
   *{
    "finalizado": true,
    "mensaje": "Datos del proyecto obtenidos correctamente.",
    "datos": [
        {
            "version": 5,
            "tipo_modificacion": "TM_CONTRATO_MODIFICATORIO",
            "estado_proyecto": "MODIFICADO",
            "estado_actual": {
                "nombre": "Modificación al proyecto"
            },
            "nombre": "Contrato Modificatorio N° 1"
        },
        {
            "version": 4,
            "tipo_modificacion": "TM_ORDEN_TRABAJO",
            "estado_proyecto": "MODIFICADO",
            "estado_actual": {
                "nombre": "Modificación al proyecto"
            },
            "nombre": "Modificación de Orden de Trabajo N° 2"
        },
        {
            "version": 3,
            "tipo_modificacion": "TM_ORDEN_CAMBIO",
            "estado_proyecto": "MODIFICADO",
            "estado_actual": {
                "nombre": "Modificación al proyecto"
            },
            "nombre": "Modificación de Orden de Cambio N° 1"
        },
        {
            "version": 2,
            "tipo_modificacion": "TM_ORDEN_TRABAJO",
            "estado_proyecto": "MODIFICADO",
            "estado_actual": {
                "nombre": "Modificación al proyecto"
            },
            "nombre": "Modificación de Orden de Trabajo N° 1"
        },
        {
            "version": 1,
            "tipo_modificacion": null,
            "estado_proyecto": "APROBADO",
            "estado_actual": {
                "nombre": "Aprobado"
            }
        }
    ]
    }
   */
  app.api.get('/proyectos/:id/modificaciones/historial', app.controller.proyecto.obtenerHistorialModificaciones);
  app.api.get('/proyectos/:id/modificaciones/:id_adjudicacion/historial', app.controller.proyecto.obtenerHistorialModificacionesAdjudicacion);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Proyectos
   * @apiName Empresas paralizar
   * @api {put} /proyectos/:id/empresas/paralizar Paralizar la supervision de un proyecto
   *
   * @apiDescription Paraliza las modificaciones al proyecto
   *
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
   *{
      "finalizado": true,
      "mensaje": "Proyecto paralizado.",
      "datos": null
    }
   */
  app.api.put('/proyectos/:id/empresas/paralizar', validate(paramValidation.empresasParalizar), app.controller.proyecto.empresasParalizar);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Proyectos
   * @apiName Empresas Avance
   * @api {get} /proyectos/:id/empresas/avance Obtiene avances de items por proyecto
   *
   * @apiDescription Obtiene los avences de las empresas que participaron en el proyecto
   *
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
   */
  app.api.get('/proyectos/:id/empresas/avance', validate(paramValidation.empresasAvance), app.controller.proyecto.empresasAvance);
  app.api.post('/proyectos/:id_proyecto/contactos/:id_contacto/notificar', app.controller.proyecto.notificar);
  app.api.put('/proyectos/:id_proyecto/contactos/:id_contacto/reemplazar', app.controller.proyecto.reemplazarContacto);
};
