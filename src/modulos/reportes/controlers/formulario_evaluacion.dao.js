import util from '../../../lib/util';
import Reporte from '../../../lib/reportes';
import moment from 'moment';
import shortid from 'shortid';

module.exports = (app) => {
  const models = app.src.db.models;
  app.dao.formulario_evaluacion = {};
  const sequelize = app.src.db.sequelize;

  async function crear (nombre, rutaInforme, datosProyecto) {
    const datosInforme = await obtenerDatos(datosProyecto);
    const rep = new Reporte('formulario_evaluacion');
    return rep.pdf(datosInforme, { orientation: 'portrait', output: `${rutaInforme}/${nombre}` });
  }

  async function regenerar (id_proyecto,codigo) {
      //let proyecto = await app.dao.proyecto.getPorId(id_proyecto);
      let proyecto = util.json(await app.dao.proyecto.obtenerDatosProyecto(id_proyecto));
      console.log("***************************");
      let formulario = await models.formulario_historico.findOne({
        where:{
          codigo_documento: codigo,
          fid_proyecto: id_proyecto,
          nombre: 'evaluacion'
        }
      });
      formulario = util.json(formulario);

      const fechaActual = moment(formulario.fecha_documento);
      formulario.form_model.fechaActual = fechaActual.format('DD/MM/YYYY HH:mm:ss');
      formulario.form_model.plazo_convenio = (formulario.form_model.financiamiento.plazo_tiempo_administrativo?formulario.form_model.financiamiento.plazo_tiempo_administrativo.codigo:270) + (formulario.form_model.beneficiarios.plazo_ejecucion?formulario.form_model.beneficiarios.plazo_ejecucion:0);
      let { nombreArchivo, carpeta } = await app.dao.reporte.obtenerNombreArchivoYCarpeta('EVAL');
      nombreArchivo = nombreArchivo.replace('{{codigo}}', codigo);
      carpeta = `${_path}/uploads/proyectos/${proyecto.codigo}/${carpeta}`;
      //Actualizacion de datos....
      formulario.form_model.nombre = proyecto.nombre;
      formulario.form_model.nro_convenio = proyecto.nro_convenio;

      return await app.dao.formulario_evaluacion.crear(nombreArchivo, carpeta, formulario.form_model);
  }

  async function obtenerUltimo (id_proyecto,tipo) {
      let formulario = await models.formulario_historico.findOne({
        where:{
          fid_proyecto: id_proyecto,
          nombre: tipo
        },
        order: sequelize.literal('_fecha_modificacion DESC')
      });
      return util.json(formulario);
  }

  async function obtenerDatos (datosProyecto) {
    const datosInforme = {
      path: `${_path}`,
      municipio: datosProyecto.municipio,
      provincia: datosProyecto.provincia,
      departamento: datosProyecto.departamento,
      nombre: datosProyecto.nombre,
      nro_convenio: datosProyecto.nro_convenio,
      beneficiarios: datosProyecto.beneficiarios,
      financiamiento1: datosProyecto.financiamiento,
      financiamiento: {
        monto_fdi: util.convertirNumeroAString(datosProyecto.financiamiento.monto_fdi),
        monto_contraparte: util.convertirNumeroAString(datosProyecto.financiamiento.monto_contraparte),
        monto_beneficiarios: util.convertirNumeroAString(datosProyecto.financiamiento.monto_beneficiarios),
        monto_monto_otros: util.convertirNumeroAString(datosProyecto.financiamiento.monto_monto_otros),
        monto_total_proyecto: util.convertirNumeroAString(datosProyecto.financiamiento.monto_total_proyecto),
      },
      plazo_convenio: (datosProyecto.financiamiento.plazo_tiempo_administrativo?datosProyecto.financiamiento.plazo_tiempo_administrativo.codigo:270) + (datosProyecto.beneficiarios.plazo_ejecucion?datosProyecto.beneficiarios.plazo_ejecucion:0),
      responsable_gam_gaioc: datosProyecto.responsable_gam_gaioc,
      responsable_edtp: datosProyecto.responsable_edtp,
      evaluacion: datosProyecto,
      fecha_actual: datosProyecto.fechaActual,
    };

    return datosInforme;
  }


  async function crearInformeRechazo (nombre, rutaEvaluaciones, datosProyecto, idProyecto, req) {
    let datosInforme = {};

    let informeRechazo = await models.formulario.findOne({
        where:{
          fid_proyecto: idProyecto,
          nombre: 'informe_rechazo'
        }
      });
    //console.log('informe_rechazo------>',informeRechazo);
    /***if(informeRechazo){
      informeRechazo = util.json(informeRechazo);
      datosInforme = informeRechazo.form_model;
      const resumen = new Reporte('informe_rechazo');
      return resumen.pdf(datosInforme, {
        orientation: 'portrait',
        marginLeft: '2cm',
        marginRight: '2cm',
        marginTop: '2cm',
        marginBottom: '2cm',
        output: `${rutaEvaluaciones}/${nombre}`
      });
    }else{***/
      const codigo = `${shortid.generate()}`;
      //console.log('crear informe rechazo------------->',codigo);
      const datosResumen = await obtenerDatosResumen(datosProyecto, req);
      const form_proy = {
        id_proyecto: idProyecto,
        estado_proyecto: 'RECHAZADO_FDI',
      };

      const form = datosResumen;
      form.$_nombre = 'informe_rechazo';
      form.$_plantilla = 102;
      form.version = 1;
      form.codigo_documento = codigo;

      datosInforme = datosResumen;
      sequelize.transaction(async(t) => {
        await app.dao.formulario.crearActualizar(req.body.audit_usuario,form_proy, form, t);
        //console.log('respuesta------------->',resp);
      }).then((result) => {
        const resumen = new Reporte('informe_rechazo');
        return resumen.pdf(datosInforme, {
          orientation: 'portrait',
          marginLeft: '2cm',
          marginRight: '2cm',
          marginTop: '2cm',
          marginBottom: '2cm',
          output: `${rutaEvaluaciones}/${nombre}`
        });
      }).catch((error)=>{
        console.log('error------------->',error);
      });
    //}

  };

  async function obtenerDatosResumen (datosProyecto) {

    let fechaActual = moment(datosProyecto.fechaActual);
    let director_general = await app.dao.parametrica.obtenerNombreTipo('DIRECTOR_GRAL_EJECUTIVO','ORG_INSTITUCION');
    let jefe_depto_tecnico = await app.dao.parametrica.obtenerNombreTipo('JEFE_DEPTO_TECNICO','ORG_INSTITUCION');

    let _usuario_designacion = await app.dao.proyecto.getUsuarioTransicion(datosProyecto.id_proyecto,'ASIGNACION_TECNICO_FDI');
      _usuario_designacion = util.json(_usuario_designacion);
      _usuario_designacion = await app.dao.usuario.getUsuarioPersona(_usuario_designacion.fid_usuario_asignado);
      _usuario_designacion = util.json(_usuario_designacion);

    let _usuario_rechazo = await app.dao.proyecto.getUsuarioTransicion(datosProyecto.id_proyecto,'RECHAZADO_FDI');
      _usuario_rechazo = util.json(_usuario_rechazo);
      console.log('usuario rechazo-------------',_usuario_rechazo);
      fechaActual = moment(_usuario_rechazo._fecha_creacion);

      _usuario_rechazo = await app.dao.usuario.getUsuarioPersona(_usuario_rechazo.fid_usuario);
      _usuario_rechazo = util.json(_usuario_rechazo);

    const datosInforme = {
      path: `${_path}`,
      director_general: {nombre: director_general.nombre, cargo: director_general.descripcion},
      jefe_depto_tecnico: {nombre: jefe_depto_tecnico.nombre, cargo: jefe_depto_tecnico.descripcion},
      jefe_designacion: {nombre: `${_usuario_designacion.persona.nombres} ${_usuario_designacion.persona.primer_apellido} ${_usuario_designacion.persona.segundo_apellido}`, cargo: _usuario_designacion.cargo},
      tecnico_evaluacion: {nombre: `${_usuario_rechazo.persona.nombres} ${_usuario_rechazo.persona.primer_apellido} ${_usuario_rechazo.persona.segundo_apellido}`, cargo: _usuario_rechazo.cargo},
      nombre: datosProyecto.nombre,
      motivo_rechazo: datosProyecto.motivo_rechazo,
      codigo: datosProyecto.nro_convenio,
      fecha: {
        dia: fechaActual.format('DD'),
        mes: fechaActual.locale('es').format('MMMM'),
        anio: fechaActual.format('YYYY'),
      },
    };
    return datosInforme;
  }

  app.dao.formulario_evaluacion.crear = crear;
  app.dao.formulario_evaluacion.regenerar = regenerar;
  app.dao.formulario_evaluacion.obtenerUltimo = obtenerUltimo;
  app.dao.formulario_evaluacion.crearInformeRechazo = crearInformeRechazo;

};
