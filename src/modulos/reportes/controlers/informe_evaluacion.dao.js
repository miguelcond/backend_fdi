import util from '../../../lib/util';
import Reporte from '../../../lib/reportes';
import moment from 'moment';

module.exports = (app) => {
  const models = app.src.db.models;
  app.dao.informe_evaluacion = {};

  async function crear (nombre, rutaInforme, datosProyecto, datosInforme) {
    const rep = new Reporte('informe_evaluacion');
    datosInforme = await obtenerDatos(datosProyecto, datosInforme);
    return rep.pdf(datosInforme, {
      orientation: 'portrait',
      marginLeft: '2cm',
      marginRight: '2cm',
      marginTop: '2cm',
      marginBottom: '2cm',
      output: `${rutaInforme}/${nombre}`
    });
  }

  async function regenerar (id_proyecto,codigo) {
      let proyecto = util.json(await app.dao.proyecto.obtenerDatosProyecto(id_proyecto));
      let formulario = await models.formulario_historico.findOne({
        where:{
          codigo_documento: codigo,
          fid_proyecto: id_proyecto,
          nombre: 'informe'
        }
      });
      formulario = util.json(formulario);
      
      let completar = false;
      if(!formulario.form_model.financiamiento){
        completar = true;
        let financiamiento = await models.formulario.findOne({
          where:{
            // codigo_documento: codigo,
            fid_proyecto: id_proyecto,
            nombre: 'financiamiento'
          }
        });        
        financiamiento = util.json(financiamiento);
        formulario.form_model.financiamiento = financiamiento.form_model;
      }
      ///completar datos beneficiarios
      if(!formulario.form_model.beneficiarios){
        completar = true;
        let beneficiarios =  await models.formulario.findOne({
          where:{
            fid_proyecto: id_proyecto,
            nombre: 'beneficiarios'
          }
        });
        beneficiarios = util.json(beneficiarios);
        // Guardar informe de evaluacion
        formulario.form_model.beneficiarios = beneficiarios.form_model;
        formulario.form_model.nro_familias = beneficiarios.form_model.nro_familias||0;
        formulario.form_model.nro_comunidades = beneficiarios.form_model.nro_comunidades||0;
        formulario.form_model.comunidades = beneficiarios.form_model.comunidades;
      }

      if(completar){
        await models.formulario_historico.update(formulario, {where:{
          codigo_documento: codigo,
          fid_proyecto: id_proyecto,
          nombre: 'informe'
        }});
      }      

      const fechaActual = moment(formulario.fecha_documento);
      formulario.form_model.fechaActual = fechaActual;

      let { nombreArchivo, carpeta } = await app.dao.reporte.obtenerNombreArchivoYCarpeta('INF');
      nombreArchivo = nombreArchivo.replace('{{codigo}}', codigo);
      carpeta = `${_path}/uploads/proyectos/${proyecto.codigo}/${carpeta}`;

      return await app.dao.informe_evaluacion.crear(nombreArchivo, carpeta, proyecto, formulario.form_model);
  }

  async function crearResumen (nombre, rutaEvaluaciones, datosProyecto, idProyecto, req) {
    const datosInforme = await obtenerDatosResumen(datosProyecto, req);
    const resumen = new Reporte('resumen_informe_evaluacion');
    return resumen.pdfDownload(datosInforme, {
      orientation: 'portrait',
      marginLeft: '2cm',
      marginRight: '2cm',
      marginTop: '3cm',
      marginBottom: '3cm',
      output: `${rutaEvaluaciones}/${nombre}`
    });
  };

  async function obtenerDatos (dProyecto, dInforme) {
    const datosInforme = {
      codigo: dProyecto.nro_convenio,
      nombre: dProyecto.nombre,
      cartera: dProyecto.cartera,
      path: `${_path}`,
      municipio: dProyecto.municipio.nombre,
      provincia: dProyecto.municipio.provincia.nombre,
      departamento: dProyecto.municipio.provincia.departamento.nombre,
      fecha: {
        dia: dInforme.fechaActual.format('DD'),
        mes: dInforme.fechaActual.locale('es').format('MMMM'),
        anio: dInforme.fechaActual.format('YYYY'),
      },

      procedencia: dProyecto.entidad_beneficiaria,
      plazo_ejecucion: dProyecto.plazo_ejecucion,

      informe: dInforme,
    };

    if (datosInforme.informe.valoracion.length >= 9) {
      datosInforme.incluirFPS = datosInforme.informe.valoracion[8].cols[2] && datosInforme.informe.valoracion[8].cols[2].contenido && datosInforme.informe.valoracion[8].cols[2].contenido.value && datosInforme.informe.valoracion[8].cols[2].contenido.value.nombre ? datosInforme.informe.valoracion[8].cols[2].contenido.value.nombre : 'No';
      datosInforme.informe.valoracion.splice(-1,1);
    }

    return datosInforme;
  }

  async function obtenerDatosResumen (datosProyecto, req) {
    let informe = await app.dao.formulario.obtener({
      id_proyecto: datosProyecto.id_proyecto,
      $_plantilla: 102,
      $_nombre: 'informe'
    });
    let financiamiento =  await app.dao.formulario.obtener({
      id_proyecto: datosProyecto.id_proyecto,
      $_plantilla: 2,
      $_nombre: 'financiamiento'
    });
    let beneficiarios =  await app.dao.formulario.obtener({
      id_proyecto: datosProyecto.id_proyecto,
      $_plantilla: 1,
      $_nombre: 'beneficiarios'
    });
    const tecnico = await app.dao.usuario.getUsuarioPersona(req.body.audit_usuario.id_usuario);

    let fechaActual = moment(informe.fecha_documento);

    req.body = informe.form_model;
    req.body.fechaActual = fechaActual;
    req.body.plazoDias = beneficiarios.form_model.plazo_ejecucion;
    req.body.plazoMeses = parseFloat(beneficiarios.form_model.plazo_ejecucion / 30).toFixed(2);
    req.body.financiamiento = financiamiento.form_model;
    req.body.beneficiarios = beneficiarios.form_model;
    req.body.proyecto = {
      tipo: beneficiarios.form_model && beneficiarios.form_model.tipo ? beneficiarios.form_model.tipo.replace('OP_TP_','') : '',
      indicadores: []
    }
    req.body.tecnico = {
      nombre: `${tecnico.persona.nombres} ${tecnico.persona.primer_apellido} ${tecnico.persona.segundo_apellido}`,
      cargo: tecnico.cargo
    }

    // for(let i in beneficiarios.form_model.productivo) {
    //   if (beneficiarios.form_model.productivo[i]) {
    //     req.body.proyecto.indicadores.push(nombreIndicador(i));
    //   }
    // }
    if (req.body.proyecto.indicadores.length === 0) {
      req.body.proyecto.indicadores.push(req.body.proyecto.tipo);
    }
    req.body.nro_familias = beneficiarios.form_model.nro_familias || 0;
    req.body.nro_comunidades = beneficiarios.form_model.nro_comunidades || 0;

    const datosInforme = await obtenerDatos(datosProyecto, req.body);

    return datosInforme;
  }

  app.dao.informe_evaluacion.crear = crear;
  app.dao.informe_evaluacion.regenerar = regenerar;
  app.dao.informe_evaluacion.crearResumen = crearResumen;
};
