import fs from 'fs-extra';
import Util from '../../../lib/util';
import errors from '../../../lib/errors';
import moment from 'moment';
import shortid from 'shortid';

module.exports = (app) => {
  const sequelize = app.src.db.sequelize;
  const models = app.src.db.models;

  // Funcion listar proyectos por municipio
  async function porMunicipio (req, res, next) {
    try {
      console.log('----------------------->>>>>>>>>>>>>>>>>>>>>>');
      console.log(req.query);      
      console.log('----------------------->>>>>>>>>>>>>>>>>>>>>>');
      req.query.gestion = !req.query.gestion? '': req.query.gestion;
      let departamento = req.params.departamento;
      let municipio = req.params.municipio || `${departamento}%`;
      let _departamento = Util.json(await models.departamento.findOne({where: {codigo: req.params.departamento}}));
      let titulo = `Reportes - Departamento ${_departamento.nombre}`;
      let fechaReporte = moment().format('DD/MM/YYYY-hh:mm:ss a');

      if(req.params.municipio){
        let _municipio = Util.json(await models.municipio.findOne({where: {codigo: req.params.municipio}}));
        titulo += ` - Municipio ${_municipio.nombre}`;        
      }
      if(req.query.rol == 14){
        // es rol legal
        let options = {
          attributes: ['id_proyecto', 'nombre', 'nro_convenio','plazo_ejecucion','cartera', 'org_sociales','estado_proyecto','orden_proceder','doc_convenio','doc_convenio_extendido','doc_convenio_extendido2','doc_convenio_extendido3', 'fcod_municipio'],
          where: {
            // estado_proyecto: {$notIn:['REGISTRO_SOLICITUD','ASIGNACION_TECNICO_FDI','REGISTRO_PROYECTO_FDI','RECHAZADO_FDI','EN_EJECUCION_FDI','REGISTRO_CONVENIO_FDI','REGISTRO_DESEMBOLSO_FDI']},
            cartera: req.query.cartera,
            fcod_municipio: {$like:`${municipio}`},
            estado: 'ACTIVO'
          },
          include: [{
            model: sequelize.models.formulario,
            attributes: ['form_model','codigo_estado','fid_plantilla','nombre'],
            as: 'formularios',
            where: {
              nombre: {$in: ['convenio']}
            }
          }],
          order: ['nro_convenio']
        };
        let datos = await models.proyecto.findAll(options);
        datos = Util.json(datos);
        // console.log(datos[0].formularios)
          // Parsear datos de los formularios obtenidos
        for (let i in datos) {
          let forms = datos[i].formularios;

          for (let j in forms) {
            // console.log("datos formulario : ?>>>>",forms[j].form_model)
            datos[i][forms[j].nombre] = forms[j].form_model;
          }
          delete datos[i].formularios;
          
          let fconclusion = moment(datos[i].orden_proceder).add(datos[i].plazo_ejecucion,'days')
         
          if(datos[i].convenio && datos[i].convenio.plazo_convenio_ampliado){
            fconclusion = moment(datos[i].orden_proceder).add(datos[i].plazo_ejecucion,'days').add(datos[i].convenio.plazo_convenio_ampliado,'days')
          }
          if(datos[i].convenio && datos[i].convenio.plazo_convenio_ampliado2){
            fconclusion = moment(datos[i].orden_proceder).add(datos[i].plazo_ejecucion,'days').add(datos[i].convenio.plazo_convenio_ampliado,'days').add(datos[i].convenio.plazo_convenio_ampliado2,'days')
          }
          if(datos[i].convenio && datos[i].convenio.plazo_convenio_ampliado3){
            fconclusion = moment(datos[i].orden_proceder).add(datos[i].plazo_ejecucion,'days').add(datos[i].convenio.plazo_convenio_ampliado,'days').add(datos[i].convenio.plazo_convenio_ampliado2,'days').add(datos[i].convenio.plazo_convenio_ampliado3,'days')
          }
          if(datos[i].convenio && datos[i].convenio.plazo_convenio_ampliado4){
            fconclusion = moment(datos[i].orden_proceder).add(datos[i].plazo_ejecucion,'days').add(datos[i].convenio.plazo_convenio_ampliado,'days').add(datos[i].convenio.plazo_convenio_ampliado2,'days').add(datos[i].convenio.plazo_convenio_ampliado3,'days').add(datos[i].convenio.plazo_convenio_ampliado4,'days')
          }
          datos[i].fconclusion = fconclusion;          
        }
        return res.status(200).json({
          finalizado: true,
          mensaje: 'Datos obtenidos correctamente.',
          titulo: `${titulo}`,
          fechaRep: `${fechaReporte}`,
          datos: datos
        });
      }
      let options = {
        where: {
          estado_proyecto: {$notIn:['REGISTRO_SOLICITUD','ASIGNACION_TECNICO_FDI','REGISTRO_PROYECTO_FDI','RECHAZADO_FDI','EN_EJECUCION_FDI','REGISTRO_CONVENIO_FDI','REGISTRO_DESEMBOLSO_FDI']},
          cartera: req.query.cartera,
          fcod_municipio: {$like:`${municipio}`},
          estado: 'ACTIVO'
        },
        include: [{
          model: sequelize.models.formulario,
          attributes: ['form_model','codigo_estado','fid_plantilla','nombre'],
          as: 'formularios',
          where: {
            nombre: {$in: ['financiamiento','beneficiarios','cronograma_desembolsos']}
          }
        }],
        order: ['nro_convenio']
      };

      // Proyectos con evaluación aprobada
      let datos = await models.proyecto.findAll(options);

      datos = Util.json(datos);

      // Parsear datos de los formularios obtenidos
      for (let i in datos) {
        let forms = datos[i].formularios;
        for (let j in forms) {
          datos[i][forms[j].nombre] = forms[j].form_model;
        }
        delete datos[i].formularios;
      }
      options.where.estado_proyecto = {$in:['REGISTRO_SOLICITUD','ASIGNACION_TECNICO_FDI','REGISTRO_PROYECTO_FDI']};
      // Proyectos en fase de evaluación
      let datosEval = await models.proyecto.findAll(options);

      datosEval = Util.json(datosEval);

      // Parsear datosEval de los formularios obtenidos
      for (let i in datosEval) {
        let forms = datosEval[i].formularios;
        for (let j in forms) {
          datosEval[i][forms[j].nombre] = forms[j].form_model;
        }
        delete datosEval[i].formularios;
      }

      // Proyectos en fase de rechazo
      options.where.estado_proyecto = {$in:['RECHAZADO_FDI']};
      let datosRechazados = await models.proyecto.findAll(options);
      datosRechazados = Util.json(datosRechazados);

      // Parsear datosEval de los formularios obtenidos
      for (let i in datosRechazados) {
        let forms = datosRechazados[i].formularios;
        for (let j in forms) {
          datosRechazados[i][forms[j].nombre] = forms[j].form_model;
        }
        delete datosRechazados[i].formularios;
      }
      // Proyectos en registro convenio
      options.where.estado_proyecto = {$in:['REGISTRO_CONVENIO_FDI']};
      let datosConv = await models.proyecto.findAll(options);
      datosConv = Util.json(datosConv);
      // Parsear datosConv de los formularios obtenidos
      for (let i in datosConv) {
        let forms = datosConv[i].formularios;
        for (let j in forms) {
          datosConv[i][forms[j].nombre] = forms[j].form_model;
        }
        delete datosConv[i].formularios;
      }
      // Proyectos en registro desembolso
      options.where.estado_proyecto = {$in:['REGISTRO_DESEMBOLSO_FDI']};
      let datosDesem = await models.proyecto.findAll(options);
      datosDesem = Util.json(datosDesem);
      // Parsear datosDesem de los formularios obtenidos
      for (let i in datosDesem) {
        let forms = datosDesem[i].formularios;
        for (let j in forms) {
          datosDesem[i][forms[j].nombre] = forms[j].form_model;
        }
        delete datosDesem[i].formularios;
      }

      // Proyectos en ejecucion
      options.where.estado_proyecto = {$in:['EN_EJECUCION_FDI']};
      let datosEjec = await models.proyecto.findAll(options);
      datosEjec = Util.json(datosEjec);
      // Parsear datosConv de los formularios obtenidos
      for (let i in datosEjec) {
        let forms = datosEjec[i].formularios;
        let avance_fisico = await app.dao.item.obtenerAvanceEjecutado(datosEjec[i].id_proyecto);
        // el calculo de % avance fisico es sobre monto fdi
        let porcentaje_avance_fisico = Util.calcularPorcentaje(avance_fisico,datosEjec[i].monto_fdi);
        porcentaje_avance_fisico = `${Util.convertirNumeroAString(porcentaje_avance_fisico)} %`;
        datosEjec[i].avance_fisico = porcentaje_avance_fisico;
        for (let j in forms) {
          datosEjec[i][forms[j].nombre] = forms[j].form_model;
        }
        delete datosEjec[i].formularios;
      }
      // Lista de Proyectos
      options.where.estado_proyecto = {$in:['EN_EJECUCION_FDI','APROBADO_FDI', 'ASIGNACION_EQUIPO_TECNICO_FDI']};
      let datoslista = await models.proyecto.findAll(options);
      datoslista = Util.json(datoslista);
      // Parsear datosConv de los formularios obtenidos
      for (let i in datoslista) {
        let forms = datoslista[i].formularios;
        let avance_fisico = await app.dao.item.obtenerAvanceEjecutado(datoslista[i].id_proyecto);
        // el calculo de % avance fisico es sobre monto fdi
        let porcentaje_avance_fisico = Util.calcularPorcentaje(avance_fisico,datoslista[i].monto_fdi);
        porcentaje_avance_fisico = `${Util.convertirNumeroAString(porcentaje_avance_fisico)} %`;
        datoslista[i].avance_fisico = porcentaje_avance_fisico;
        for (let j in forms) {
          datoslista[i][forms[j].nombre] = forms[j].form_model;
        }
        if (datoslista[i].beneficiarios.tipo == 'OP_TP_PRODUCTIVO'){ datoslista[i].beneficiarios.tipo = 'PRODUCTIVO'}
        if (datoslista[i].beneficiarios.tipo == 'OP_TP_MAQUINARIA'){ datoslista[i].beneficiarios.tipo = 'MAQUINARIA'}
        if (datoslista[i].beneficiarios.tipo == 'OP_TP_RIEGO'){ datoslista[i].beneficiarios.tipo = 'RIEGO'}
        if (datoslista[i].beneficiarios.tipo == 'OP_TP_PUENTES'){ datoslista[i].beneficiarios.tipo = 'PUENTES'}
        if (datoslista[i].beneficiarios.tipo_financiamiento == 'OP_TF_PRIVADA'){ datoslista[i].beneficiarios.tipo_financiamiento = 'PRIVADA'}
        if (datoslista[i].beneficiarios.tipo_financiamiento == 'OP_TF_MIXTA'){ datoslista[i].beneficiarios.tipo_financiamiento = 'MIXTA'}
        if (datoslista[i].beneficiarios.tipo_financiamiento == 'OP_TF_PUBLICA'){ datoslista[i].beneficiarios.tipo_financiamiento = 'PUBLICA'}
        delete datoslista[i].formularios;
      }
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Datos obtenidos correctamente.',
        titulo: `${titulo}`,
        fechaRep: `${fechaReporte}`,
        datos: datos,
        datosEval,
        datosRechazados,
        datosConv,
        datosDesem,
        datosEjec,
        datoslista
      });
    } catch(error) {
      return next(error);
    }
  }

  app.controller.reporteProyectos = {
    porMunicipio,
  };
};
