import errors from '../../../lib/errors';
import Reporte from '../../../lib/reportes';

module.exports = (app) => {
  app.dao.formularioA7 = {};

  async function crear (rutaFormularios, datosProyecto) {
    const { nombreArchivo } = await app.dao.reporte.obtenerNombreArchivoYCarpeta(`A7`);
    const datosFormulario = await obtenerDatos(datosProyecto);
    const rep = new Reporte('formulario_A7');
    return rep.pdf(datosFormulario, { orientation: 'portrait', output: `${rutaFormularios}/${nombreArchivo}` });
  }

  async function obtenerDatos (datosProyecto) {
    const datos = datosProyecto.documentacion_exp;
    if (!datos) {
      throw new errors.ValidationError(`No se encontró el reporte solicitado.`);
    }
    let equiposPermanentes = [];
    let cnt = 1;
    for (let i in datos.equipos_trabajo.permanente) {
      let equipo = datos.equipos_trabajo.permanente[i];
      equiposPermanentes.push({
        nro: cnt++,
        descripcion: equipo.descripcion,
        unidad: equipo.unidad,
        cantidad: equipo.cantidad,
        potencia: equipo.potencia,
        capacidad: equipo.capacidad
      });
    }
    let equiposRequerimientos = [];
    cnt = 1;
    for (let i in datos.equipos_trabajo.requerido) {
      let equipo = datos.equipos_trabajo.requerido[i];
      equiposRequerimientos.push({
        nro: cnt++,
        descripcion: equipo.descripcion,
        unidad: equipo.unidad,
        cantidad: equipo.cantidad,
        potencia: equipo.potencia,
        capacidad: equipo.capacidad
      });
    }
    let datosFormularioA7 = {
      permanentes: equiposPermanentes,
      requerimientos: equiposRequerimientos
    };

    return datosFormularioA7;
  }

  app.dao.formularioA7.crear = crear;
};
