import util from '../../../lib/util';
import errors from '../../../lib/errors';
import Reporte from '../../../lib/reportes';
import moment from 'moment';

module.exports = (app) => {
  app.dao.ficha_tecnica = {};

  const models = app.src.db.models;
  moment.locale('es');

  async function crear (rutaFormularios, datos) {
    // const { nombreArchivo } = await app.dao.reporte.obtenerNombreArchivoYCarpeta(`B1`);
    const nombreArchivo = 'ficha_tecnica.pdf';
    const datosFormulario = await obtenerDatos(datos);
    const rep = new Reporte('ficha_tecnica');
    return rep.pdf(datosFormulario, { orientation: 'portrait', output: `${rutaFormularios}${nombreArchivo}`, noHeader: true });
  }

  async function obtenerDatos (datos) {
    let adjudicaciones = await models.adjudicacion.findAll({
      where: {
        fid_proyecto: datos.datos.id_proyecto,
        $not:[{estado:'INACTIVO'}]
      },
      include:[
        {
          //attributes:['id_supervision','nro_supervision'],
          model: models.supervision,
          as: 'supervision',
          order: [['nro_supervision','DESC']],
          $not:[{estado:'INACTIVO'}]
        }
      ]
    });
    adjudicaciones = util.json(adjudicaciones);

    const componentes= [];
    const avance = {
      fisico: 0,
      financiero: 0,
    };
    let orden_proceder = '';
    if (adjudicaciones[0] && adjudicaciones[0].orden_proceder) orden_proceder = adjudicaciones[0].orden_proceder;
    for (let i in adjudicaciones) {
      const adj = adjudicaciones[i];
      if (adj.orden_proceder < orden_proceder) {
        orden_proceder = adj.orden_proceder;
      }
      componentes.push({
        nombre: adj.referencia,
      });
      let supervision = await app.dao.supervision.obtener(datos.datos.id_proyecto, adjudicaciones[i].id_adjudicacion, adjudicaciones[i].supervision[adjudicaciones[i].supervision.length -1].nro_supervision);
      supervision = util.json(supervision);
      avance.financiero = util.sumar([avance.financiero, supervision.porcentaje_avance_fin]);
      avance.fisico = util.sumar([avance.fisico, supervision.avance_fisico_ponderado]);
    }
    let provisional = '';
    if (orden_proceder) {
      provisional = moment.utc(orden_proceder).add(datos.datos.plazo_ejecucion + datos.convenio.plazo_convenio_ampliado -1, 'days');
    }
    let definitiva = '';
    if (orden_proceder) {
      definitiva = moment.utc(orden_proceder).add(datos.datos.plazo_ejecucion + datos.convenio.plazo_convenio_ampliado + 90 -1, 'days');
    }

    const datosFichaTecnica = {
      proyecto: datos.datos.nombre,
      tipo: datos.proyecto.tipo,
      fecha: moment(datos.convenio.fecha_suscripcion).format('DD/MM/YYYY'),

      departamento: datos.datos.municipio.provincia.departamento.nombre.toUpperCase(),
      provincia: datos.datos.municipio.provincia.nombre.toUpperCase(),
      municipio: datos.datos.municipio.nombre.toUpperCase(),
      comunidades: datos.beneficiarios.comunidades,
      nro_familias: datos.beneficiarios.nro_familias,
      cartera: datos.datos.cartera,

      orden_proceder: orden_proceder? moment.utc(orden_proceder).format('D [de] MMMM [de] YYYY'): '',
      plazo_ejecucion: datos.datos.plazo_ejecucion,
      plazo_ampliacion: datos.convenio.plazo_convenio_ampliado,
      entrega_provisional: provisional? provisional.format('D [de] MMMM [de] YYYY'): '',
      entrega_definitiva: definitiva? definitiva.format('D [de] MMMM [de] YYYY'): '',

      avance: {
        fisico: util.convertirNumeroAString(avance.fisico),
        financiero: util.convertirNumeroAString(avance.financiero),
      },

      financiamiento: {
        monto_fdi: util.convertirNumeroAString(datos.financiamiento.monto_fdi),
        monto_contraparte: util.convertirNumeroAString(datos.financiamiento.monto_contraparte),
        monto_beneficiarios: util.convertirNumeroAString(datos.financiamiento.monto_beneficiarios),
        monto_total_proyecto: util.convertirNumeroAString(datos.financiamiento.monto_total_proyecto),
      },

      informe: {
        indicador_descripcion: datos.informe.indicador.descripcion.toUpperCase(),
        metas_resultados: datos.informe.metas_resultados.toUpperCase(),
      },
      desembolsos: {
        fecha_1: datos.desembolsos.fecha_1? moment(datos.desembolsos.fecha_1).format('DD/MM/YYYY'): '',
        monto_1: datos.desembolsos.monto_1? util.convertirNumeroAString(datos.desembolsos.monto_1): '',
        fecha_2: datos.desembolsos.fecha_2? moment(datos.desembolsos.fecha_2).format('DD/MM/YYYY'): '',
        monto_2: datos.desembolsos.monto_2? util.convertirNumeroAString(datos.desembolsos.monto_2): '',
        fecha_3: datos.desembolsos.fecha_3? moment(datos.desembolsos.fecha_3).format('DD/MM/YYYY'): '',
        monto_3: datos.desembolsos.monto_3? util.convertirNumeroAString(datos.desembolsos.monto_3): '',
      },

      componentes,
    };

    return datosFichaTecnica;
  }

  app.dao.ficha_tecnica.crear = crear;
};
