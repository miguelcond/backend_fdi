import fs from 'fs-extra';
import Util from '../../../lib/util';
import errors from '../../../lib/errors';
import moment from 'moment';
import shortid from 'shortid';

module.exports = (app) => {
  const sequelize = app.src.db.sequelize;

  app.controller.reporte = {};

  // PARA PRUEBAS (Esta acción se realiza dentro del flujo)
  async function crearFormularios (req, res, next) {
    try {
      const ID_PROYECTO = req.params.id_proyecto;
      await app.dao.reporte.crearFormularios(ID_PROYECTO);
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Los formularios han sido creados exitosamente.',
        datos: null
      });
    } catch (error) {
      next(error);
    }
  }

  // PARA PRUEBAS (Esta acción se realiza dentro del flujo)
  async function crearPlanilla (req, res, next) {
    try {
      const ID_PROYECTO = req.params.id_proyecto;
      const NRO_PLANILLA = req.params.nro_planilla;
      await app.dao.reporte.crearPlanilla(ID_PROYECTO, NRO_PLANILLA);
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Los reportes de la planilla y los computos han sido creados exitosamente.',
        datos: null
      });
    } catch (error) {
      next(error);
    }
  }

  async function descargarFormularios (req, res, next) {
    try {
      const ID_PROYECTO = req.params.id_proyecto;
      const CODIGO_PROYECTO = await app.dao.proyecto.obtenerCodigoProyecto(ID_PROYECTO);
      res.set('Content-Type', 'application/zip');
      res.set('Content-Disposition', `attachment; filename=Formularios-${CODIGO_PROYECTO}.zip`);
      await app.dao.reporte.crearFormulariosZIP(ID_PROYECTO, res);
    } catch (error) {
      next(error);
    }
  }

  // Las siguientes funciones son solo PARA PRUEBAS, las mismas tareas se realizan dentro del flujo.
  async function crearArchivo (req, res, next) {
    try {
      const ID_PROYECTO = req.params.id_proyecto;
      const CODIGO_DOCUMENTO = req.params.codigo_documento;
      await app.dao.reportes.crearArchivo(ID_PROYECTO, CODIGO_DOCUMENTO);
      let { nombreArchivo } = await app.dao.reporte.obtenerNombreArchivoYCarpeta(CODIGO_DOCUMENTO);
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Archivo creado exitosamente.',
        datos: {
          documento: {
            codigo_documento: CODIGO_DOCUMENTO,
            nombre: nombreArchivo.replace('.pdf', ''),
            nombre_archivo: nombreArchivo
          }
        }
      });
    } catch (error) {
      next(error);
    }
  }

  async function subirArchivo (req, res, next) {
    try {
      const ID_PROYECTO = req.params.id_proyecto;
      const CODIGO_DOCUMENTO = req.params.codigo_documento;
      const CODIGO_PROYECTO = await app.dao.proyecto.obtenerCodigoProyecto(ID_PROYECTO);
      let { nombreArchivo, carpeta } = await app.dao.reporte.obtenerNombreArchivoYCarpeta(CODIGO_DOCUMENTO);
      await app.dao.reporte.crearObtenerDirectorioProyecto(CODIGO_PROYECTO);
      const path = `proyectos/${CODIGO_PROYECTO}/${carpeta}/${nombreArchivo}`;
      const base64 = req.body.documento.base64;
      await app.dao.proyecto.escribirBase64(path, base64);
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Archivo guardado exitosamente.',
        datos: {
          documento: {
            codigo_documento: CODIGO_DOCUMENTO,
            nombre: nombreArchivo.replace('.pdf', ''),
            nombre_archivo: nombreArchivo
          }
        }
      });
    } catch (error) {
      next(error);
    }
  }

  async function listarArchivos (req, res, next) {
    try {
      const ID_PROYECTO = req.params.id_proyecto;
      const documentos = await app.dao.reporte.obtenerListaArchivos(ID_PROYECTO, req.query);
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Lista de documentos obtenida exitosamente.',
        datos: documentos
      });
    } catch (error) {
      next(error);
    }
  }

  async function obtenerArchivo (req, res, next) {

    try {
      const ID_PROYECTO = req.params.id_proyecto;
      const CODIGO_DOCUMENTO = req.params.codigo_documento;
      const CODIGO_PROYECTO = await app.dao.proyecto.obtenerCodigoProyecto(ID_PROYECTO);
      let { nombreArchivo, carpeta } = await app.dao.reporte.obtenerNombreArchivoYCarpeta(CODIGO_DOCUMENTO, req.query.name);
      let path = `/proyectos/${CODIGO_PROYECTO}/${carpeta}/${nombreArchivo}`;
      let regenerarArchivo = false;
      let stream = null;
      if (CODIGO_DOCUMENTO=='EVAL'||CODIGO_DOCUMENTO=='INF' || CODIGO_DOCUMENTO == 'SupCP') {
        path = path.replace('{{codigo}}', req.query.codigo);
        const dir = `${_path}/uploads`;
        console.log("***************************************");
        if(req.query.reload){
          console.log("********************************");
          console.log("RECARGAR ARCHIVO....");
          await fs.unlinkSync(`${dir}/${path}`);
          console.log("ELIMINANDO EL ARCHIVO....");
        }

        if(!fs.existsSync(`${dir}/${path}`)){
          regenerarArchivo = true;
          console.log("NO EXISTE EL ARCHIVO....");
          if (CODIGO_DOCUMENTO=='EVAL'){
            stream = await app.dao.formulario_evaluacion.regenerar(ID_PROYECTO,req.query.codigo);
          }
          if (CODIGO_DOCUMENTO=='INF'){
            stream = await app.dao.informe_evaluacion.regenerar(ID_PROYECTO,req.query.codigo);
          }
        }
      }
      try {
        if (stream) {
          stream.on('close', async (a)=>{
            const datos = await app.dao.proyecto.leerArchivo(path);
            return res.status(200).json({
              finalizado: true,
              mensaje: 'Se obtuvo el archivo correctamente',
              datos: datos
            });
          });
        } else {
          let delayTime = 100;
          // if(regenerarArchivo) delayTime = 3000;
          setTimeout(async function(){
            const datos = await app.dao.proyecto.leerArchivo(path);
            //console.log(">>>>>>>>>>>>",datos,"<<<<<<<<<<<<<<<")
            if(!datos){
              return res.status(200).json({
              finalizado: true,
              mensaje: 'No existe archivo ',
              datos: datos
            });
            }
            else{
              return res.status(200).json({
                finalizado: true,
                mensaje: 'Se obtuvo el archivo correctamente',
                datos: datos
              });
            }
          },delayTime);
        }
      } catch (err) {
        throw new errors.NotFoundError(`No se puede encontrar el archivo solicitado.`);
      }
    } catch (error) {
      next(error);
    }
  }

  async function descargarArchivo (req, res, next) {
    try {
      const ID_PROYECTO = req.params.id_proyecto;
      const CODIGO_DOCUMENTO = req.params.codigo_documento;
      const CODIGO_PROYECTO = await app.dao.proyecto.obtenerCodigoProyecto(ID_PROYECTO);
      let { nombreArchivo, carpeta } = await app.dao.reporte.obtenerNombreArchivoYCarpeta(CODIGO_DOCUMENTO);
      if (nombreArchivo.startsWith('[PAC_')) {
        await app.dao.reporte.crearPlanillaPorProyecto(ID_PROYECTO);
      }
      if (nombreArchivo.startsWith('[PA_')) {
        let datosSupervision = req.params.codigo_documento.split('_')[1];
        await app.dao.reporte.crearPlanilla(ID_PROYECTO, datosSupervision.split('-')[0], datosSupervision.split('-')[1]);
        nombreArchivo = nombreArchivo.replace('[SUP_','[PA_');
      }
      if (nombreArchivo.startsWith('[SUP_')) {
        let datosSupervision = req.params.codigo_documento.split('_')[1];
        let supervision = await app.dao.supervision.getDatos(datosSupervision);
        await app.dao.reporte.crearPlanilla(ID_PROYECTO, supervision.fid_adjudicacion, supervision.nro_supervision);
        nombreArchivo = nombreArchivo.replace('[SUP_','[PA_');
      }
      if (nombreArchivo.startsWith('[RIE')) {
        await app.dao.reporte.crearInformeResumen(nombreArchivo, ID_PROYECTO, req);
      }
      if (nombreArchivo.startsWith('[RIR')) {
        await app.dao.reporte.crearInformeRechazo(nombreArchivo, ID_PROYECTO, req);
      }

      const file = `${_path}/uploads/proyectos/${CODIGO_PROYECTO}/${carpeta}/${nombreArchivo}`;
      res.set('Content-Type', 'application/pdf');
      res.set('Content-Disposition', `attachment; filename="${nombreArchivo}"`);
      // Set timeout to write file to disk before download
      // setTimeout(function() {
      res.download(file);
      // }, 1500);
    } catch (error) {
      next(error);
    }
  }

  async function csvPorTipo (req, res, next) {
    const parametros = {
      tipoReporte: req.params.tipo,
      gestion: req.query.gestion,
      cartera:req.query.cartera,
      codigoDepartamento: req.query.dpto ? req.query.dpto : null,
      codigoMunicipio: req.query.municipio ? req.query.municipio : null
    };

    try {
      // console.log('-------params-----------------------');
      // console.log(req.params);
      // console.log('--------query-----------------------');
      // console.log(req.query);
      // console.log('------------------------------------');
      if (req.query.format === 'xlsx') {
        const datos = await app.dao.reporte.obtenerCSVPorTipo(parametros, 'xlsx');
        res.writeHead(200, {
          'Content-Disposition': `attachment; filename="reporte.xlsx"`,
          'Content-Type': 'application/octet-stream',
        })
        return res.end(datos);
      }
      const datos = await app.dao.reporte.obtenerCSVPorTipo(parametros);
      let mensaje = datos && datos.count > 0 ? 'Se obtuvieron los datos de los proyectos' : 'No existen proyectos para generar el reporte seleccionado';
      return res.status(200).json({
        finalizado: true,
        mensaje: mensaje,
        datos: datos
      });
    } catch (error) {
      next(error);
    }
  }

  async function csvGeneral (req, res, next) {
    try {
      if (req.query.format === 'xlsx') {
        const datos = await app.dao.reporte.obtenerCSVGeneral('xlsx',req.query.cartera);
        res.writeHead(200, {
          'Content-Disposition': `attachment; filename="reporte.xlsx"`,
          'Content-Type': 'application/octet-stream',
        })
        return res.end(datos);
      }
      const datos = await app.dao.reporte.obtenerCSVGeneral('',req.query.cartera);
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se obtuvo los datos de los proyectos',
        datos: datos
      });
    } catch (error) {
      next(error);
    }
  }

  async function informeEvaluacion (req, res, next) {
    return sequelize.transaction (async(t) => {
      const USUARIO = req.body.audit_usuario;
      const codigo = `${shortid.generate()}`;
      req.body.version = 1;
      // Obtener datos del proyecto
      let proyecto = Util.json(await app.dao.proyecto.obtenerDatosProyecto(req.body.id_proyecto));
      let financiamiento =  await app.dao.formulario.obtener({
        id_proyecto: proyecto.id_proyecto,
        $_plantilla: 2,
        $_nombre: 'financiamiento'
      });
      let beneficiarios =  await app.dao.formulario.obtener({
        id_proyecto: proyecto.id_proyecto,
        $_plantilla: 1,
        $_nombre: 'beneficiarios'
      });
      // Guardar informe de evaluacion
      let form_proy = {
        id_proyecto: req.body.id_proyecto,
        estado_proyecto: 'REGISTRO_PROYECTO_FDI', // TODO Debe serproyecto en estado EVALUACION obtenido desde estados FDI
      };
      // financiamiento y beneficiarios
      req.body.financiamiento = financiamiento.form_model;
      req.body.beneficiarios = beneficiarios.form_model;
      req.body.proyecto = {
        tipo: beneficiarios.form_model && beneficiarios.form_model.tipo ? beneficiarios.form_model.tipo.replace('OP_TP_','') : '',
        indicadores: []
      }
      for(let i in beneficiarios.form_model.productivo) {
        if (beneficiarios.form_model.productivo[i]) {
          req.body.proyecto.indicadores.push(nombreIndicador(i));
        }
      }
      if (req.body.proyecto.indicadores.length === 0) {
        req.body.proyecto.indicadores.push(req.body.proyecto.tipo);
      }
      req.body.nro_familias = beneficiarios.form_model.nro_familias||0;
      req.body.nro_comunidades = beneficiarios.form_model.nro_comunidades||0;
      req.body.comunidades = beneficiarios.form_model.comunidades;

      let resp;
      if (req.body.$_plantilla && req.body.$_nombre) {
        req.body.codigo_documento = codigo;
        delete req.body.audit_usuario;
        resp = await app.dao.formulario.crearActualizar(USUARIO,form_proy, req.body, t);
        await app.dao.formulario_historico.crear(resp, t);
      }
      const fechaActual = moment(resp._fecha_modificacion);
      // Agregando campos necesarios para el informe
      req.body.fechaActual = fechaActual;


      let { nombreArchivo, carpeta } = await app.dao.reporte.obtenerNombreArchivoYCarpeta('INF');
      nombreArchivo = nombreArchivo.replace('{{codigo}}', req.body.codigo_documento);
      carpeta = `${_path}/uploads/proyectos/${req.body.codigo}/${carpeta}`;
      Util.crearDirectorio(`${_path}/uploads/proyectos/`);
      Util.crearDirectorio(`${_path}/uploads/proyectos/${req.body.codigo}`);
      Util.crearDirectorio(`${carpeta}`);
      await app.dao.informe_evaluacion.crear(nombreArchivo, carpeta, proyecto, req.body, t);
      return {
        nombre: resp.nombre,
        codigo_documento: resp.codigo_documento,
        fecha_documento: resp.fecha_documento,
        _fecha_creacion: resp.fecha_documento
      };
    }).then( (datos)=>{
      res.status(200).json({
        finalizado: true,
        mensaje: 'Se obtuvo los datos de los proyectos',
        datos: datos
      });
    }).catch( (error)=>{
      next(error);
    });
  }

  async function informeHistorico (req, res, next) {
    const USUARIO = req.body.audit_usuario;
    try {
      let form = {
        id_proyecto: req.params.id_proyecto,
        id_plantilla: 102,
        nombre: 'informe',
      };
      let result = await app.dao.formulario_historico.listarHistorico(form);;

      res.json({
        finalizado: true,
        mensaje: 'Historial de evaluaciones obtenido correctamente.',
        datos: result
      });
    } catch(error) {
      next(error);
    }
  }

  async function fichaTecnica (req, res, next) {
    return sequelize.transaction (async(t) => {
      const codigo = `${shortid.generate()}`;
      req.body.version = 1;
      // Obtener datos del proyecto
      let proyecto = Util.json(await app.dao.proyecto.obtenerDatosProyecto(req.params.id));
      let financiamiento =  await app.dao.formulario.obtener({
        id_proyecto: proyecto.id_proyecto,
        $_plantilla: 2,
        $_nombre: 'financiamiento'
      });
      let beneficiarios =  await app.dao.formulario.obtener({
        id_proyecto: proyecto.id_proyecto,
        $_plantilla: 1,
        $_nombre: 'beneficiarios'
      });
      let convenio =  await app.dao.formulario.obtener({
        id_proyecto: proyecto.id_proyecto,
        $_plantilla: 5,
        $_nombre: 'convenio'
      });
      if(!convenio) {// NO TIENE CONVENIO
         convenio = {
          form_model:{}
        }
      } 
      let desembolsos =  await app.dao.formulario.obtener({
        id_proyecto: proyecto.id_proyecto,
        $_plantilla: 6,
        $_nombre: 'cronograma_desembolsos'
      });
      if(!desembolsos) {// NO TIENE DESEMBOLSOS
         desembolsos = {
          form_model:{}
        }
      } 
      let informe =  await app.dao.formulario.obtener({
        id_proyecto: proyecto.id_proyecto,
        $_plantilla: 102,
        $_nombre: 'informe'
      });
      if(!informe) {//NO TIENE INFORME
        informe ={
          form_model:{
            indicador:{
              descripcion:'n/a'
            },
            metas_resultados:'n/a'
          }
        }
      }
      // financiamiento y beneficiarios
      req.body.financiamiento = financiamiento.form_model;
      req.body.beneficiarios = beneficiarios.form_model;
      req.body.desembolsos = desembolsos.form_model;
      req.body.informe = informe.form_model;
      req.body.convenio = convenio.form_model;
      req.body.proyecto = {
        tipo: beneficiarios.form_model && beneficiarios.form_model.tipo ? beneficiarios.form_model.tipo.replace('OP_TP_','') : '',
        indicadores: []
      }
      req.body.datos = proyecto;
      for(let i in beneficiarios.form_model.productivo) {
        if (beneficiarios.form_model.productivo[i]) {
          req.body.proyecto.indicadores.push(nombreIndicador(i));
        }
      }

      let carpeta = `${_path}/uploads/`;
      Util.crearDirectorio(`${_path}/uploads/`);
      // Util.crearDirectorio(`${_path}/uploads/proyectos/${req.body.codigo}`);
      Util.crearDirectorio(`${carpeta}`);
      const stream = await app.dao.ficha_tecnica.crear(carpeta, req.body, t);
      if (stream) {
        const fileToBase64 = (stream) => {
          return new Promise((resolve, reject) => {
            stream.on('close', async () => {
              const datos = await app.dao.proyecto.leerArchivo(`/ficha_tecnica.pdf`);
              return resolve(datos);
            });
            stream.on('error', err => reject(err));
          });
        };
        return await fileToBase64(stream);
      }
      return {
        tipo: 'application/pdf',
      };
    }).then( (datos)=>{
      res.status(200).json({
        finalizado: true,
        mensaje: 'Se obtuvo los datos de los proyectos',
        datos: datos
      });
    }).catch( (error)=>{
      next(error);
    });
  }

  // Obtener nombre segun tipo dato
  function nombreIndicador(indicador) {
    switch(indicador) {
      case 'agricola':
        return 'Agrícola';
      case 'prod_agroeco_organica':
        return 'Producción agroecológica y orgánica';
      case 'integral_sustentable_bosque':
        return 'Manejo integral y sustentable de bosques';
      case 'sis_agroforestales':
        return 'Manejo de sistemas agroforestales';
      case 'aprovecha_biodiversidad':
        return 'Aprovechamiento sustentable de la biodiversidad';
      case 'apicultura':
        return 'Apicultura';
      case 'pecuario':
        return 'Pecuario';
      case 'piscicultura':
        return 'Piscicultura';
      case 'transformacion':
        return 'Transformación';
      case 'maquinaria':
        return 'Maquinaria';
      case 'infraestructura':
        return 'Infraestructura';
    }
    return '';
  }

  app.controller.reporte.crearFormularios = crearFormularios;
  app.controller.reporte.crearPlanilla = crearPlanilla;
  app.controller.reporte.descargarFormularios = descargarFormularios;
  app.controller.reporte.crearArchivo = crearArchivo;
  app.controller.reporte.subirArchivo = subirArchivo;
  app.controller.reporte.listarArchivos = listarArchivos;
  app.controller.reporte.obtenerArchivo = obtenerArchivo;
  app.controller.reporte.descargarArchivo = descargarArchivo;
  app.controller.reporte.csvPorTipo = csvPorTipo;
  app.controller.reporte.csvGeneral = csvGeneral;
  app.controller.reporte.informeEvaluacion = informeEvaluacion;
  app.controller.reporte.informeHistorico = informeHistorico;
  app.controller.reporte.fichaTecnica = fichaTecnica;
};
