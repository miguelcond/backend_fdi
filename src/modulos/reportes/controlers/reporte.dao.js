import util from '../../../lib/util';
import errors from '../../../lib/errors';
import fs from 'fs';
import path from 'path';
import archiver from 'archiver';
import moment from 'moment';
import seqOpt from '../../../lib/sequelize-options';
import XLSX from 'xlsx';

module.exports = (app) => {
  app.dao.reporte = {};
  const models = app.src.db.models;
  const sequelize = app.src.db.sequelize;

  async function crearPlanilla (idProyecto, idAdjudicacion, nroSupervision) {
    // const SUPERVISION = await app.dao.supervision.getDatos(idSupervision);
    const SUPERVISION = await app.dao.supervision.getDatosPorProyectoyNumero(idProyecto, idAdjudicacion, nroSupervision);
    const idSupervision = SUPERVISION.id_supervision;
    if (!SUPERVISION) {
      throw new errors.NotFoundError(`No existe la supervision solicitada.`);
    }
    // const datosProyecto = await app.dao.proyecto.obtenerDatosProyecto(SUPERVISION.fid_proyecto);
    const datosProyecto = await app.dao.proyecto.obtenerDatosProyectoParaPlanilla(SUPERVISION.fid_proyecto);
    const CODIGO_PROYECTO = datosProyecto.codigo;
    const rutaProyecto = await crearObtenerDirectorioProyecto(CODIGO_PROYECTO);
    const rutaCertificados = path.join(rutaProyecto, 'certificados');
    const rutaPlanillas = path.join(rutaProyecto, 'planillas');
    const rutaComputos = path.join(rutaProyecto, 'computos');
    const RESUMEN_ITEMS = [];
    for (var i = 0; i < SUPERVISION.modulos.length; i++) {
      const MOD = SUPERVISION.modulos[i];
      for (var j = 0; j < MOD.items.length; j++) {
        const ITEM = MOD.items[j];
        const sum = await models.computo_metrico.getSumCantidadParcial(ITEM.id_item, 'lt', idSupervision);
        const CANTIDAD_ANTERIOR = sum || 0;
        const RESUMEN = {
          total_actual: 0,
          cant_ejecutada: 0,
          cant_faltante: 0,
          cant_anterior: CANTIDAD_ANTERIOR
        };
        const COMPUTOS = await app.dao.computoMetrico.getPorItemSupervision(ITEM.id_item, SUPERVISION.id_supervision);
        RESUMEN.total_actual = 0;
        COMPUTOS.forEach(computo => {
          RESUMEN.total_actual += parseFloat(computo.cantidad_parcial) || 0;
        });
        RESUMEN.total_actual = util.redondear(RESUMEN.total_actual);
        RESUMEN.cant_ejecutada = util.sumar([RESUMEN.total_actual, RESUMEN.cant_anterior]);
        RESUMEN.cant_faltante = util.restar(ITEM.cantidad, RESUMEN.cant_ejecutada);
        if (COMPUTOS.length > 0) {
          const xfotos = await app.dao.fotoSupervision.obtenetFotos(idSupervision, ITEM.id_item);
          const FOTOGRAFIAS = xfotos.map(foto => {
            return {
              id_foto_supervision: foto.id_foto_supervision,
              nombre: foto.path_fotografia.substring(foto.path_fotografia.indexOf('_') + 1),
              path: foto.path_fotografia
            };
          });
          RESUMEN_ITEMS.push({
            nro: ITEM.nro_item,
            descripcion: ITEM.nombre,
            unidad: ITEM.unidad.nombre.toUpperCase(),
            cantidad_ejecutada: util.convertirNumeroAString(RESUMEN.cant_ejecutada)
          });
          await app.dao.computo.crear(rutaComputos, datosProyecto, MOD.nombre, ITEM, RESUMEN, COMPUTOS, FOTOGRAFIAS, SUPERVISION.nro_supervision);
        }
      }
    }
    await app.dao.planillaAvance.crear(rutaPlanillas, datosProyecto, SUPERVISION.fid_adjudicacion, SUPERVISION.nro_supervision);
    await app.dao.certificadoAvance.crear(rutaCertificados, datosProyecto, SUPERVISION, RESUMEN_ITEMS);
    await app.dao.computo_resumen.crear(rutaComputos, datosProyecto, SUPERVISION, RESUMEN_ITEMS);
  }

  async function crearPlanillaPorProyecto (idProyecto) {
    // const SUPERVISION = await app.dao.supervision.getDatos(idSupervision);
    // const datosProyecto = await app.dao.proyecto.obtenerDatosProyectoParaPlanilla(idProyecto);
    const datosProyecto = await app.dao.proyecto.obtenerDatosParaPlanilla(idProyecto, null, false, false);
    const CODIGO_PROYECTO = datosProyecto.codigo;
    const rutaProyecto = await crearObtenerDirectorioProyecto(CODIGO_PROYECTO);
    const rutaPlanillas = path.join(rutaProyecto, 'planillas');
    await app.dao.planillaAvance.crearPorProyecto(rutaPlanillas, datosProyecto, idProyecto);
  }

  async function crearInformeResumen (nombreArchivo, idProyecto, req) {
    const datosProyecto = await app.dao.proyecto.obtenerDatosProyecto(idProyecto);
    const CODIGO_PROYECTO = datosProyecto.codigo;
    const rutaProyecto = await crearObtenerDirectorioProyecto(CODIGO_PROYECTO);
    const rutaEvaluaciones = path.join(rutaProyecto, 'evaluaciones');
    await app.dao.informe_evaluacion.crearResumen(nombreArchivo, rutaEvaluaciones, datosProyecto, idProyecto, req);
  }

  async function crearInformeRechazo (nombreArchivo, idProyecto, req) {
    const datosProyecto = await app.dao.proyecto.obtenerDatosProyecto(idProyecto);
    const CODIGO_PROYECTO = datosProyecto.codigo;
    const rutaProyecto = await crearObtenerDirectorioProyecto(CODIGO_PROYECTO);
    const rutaEvaluaciones = path.join(rutaProyecto, 'evaluaciones');
    await app.dao.formulario_evaluacion.crearInformeRechazo(nombreArchivo, rutaEvaluaciones, datosProyecto, idProyecto, req);
  }

  async function crearArchivo (idProyecto, codigoDocumento) {
    const datosProyecto = await app.dao.proyecto.obtenerDatosProyecto(idProyecto);
    const datosItems = (await app.dao.item.obtenerDatosItems(idProyecto)).rows;
    if (!datosProyecto) {
      throw new errors.NotFoundError(`No existe el proyecto.`);
    }
    const CODIGO_PROYECTO = datosProyecto.codigo;
    const rutaProyecto = await crearObtenerDirectorioProyecto(CODIGO_PROYECTO);
    const rutaCertificados = path.join(rutaProyecto, 'certificados');
    const rutaFormularios = path.join(rutaProyecto, 'formularios');
    const rutaPlanillas = path.join(rutaProyecto, 'planillas');
    const rutaComputos = path.join(rutaProyecto, 'computos');
    switch (codigoDocumento) {
      case 'A1': return app.dao.formularioA1.crear(rutaFormularios, datosProyecto);
      case 'A2a': return app.dao.formularioA2a.crear(rutaFormularios, datosProyecto);
      case 'A2b': return app.dao.formularioA2b.crear(rutaFormularios, datosProyecto);
      case 'A3': return app.dao.formularioA3.crear(rutaFormularios, datosProyecto);
      case 'A4': return app.dao.formularioA4.crear(rutaFormularios, datosProyecto);
      case 'A7': return app.dao.formularioA7.crear(rutaFormularios, datosProyecto);
      case 'A8': return app.dao.formularioA8.crear(rutaFormularios, datosProyecto);
      case 'B1': return app.dao.formularioB1.crear(rutaFormularios, datosItems);
      default:
        const parametros = codigoDocumento.split('_');
        if (codigoDocumento.startsWith('A2c_') && (parametros.length === 2)) {
          const NRO_ASOCIADO = parseInt(parametros[1]);
          if (datosProyecto.documentacion_exp.datos_asociacion.asociados[NRO_ASOCIADO-1]) {
            return app.dao.formularioA2c.crear(rutaFormularios, datosProyecto, NRO_ASOCIADO);
          }
        }
        if (codigoDocumento.startsWith('A5_') && (parametros.length === 2)) {
          const NRO_EMPLEADO = parseInt(parametros[1]);
          let nro = 1;
          for (let i in datosProyecto.documentacion_exp.empleados) {
            let empleado = datosProyecto.documentacion_exp.empleados[i];
            if (['GERENTE', 'SUPERINTENDENTE', 'DIRECTOR', 'RESIDENTE'].includes(empleado.cargo_actual) && (nro++ === NRO_EMPLEADO)) {
              return app.dao.formularioA5.crear(rutaFormularios, datosProyecto, empleado.id_empleado, empleado.cargo_actual, (nro - 1));
            }
          }
        }
        if (codigoDocumento.startsWith('A6_') && (parametros.length === 2)) {
          const NRO_EMPLEADO = parseInt(parametros[1]);
          let nro = 1;
          for (let i in datosProyecto.documentacion_exp.empleados) {
            let empleado = datosProyecto.documentacion_exp.empleados[i];
            if ((empleado.cargo_actual === 'ESPECIALISTA') && (nro++ === NRO_EMPLEADO)) {
              return app.dao.formularioA6.crear(rutaFormularios, datosProyecto, empleado.id_empleado, nro - 1);
            }
          }
        }
        if (codigoDocumento.startsWith('PAC_') && (parametros.length === 2)) {
          const ID_PROYECTO = parseInt(parametros[1]);
          return app.dao.planillaAvance.crearPorProyecto(rutaPlanillas, datosProyecto, ID_PROYECTO);
        }
        if (codigoDocumento.startsWith('PA_') && (parametros.length === 2)) {
          const ID_ADJUDICACION = parseInt(parametros[1].split('-')[0]);
          const NRO_PLANILLA = parseInt(parametros[1].split('-')[1]);
          return app.dao.planillaAvance.crear(rutaPlanillas, datosProyecto, ID_ADJUDICACION, NRO_PLANILLA);
        }
        if (codigoDocumento.startsWith('CA_') && (parametros.length === 2)) {
          const NRO_CERTIFICADO = parseInt(parametros[1]);
          return app.dao.certificadoAvance.crear(rutaCertificados, datosProyecto, NRO_CERTIFICADO);
        }
        if (codigoDocumento.startsWith('CM_') && (parametros.length === 3)) {
          const NRO_ITEM = parseInt(parametros[1]);
          const NRO_PLANILLA = parseInt(parametros[2]);
          return app.dao.computo.crear(rutaComputos, datosProyecto, NRO_ITEM, NRO_PLANILLA);
        }
    }
    throw new errors.ValidationError(`El código del documento es inválido.`);
  }

  async function crearFormularios (idProyecto) {
    const datosProyecto = await app.dao.proyecto.obtenerDatosProyecto(idProyecto);
    if (!datosProyecto) {
      throw new errors.NotFoundError(`No existe el proyecto.`);
    }
    const datos = datosProyecto.documentacion_exp;
    await crearArchivo(idProyecto, 'A1');
    await crearArchivo(idProyecto, 'A2a');
    if(datos.datos_asociacion && datos.datos_asociacion.nombre_asociacion && datos.datos_asociacion.asociados && datos.datos_asociacion.asociados.length>1) { // Verifica tiene por lo menos un asociado
      await crearArchivo(idProyecto, 'A2b');
      for (let i in datos.datos_asociacion.asociados) {
        await crearArchivo(idProyecto, `A2c_${1+parseInt(i)}`);
      }
    }
    await crearArchivo(idProyecto, 'A3');
    await crearArchivo(idProyecto, 'A4');
    const empleados = datos.empleados;
    let nro = 1;
    for (let i in empleados) {
      let empleado = empleados[i];
      if (['GERENTE', 'SUPERINTENDENTE', 'DIRECTOR', 'RESIDENTE'].includes(empleado.cargo_actual)) {
        await crearArchivo(idProyecto, `A5_${nro++}`);
      }
    }
    nro = 1;
    for (let i in empleados) {
      let empleado = empleados[i];
      if (empleado.cargo_actual === 'ESPECIALISTA') {
        await crearArchivo(idProyecto, `A6_${nro++}`);
      }
    }
    await crearArchivo(idProyecto, 'A7');
    await crearArchivo(idProyecto, 'B1');
  }

  async function crearFormulariosZIP (idProyecto, res) {
    const datosProyecto = await app.dao.proyecto.obtenerDatosProyecto(idProyecto);
    if (!datosProyecto) {
      throw new errors.ValidationError(`No se encontró el reporte solicitado.`);
    }
    const CODIGO_PROYECTO = datosProyecto.codigo;
    const rutaProyecto = await crearObtenerDirectorioProyecto(CODIGO_PROYECTO);
    const rutaFormularios = path.join(rutaProyecto, 'formularios');
    let archivos = [];
    fs.readdirSync(rutaFormularios).forEach((filename) => {
      const pathFile = path.join(rutaFormularios, filename);
      archivos.push({ file: pathFile, filename: filename });
    });
    await comprimir(res, archivos);
  }

  async function comprimir (res, archivos) {
    var archive = archiver('zip', {
      zlib: { level: 9 } // Nivel de compresión.
    });
    archive.on('error', function (err) {
      throw err;
    });
    archive.pipe(res);
    for (let i in archivos) {
      let file = archivos[i].file;
      let filename = archivos[i].filename;
      if (filename.endsWith('.pdf')) {
        archive.append(fs.createReadStream(file), { name: filename });
      }
    }
    archive.finalize();
  }

  async function crearObtenerDirectorioProyecto (codigoProyecto) {
    util.crearDirectorio(`${_path}/uploads`);
    util.crearDirectorio(`${_path}/uploads/proyectos`);
    util.crearDirectorio(`${_path}/uploads/proyectos/${codigoProyecto}`);
    util.crearDirectorio(`${_path}/uploads/proyectos/${codigoProyecto}/documentos`);
    util.crearDirectorio(`${_path}/uploads/proyectos/${codigoProyecto}/evaluaciones`);
    util.crearDirectorio(`${_path}/uploads/proyectos/${codigoProyecto}/planillas`);
    util.crearDirectorio(`${_path}/uploads/proyectos/${codigoProyecto}/formularios`);
    util.crearDirectorio(`${_path}/uploads/proyectos/${codigoProyecto}/certificados`);
    util.crearDirectorio(`${_path}/uploads/proyectos/${codigoProyecto}/computos`);
    util.crearDirectorio(`${_path}/uploads/proyectos/${codigoProyecto}/especificaciones`);
    const path = `${_path}/uploads/proyectos/${codigoProyecto}`;
    return path;
  }

  async function obtenerListaArchivos (idProyecto, query = {}) {
    const datosProyecto = await app.dao.proyecto.obtenerDatosProyecto(idProyecto);
    if (!datosProyecto) {
      throw new errors.ValidationError(`No se encontró el reporte solicitado.`);
    }
    const CODIGO_PROYECTO = datosProyecto.codigo;
    const rutaProyecto = await crearObtenerDirectorioProyecto(CODIGO_PROYECTO);

    const rutaCertificados = path.join(rutaProyecto, 'certificados');
    const rutaDocumentos = path.join(rutaProyecto, 'documentos');
    const rutaFormularios = path.join(rutaProyecto, 'formularios');
    const rutaPlanillas = path.join(rutaProyecto, 'planillas');
    const rutaComputos = path.join(rutaProyecto, 'computos');
    const rutaEspecificaciones = path.join(rutaProyecto, 'especificaciones');

    let certificados = [];
    let documentos = [];
    let formularios = [];
    let planillas = [];
    let computos = [];
    let especificaciones = [];

    fs.readdirSync(rutaCertificados).forEach((filename) => { certificados.push(obtenerDocumentoInfo(filename)); });
    fs.readdirSync(rutaDocumentos).forEach((filename) => { documentos.push(obtenerDocumentoInfo(filename)); });
    fs.readdirSync(rutaFormularios).forEach((filename) => { formularios.push(obtenerDocumentoInfo(filename)); });
    fs.readdirSync(rutaPlanillas).forEach((filename) => { planillas.push(obtenerDocumentoInfo(filename)); });
    fs.readdirSync(rutaComputos).forEach((filename) => { computos.push(obtenerDocumentoInfo(filename)); });
    fs.readdirSync(rutaEspecificaciones).forEach((filename) => { especificaciones.push(obtenerDocumentoInfo(filename)); });

    const output = {
      fields: {
        certificados: ['array'],
        documentos: ['array'],
        formularios: ['array'],
        planillas: ['array'],
        computos: ['array'],
        especificaciones: ['array']
      }
    };
    let listaArchivos = { certificados, documentos, formularios, planillas, computos, especificaciones };
    let listaArchivosOptimizada = seqOpt.createResult(query, listaArchivos, output);
    return listaArchivosOptimizada;
  }

  function obtenerDocumentoInfo (filename) {
    return {
      tipo: 'application/pdf',
      codigo_documento: filename.substr(1, filename.indexOf(']') - 1),
      nombre: filename.substr(filename.indexOf(']') + 1).replace('.pdf', ''),
      nombre_archivo: filename
    };
  }

  async function obtenerNombreArchivoYCarpeta (codigoDocumento, nombreExtra) {
    switch (codigoDocumento) {
      case 'A1': return {carpeta: 'formularios', nombreArchivo: '[A1] Formulario A1.pdf'};
      case 'A2a': return {carpeta: 'formularios', nombreArchivo: '[A2a] Formulario A2a (Identificacion del proponente).pdf'};
      case 'A2b': return {carpeta: 'formularios', nombreArchivo: '[A2b] Formulario A2b (Identificacion de la asociacion).pdf'};
      case 'A3': return {carpeta: 'formularios', nombreArchivo: '[A3] Formulario A3 (Experiencia general de la empresa).pdf'};
      case 'A4': return {carpeta: 'formularios', nombreArchivo: '[A4] Formulario A4 (Experiencia especifica de la empresa).pdf'};
      case 'A7': return {carpeta: 'formularios', nombreArchivo: '[A7] Formulario A7 (Equipo minimo).pdf'};
      case 'A8': return {carpeta: 'formularios', nombreArchivo: '[A8] Formulario A8 (Cronograma de ejecucion).pdf'};
      case 'B1': return {carpeta: 'formularios', nombreArchivo: '[B1] Formulario B1 (Presupuesto por items y general de la obra).pdf'};
      case 'B2': return {carpeta: 'formularios', nombreArchivo: '[B2] Formulario B2 (Analisis de precios unitarios).pdf'};
      case 'B5': return {carpeta: 'formularios', nombreArchivo: '[B5] Formulario B5 (Cronograma de desembolsos).pdf'};
      case 'BG': return {carpeta: 'documentos', nombreArchivo: '[BG] Boleta de garantia.pdf'};
      case 'C': return {carpeta: 'documentos', nombreArchivo: '[C] Convenio.pdf'};
      case 'CXT': return {carpeta: 'documentos', nombreArchivo: '[CXT] Extension de convenio.pdf'};
      case 'CXT2': return {carpeta: 'documentos', nombreArchivo: '[CXT2] Extension de convenio.pdf'};
      case 'CXT3': return {carpeta: 'documentos', nombreArchivo: '[CXT3] Extension de convenio.pdf'};
      case 'CXT4': return {carpeta: 'documentos', nombreArchivo: '[CXT4] Extension de convenio.pdf'};
      case 'CXT5': return {carpeta: 'documentos', nombreArchivo: '[CXT5] Extension de convenio.pdf'};
      case 'RM': return {carpeta: 'documentos', nombreArchivo: '[RM] Resolucion ministerial.pdf'};
      case 'CIP': return {carpeta: 'documentos', nombreArchivo: '[CIP] Certificado de inscripcion presupuestaria.pdf'};
      case 'CIP2': return {carpeta: 'documentos', nombreArchivo: '[CIP2] Certificado de inscripcion presupuestaria.pdf'};
      case 'CIP3': return {carpeta: 'documentos', nombreArchivo: '[CIP3] Certificado de inscripcion presupuestaria.pdf'};
      case 'DBC': return {carpeta: 'documentos', nombreArchivo: '[DBC] Documento base de contratacion.pdf'};
      case 'ET': return {carpeta: 'documentos', nombreArchivo: '[ET] Especificaciones tecnicas.pdf'};
      case 'EVAL': return {carpeta: 'evaluaciones', nombreArchivo: 'Evaluacion_{{codigo}}.pdf'};
      case 'INF': return {carpeta: 'evaluaciones', nombreArchivo: 'Informe_{{codigo}}.pdf'};
      case 'EVE': return {carpeta: 'evaluaciones', nombreArchivo: '[EVE] Evaluacion externa.pdf'};
      case 'RIE': return {carpeta: 'evaluaciones', nombreArchivo: '[RIE] Resumen de informe de evaluacion.pdf'};
      case 'RIR': return {carpeta: 'evaluaciones', nombreArchivo: '[RIR] Informe de Rechazo.pdf'};
    }
    if (codigoDocumento) {
      const parametros = codigoDocumento.split('_');
      if (codigoDocumento.startsWith('A2c_') && (parametros.length === 2)) {
        return {carpeta: 'formularios', nombreArchivo: `[${codigoDocumento}] Formulario A2c - No.${parametros[1]} (Identificacion del integrante).pdf`};
      }
      if (codigoDocumento.startsWith('A5_') && (parametros.length === 2)) {
        return {carpeta: 'formularios', nombreArchivo: `[${codigoDocumento}] Formulario A5 - No.${parametros[1]} (Hoja de vida).pdf`};
      }
      if (codigoDocumento.startsWith('A6_') && (parametros.length === 2)) {
        return {carpeta: 'formularios', nombreArchivo: `[${codigoDocumento}] Formulario A6 - No.${parametros[1]} (Hoja de vida del especialista).pdf`};
      }
      if (codigoDocumento.startsWith('PAC_') && (parametros.length === 2)) {
        return {carpeta: 'planillas', nombreArchivo: `[${codigoDocumento}] Planilla de Avance Consolidada.pdf`};
      }
      if (codigoDocumento.startsWith('PA_') && (parametros.length === 2)) {
        return {carpeta: 'planillas', nombreArchivo: `[${codigoDocumento}] Planilla de avance No.${parametros[1].split('-')[1]}.pdf`};
      }
      if (codigoDocumento.startsWith('SUP_') && (parametros.length === 2)) {
        let supervision = await app.dao.supervision.getDatos(parametros[1]);
        return {carpeta: 'planillas', nombreArchivo: `[SUP_${supervision.fid_adjudicacion}-${supervision.nro_supervision}] Planilla de avance No.${supervision.nro_supervision}.pdf`};
      }
      if (codigoDocumento.startsWith('CA_') && (parametros.length === 2)) {
        return {carpeta: 'certificados', nombreArchivo: `[${codigoDocumento}] Certificado de avance No.${parametros[1]}.pdf`};
      }
      if (codigoDocumento.startsWith('CM_') && (parametros.length === 3)) {
        return {carpeta: 'computos', nombreArchivo: `[${codigoDocumento}] item No.${parametros[1]} - Planilla de avance No.${parametros[2]}.pdf`};
      }
      if (codigoDocumento.startsWith('CMR_') && (parametros.length === 2)) {
        return {carpeta: 'computos', nombreArchivo: `[${codigoDocumento}] Resumen de computos metricos de la Planilla de avance No.${parametros[1]}.pdf`};
      }
      if (codigoDocumento.startsWith('ET_') && (parametros.length === 2)) {
        return {carpeta: 'especificaciones', nombreArchivo: `[${codigoDocumento}] Especificacion tecnica - Item.pdf`};
      }
      if (codigoDocumento.startsWith('ETA_') && (parametros.length === 2)) {
        return {carpeta: 'especificaciones', nombreArchivo: `[${codigoDocumento}] Especificaciones tecnicas - Adjudicacion.pdf`};
      }
      if (codigoDocumento.startsWith('APU_') && (parametros.length === 2)) {
        return {carpeta: 'especificaciones', nombreArchivo: `[${codigoDocumento}] Analisis precios unitarios - Item.pdf`};
      }
      if (codigoDocumento.startsWith('DET_') && (parametros.length === 2)) {
        return {carpeta: 'documentos', nombreArchivo: `[${codigoDocumento}] Designacion equipo tecnico.pdf`};
      }
      if (codigoDocumento.startsWith('ADJ_') && (parametros.length === 2)) {
        return {carpeta: 'documentos', nombreArchivo: `[${codigoDocumento}] Documento de Adjudicacion.pdf`};
      }
      if (codigoDocumento.startsWith('SupCP_') && (parametros.length === 2)) {
        return {carpeta: 'documentos', nombreArchivo: `[${codigoDocumento}] Comprobante de Pago.pdf`};
      }
      if (codigoDocumento.startsWith('RMP_')) {
        return {carpeta: 'documentos', nombreArchivo: `[${codigoDocumento}] Respaldo de la modificacion.pdf`};
      }
      if (codigoDocumento.startsWith('RMT_')) {
        return {carpeta: 'documentos', nombreArchivo: `[${codigoDocumento}] ${nombreExtra||''}.pdf`};
      }
      if (codigoDocumento.startsWith('CMPDF_')) {
        return {carpeta: 'fotos', nombreArchivo: `${codigoDocumento.replace('CMPDF_','')}`};
      }
    }
    throw new errors.ValidationError(`El código del documento es inválido..`);
  }

  async function obtenerCSVPorTipo (Parametros, format) {

    let options = {
      where: {},
      distinct: true,
      include: [
        {
          model: models.estado,
          as: 'estado_actual',
          attributes: ['codigo', 'nombre']
        },
        {
          model: models.formulario,
          as: 'formularios',
          attributes: ['fid_plantilla','nombre','form_model'],
          where: {
            fid_plantilla: {
              $lt: 50
            }
          }
        },
        {
          model: models.municipio,
          as: 'municipio',
          attributes: ['codigo_gam', 'nombre'],
          include: [
            {
              model: models.provincia,
              as: 'provincia',
              attributes: ['nombre'],
              include: [
                {
                  model: models.departamento,
                  as: 'departamento',
                  attributes: ['nombre']
                }
              ]
            }
          ]
        },
        {
          model: models.usuario,
          as: 'usuario',
          attributes: ['fid_persona'],
          include: [
            {
              model: models.persona,
              as: 'persona',
              attributes: ['nombres','primer_apellido','segundo_apellido']
            }
          ]
        },
      ],
      order: ['nro_convenio']
    };

    if (Parametros.tipoReporte === 'agrupadoFisicoFinanciero') {
      options.where = {
        estado_proyecto: {$notIn:['REGISTRO_SOLICITUD','ASIGNACION_TECNICO_FDI','REGISTRO_PROYECTO_FDI','RECHAZADO_FDI']},
        cartera: Parametros.cartera
      };
    } else if (Parametros.tipoReporte === 'agrupadoTiposDpto') {
      options.where = {
        fcod_municipio: {$like: `${Parametros.codigoDepartamento}%`},
        estado_proyecto: {$notIn:['REGISTRO_SOLICITUD','ASIGNACION_TECNICO_FDI','REGISTRO_PROYECTO_FDI','RECHAZADO_FDI']},
        cartera: Parametros.cartera
      };
    } else if (Parametros.tipoReporte === 'porMunicipio') {
      options.where = {
        fcod_municipio: Parametros.codigoMunicipio,
        cartera: Parametros.cartera
      };
    } else {
      // Por defecto reporte porDepartamento
      options.where = {
        fcod_municipio: {$like: `${Parametros.codigoDepartamento}%`},
        cartera: Parametros.cartera
      };

      options.include.push({
        model: models.supervision,
        as: 'supervisiones',
        attributes: ['mano_obra_calificada','mano_obra_no_calificada']
      });
    }

    let result = util.json(await models.proyecto.findAndCountAll(options));

    // let plantillas_base = util.json(await models.contenido_formulario.findOne({
    //   attributes: ['estructura'],
    //   where: {
    //       nombre: 'plantilla_csv_general',
    //       seccion: "1"
    //   }
    // }));

    for (let i in result.rows) {
      // Parsear municipio, provincia, departamento
      result.rows[i].departamento = result.rows[i].municipio.provincia.departamento.nombre;
      result.rows[i].provincia = result.rows[i].municipio.provincia.nombre;
      result.rows[i].codigo_gam = result.rows[i].municipio.codigo_gam;
      result.rows[i].municipio = result.rows[i].municipio.nombre;
      result.rows[i].tecnico_responsable = '';

      if (result.rows[i].usuario && result.rows[i].usuario.persona) {
        let persona = result.rows[i].usuario.persona;
        result.rows[i].tecnico_responsable = `${persona.nombres||''} ${persona.primer_apellido||''} ${persona.segundo_apellido||''}`;
      }

      if (result.rows[i].orden_proceder) {
        result.rows[i].orden_proceder = moment(result.rows[i].orden_proceder).format('YYYY-MM-DD');
      }

      if (result.rows[i].estado_actual && result.rows[i].estado_actual.nombre) {
        result.rows[i].estado_proyecto_descripcion = result.rows[i].estado_actual.nombre;
        delete result.rows[i].estado_actual;
      }

      delete result.rows[i].usuario;

      // Obtener datos de formularios dinamicos
      let evaluacion = await app.dao.formulario.obtenerFormulario(result.rows[i].id_proyecto, 'evaluacion');
      let informe = await app.dao.formulario.obtenerFormulario(result.rows[i].id_proyecto, 'informe');
      let resultado_evaluacion = '--';
      let metasResultados = '--';
      if (evaluacion) {
        if (evaluacion.form_model.datos5) {
          resultado_evaluacion = '---';
          if (evaluacion.form_model.datos5[0].cols && evaluacion.form_model.datos5[0].cols.length > 0) {
            evaluacion.form_model.datos5[0].cols.forEach((item) => {
              if (item.contenido && typeof item.contenido == "object") {
                if (item.contenido.value) {
                  resultado_evaluacion = item.contenido.name;
                }
              }
            });
          }
        }
      }
      if (informe) {
        if (informe.form_model.estado_evaluacion && informe.form_model.estado_evaluacion != "") {
          resultado_evaluacion = informe.form_model.estado_evaluacion;
          if (resultado_evaluacion == 'APROBAR') { resultado_evaluacion = 'APROBADO'; }
          if (resultado_evaluacion == 'RECHAZAR') { resultado_evaluacion = 'RECHAZADO'; }
        }
        if (informe.form_model.metas_resultados && informe.form_model.metas_resultados != "") {
          metasResultados = informe.form_model.metas_resultados;
        }
      }

      if (result.rows[i].estado_proyecto === 'RECHAZADO_FDI' || resultado_evaluacion == 'RECHAZO') {
        resultado_evaluacion = 'RECHAZADO';
      }

      result.rows[i].resultado_evaluacion = resultado_evaluacion;
      result.rows[i].metas_resultados = metasResultados;

      let financiamiento = await app.dao.formulario.obtenerFormulario(result.rows[i].id_proyecto, 'financiamiento');
      let avance_fisico = await app.dao.item.obtenerMontoProyecto(result.rows[i].id_proyecto);
      let porcentaje_avance_fisico = util.calcularPorcentaje(avance_fisico, financiamiento.form_model.monto_total_proyecto);
      porcentaje_avance_fisico = `${util.convertirNumeroAString(porcentaje_avance_fisico)} %`;

      let porcentaje_avance_financiero = util.calcularPorcentaje(avance_fisico, financiamiento.form_model.monto_total_proyecto);
      porcentaje_avance_financiero = `${util.convertirNumeroAString(porcentaje_avance_financiero)} %`;

      let monto_desembolso_fdi = 0;
      let porcentaje_desembolso_fdi = 0;

      let cronograma = await app.dao.formulario.obtenerFormulario(result.rows[i].id_proyecto, 'cronograma_desembolsos');
      if (cronograma) {
        monto_desembolso_fdi = cronograma.form_model.monto_1;
        porcentaje_desembolso_fdi = cronograma.form_model.porcentaje_1;
        let today = moment();
        if (cronograma.form_model.fecha_2) {
          let fecha_2 = moment(cronograma.form_model.fecha_2, 'YYYY-MM-DD');
          if (today.diff(fecha_2,'days') >= 0) {
            monto_desembolso_fdi += cronograma.form_model.monto_2;
            porcentaje_desembolso_fdi += cronograma.form_model.porcentaje_2;
            if (cronograma.form_model.fecha_3) {
              let fecha_3 = moment(cronograma.form_model.fecha_3, 'YYYY-MM-DD');
              if (today.diff(fecha_3,'days') >= 0) {
                monto_desembolso_fdi += cronograma.form_model.monto_3;
                porcentaje_desembolso_fdi += cronograma.form_model.porcentaje_3;
              }
            }
          }
        }
      }
      monto_desembolso_fdi = `${util.convertirNumeroAString(monto_desembolso_fdi)}`;
      porcentaje_desembolso_fdi = `${util.convertirNumeroAString(porcentaje_desembolso_fdi)} %`;

      let plantillas = {
        convenio: {
          nro_convenio: '',
          plazo_ejecucion: '',
          fecha_suscripcion: '',
          plazo_convenio: '',
        },
        cronograma_desembolsos: {
          fecha_1: '',
          fecha_2: '',
          fecha_3: '',
          monto_1: '0',
          monto_2: '0',
          monto_3: '0',
          monto_4: '0',
          codigo_sisin: '',
          porcentaje_1: '',
          porcentaje_2: '',
          porcentaje_3: '',
          porcentaje_4: '',
          fecha_inscripcion: '',
        },
        beneficiarios: {
          tipo: '',
          tipo_financiamiento: '',
          tamanio_proyecto: '',
          nro_familias: '',
          nro_comunidades: '',
          plazo_ejecucion: '',
          indicadores_impacto: '',
          indicadores_impacto_unidades: '',
          indicadores_impacto_otro: '',
          indicadores_impacto_numero: ''
        },
        financiamiento: {
          monto_fdi: '0',
          monto_otros: '0',
          monto_contraparte: '0',
          monto_beneficiarios: '0',
          monto_total_proyecto: '0'
        },
        avance:{
          fisico: `${porcentaje_avance_fisico}`,
          financiero: `${porcentaje_avance_financiero}`
        },
        desembolso_fdi:{
          monto: `${monto_desembolso_fdi}`,
          porcentaje: `${porcentaje_desembolso_fdi}`
        }
      };

      let forms = result.rows[i].formularios;
      for (let j in plantillas) {
        let form = forms.find((f) => { return f.nombre == j; });
        if (form) {
          plantillas[j] = objMerge(plantillas[j], form.form_model);
        }
        delete plantillas[j].$_nombre;
        delete plantillas[j].$_plantilla;
        delete plantillas[j].version;
        for (let k in plantillas[j]) {
          if (typeof(plantillas[j][k]) == 'object' && plantillas[j][k].nombre) {
            plantillas[j][k] = plantillas[j][k].nombre;
          }
          if (typeof(plantillas[j][k]) != 'object') {
            if (j == 'beneficiarios' && k == 'referencia') continue;
            if (j == 'res_administrativa' && k == 'fecha_emision') continue;
            if (j == 'cronograma_desembolsos' && k == 'categoria_programatica') continue;
            if (j == 'convenio' && k == 'plazo_ejecucion') continue;
            if (['beneficiarios'].indexOf(j) >= 0) {
              result.rows[i][`${k}`] = plantillas[j][k];
            } else {
              result.rows[i][`${j}.${k.replace('_convenio','')}`] = plantillas[j][k];
            }
          }
        }
      }

      // result.rows[i].subtipo_productivo = result.rows[i].subtipo_productivo ? result.rows[i].subtipo_productivo : null;
      // result.rows[i].indicadores_impacto = result.rows[i].indicadores_impacto ? result.rows[i].indicadores_impacto : null;
      // result.rows[i].indicadores_impacto_unidades = result.rows[i].indicadores_impacto_unidades ? result.rows[i].indicadores_impacto_unidades : null;
      // result.rows[i].indicadores_impacto_otro = result.rows[i].indicadores_impacto_otro ? result.rows[i].indicadores_impacto_otro : null;
      // result.rows[i].indicadores_impacto_numero = result.rows[i].indicadores_impacto_numero ? result.rows[i].indicadores_impacto_numero : null;

      delete result.rows[i].formularios;

      if (result.rows[i].supervisiones) {
        if (result.rows[i].supervisiones.length > 0) {
          let max_calificada = 0;
          let max_no_calificada = 0;
          let count = result.rows[i].supervisiones.length;

          for (let j = 0; j < result.rows[i].supervisiones.length; j++) {
            let sup = result.rows[i].supervisiones[j];
            max_calificada += parseInt(sup.mano_obra_calificada);
            max_no_calificada += parseInt(sup.mano_obra_no_calificada);
          }

          result.rows[i].mano_obra_calificada = Math.trunc(parseFloat(parseInt(max_calificada) / parseInt(count)));
          result.rows[i].mano_obra_no_calificada = Math.trunc(parseFloat(parseInt(max_no_calificada) / parseInt(count)));
        }

        delete result.rows[i].supervisiones;
      }
    }

    // Parseo según contenido de reporte
    let resultado = {};
    if (Parametros.tipoReporte === 'porMunicipio') {
      resultado = parseoReportePorMunicipio(result.rows);
    } else if (Parametros.tipoReporte === 'agrupadoTiposDpto') {
      resultado = parseReportePorTipoProyecto(result.rows);
    } else if (Parametros.tipoReporte === 'agrupadoFisicoFinanciero') {
      resultado = parseReporteFisicoFinanciero(result.rows);
    } else {
      // if (Parametros.tipoReporte === 'porDepartamento') {
      resultado = parseoReportePorDepartamento(result.rows, Parametros.tipoReporte);
      // }
    }

    if (format === 'xlsx') {
      const wb = XLSX.utils.book_new();
      wb.Props = {
        Title: "Reporte",
        Subject: "Proyectos",
        Author: "FDI",
        CreatedDate: new Date()
      };
      wb.SheetNames.push("hoja_reporte");
      const ws_data = resultado.rows;
      const ws = XLSX.utils.json_to_sheet(ws_data);
      wb.Sheets["hoja_reporte"] = ws;
      const wbout = XLSX.write(wb, {bookType:'xlsx',  type: 'buffer'});
      return wbout;
    }

    return resultado;
  }

  async function obtenerCSVGeneral (format,car) {
    let options = {
      attributes: ['id_proyecto','cartera','nombre','estado_proyecto','plazo_ejecucion','orden_proceder',
                    ['nro_convenio','codigo']],
      distinct: true,
      include: [
        {
          model: models.formulario,
          as: 'formularios',
          attributes: ['fid_plantilla','nombre','form_model'],
          where: {
            fid_plantilla: {
              $lt: 50
            }
          }
        },
        {
          model: models.municipio,
          as: 'municipio',
          attributes: ['nombre'],
          include: [
            {
              model: models.provincia,
              as: 'provincia',
              attributes: ['nombre'],
              include: [
                {
                  model: models.departamento,
                  as: 'departamento',
                  attributes: ['nombre']
                }
              ]
            }
          ]
        },
        {
          model: models.usuario,
          as: 'usuario',
          attributes: ['fid_persona'],
          include: [
            {
              model: models.persona,
              as: 'persona',
              attributes: ['nombres','primer_apellido','segundo_apellido']
            }
          ]
        },

      ],
      where: {
          cartera :car,
          estado:'ACTIVO'
        }
    };
    let result = util.json(await models.proyecto.findAndCountAll(options));

    // let plantillas_base = util.json(await models.contenido_formulario.findOne({
    //   attributes: ['estructura'],
    //   where: {
    //       nombre: 'plantilla_csv_general',
    //       seccion: "1"
    //   }
    // }));

    for (let i in result.rows) {
      // Parsear municipio, provincia, departamento
      result.rows[i].num = parseInt(i) + 1;
      result.rows[i].provincia = result.rows[i].municipio.provincia.nombre;
      result.rows[i].departamento = result.rows[i].municipio.provincia.departamento.nombre;
      result.rows[i].municipio = result.rows[i].municipio.nombre;
      result.rows[i].tecnico_responsable = '';
      if (result.rows[i].usuario && result.rows[i].usuario.persona) {
        let persona = result.rows[i].usuario.persona;
        result.rows[i].tecnico_responsable = `${persona.nombres||''} ${persona.primer_apellido||''} ${persona.segundo_apellido||''}`;
      }
      if (result.rows[i].orden_proceder) {
        result.rows[i].orden_proceder = moment(result.rows[i].orden_proceder).format('YYYY-MM-DD');
      }

      delete result.rows[i].usuario;
      // Obtener datos de formularios dinamicos
      // TODO obtener campos y sus valores por defecto de las plantillas de formularios, se debe crear la tabla de plantillas
      // let plantillas = Object.assign({}, plantillas_base.estructura);
      let evaluacion = await app.dao.formulario.obtenerFormulario(result.rows[i].id_proyecto,'evaluacion');
      let informe = await app.dao.formulario.obtenerFormulario(result.rows[i].id_proyecto,'informe');
      let resultado_evaluacion = '--';
      if(evaluacion){
        //throw new errors.NotFoundError(`No existe el proyecto.`);
        if(evaluacion.form_model.datos5 && evaluacion.form_model.datos5[0]){
          resultado_evaluacion = '---';
          if(evaluacion.form_model.datos5[0].cols && evaluacion.form_model.datos5[0].cols.length > 0){
            evaluacion.form_model.datos5[0].cols.forEach((item)=>{
              if(item.contenido && typeof item.contenido == "object"){
                if(item.contenido.value){
                  resultado_evaluacion = item.contenido.name;
                }
              }
            });
          }
        }
      }
      if (informe) {
        if (informe.form_model.estado_evaluacion && informe.form_model.estado_evaluacion != "") {
          resultado_evaluacion = informe.form_model.estado_evaluacion;
          if (resultado_evaluacion == 'APROBAR') { resultado_evaluacion = 'APROBADO'; }
          if (resultado_evaluacion == 'RECHAZAR') { resultado_evaluacion = 'RECHAZADO'; }
        }
      }

      if (result.rows[i].estado_proyecto === 'RECHAZADO_FDI' || resultado_evaluacion == 'RECHAZO') {
        resultado_evaluacion = 'RECHAZADO';
      }
      result.rows[i].resultado_evaluacion = resultado_evaluacion;

      let financiamiento = await app.dao.formulario.obtenerFormulario(result.rows[i].id_proyecto,'financiamiento');

      let avance_fisico = await app.dao.item.obtenerAvanceEjecutado(result.rows[i].id_proyecto);
      // calculo de % avance fisico financiero es sobre el monto fdi
      let porcentaje_avance_fisico = util.calcularPorcentaje(avance_fisico,financiamiento.form_model.monto_fdi);
      porcentaje_avance_fisico = `${util.convertirNumeroAString(porcentaje_avance_fisico)} %`;

      let porcentaje_avance_financiero = util.calcularPorcentaje(avance_fisico,financiamiento.form_model.monto_fdi);
      porcentaje_avance_financiero = `${util.convertirNumeroAString(porcentaje_avance_financiero)} %`;

      let monto_desembolso_fdi = 0;
      let porcentaje_desembolso_fdi = 0;

      let cronograma = await app.dao.formulario.obtenerFormulario(result.rows[i].id_proyecto,'cronograma_desembolsos');
      if (cronograma) {
        monto_desembolso_fdi = cronograma.form_model.monto_1;
        porcentaje_desembolso_fdi = cronograma.form_model.porcentaje_1;
        let today = moment();
        if (cronograma.form_model.fecha_2) {
          let fecha_2 = moment(cronograma.form_model.fecha_2, 'YYYY-MM-DD');
          if (today.diff(fecha_2,'days') >= 0) {
            monto_desembolso_fdi += cronograma.form_model.monto_2;
            porcentaje_desembolso_fdi += cronograma.form_model.porcentaje_2;
            if (cronograma.form_model.fecha_3) {
              let fecha_3 = moment(cronograma.form_model.fecha_3,'YYYY-MM-DD');
              if (today.diff(fecha_3,'days') >= 0) {
                monto_desembolso_fdi += cronograma.form_model.monto_3;
                porcentaje_desembolso_fdi += cronograma.form_model.porcentaje_3;
              }
            }
          }
        }
      }
      monto_desembolso_fdi = `${util.convertirNumeroAString(monto_desembolso_fdi)}`;
      porcentaje_desembolso_fdi = `${util.convertirNumeroAString(porcentaje_desembolso_fdi)} %`;

      let plantillas = {
        convenio: {
          nro_convenio: '',
          plazo_ejecucion: '',
          fecha_suscripcion: '',
          plazo_convenio: '',
        },
        cronograma_desembolsos: {
          fecha_1: '',
          fecha_2: '',
          fecha_3: '',
          monto_1: '0',
          monto_2: '0',
          monto_3: '0',
          monto_4: '0',
          codigo_sisin: '',
          porcentaje_1: '',
          porcentaje_2: '',
          porcentaje_3: '',
          porcentaje_4: '',
          fecha_inscripcion: '',
        },
        beneficiarios: {
          tipo: '',
          tipo_financiamiento: '',
          tamanio_proyecto: '',
          nro_familias: '',
          nro_comunidades: '',
          comunidades: '',
          plazo_ejecucion: '',
          indicadores_impacto: '',
          indicadores_impacto_unidades: '',
          indicadores_impacto_otro: '',
          indicadores_impacto_numero: ''
        },
        financiamiento: {
          monto_fdi: '0',
          monto_otros: '0',
          monto_contraparte: '0',
          monto_beneficiarios: '0',
          monto_total_proyecto: '0'
        },
        avance:{
          ejecutado:`${avance_fisico}`,
          fisico: `${porcentaje_avance_fisico}`,
          financiero: `${porcentaje_avance_financiero}`
        },
        desembolso_fdi:{
          monto: `${monto_desembolso_fdi}`,
          porcentaje: `${porcentaje_desembolso_fdi}`
        }
      };

      let forms = result.rows[i].formularios;
      // console.log(forms,"formularioooooooooo")
      for (let j in plantillas) {
        let form = forms.find((f) => { return f.nombre == j; });
        if (form) {
          plantillas[j] = objMerge(plantillas[j], form.form_model);
        }
        delete plantillas[j].$_nombre;
        delete plantillas[j].$_plantilla;
        delete plantillas[j].version;
        for (let k in plantillas[j]) {
          // console.log(Array.isArray(plantillas[j][k]),plantillas[j][k])
          // console.log(k)
          if (j=='beneficiarios' && k=='comunidades' && Array.isArray(plantillas[j][k])) {
            let concatenado = '';
            for (let l in plantillas[j][k]) {
              if (plantillas[j][k][l] && plantillas[j][k][l].nombre) {
                if (concatenado.length === 0) {
                  concatenado = plantillas[j][k][l].nombre;
                } else {
                  concatenado = concatenado + ', ' + plantillas[j][k][l].nombre;
                }
              }
            }
            plantillas[j][k] = concatenado;
          }
          // if(j == 'beneficiarios'){
          //    console.log(j,k,plantillas[j][k],typeof(plantillas[j][k]))  
 
          // }
          
          if (j=='beneficiarios' && k=='indicadores_impacto' && typeof(plantillas[j][k])=='object') {
            // console.log("entro")
            // console.log(j,k,plantillas[j][k][0])
            // console.log('es objeto?',typeof(plantillas[j][k][0].unidades)=='object')
            if (typeof(plantillas[j][k][0].unidades)=='object' && !(plantillas[j][k][0].unidades==null)){
              plantillas[j].indicadores_impacto_unidades = plantillas[j][k][0].unidades.nombre ? plantillas[j][k][0].unidades.nombre : '';
            }else{
              plantillas[j].indicadores_impacto_unidades = plantillas[j][k][0].unidades ? plantillas[j][k][0].unidades: '';
            }
            plantillas[j].indicadores_impacto_numero = plantillas[j][k][0].numero ? plantillas[j][k][0].numero: '';
            plantillas[j].indicadores_impacto_otro = plantillas[j][k][0].otro ? plantillas[j][k][0].otro: '';
            plantillas[j].indicadores_impacto = plantillas[j][k][0].combinado ? plantillas[j][k][0].combinado: '';


            // console.log(plantillas[j].indicadores_impacto_unidades)

            // if (plantillas[j][k][0].combinado == null){
            //   plantillas[j][k]= '';
            // }else{
            //   plantillas[j][k]=plantillas[j][k][0].combinado;
            // }
            // console.log(plantillas[j][k],j,k)
            // plantillas[j][k] = plantillas[j][k].nombre ? plantillas[j][k].nombre : '';

            // console.log("pl",plantillas[j][k][0])
            // if (Array.isArray(result.rows[i].indicadores_impacto) && result.rows[i].indicadores_impacto[0]) {
            //   console.log(result.rows[i].indicadores_impacto[0],"DEresultttttt")
            //   result.rows[i].indicadores_impacto_numero = result.rows[i].indicadores_impacto[0].numero;
            //   result.rows[i].indicadores_impacto_unidades = result.rows[i].indicadores_impacto[0].unidades;
            //   result.rows[i].indicadores_impacto_otro = result.rows[i].indicadores_impacto[0].otro;
            //   result.rows[i].indicadores_impacto = result.rows[i].indicadores_impacto[0].combinado;
            // }
          }
          // console.log(plantillas[j][k])
          // if (j=='convenio' && k=='plazo_convenio' && typeof(plantillas[j][k])=='object') {
          //   plantillas[j][k] = plantillas[j][k].nombre ? plantillas[j][k].nombre : '';
          // }
          if ((plantillas[j][k]) && plantillas[j][k].nombre) {
            plantillas[j][k] = plantillas[j][k].nombre;
          }
          if (typeof(plantillas[j][k]) != 'object') {
            if (j == 'beneficiarios' && k == 'referencia') continue;
            if (j == 'res_administrativa' && k == 'fecha_emision') continue;
            if (j == 'cronograma_desembolsos' && k == 'categoria_programatica') continue;
            if (j == 'convenio' && k == 'plazo_ejecucion') continue;
            if (['beneficiarios'].indexOf(j) >= 0) {
              
              result.rows[i][`${k}`] = plantillas[j][k];
            } else {
              result.rows[i][`${j}.${k.replace('_convenio','')}`] = plantillas[j][k];
            }
          }
        }
      }
       // console.log(result.rows[i].indicadores_impacto,"DEresultttttt")
      

      result.rows[i].indicadores_impacto_numero = !isNaN(result.rows[i].indicadores_impacto_numero) && result.rows[i].indicadores_impacto_numero > 0 ? `${util.convertirNumeroAString(result.rows[i].indicadores_impacto_numero)}` : 0;

      result.rows[i]['cronograma_desembolsos.monto_1'] = !isNaN(result.rows[i]['cronograma_desembolsos.monto_1']) && result.rows[i]['cronograma_desembolsos.monto_1'] > 0 ? `${util.convertirNumeroAString(result.rows[i]['cronograma_desembolsos.monto_1'])}` : 0;
      result.rows[i]['cronograma_desembolsos.monto_2'] = !isNaN(result.rows[i]['cronograma_desembolsos.monto_2']) && result.rows[i]['cronograma_desembolsos.monto_2'] > 0 ? `${util.convertirNumeroAString(result.rows[i]['cronograma_desembolsos.monto_2'])}` : 0;
      result.rows[i]['cronograma_desembolsos.monto_3'] = !isNaN(result.rows[i]['cronograma_desembolsos.monto_3']) && result.rows[i]['cronograma_desembolsos.monto_3'] > 0 ? `${util.convertirNumeroAString(result.rows[i]['cronograma_desembolsos.monto_3'])}` : 0;
      result.rows[i]['cronograma_desembolsos.monto_4'] = !isNaN(result.rows[i]['cronograma_desembolsos.monto_4']) && result.rows[i]['cronograma_desembolsos.monto_4'] > 0 ? `${util.convertirNumeroAString(result.rows[i]['cronograma_desembolsos.monto_4'])}` : 0;

      result.rows[i]['financiamiento.monto_fdi'] = !isNaN(result.rows[i]['financiamiento.monto_fdi']) && result.rows[i]['financiamiento.monto_fdi'] > 0 ? `${util.convertirNumeroAString(result.rows[i]['financiamiento.monto_fdi'])}` : 0;
      result.rows[i]['financiamiento.monto_otros'] = !isNaN(result.rows[i]['financiamiento.monto_otros']) && result.rows[i]['financiamiento.monto_otros'] > 0 ? `${util.convertirNumeroAString(result.rows[i]['financiamiento.monto_otros'])}` : 0;
      result.rows[i]['financiamiento.monto_contraparte'] = !isNaN(result.rows[i]['financiamiento.monto_contraparte']) && result.rows[i]['financiamiento.monto_contraparte'] > 0 ? `${util.convertirNumeroAString(result.rows[i]['financiamiento.monto_contraparte'])}` : 0;
      result.rows[i]['financiamiento.monto_beneficiarios'] = !isNaN(result.rows[i]['financiamiento.monto_beneficiarios']) && result.rows[i]['financiamiento.monto_beneficiarios'] > 0 ? `${util.convertirNumeroAString(result.rows[i]['financiamiento.monto_beneficiarios'])}` : 0;
      result.rows[i]['financiamiento.monto_total_proyecto'] = !isNaN(result.rows[i]['financiamiento.monto_total_proyecto']) && result.rows[i]['financiamiento.monto_total_proyecto'] > 0 ? `${util.convertirNumeroAString(result.rows[i]['financiamiento.monto_total_proyecto'])}` : 0;

      // result.rows[i].subtipo_productivo = result.rows[i].subtipo_productivo ? result.rows[i].subtipo_productivo : null;
      // result.rows[i].indicadores_impacto = result.rows[i].indicadores_impacto ? result.rows[i].indicadores_impacto : null;
      // result.rows[i].indicadores_impacto_unidades = result.rows[i].indicadores_impacto_unidades ? result.rows[i].indicadores_impacto_unidades : null;
      // result.rows[i].indicadores_impacto_otro = result.rows[i].indicadores_impacto_otro ? result.rows[i].indicadores_impacto_otro : null;
      // result.rows[i].indicadores_impacto_numero = result.rows[i].indicadores_impacto_numero ? result.rows[i].indicadores_impacto_numero : null;

      delete result.rows[i].formularios;

      delete result.rows[i].id_proyecto;

    }
    let allowed_fields = ['num', 'codigo','nombre','cartera','estado_proyecto','plazo_ejecucion','orden_proceder','municipio','provincia','departamento',
     'tecnico_responsable','resultado_evaluacion', 'convenio.nro', 'convenio.fecha_suscripcion','cronograma_desembolsos.fecha_1', 'cronograma_desembolsos.monto_1', 'cronograma_desembolsos.fecha_2', 
     'cronograma_desembolsos.monto_2', 'cronograma_desembolsos.fecha_3', 'cronograma_desembolsos.monto_3', 'cronograma_desembolsos.monto_4', 'cronograma_desembolsos.codigo_sisin','cronograma_desembolsos.porcentaje_1',
     'cronograma_desembolsos.porcentaje_2','cronograma_desembolsos.porcentaje_3', 'cronograma_desembolsos.porcentaje_4', 'cronograma_desembolsos.fecha_inscripcion','tipo', 'tipo_financiamiento','tamanio_proyecto', 
     'nro_familias', 'nro_comunidades', 'comunidades', 'indicadores_impacto_unidades', 'indicadores_impacto_otro','indicadores_impacto_numero','indicadores_impacto','financiamiento.monto_fdi','financiamiento.monto_otros',
     'financiamiento.monto_contraparte', 'financiamiento.monto_beneficiarios','financiamiento.monto_total_proyecto', 'avance.ejecutado','avance.fisico', 'avance.financiero', 'desembolso_fdi.monto', 'desembolso_fdi.porcentaje'
];
    for (let j = 0; j < result.rows.length; j++) {
      let fila = result.rows[j];

      const filtered = allowed_fields
      .filter(key => Object.keys(fila).includes(key))
      .reduce((obj, key) => {
        obj[key] = fila[key];
        return obj;
      }, {});

      result.rows[j] = filtered;
    }
    if (format === 'xlsx') {
      const wb = XLSX.utils.book_new();
      wb.Props = {
        Title: "Reporte",
        Subject: "Proyectos",
        Author: "FDI",
        CreatedDate: new Date()
      };
      wb.SheetNames.push("hoja_reporte");
      const ws_data = result.rows;
      const ws = XLSX.utils.json_to_sheet(ws_data);
      wb.Sheets["hoja_reporte"] = ws;
      const wbout = XLSX.write(wb, {bookType:'xlsx',  type: 'buffer'});
      return wbout;
    }

    return result;
  }

  function objMerge(a, b) {
    var c = {};
    for (var idx in a) {
      c[idx] = b[idx] ? b[idx] : a[idx];
    }
    // for(var idx in b) {
    //     c[idx] = b[idx];
    // }
    return c;
  }

  function parseoReportePorDepartamento (datos, tipoReporte) {
    let devolver = {
      count: datos.length,
      rows: []
    };

    for (let i = 0; i < datos.length; i++) {
      let dato = datos[i];
      let fila = {
        num: parseInt(i) + 1,
        cartera: dato.cartera,
        codigo: dato.nro_convenio,
        departamento: dato.departamento,
        codigo_gam: dato.codigo_gam,
        municipio: dato.municipio,
        sisin: dato['cronograma_desembolsos.codigo_sisin'],
        tipo_de_proyecto: dato.tipo ? dato.tipo.split('_')[dato.tipo.split('_').length - 1] : '',
        nombre_del_proyecto: dato.nombre,
        orden_proceder: dato.orden_proceder,
        plazo_ejecucion_dias: dato.plazo_ejecucion,
        tipo_de_inversion: dato.tipo_financiamiento ? dato.tipo_financiamiento.split('_')[dato.tipo_financiamiento.split('_').length - 1] : '',
        costo_total_Bs: (dato['financiamiento.monto_total_proyecto'] && dato['financiamiento.monto_total_proyecto'].toFixed) ? dato['financiamiento.monto_total_proyecto'].toFixed(2).replace(".", ",") : 0,
        financiamiento_fdi: dato['financiamiento.monto_fdi'].toString().replace(".", ","),
        financiamiento_gam: dato['financiamiento.monto_contraparte'].toString().replace(".", ","),
        financiamiento_beneficiarios: dato['financiamiento.monto_beneficiarios'].toString().replace(".", ","),
        avance_fisico: dato['avance.fisico'],
        avance_financiero: dato['avance.financiero'],
        fecha_desembolso_1: dato['cronograma_desembolsos.fecha_1'],
        desembolso_1: dato['cronograma_desembolsos.monto_1'].toString().replace(".", ","),
        fecha_desembolso_2: dato['cronograma_desembolsos.fecha_2'],
        desembolso_2: dato['cronograma_desembolsos.monto_2'].toString().replace(".", ","),
        fecha_desembolso_3: dato['cronograma_desembolsos.fecha_3'],
        desembolso_3: dato['cronograma_desembolsos.monto_3'].toString().replace(".", ","),
        desembolso_4: dato['cronograma_desembolsos.monto_4'].toString().replace(".", ","),
        monto_ejecutado_fdi: (parseFloat(dato['cronograma_desembolsos.monto_1']) + parseFloat(dato['cronograma_desembolsos.monto_2']) + parseFloat(dato['cronograma_desembolsos.monto_3']) + parseFloat(dato['cronograma_desembolsos.monto_4'])).toFixed(2).replace(".", ","),
        nro_familias_beneficiadas: dato.nro_familias,
        mano_de_obra_calificada: dato.mano_obra_calificada ? dato.mano_obra_calificada : 0,
        mano_de_obra_no_calificada: dato.mano_obra_no_calificada ? dato.mano_obra_no_calificada : 0,
        unidad_de_medida: dato.indicadores_impacto_unidades === 'Otro' && dato.indicadores_impacto_otro ? dato.indicadores_impacto_otro : dato.indicadores_impacto_unidades,
        meta: 0,
        meta_ejecutado: 0,
        meta_porcentaje: '0,00%',
        fecha_ejecucion_meta: '',
        estado: dato.estado_proyecto,
        metas_resultados : dato.metas_resultados,
        estado_descripcion: dato.estado_proyecto_descripcion
      };

      devolver.rows.push(fila);
    }

    let allowed_fields = ['num', 'departamento', 'municipio', 'nombre_del_proyecto'];

    if (tipoReporte === 'porDepartamento') {
      allowed_fields = ['num', 'departamento', 'codigo_gam', 'municipio', 'tipo_de_proyecto', 'nombre_del_proyecto', 'orden_proceder',
        'plazo_ejecucion_dias', 'costo_total_Bs', 'financiamiento_fdi', 'financiamiento_gam', 'financiamiento_beneficiarios', 'avance_fisico',
        'avance_financiero', 'fecha_desembolso_1', 'desembolso_1', 'fecha_desembolso_2', 'desembolso_2', 'fecha_desembolso_3', 'desembolso_3',
        'desembolso_4', 'estado_descripcion'];
    } else if (tipoReporte === 'porDptoEmpleo') {
      allowed_fields = ['num', 'departamento', 'codigo_gam', 'municipio', 'sisin', 'nombre_del_proyecto', 'tipo_de_inversion', 'financiamiento_fdi',
        'monto_ejecutado_fdi', 'tipo_de_proyecto', 'mano_de_obra_calificada', 'mano_de_obra_no_calificada', 'nro_familias_beneficiadas'];
    } else if (tipoReporte === 'porDptoPPTO') {
      allowed_fields = ['num', 'departamento', 'codigo_gam', 'municipio', 'nombre_del_proyecto', 'financiamiento_fdi', 'monto_ejecutado_fdi',
        'tipo_de_proyecto', 'nro_familias_beneficiadas'];
    } else if (tipoReporte === 'porDptoMetas') {
      allowed_fields = ['num', 'cartera','codigo','departamento', 'cogido_gam', 'municipio', 'nombre_del_proyecto', 'tipo_de_proyecto',
        'metas_resultados'];
    }

    for (let j = 0; j < devolver.rows.length; j++) {
      let fila = devolver.rows[j];

      const filtered = allowed_fields
      .filter(key => Object.keys(fila).includes(key))
      .reduce((obj, key) => {
        obj[key] = fila[key];
        return obj;
      }, {});

      devolver.rows[j] = filtered;
    }
    return devolver;
  }

  function parseoReportePorMunicipio (datos) {
    let devolver = {
      count: datos.length,
      rows: []
    };

    let suma_total_proyecto_Bs = 0;
    let suma_monto_fdi_Bs = 0;
    let suma_monto_gam_Bs = 0;
    let suma_monto_beneficiarios_Bs = 0;
    let suma_ejecucion_presupuestaria_fdi = 0;
    let suma_avance_fisico = 0;
    let suma_plazo_convenio = 0;
    let suma_fecha_de_conclusion = 0;

    for (let i = 0; i < datos.length; i++) {
      let dato = datos[i];
      let convenio_restante = dato['convenio.fecha_suscripcion'] ? moment(dato['convenio.fecha_suscripcion'], 'YYYY-MM-DD').add(dato['convenio.plazo'], 'days').diff(moment(), 'days') : 0;
      let fila = {
        num: parseInt(i) + 1,
        codigo_gam: dato.codigo_gam,
        municipio: dato.municipio,
        nombre_del_proyecto: dato.nombre,
        total_proyecto_Bs: (dato['financiamiento.monto_total_proyecto'] && dato['financiamiento.monto_total_proyecto'].toFixed)? dato['financiamiento.monto_total_proyecto'].toFixed(2).replace(".", ",") : '0',
        monto_fdi_Bs: dato['financiamiento.monto_fdi'].toString().replace(".", ","),
        monto_gam_Bs: dato['financiamiento.monto_contraparte'].toString().replace(".", ","),
        monto_beneficiarios_Bs: dato['financiamiento.monto_beneficiarios'].toString().replace(".", ","),
        ejecucion_presupuestaria_fdi: dato['desembolso_fdi.porcentaje'],
        avance_fisico: dato['avance.fisico'],
        fecha_de_inicio: dato['convenio.fecha_suscripcion'],
        plazo_convenio: convenio_restante,
        fecha_de_conclusion: dato.plazo_ejecucion,
        estado: dato.estado_proyecto
      };

      devolver.rows.push(fila);

      suma_total_proyecto_Bs += parseFloat(dato['financiamiento.monto_total_proyecto']);
      suma_monto_fdi_Bs += parseFloat(dato['financiamiento.monto_fdi']);
      suma_monto_gam_Bs += parseFloat(dato['financiamiento.monto_contraparte']);
      suma_monto_beneficiarios_Bs += parseFloat(dato['financiamiento.monto_beneficiarios']);
      suma_ejecucion_presupuestaria_fdi += parseFloat(dato['desembolso_fdi.porcentaje'].split(' ')[0]);
      suma_avance_fisico += parseFloat(dato['avance.fisico'].split(' ')[0]) || 0;
      suma_plazo_convenio += parseInt(convenio_restante);
      suma_fecha_de_conclusion += dato.plazo_ejecucion ? parseInt(dato.plazo_ejecucion) : 0;
    }

    let totales = {
      num: '',
      codigo_gam: '',
      municipio: '',
      nombre_del_proyecto: 'Total general',
      total_proyecto_Bs: suma_total_proyecto_Bs.toFixed(2).replace(".", ","),
      monto_fdi_Bs: suma_monto_fdi_Bs.toFixed(2).replace(".", ","),
      monto_gam_Bs: suma_monto_gam_Bs.toFixed(2).replace(".", ","),
      monto_beneficiarios_Bs: suma_monto_beneficiarios_Bs.toFixed(2).replace(".", ","),
      ejecucion_presupuestaria_fdi: suma_ejecucion_presupuestaria_fdi,
      avance_fisico: suma_avance_fisico,
      fecha_de_inicio: '',
      plazo_convenio: suma_plazo_convenio,
      fecha_de_conclusion: suma_fecha_de_conclusion,
      estado: ''
    };

    devolver.rows.push(totales);

    return devolver;
  }

  function parseReportePorTipoProyecto (datos) {
    let devolver = {
      count: datos.length,
      rows: []
    };

    let agrupados = {};

    for (let i = 0; i < datos.length; i++) {
      let dato = datos[i];

      if (!dato.tipo) {
        dato.tipo = 'NE';
      }

      if (!agrupados.hasOwnProperty(dato.tipo) || !agrupados[dato.tipo] || typeof agrupados[dato.tipo] !== 'object') {
        agrupados[dato.tipo] = {
          aprobados: 0,
          costo_total: 0,
          fuente_fdi: 0,
          fuente_gam: 0,
          fuente_beneficiarios: 0,
          num_comunidades: 0,
          num_familias: 0,
          concluidos: 0
        }
      }

      if (dato.resultado_evaluacion === 'APROBADO') {
        agrupados[dato.tipo].aprobados += 1;
      }

      agrupados[dato.tipo].costo_total += parseFloat(dato['financiamiento.monto_total_proyecto']);
      agrupados[dato.tipo].fuente_fdi += parseFloat(dato['financiamiento.monto_fdi']);
      agrupados[dato.tipo].fuente_gam += parseFloat(dato['financiamiento.monto_contraparte']);
      agrupados[dato.tipo].fuente_beneficiarios += parseFloat(dato['financiamiento.monto_beneficiarios']);
      agrupados[dato.tipo].num_comunidades += dato.nro_comunidades ? parseInt(dato.nro_comunidades) : 0;
      agrupados[dato.tipo].num_familias += parseInt(dato.nro_familias);

      if (dato.estado_proyecto === 'CERRADO_FDI') {
        agrupados[dato.tipo].concluidos += 1;
      }
    }

    let tipos = Object.keys(agrupados);

    for (let i in tipos) {
      let fila = {
        tipo_de_proyecto: tipos[i] ? tipos[i].split('_')[tipos[i].split('_').length - 1] : '',
        proyectos_aprobados: agrupados[tipos[i]].aprobados,
        costo_total_Bs: agrupados[tipos[i]].costo_total.toFixed(2).replace(".", ","),
        financiamiento_aprobado_fdi: agrupados[tipos[i]].fuente_fdi.toFixed(2).replace(".", ","),
        financiamiento_gam: agrupados[tipos[i]].fuente_gam.toFixed(2).replace(".", ","),
        financiamiento_beneficiarios: agrupados[tipos[i]].fuente_beneficiarios.toFixed(2).replace(".", ","),
        nro_comunidades: agrupados[tipos[i]].num_comunidades,
        familias: agrupados[tipos[i]].num_familias,
        proyectos_concluidos: agrupados[tipos[i]].concluidos
      };

      devolver.rows.push(fila);
    }

    return devolver;
  }

  function parseReporteFisicoFinanciero (datos) {
    let devolver = {
      count: datos.length,
      rows: []
    };

    let agrupados = {};

    for (let i = 0; i < datos.length; i++) {
      let dato = datos[i];

      if (!dato.departamento) {
        dato.departamento = 'NE';
      }

      let keyDpto = dato.departamento.replace(' ', '_');

      if (!agrupados.hasOwnProperty(keyDpto) || !agrupados[keyDpto] || typeof agrupados[keyDpto] !== 'object') {
        agrupados[keyDpto] = {
          municipios: [],
          familias: 0,
          total_proyectos: 0,
          proyectos_riego: 0,
          proyectos_puente: 0,
          proyectos_productivos: 0,
          proyectos_maquinaria: 0,
          inversion_total_fdi: 0,
          inversion_riego: 0,
          inversion_puentes: 0,
          inversion_productivos: 0,
          inversion_maquinaria: 0,
          avance_fisico: 0,
          avance_financiero: 0,
          concluidos: 0
        }
      }

      if (agrupados[keyDpto].municipios.indexOf(dato.municipio) === -1) {
        agrupados[keyDpto].municipios.push(dato.municipio);
      }

      agrupados[keyDpto].familias += parseInt(dato.nro_familias);
      agrupados[keyDpto].total_proyectos += 1;

      agrupados[keyDpto].inversion_total_fdi += parseFloat(dato['financiamiento.monto_fdi']);

      if (dato.tipo === 'OP_TP_PRODUCTIVO') {
        agrupados[keyDpto].proyectos_productivos += 1;
        agrupados[keyDpto].inversion_productivos += parseFloat(dato['financiamiento.monto_fdi']);
      } else if (dato.tipo === 'OP_TP_PUENTES') {
        agrupados[keyDpto].proyectos_puente += 1;
        agrupados[keyDpto].inversion_puentes += parseFloat(dato['financiamiento.monto_fdi']);
      } else if (dato.tipo === 'OP_TP_RIEGO') {
        agrupados[keyDpto].proyectos_riego += 1;
        agrupados[keyDpto].inversion_riego += parseFloat(dato['financiamiento.monto_fdi']);
      } else {
        agrupados[keyDpto].proyectos_maquinaria += 1;
        agrupados[keyDpto].inversion_maquinaria += parseFloat(dato['financiamiento.monto_fdi']);
      }

      agrupados[keyDpto].avance_fisico += parseFloat(dato['avance.fisico'].replace(',','.'));
      agrupados[keyDpto].avance_financiero += parseFloat(dato['avance.financiero'].replace(',','.'));

      if (dato.estado_proyecto === 'CERRADO_FDI') {
        agrupados[keyDpto].concluidos += 1;
      }
    }

    let departamentos = Object.keys(agrupados);

    for (let i in departamentos) {
      let dpto = departamentos[i];
      let fila = {
        descripcion: dpto ? dpto.replace('_', ' ') : '',
        municipios_beneficiarios: agrupados[dpto].municipios.length,
        familias_beneficiarias: agrupados[dpto].familias,
        total_proyectos: agrupados[dpto].total_proyectos,
        proyectos_de_riego: agrupados[dpto].proyectos_riego,
        proyectos_de_puente: agrupados[dpto].proyectos_puente,
        proyectos_productivos: agrupados[dpto].proyectos_productivos,
        proyectos_de_maquinaria: agrupados[dpto].proyectos_maquinaria,
        inversion_total_fdi: agrupados[dpto].inversion_total_fdi.toFixed(2).replace(".", ","),
        inversion_para_riego_y_atajados: agrupados[dpto].inversion_riego.toFixed(2).replace(".", ","),
        inversion_para_puentes: agrupados[dpto].inversion_puentes.toFixed(2).replace(".", ","),
        inversion_proyectos_productivos: agrupados[dpto].inversion_productivos.toFixed(2).replace(".", ","),
        inversion_para_maquinaria: agrupados[dpto].inversion_maquinaria.toFixed(2).replace(".", ","),
        avance_fisico: `${(agrupados[dpto].avance_fisico / agrupados[dpto].total_proyectos).toFixed(2).replace('.',',')} %`,
        avance_financiero: `${(agrupados[dpto].avance_financiero / agrupados[dpto].total_proyectos).toFixed(2).replace('.',',')} %`,
        concluidos: agrupados[dpto].concluidos
      };

      devolver.rows.push(fila);
    }

    return devolver;
  }

  async function crearFichaTecnica (nombreArchivo, idProyecto, req) {
    const datosProyecto = await app.dao.proyecto.obtenerDatosProyecto(idProyecto);
    const CODIGO_PROYECTO = datosProyecto.codigo;
    const rutaProyecto = await crearObtenerDirectorioProyecto(CODIGO_PROYECTO);
    const rutaEvaluaciones = path.join(rutaProyecto, 'evaluaciones');
    await app.dao.ficha_tecnica.crear(ruta, datosProyecto);
  }

  app.dao.reporte.crearPlanilla = crearPlanilla;
  app.dao.reporte.crearPlanillaPorProyecto = crearPlanillaPorProyecto;
  app.dao.reporte.crearInformeResumen = crearInformeResumen;
  app.dao.reporte.crearInformeRechazo = crearInformeRechazo;
  app.dao.reporte.crearArchivo = crearArchivo;
  app.dao.reporte.crearFormularios = crearFormularios;
  app.dao.reporte.crearFormulariosZIP = crearFormulariosZIP;
  app.dao.reporte.crearObtenerDirectorioProyecto = crearObtenerDirectorioProyecto;
  app.dao.reporte.obtenerListaArchivos = obtenerListaArchivos;
  app.dao.reporte.obtenerNombreArchivoYCarpeta = obtenerNombreArchivoYCarpeta;
  app.dao.reporte.obtenerCSVPorTipo = obtenerCSVPorTipo;
  app.dao.reporte.obtenerCSVGeneral = obtenerCSVGeneral;
  app.dao.reporte.crearFichaTecnica = crearFichaTecnica;
};
