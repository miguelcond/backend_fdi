import errors from '../../../lib/errors';
import Reporte from '../../../lib/reportes';
import moment from 'moment';

module.exports = (app) => {
  app.dao.formularioA2a = {};

  async function crear (rutaFormularios, datosProyecto) {
    const { nombreArchivo } = await app.dao.reporte.obtenerNombreArchivoYCarpeta(`A2a`);
    const datosFormulario = await obtenerDatos(datosProyecto);
    const rep = new Reporte('formulario_A2a');
    return rep.pdf(datosFormulario, { orientation: 'portrait', output: `${rutaFormularios}/${nombreArchivo}` });
  }

  async function obtenerDatos (datosProyecto) {
    const datos = datosProyecto.documentacion_exp;
    if (!datos) {
      throw new errors.ValidationError(`No se encontró el reporte solicitado.`);
    }
    const tipo = datos.datos_empresa.tipo;
    const fechaInscripcion = moment(datos.datos_empresa.fecha_inscripcion, 'YYYY-MM-DD');
    const fechaExpedicion = moment(datos.datos_empresa.representante_legal.poder.fecha_expedicion, 'YYYY-MM-DD');
    const datosFormularioA2a = {
      datos_generales: {
        razon_social: datos.datos_empresa.razon_social,
        nit: datos.datos_empresa.nit,
        tipo: datos.datos_empresa.tipo,
        es_nacional: (tipo === 'Empresa Nacional'),
        es_extranjera: (tipo === 'Empresa Extranjera'),
        es_otro: ((tipo !== 'Empresa Nacional') && (tipo !== 'Empresa Extranjera')),
        pais: datos.datos_empresa.pais,
        ciudad: datos.datos_empresa.ciudad,
        direccion: datos.datos_empresa.direccion,
        telefono: datos.datos_empresa.telefono,
        nro_matricula: datos.datos_empresa.nro_matricula,
        fecha_inscripcion: {
          dia: fechaInscripcion.format('DD'),
          mes: fechaInscripcion.format('MM'),
          anio: fechaInscripcion.format('YYYY')
        }
      },
      datos_complementarios: {
        representante_legal: {
          primer_apellido: datos.datos_empresa.representante_legal.persona.primer_apellido,
          segundo_apellido: datos.datos_empresa.representante_legal.persona.segundo_apellido,
          nombres: datos.datos_empresa.representante_legal.persona.nombres,
          ci: datos.datos_empresa.representante_legal.persona.documento_identidad,
          lugar_expedicion: datos.datos_empresa.representante_legal.persona.lugar_expedicion_documento,
          poder: {
            nro_testimonio: datos.datos_empresa.representante_legal.poder.nro_testimonio,
            lugar_emision: datos.datos_empresa.representante_legal.poder.lugar_emision,
            fecha_expedicion: {
              dia: datos.datos_empresa.representante_legal.poder.fecha_expedicion? fechaExpedicion.format('DD'): '',
              mes: datos.datos_empresa.representante_legal.poder.fecha_expedicion? fechaExpedicion.format('MM'): '',
              anio: datos.datos_empresa.representante_legal.poder.fecha_expedicion? fechaExpedicion.format('YYYY'): ''
            }
          }
        }
      },
      informacion_contacto: {
        fax_check: datos.datos_empresa.notificacion_fax_check,
        fax: datos.datos_empresa.notificacion_fax,
        correo_check: datos.datos_empresa.notificacion_correo_check,
        correo: datos.datos_empresa.notificacion_correo
      }
    };

    if(datos.datos_asociacion) {
      if(datos.datos_asociacion.nombre_asociacion && datos.datos_asociacion.asociados && datos.datos_asociacion.asociados.length>1) {
        datosFormularioA2a.datos_generales.razon_social = datos.datos_asociacion.nombre_asociacion;
      }
      if(datos.datos_asociacion.nit && datos.datos_asociacion.nro_matricula && datos.datos_asociacion.fecha_inscripcion) {
        const fechaInscripcionA = moment(datos.datos_asociacion.fecha_inscripcion, 'YYYY-MM-DD');
        datosFormularioA2a.datos_generales.nit = datos.datos_asociacion.nit;
        datosFormularioA2a.datos_generales.nro_matricula = datos.datos_asociacion.nro_matricula;
        datosFormularioA2a.datos_generales.fecha_inscripcion = {
          dia: fechaInscripcionA.format('DD'),
          mes: fechaInscripcionA.format('MM'),
          anio: fechaInscripcionA.format('YYYY')
        };
      }
    }

    return datosFormularioA2a;
  }

  app.dao.formularioA2a.crear = crear;
};
