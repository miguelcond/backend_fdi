const BaseJoi = require('./../../../lib/joi');
const Extension = require('joi-date-extensions');
const Joi = BaseJoi.extend(Extension);

module.exports = {
  obtenerListaFormularios: {
    params: {
      id_proyecto: Joi.number().required()
    }
  },
  crearFormularios: {
    params: {
      id_proyecto: Joi.number().required()
    }
  },
  subirFormulario: {
    params: {
      id_proyecto: Joi.number().required(),
      sigla_formulario: Joi.string().required()
    },
    body: {
      documento: Joi.object({
        base64: Joi.string().required()
      })
    }
  },
  obtenerFormulario: {
    params: {
      id_proyecto: Joi.number().required(),
      nombre_archivo: Joi.string().required()
    }
  },
  descargarFormulario: {
    params: {
      id_proyecto: Joi.number().required(),
      nombre_archivo: Joi.string().required()
    }
  },
  certificadoAvanceObra: {
    params: {
      id_proyecto: Joi.number().required()
    }
  },
  planillaAvanceObra: {
    params: {
      id_proyecto: Joi.number().required()
    }
  },
  computoMetrico: {
    params: {
      id_proyecto: Joi.number().required()
    }
  },

  subirArchivo: {
    params: {
      id_proyecto: Joi.number().required(),
      codigo_documento: Joi.string().required()
    },
    body: {
      documento: Joi.object({
        base64: Joi.string().required()
      }).required()
    }
  },
  crearPlanilla: {
    params: {
      id_proyecto: Joi.number().required(),
      nro_planilla: Joi.number().required()
    }
  },
  listarArchivos: {
    params: {
      id_proyecto: Joi.number().required()
    }
  },
  obtenerArchivo: {
    params: {
      id_proyecto: Joi.number().required(),
      codigo_documento: Joi.string().required()
    }
  },
  descargarArchivo: {
    params: {
      id_proyecto: Joi.number().required(),
      codigo_documento: Joi.string().required()
    }
  },
  informeEvaluacion: {
    params: {
      id_proyecto: Joi.number().min(1).required()
    },
    body: {
      id_proyecto: Joi.number().min(1).required(),
      $_plantilla: Joi.number().only(102).required(),
      $_nombre: Joi.string().only('informe').required(),
      criterios: Joi.array().optional(),
      valoracion: Joi.array().required(),
    }
  }
};
