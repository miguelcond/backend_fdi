import validate from 'express-validation';
import paramValidation from './reporte.validation';
import { apidoc as APIDOC, field as FIELD } from '../../../lib/apidoc-generator';
const ejs = require('ejs');
const wkhtmltopdf = require('../../../lib/wkhtmltopdf')

module.exports = (app) => {
  const R = app.src.config.config.api.main;

  app.api.get('/proyectos/:id_proyecto/formularios/descargar', validate(paramValidation.obtenerListaFormularios), app.controller.reporte.descargarFormularios);
  APIDOC.get(`${R}/proyectos/:id_proyecto/formularios/descargar`, {
    name: 'Descargar Formularios',
    group: 'Formularios',
    description: 'Descarga un archivo ZIP que contiene todos los formularios.',
    input: {
      params: {
        id_proyecto: FIELD.proyecto('id_proyecto')
      }
    }
  });

  app.api.put('/proyectos/:id_proyecto/formularios', validate(paramValidation.crearFormularios), app.controller.reporte.crearFormularios);
  APIDOC.put(`${R}/proyectos/:id_proyecto/formularios`, {
    name: 'Crear Formularios',
    group: 'Formularios',
    description: 'Crea todos los formularios de un proyecto en ficheros con formato PDF y los almacena en el disco duro.',
    input: {
      params: {
        id_proyecto: FIELD.proyecto('id_proyecto')
      }
    }
  });

  app.api.put('/proyectos/:id_proyecto/doc/planillas/:nro_planilla', validate(paramValidation.crearPlanilla), app.controller.reporte.crearPlanilla);
  APIDOC.put(`${R}/proyectos/:id_proyecto/doc/planillas/:nro_planilla`, {
    name: 'Crear Planilla y Computos',
    group: 'Formularios',
    description: 'Crea los reportes de una planilla y todos los computos.',
    input: {
      params: {
        id_proyecto: FIELD.proyecto('id_proyecto'),
        nro_planilla: FIELD.supervision('nro_supervision')
      }
    }
  });

  app.api.put('/proyectos/:id_proyecto/doc/:codigo_documento/subir', validate(paramValidation.subirArchivo), app.controller.reporte.subirArchivo);
  APIDOC.put(`${R}/proyectos/:id_proyecto/doc/:codigo_documento/subir`, {
    name: 'Subir Documento',
    group: 'Formularios',
    description: 'Guarda un archivo en el servidor a partir del código del formulario.',
    input: {
      params: {
        id_proyecto: FIELD.proyecto('id_proyecto'),
        codigo_documento: FIELD.custom('codigo_documento')
      }
    }
  });

  app.api.get('/proyectos/:id_proyecto/doc', validate(paramValidation.listarArchivos), app.controller.reporte.listarArchivos);
  APIDOC.get(`${R}/proyectos/:id_proyecto/doc`, {
    name: 'Listar documentos',
    group: 'Formularios',
    description: 'Devuelve una lista con los nombres de todos los archivos disponibles.',
    input: {
      params: {
        id_proyecto: FIELD.proyecto('id_proyecto')
      }
    },
    output: {
      body: {
        finalizado: FIELD.custom('finalizado'),
        mensaje: FIELD.custom('mensaje'),
        datos: {
          certificados: [{ tipo: FIELD.custom('tipo_documento'), codigo_documento: FIELD.custom('codigo_documento'), nombre: FIELD.custom('nombre_documento'), nombre_archivo: FIELD.custom('ruta_documento') }],
          documentos: [{ tipo: FIELD.custom('tipo_documento'), codigo_documento: FIELD.custom('codigo_documento'), nombre: FIELD.custom('nombre_documento'), nombre_archivo: FIELD.custom('ruta_documento') }],
          formularios: [{ tipo: FIELD.custom('tipo_documento'), codigo_documento: FIELD.custom('codigo_documento'), nombre: FIELD.custom('nombre_documento'), nombre_archivo: FIELD.custom('ruta_documento') }],
          planillas: [{ tipo: FIELD.custom('tipo_documento'), codigo_documento: FIELD.custom('codigo_documento'), nombre: FIELD.custom('nombre_documento'), nombre_archivo: FIELD.custom('ruta_documento') }],
          computos: [{ tipo: FIELD.custom('tipo_documento'), codigo_documento: FIELD.custom('codigo_documento'), nombre: FIELD.custom('nombre_documento'), nombre_archivo: FIELD.custom('ruta_documento') }],
          especificaciones: [{ tipo: FIELD.custom('tipo_documento'), codigo_documento: FIELD.custom('codigo_documento'), nombre: FIELD.custom('nombre_documento'), nombre_archivo: FIELD.custom('ruta_documento') }]
        }
      }
    }
  });

  app.api.get('/proyectos/:id_proyecto/doc/:codigo_documento', validate(paramValidation.obtenerArchivo), app.controller.reporte.obtenerArchivo);
  app.get('/proyectos/:id_proyecto/doc/:codigo_documento', validate(paramValidation.obtenerArchivo), app.controller.reporte.obtenerArchivo);
  APIDOC.get(`${R}/proyectos/:id_proyecto/doc/:codigo_documento`, {
    name: 'Obtener documento base64',
    group: 'Formularios',
    description: 'Devuelve un documento con formato BASE64.<br><br>' +
    'Los códigos de los proyectos son los siguientes:<br>' +
    ' - [A1] Formulario A1.pdf<br>' +
    ' - [A2a] Formulario A2a (Identificación del proponente).pdf<br>' +
    ' - [A3] Formulario A3 (Experiencia general de la empresa).pdf<br>' +
    ' - [A4] Formulario A4 (Experiencia específica de la empresa).pdf<br>' +
    ' - [A7] Formulario A7 (Equipo mínimo).pdf<br>' +
    ' - [A8] Formulario A8 (Cronograma de ejecución).pdf<br>' +
    ' - [B1] Formulario B1 (Presupuesto por items y general de la obra).pdf<br>' +
    ' - [B2] Formulario B2 (Análisis de precios unitarios).pdf<br>' +
    ' - [B5] Formulario B5 (Cronograma de desembolsos).pdf<br>' +
    ' - [BG] Boleta de garantía.pdf<br>' +
    ' - [C] Convenio.pdf<br>' +
    ' - [RM] Resolución ministerial.pdf<br>' +
    ' - [EVE] Evaluacion externa.pdf<br>' +
    ' - [CIP] Certificado de inscripción presupuestaria.pdf<br>' +
    ' - [DBC] Documento base de contratación.pdf<br>' +
    ' - [ET] Especificaciones técnicas.pdf<br>' +
    ' - [DF] Designación de fiscal.pdf<br>' +
    ' - [DS] Designación de supervisor.pdf<br><br>' +
    'Para aquellos documentos que tengan el mismo nombre, se adiciona una numeración secuancial:<br>' +
    ' - ' +
    ' - [A5_1] Formulario A5 - N° 1 (Hoja de vida).pdf<br>' +
    ' - [A6_1] Formulario A6 - N° 1 (Hoja de vida del especialista).pdf<br>' +
    ' - [PA_1] Planilla de avance N° 1.pdf<br>' +
    ' - [CA_1] Certificado de avance N° 1.pdf<br>' +
    ' - [CM_1_2] item N° 1 - Planilla de avance N° 2.pdf<br>' +
    ' - [CMR_1] Resúmen de computos métricos de la Planilla de avance N° 1.pdf<br>' +
    ' - [ET_1] Especificación técnica - Item N° 1.pdf<br>',
    input: {
      params: {
        id_proyecto: FIELD.proyecto('id_proyecto'),
        codigo_documento: FIELD.custom('codigo_documento')
      }
    },
    output: {
      body: {
        finalizado: FIELD.custom('finalizado'),
        mensaje: FIELD.custom('mensaje'),
        datos: {
          tipo: FIELD.custom('tipo_documento'),
          base: FIELD.custom('base64_documento')
        }
      }
    }
  });

  app.api.get('/proyectos/:id_proyecto/doc/:codigo_documento/descargar', validate(paramValidation.descargarArchivo), app.controller.reporte.descargarArchivo);
  APIDOC.get(`${R}/proyectos/:id_proyecto/doc/:codigo_documento/descargar`, {
    name: 'Descargar documento',
    group: 'Formularios',
    description: 'Descarga un documento a partir de su código.',
    input: {
      params: {
        id_proyecto: FIELD.proyecto('id_proyecto'),
        codigo_documento: FIELD.custom('codigo_documento')
      }
    }
  });

  app.api.get('/reportes/csv/:tipo', app.controller.reporte.csvPorTipo);

  app.api.get('/reportes/csv', app.controller.reporte.csvGeneral);
  app.api.get('/reportes/ficha_tecnica/:id', app.controller.reporte.fichaTecnica);
  app.api.post('/reportes/consejo', async function(req, res){

    req.body.html = await ejs.renderFile('src/templates/reporte.ejs',{html:req.body.html});

    let buffer = await wkhtmltopdf.createBuffer(req.body.html,{orientation: 'landscape',zoom:0.50});

    res.set('Content-disposition', 'attachment; filename=prueba.pdf')
    res.set('Content-Type', 'application/pdf')
    res.send(buffer);
  });
  // APIDOC.get(`${R}/proyectos/:id_proyecto/doc/:codigo_documento/descargar`, {
  //   name: 'Descargar documento',
  //   group: 'Formularios',
  //   description: 'Descarga un documento a partir de su código.',
  //   input: {
  //     params: {
  //       id_proyecto: FIELD.proyecto('id_proyecto'),
  //       codigo_documento: FIELD.custom('codigo_documento')
  //     }
  //   }
  // });
  app.api.post('/proyectos/:id_proyecto/informe-evaluacion', validate(paramValidation.informeEvaluacion), app.controller.reporte.informeEvaluacion);
  app.api.get('/proyectos/:id_proyecto/informes', app.controller.reporte.informeHistorico);

  app.api.get('/proyectos/municipios/:departamento', app.controller.reporteProyectos.porMunicipio);
  app.api.get('/proyectos/municipios/:departamento/:municipio', app.controller.reporteProyectos.porMunicipio);
};
