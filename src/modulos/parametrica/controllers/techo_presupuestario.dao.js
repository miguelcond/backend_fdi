import util from '../../../lib/util';
import errors from '../../../lib/errors';
import moment from 'moment';
import _ from 'lodash';

module.exports = (app) => {
  const _app = app;
  const models = app.src.db.models;

  const esMontoValido = async (idProyecto, monto) => {
    let gestion = null;
    if (!monto) {
      const datoMontoFDI = await app.dao.proyecto.obtenerMontoFDI(idProyecto);
      monto = datoMontoFDI.monto_fdi;
      gestion = datoMontoFDI.nro_convenio.substr(-4);
    }

    const datoCodigoMunicipio = await app.dao.proyecto.obtenerCodigoMunicipio(idProyecto);
    const codMunicipio = datoCodigoMunicipio.fcod_municipio;
    if (!gestion) {
      gestion = datoCodigoMunicipio.nro_convenio.substr(-4);
    }

    const montoAcumulado = await obtenerAcumulado(idProyecto, codMunicipio, gestion);
    const totalAComparar = montoAcumulado + monto;
    const techo = await models.techo_presupuestario.findOne({
      attributes: ['techo_presupuestal', 'monto_usado', 'monto_disponible'],
      where: {
        codigo: codMunicipio,
        gestion,
        estado: 'ACTIVO'
      }
    });
    if(!techo){console.log("NO HAY TECHO PRESUPUESTARIO EN LA GESTION",techo,gestion)
      throw new errors.UnauthorizedError(`NO HAY TECHO PRESUPUESTARIO EN LA GESTION `+gestion+` DEL MUNICIPIO `+ codMunicipio)
    }
    if (techo.techo_presupuestal < totalAComparar) {
      return false;
    } else {
      return true;
    }

  };

  const obtenerAcumulado = async (idProyecto, codMunicipio, gestion) => {
    const Validos = moment().year();
    //const consultarDesde = moment([Validos, 0, 1]).format('YYYY-MM-DD');

    const PROYECTOS = await models.proyecto.findAll({
      attributes: ['id_proyecto', 'nombre', 'monto_fdi', '_fecha_creacion','org_sociales'],
      where: {
        fcod_municipio: codMunicipio,
        id_proyecto: { $ne: idProyecto },
        estado: 'ACTIVO',
        // _fecha_creacion: {$gte: consultarDesde},
        nro_convenio: {$like:`%-${gestion}`},
        $not: {estado_proyecto: {$in: ['REGISTRO_SOLICITUD', 'ASIGNACION_TECNICO_FDI', 'REGISTRO_PROYECTO_FDI'] }}
      }
    });

    let montoAcumulado = 0;
    if (PROYECTOS) {
      for (let i = 0; i < PROYECTOS.length; i++) {
        const proyecto = PROYECTOS[i];
        //##para verificar si es org_social no tenga monto acumulado para verificar con el techo presupuestario##
        if (!proyecto.org_sociales){
          montoAcumulado += proyecto.monto_fdi;
          // console.log("montoAcumulado...=", montoAcumulado)
        }
      }
    }

    return montoAcumulado;
  };

  const actualizarEnRegistro = async (idProyecto) => {
    const montoFDI = await app.dao.proyecto.obtenerMontoFDI(idProyecto);
    const datoCodigoMunicipio = await app.dao.proyecto.obtenerCodigoMunicipio(idProyecto);
    const codMunicipio = datoCodigoMunicipio.fcod_municipio;

    const montoAcumulado = await obtenerAcumulado(idProyecto, codMunicipio, montoFDI.nro_convenio.substr(-4));

    const techo = await models.techo_presupuestario.findOne({
      attributes: ['techo_presupuestal'],
      where: {
        codigo: codMunicipio,
        gestion: montoFDI.nro_convenio.substr(-4),
      }
    });
    // *** ACTUALIZACION DE REGISTROS EN TABLA TECHO PRESUPUESTARIO DE BASE DE DATOS DESABILITADA***
    if (!techo){
      console.log(techo,"NO HAY MONTO TECHO PRESUPUESTARIO")
      return true;
    }
    let datos = {
      monto_usado: montoAcumulado + montoFDI.monto_fdi,
      monto_disponible: techo.techo_presupuestal - montoAcumulado - montoFDI.monto_fdi
    };

    let resultado = util.json(await models.techo_presupuestario.update(datos, {
      where: {
        codigo: codMunicipio,
        gestion: montoFDI.nro_convenio.substr(-4),
      }
    }));

    return resultado;
  };

  _app.dao.techo_presupuestario = {
    esMontoValido,
    obtenerAcumulado,
    actualizarEnRegistro
  };
};
