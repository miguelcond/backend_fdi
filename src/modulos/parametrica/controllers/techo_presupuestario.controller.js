import seqOpt from '../../../lib/sequelize-options';
import moment from 'moment';

module.exports = (app) => {
    const _app = app;
    _app.controller.techo_presupuestario = {};
    const techoPresupuestarioController = _app.controller.techo_presupuestario;
    const techoPresupuestarioModel = app.src.db.models.techo_presupuestario;
    const util = app.src.lib.util;
    const sequelize = app.src.db.sequelize;

    const output = {
      model: 'techo_presupuestario',
      fields: {
        codigo: ['str', 'pk'],
        nombre_municipio: ['str'],
        num_concejales: ['int'],
        techo_presupuestal: ['float'],
        monto_usado: ['float'],
        monto_disponible: ['float'],
        estado: ['enum'],
        _usuario_creacion: ['int'],
        _usuario_modificacion: ['int'],
        _fecha_creacion: ['date'],
        _fecha_modificacion: ['date']
      }
    };

    techoPresupuestarioController.listar = (req, res) => {
      if (!req.query.order) {
        req.query.order = 'codigo';
      }
      const query = util.paginar(req.query);
      query.where = [
        { estado: { $ne: 'ELIMINADO' } }
      ];
      if (req.query.gestion) {
        query.where.push({gestion:req.query.gestion});
      }

      if (/ACTIVO|INACTIVO/.test(req.query.estado)) {
        query.where.push(sequelize.literal(`"techo_presupuestario"."estado" ILIKE '${req.query.estado}%'`));
      }
      if (req.query.codigo) {
        query.where.push(sequelize.literal(`"techo_presupuestario"."codigo" ILIKE '0${req.query.codigo}%'`));
      }
      if (req.query.nombre_municipio) {
        query.where.push(sequelize.literal(`"techo_presupuestario"."nombre_municipio" ILIKE '${req.query.nombre_municipio}%'`));
      }
      if (req.query.techo_presupuestal) {
        query.where.push(sequelize.literal(`CAST("techo_presupuestario"."techo_presupuestal" AS TEXT) ILIKE '${req.query.techo_presupuestal}%'`));
      }
      if (req.query.monto_usado) {
        query.where.push(sequelize.literal(`CAST("techo_presupuestario"."monto_usado" AS TEXT) ILIKE '${req.query.monto_usado}%'`));
      }
      if (req.query.monto_disponible) {
        query.where.push(sequelize.literal(`CAST("techo_presupuestario"."monto_disponible" AS TEXT) ILIKE '${req.query.monto_disponible}%'`));
      }
      if (typeof(query.order) === 'string') {
        query.order = sequelize.literal(query.order);
      }
      techoPresupuestarioModel.findAndCountAll({
        attributes: ['codigo', 'nombre_municipio', 'techo_presupuestal', 'monto_usado', 'monto_disponible', 'estado'],
        where: query.where,
        offset: query.offset,
        limit: query.limit,
        order: query.order,
        // distinct: true
      }).then((result) => {
        // result = util.json(result);
        if (result.rows.length > 0) {
          let optimized = seqOpt.createResult(query, util.json(result.rows), output);
          res.status(200).json({
            finalizado: true,
            mensaje: 'Datos obtenidos correctamente',
            datos: {
              count: result.count,
              rows: optimized
            }
          });
        } else {
          res.status(204).json({
            finalizado: true,
            mensaje: 'No se encontraron registros para esta tabla',
            datos: {
              count: 0,
              rows: []
            }
          });
        }
      }).catch((error) => {
        res.status(412).json({
          finalizado: false,
          mensaje: error.message,
          datos: null
        });
      });
    };

    techoPresupuestarioController.get = (req, res) => {
      const id = req.params.codigo;
      let where = {
        codigo: id,
        gestion: req.query.gestion,
      };
      techoPresupuestarioModel.findOne({
        attributes: ['codigo', 'nombre_municipio', 'num_concejales', 'techo_presupuestal', 'monto_usado', 'monto_disponible'],
        where
      }).then((result) => {
        result = util.json(result);
        res.json({
          finalizado: true,
          mensaje: 'Contenido de techo presupuestario recuperado correctamente.',
          datos: result
        });
      }).catch((error) => {
        res.status(412).json({
          finalizado: false,
          mensaje: error.message,
          datos: null
        });
      });
    };

    techoPresupuestarioController.recalcular = (req, res, next) => {
      const codigoMunicipio = req.params.codigo;
      const montoActualizar = req.body.montoActualizar;
      const fechaValidos = req.body.fechaValidos;
      const gestion = req.body.gestion;
      const Validos = moment(fechaValidos).format('YYYY');
      const consultarDesde = moment([Validos, 0, 1]).format('YYYY-MM-DD');

      app.src.db.models.proyecto.findAll({
        attributes: ['id_proyecto', 'nro_convenio', 'nombre', 'monto_fdi', '_fecha_creacion'],
        where: {
          fcod_municipio: codigoMunicipio,
          id_proyecto: { $ne: req.body.idProyecto },
          estado: 'ACTIVO',
          // _fecha_creacion: {$gte: consultarDesde},
          nro_convenio: {$like:`%-${gestion}`},
          $not: {estado_proyecto: {$in: ['REGISTRO_SOLICITUD', 'ASIGNACION_TECNICO_FDI', 'REGISTRO_PROYECTO_FDI'] }}
        }
      }).then((proyectosRes) => {
        let montoAcumulado = 0;
        for (let i = 0; i < proyectosRes.length; i++) {
          const proyecto = proyectosRes[i];
          montoAcumulado += proyecto.monto_fdi;
        }
        montoAcumulado += montoActualizar;
        techoPresupuestarioModel.findOne({
          attributes: ['techo_presupuestal', 'monto_usado', 'monto_disponible'],
          where: {
            codigo: codigoMunicipio,
            gestion: gestion,
            estado: 'ACTIVO'
          }
        }).then((techoRes) => {
          if (techoRes.techo_presupuestal < montoAcumulado) {
            throw new Error('No ha sido posible actualizar el estado del presupuesto disponible para el municipio');
          }
          req.body = techoRes.dataValues;
          req.body.monto_usado = montoAcumulado;
          req.body.monto_disponible = req.body.techo_presupuestal - montoAcumulado;
          req.query.gestion = gestion;
          return next();
        }).catch((error) => {
          return next(error);
        });
      })
      .catch((error) => {
        return next(error);
      });
    };

    techoPresupuestarioController.put = (req, res, next) => {
      return sequelize.transaction(async(t) => {
        let codigo = req.params.codigo;
        let datos = req.body;
        let result = util.json(await techoPresupuestarioModel.update(datos, {
          where: {
            codigo: codigo,
            gestion: req.query.gestion,
          },
          transaction: t
        }));
        return result;
      }).then((result) => {
        if (result[0]) {
          res.json({
            finalizado: true,
            mensaje: 'Techo presupuestario modificado correctamente.',
            datos: result
          });
        } else {
          res.status(401).json({
            finalizado: false,
            mensaje: 'No se ha guardado correctamente.',
            datos: result
          });
        }
      }).catch((error) => {
        next(error);
      });
    };

    techoPresupuestarioController.reestablecer = async (req, res, next) => {
      if (req.body.reestablecer) {
        await sequelize.query(`UPDATE techo_presupuestario t
          SET monto_usado = 0, monto_disponible = t.techo_presupuestal WHERE gestion = '${req.body.gestion}';`,
          { type: sequelize.QueryTypes.UPDATE }
        ).then((result) => {
          res.json({
            finalizado: true,
            mensaje: 'Se han reestablecido los valores de techo presupuestario por defecto correctamente.',
            datos: result
          });
        }).catch((error) => {
          next(error);
        });
      } else {
        throw new Error('No ha sido posible reestablecer los montos usados');
      }
    };
  };
