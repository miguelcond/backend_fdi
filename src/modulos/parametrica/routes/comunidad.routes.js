const validate = require('express-validation');
const { QueryTypes } = require('sequelize');
const paramValidation = require('./parametrica.validation');
module.exports = (app) => {
  const models = app.src.db.models;
  const sequelize = app.src.db.sequelize;
  app.api.get('/comunidad', async function(req,res,next){
    let departamento= req.query.departamento;
    let provincia = req.query.provincia;
    let municipio= req.query.id_municipio;
    console.log(departamento,provincia,municipio,"datossssss")
    try{
      let result = await sequelize.query('select fid, comunidad, y_lat, x_long from comunidades where cod_municipio=? order by comunidad asc',
        {
          replacements:[municipio],
          type: QueryTypes.SELECT
        }
        );
      
        res.status(200).json({
          finalizado: true,
          mensaje: 'lista de comunidades obtenidos!',
          datos:result
        })
    }catch(error){
      next(error)
    }

  });
 
};