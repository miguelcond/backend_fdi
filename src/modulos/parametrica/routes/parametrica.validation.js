const BaseJoi = require('joi');
const Extension = require('joi-date-extensions');
const Joi = BaseJoi.extend(Extension);

module.exports = {
  get: {
    params: {
      tipo: Joi.string().required().min(3).max(80),
    }
  },
  post: {
    params: {
      tipo: Joi.string().required().min(3).max(80),
    },
    body: {
      codigo: Joi.string().required().min(3).max(80),
      nombre: Joi.string().required().min(1).max(80),
      descripcion: Joi.string().required().min(3).max(255),
    }
  },
  put: {
    params: {
      tipo: Joi.string().required().min(3).max(80),
    },
    body: {
      nombre: Joi.string().required().min(1).max(80),
      descripcion: Joi.string().required().min(1).max(255),
    }
  },
};
