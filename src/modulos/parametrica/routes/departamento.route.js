import sequelizeFormly from 'sequelize-formly';
module.exports = (app) => {
  /**
    @apiVersion 1.0.0
    @apiGroup Departamento
    @apiName Get departamentos
    @api {get} /departamentos Obtiene lista de departamentos

    @apiDescription Get departamentos, obtiene los departamentos

    @apiSuccess (Respuesta) {Boleano} finalizado Indica el estado del proceso solicitado
    @apiSuccess (Respuesta) {Texto} mensaje Mensaje a ser visualizado
    @apiSuccess (Respuesta) {Objeto} datos Objeto que contiene información sobre los departamentos

    @apiSuccessExample {json} Respuesta:
    HTTP/1.1 200 OK
    [
        {
            "codigo": "01",
            "nombre": "Chuquisaca"
        },
        {
            "codigo": "02",
            "nombre": "La Paz"
        },
        {
            "codigo": "03",
            "nombre": "Cochabamba"
        },
        ...
    ]
  */
  app.api.get('/departamentos', app.controller.departamento.get);
  app.api.options('/departamentos', sequelizeFormly.formly(app.src.db.models.departamento, app.src.db.models));

  app.api.post('/departamentos', app.controller.departamento.post);
  app.api.put('/departamentos/:id', app.controller.departamento.put);
  app.api.delete('/departamentos/:id', app.controller.departamento.delete);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Departamento
   * @apiName Get bolivia
   * @api {get} /public/departamentos/bolivia Obtiene los bounds del país

   * @apiDescription Get bolivia, obtiene los bounds del país

   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK

  */
  app.api.get('/departamentos/bolivia', app.controller.departamento.getBolivia);
};
