import validate from 'express-validation';
import paramValidation from './khipus.validation';
import { apidoc as APIDOC, field as FIELD } from '../../../lib/apidoc-generator';

module.exports = (app) => {
  const R = app.src.config.config.api.main;

  // app.serviceKhipus.get('/proyectos', app.controller.servicioKhipus.obtenerProyectos);
  app.serviceKhipus.get('/proyectos', validate(paramValidation.obtenerProyectos), app.controller.servicioKhipus.obtenerProyectos);
  APIDOC.get(`${R}/proyectos`, {
    name: 'Obtener datos del proyecto',
    group: 'Servicio Khipus',
    description: 'Obtiene los datos del proyecto en formato requerido por el sistema Khipus.',
    input: {
      headers: {
        authorization: 'Bearer <Codigo de autorizacion>'
      }
    }
  });

  // app.serviceKhipus.get('/proyectos/:codigo/desembolsos', app.controller.servicioKhipus.obtenerDesembolsos);
  app.serviceKhipus.get('/proyectos/:codigo/desembolsos', validate(paramValidation.obtenerDesembolsos), app.controller.servicioKhipus.obtenerDesembolsos);
  APIDOC.get(`${R}/proyectos/:codigo/desembolso`, {
    name: 'Obtener datos de los desembolsos de un determinado proyecto',
    group: 'Servicio Khipus',
    description: 'Obtiene los datos de desembolsos del proyecto en formato requerido por el sistema Khipus.',
    input: {
      headers: {
        authorization: 'Bearer <Codigo de autorizacion>'
      }
    }
  });

  app.serviceKhipus.get('/proyectos/:codigo/fotos/:nombre', validate(paramValidation.obtenerFotografia), app.controller.servicioKhipus.obtenerFotografia);
  APIDOC.get(`${R}/proyectos/:codigo/fotos/:nombre`, {
    name: 'Obtener fotografías correspondientes a un desembolso',
    group: 'Servicio Khipus',
    description: 'Obtiene la imágen de la fotografía.',
    input: {
      headers: {
        authorization: 'Bearer <Codigo de autorizacion>'
      }
    }
  });

};
