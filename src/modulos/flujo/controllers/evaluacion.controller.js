import util from '../../../lib/util';
import errors from '../../../lib/errors';
import moment from 'moment';
import shortid from 'shortid';

module.exports = (app) => {
  const _app = app;
  _app.controller.evaluacion = {};
  const logController = _app.controller.evaluacion;
  const formModel = app.src.db.models.formulario;
  const formHistModel = app.src.db.models.formulario_historico;
  const sequelize = app.src.db.sequelize;

  async function generar (req, res, next) {
    const USUARIO = req.body.audit_usuario;
    sequelize.transaction(async(t) => {

      const codigo = `${shortid.generate()}`;
      req.body.version = 1;
      // Guardar formulario evaluacion
      let form_proy = {
        id_proyecto: req.body.cod,
        estado_proyecto: 'REGISTRO_PROYECTO_FDI'
      };
      let resp;
      if (req.body.$_plantilla && req.body.$_nombre) {
        console.log("Formulario", req.body.$_plantilla, req.body.$_nombre);
        req.body.codigo_documento = codigo;
        delete req.body.audit_usuario;
        resp = await app.dao.formulario.crearActualizar(USUARIO,form_proy, req.body, t);
        await app.dao.formulario_historico.crear(JSON.parse(JSON.stringify(resp)), t);
      }
      const fechaActual = moment(resp._fecha_modificacion);
      req.body.fechaActual = fechaActual.format('DD/MM/YYYY HH:mm:ss');

      let { nombreArchivo, carpeta } = await app.dao.reporte.obtenerNombreArchivoYCarpeta('EVAL');
      nombreArchivo = nombreArchivo.replace('{{codigo}}', req.body.codigo_documento);
      carpeta = `${_path}/uploads/proyectos/${req.body.codigo}/${carpeta}`;
      util.crearDirectorio(`${_path}/uploads/proyectos/`);
      util.crearDirectorio(`${_path}/uploads/proyectos/${req.body.codigo}`);
      util.crearDirectorio(`${carpeta}`);
      await app.dao.formulario_evaluacion.crear(nombreArchivo, carpeta, req.body, t);
      return {
        nombre: resp.nombre,
        codigo_documento: resp.codigo_documento,
        fecha_documento: resp.fecha_documento,
        _fecha_creacion: resp.fecha_documento
      };
    }).then((result)=>{
      res.json({
        finalizado: true,
        mensaje: 'Documento de evaluación generado correctamente.',
        datos: result
      });
    }).catch((error)=>{
      next(error);
    });
  }

  async function listarHistorico (req, res, next) {
    const USUARIO = req.body.audit_usuario;
    try {
      let form = {
        id_proyecto: req.params.id_proyecto,
        id_plantilla: 101,
        nombre: 'evaluacion',
      };
      let result = await app.dao.formulario_historico.listarHistorico(form);;

      res.json({
        finalizado: true,
        mensaje: 'Historial de evaluaciones obtenido correctamente.',
        datos: result
      });
    } catch(error) {
      next(error);
    }
  }

  async function obtenerHistorico (req, res, next) {
    const USUARIO = req.body.audit_usuario;
    try {
      let form = {
        fid_proyecto: req.params.id_proyecto,
        fid_plantilla: 101,
        nombre: 'evaluacion',
        id_formulario: req.params.id_formulario,
        codigo_documento: `${req.query.codigo}`,
      };
      let result = await app.dao.formulario_historico.obtenerHistorico(form);;

      res.json({
        finalizado: true,
        mensaje: 'Historial de evaluaciones obtenido correctamente.',
        datos: result
      });
    } catch(error) {
      next(error);
    };
  }

  // TODO Servicio temporal para corregir documentos
  async function corregirDocumento (req, res, next) {
    return app.dao.common.crearTransaccion(async (t) => {
      if (['evaluacion','informe'].indexOf(req.params.nombre)<0) {
        throw new errors.ValidationError('Documento desconocido.');
      }
      let formulario = await formHistModel.findOne({
        attributes: ['id_formulario', 'fecha_documento', 'codigo_documento'],
        where: {
          // id_formulario: req.params.id_formulario,
          nombre: req.params.nombre,
          codigo_documento: req.body.codigo_documento
        }
      });
      let datos = {};
      if (formulario) {
        if (req.body.fecha_documento) {
          // datos.fecha_documento = moment(req.body.fecha_documento,'DD-MM-YYYY').format('YYYY-MM-DD') + ' ' + moment(formulario.dataValues.fecha_documento).format('HH:mm');
          datos.fecha_documento = req.body.fecha_documento + ' ' + moment(formulario.dataValues.fecha_documento).format('HH:mm');
          datos.fecha_documento = moment(datos.fecha_documento,'YYYY-MM-DD');
        }
        if (req.body.estado) {
          datos.estado = req.body.estado;
        }
        formulario = await formModel.update(datos,{
          where: {
            // id_formulario: req.params.id_formulario,
            nombre: req.params.nombre,
            codigo_documento: req.body.codigo_documento
          }
        });
        formulario = await formHistModel.update(datos,{
          where: {
            // id_formulario: req.params.id_formulario,
            nombre: req.params.nombre,
            codigo_documento: req.body.codigo_documento
          }
        });
        if (req.params.nombre=='evaluacion'){
          await app.dao.formulario_evaluacion.regenerar(req.params.id_proyecto, req.body.codigo_documento);
        }
        if (req.params.nombre=='informe'){
          await app.dao.informe_evaluacion.regenerar(req.params.id_proyecto, req.body.codigo_documento);
        }
      } else {
        throw new errors.ValidationError('No se puede modificar el dato.');
      }
      return datos;
    }).then(response => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'Se modificó los datos exitosamente.',
        datos: response
      });
    }).catch(error => {
      return next(error);
    });
  }

  _app.controller.evaluacion.corregirDocumento = corregirDocumento;
  _app.controller.evaluacion.generar = generar;
  _app.controller.evaluacion.listarHistorico = listarHistorico;
  _app.controller.evaluacion.obtenerHistorico = obtenerHistorico;
};
