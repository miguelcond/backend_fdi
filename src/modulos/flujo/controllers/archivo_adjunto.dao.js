import util from '../../../lib/util';
import errors from '../../../lib/errors';
import fs from 'fs-extra';
import fileType from 'file-type';
import shortid from 'shortid';
const md5File = require('md5-file')
import _ from 'lodash';

module.exports = (app) => {
  const _app = app;
  _app.dao.archivo_adjunto = {};
  const models = app.src.db.models;
  const sequelize = app.src.db.sequelize;
  const archivoDao = _app.dao.archivo_adjunto;

  archivoDao.escribirBase64 = (path, base64) => {
    let tipo;
    let decoded;
    let md5;
    try {
      const partes = base64.split('base64,');
      if (partes.length === 2) {
        decoded = Buffer.from(partes[1], 'base64');
        tipo = partes[0];
      } else {
        decoded = Buffer.from(partes[0], 'base64');
        let type = fileType(decoded);
        if (type) {
          tipo = type.mime;
        }
      }
      const dir = `${_path}/uploads`;
      if (!fs.existsSync(`${dir}/${path}`)) {
        fs.mkdirSync(`${dir}/${path}`, {recursive : true});
      }
      let filename = shortid.generate();
      // if (!path) path=shortid.generate();
      fs.writeFileSync(`${dir}/${path}/${filename}`, decoded);
      md5 = md5File.sync(`${dir}/${path}/${filename}`);
      fs.renameSync(`${dir}/${path}/${filename}`, `${dir}/${path}/${md5}`);
      path = md5;
    } catch(error) {
      console.error('*****************************');
      console.error('ERROR', error && error.message);
      throw new errors.ValidationError(error && error.message && error.message.replace(/'(.*)'/g,''));
    }
    return {path,tipo};
  };

  archivoDao.leerArchivo = (path) => {
    const dir = `${_path}/uploads`;
    const buffer = fs.readFileSync(`${dir}/${path}`);
    let type = fileType(buffer);
    if (type) {
      type = type.mime;
    } else {
      type = 'text/plain';
    }
    return {
      tipo: type,
      base64: buffer.toString('base64')
    };
  };

  archivoDao.eliminarArchivo = async (path) => {
    const dir = `${_path}/uploads`;
    const buffer = await fs.remove(`${dir}/${path}`);
    console.info('*****************************');
    console.info('INFO', `Archivo eliminado ${path}`);
    return 1;
  };

};
