import util from '../../../lib/util';
import errors from '../../../lib/errors';

module.exports = (app) => {
  const _app = app;
  _app.dao.formulario = {};
  const models = app.src.db.models;

  async function crearActualizar (idUsuario, proyecto, datos, t) {
    let fechaActual = util.fechaActual();
    let form = null;
    try {
      form = await models.formulario.update({
        form_model: datos,
        codigo_estado: proyecto.estado_proyecto,
        version: datos.version,
        codigo_documento: datos.codigo_documento,
        fecha_documento: fechaActual,
        _usuario_modificacion: idUsuario.id_usuario,
      },{
        where: {
          fid_proyecto: proyecto.id_proyecto,
          fid_plantilla: datos.$_plantilla,
          nombre: datos.$_nombre,
        },
        transaction: t,
      });
      if (!form || form[0]<1) {
        form = await models.formulario.create({
          fid_proyecto: proyecto.id_proyecto,
          fid_plantilla: datos.$_plantilla,
          nombre: datos.$_nombre,
          codigo_estado: proyecto.estado_proyecto,
          codigo_documento: datos.codigo_documento,
          fecha_documento: fechaActual,
          version: datos.version,
          form_model: datos,
          _usuario_creacion: idUsuario.id_usuario,
          _fecha_creacion: fechaActual,
        }, { transaction: t });
        if (!form) {
          throw new errors.ValidationError('No se actualizó los datos del formulario.');
        }
        return util.json(form);
      }
      form = await models.formulario.findOne({
        where: {
          fid_proyecto: proyecto.id_proyecto,
          fid_plantilla: datos.$_plantilla,
          nombre: datos.$_nombre,
        },
        transaction: t
      });
      return util.json(form);
    } catch(e) {
      console.log('ERROR', e);
      throw new errors.ValidationError('Error interno...');
    }
  }

  async function obtener (formulario, t) {
    // try {
      let form = await models.formulario.findOne({
        where: {
          fid_proyecto: formulario.id_proyecto,
          fid_plantilla: formulario.$_plantilla,
          nombre: formulario.$_nombre
        },
        transaction: t,
        include:[
          {
            model: models.proyecto,
            as : 'proyecto',
            atributes:['cartera'],
            where:{
              id_proyecto : formulario.id_proyecto
            }

          }
        ]
      });
      return util.json(form);
    // } catch(e) {
    //   console.log(e);
    //   throw new errors.ValidationError('Error interno...');
    // }
  }

  async function obtenerFormulario(id_proyecto,nombre){
    let form = await models.formulario.findOne({
        where: {
          fid_proyecto: id_proyecto,          
          nombre: nombre,
          estado: 'ELABORADO'
        }});
    return util.json(form);
  }

  _app.dao.formulario.crearActualizar = crearActualizar;
  _app.dao.formulario.obtener = obtener;
  _app.dao.formulario.obtenerFormulario = obtenerFormulario;
};
