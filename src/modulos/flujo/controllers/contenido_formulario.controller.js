import util from '../../../lib/util';
import moment from 'moment';
import sequelize from 'sequelize';

module.exports = (app) => {
  const _app = app;
  _app.controller.contenido_formulario = {};
  const contenidoFormularioController = _app.controller.contenido_formulario;
  const contenidoFormularioModel = app.src.db.models.contenido_formulario;

  contenidoFormularioController.listarContenido = (req, res) => {
    const nombre = req.params.nombre;
    contenidoFormularioModel.findAll({
        attributes: ['seccion', 'estructura'],
        where: {
            nombre: req.params.nombre,
            version: req.query.version || 1
        },
        order: [['seccion']]
    }).then((result) => {
        result = util.json(result);
        res.json({
            finalizado: true,
            mensaje: 'Se obtuvo la lista exitosamente.',
            datos: result
        });
    }).catch((error) => {
        res.status(412).json({
            finalizado: false,
            mensaje: error.message,
            datos: null
        });
    });
  };

  contenidoFormularioController.obtenerContenido = (req, res) => {
    console.log("LISTANDO CONTENIDO: "+req.params.nombre);
    const nombre = req.params.nombre;
    const seccion = req.params.seccion;
    contenidoFormularioModel.findOne({
        attributes: ['estructura'],
        where: {
            nombre: req.params.nombre,
            seccion: req.params.seccion
    }
    }).then((result) => {
        result = util.json(result);
        res.json({
            finalizado: true,
            mensaje: 'Contenido de formulario recuperado correctamente.',
            datos: result
        });
    }).catch((error) => {
        res.status(412).json({
            finalizado: false,
            mensaje: error.message,
            datos: null
        });
    });
  };
};
