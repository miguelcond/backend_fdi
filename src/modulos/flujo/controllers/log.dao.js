import util from '../../../lib/util';
import errors from '../../../lib/errors';

module.exports = (app) => {
  const _app = app;
  _app.dao.log = {};
  const models = app.src.db.models;
  const sequelize = app.src.db.sequelize;

  async function obtenerLog (codigo, usuario) {
    let proy = await models.proyecto.findOne({
      attributes: ['id_proyecto'],
      where: {
        id_proyecto: codigo,
      }
    });
    let sup = await models.supervision.findAll({
      attributes: ['id_supervision'],
      where: {
        fid_proyecto: codigo,
      }
    });
    sup = sup.map((s)=>{ return s.id_supervision;})
    let logs = await models.log.findAll({
      attributes: ['objeto_tipo', 'observacion','_fecha_creacion'],
      where: {
        $or: [
          {
            objeto_id: codigo,
            objeto_tipo: 'proyecto',
          },
          {
            objeto_id: {
              $in: sup
            },
            objeto_tipo: 'supervision',
          },
        ]
      },
      include: [{
        model: models.estado,
        as: 'estado',
        attributes: ['codigo','codigo_proceso','nombre','tipo'],
        where: {
          estado: 'ACTIVO'
        },
      },{
        model: models.usuario,
        as: 'usuario',
        attributes: ['id_usuario'],
        include: [{
          model: models.persona,
          as: 'persona',
          attributes: ['nombres','primer_apellido','segundo_apellido'],
          // where: {
          //   estado: 'ACTIVO'
          // },
          required: false,
        }]
      },{
        model: models.rol,
        as: 'usuario_rol',
        attributes: ['id_rol','nombre','descripcion'],
        where: {
          estado: 'ACTIVO'
        },
      }],
      order: [['_fecha_creacion', 'DESC']],
      //order: [[{model: models.log, as: 'log'}, '_fecha_creacion', 'DESC']],
    });
    proy = util.json(proy);
    if (proy.documentacion_exp) {
      delete proy.documentacion_exp.experiencias;
      delete proy.documentacion_exp.empleados;
      delete proy.documentacion_exp.equipos_trabajo;
    }
    proy.logs = util.json(logs);;
    return proy;
  }

  async function crearLog (codigo, tipo, usuario, estado, observacion) {
    if (observacion&&typeof(observacion)=='string') {
      observacion = observacion.replace('undefined','');
    }
    let logs = await models.log.create({
      objeto_id: codigo,
      objeto_tipo: tipo,
      objeto_estado: estado,
      observacion: observacion||'',
      fid_usuario: usuario.id_usuario,
      fid_usuario_rol: usuario.id_rol,
      _usuario_creacion: usuario.id_usuario,
      _usuario_modificacion: usuario.id_usuario,
      _fecha_creacion: new Date(),
    });
    return logs;
  }

  _app.dao.log = {
    obtenerLog,
    crearLog,
  };
};
