import errors from '../../../lib/errors';

module.exports = (app) => {
  const _app = app;
  _app.dao.transicion = {};
  const models = app.src.db.models;
  const sequelize = app.src.db.sequelize;

  async function registrarTransicion (objeto, tipo, codigo, obs, idUsuario, idRol, msgLog, t) {
    const ESTADOS_PROCESO = await _app.dao.estado.getEstadosPorCodigo(codigo);
    let transicion = await models.transicion.findOne({
      atributes: ['codigo_estado'],
      where: {
        objeto_transicion: objeto,
        tipo_transicion: tipo,
        codigo_estado: {$in: ESTADOS_PROCESO},
        fid_usuario_asignado: null
      },
      order: [['id_transicion', 'DESC']],
      limit: 1,
      transaction: t
    });
    if (transicion) {
      transicion.fid_usuario_asignado = idUsuario;
      transicion.fid_usuario_asignado_rol = idRol;
      await transicion.save();
    }
    if (!transicion || transicion.dataValues.codigo_estado !== codigo || (transicion.dataValues.codigo_estado === 'EN_EJECUCION_FDI' && idRol === 16)) {
      await models.transicion.create({
        objeto_transicion: objeto,
        tipo_transicion: tipo,
        codigo_estado: codigo,
        observacion: obs,
        fid_usuario: idUsuario,
        fid_usuario_rol: idRol,
        _usuario_creacion: idUsuario
      }, {
        transaction: t
      });
    }
    app.dao.log.crearLog(objeto, tipo, {id_usuario:idUsuario,id_rol:idRol}, codigo, obs||'' + msgLog||'');
  }

  // async function obtenerUsuariosAsignados (idUsuario) {
  //   const result = await models.transicion.findAll({
  //     attributes: ['id_transicion', 'codigo_estado', 'observacion', 'fid_usuario', 'fid_usuario_asignado'],
  //     where: {
  //       fid_usuario: idUsuario,
  //       fid_usuario_asignado: {
  //         $ne: null
  //       }
  //     }
  //   });
  //   const usuariosAsignados = [];
  //   for (let i in result) {
  //     if (!usuariosAsignados.includes(result[i].fid_usuario_asignado)) {
  //       usuariosAsignados.push(result[i].fid_usuario_asignado);
  //     }
  //   }
  //   return usuariosAsignados;
  // }

  async function verificarProcesoUsuario (idObjetoTransicion, idUsuario, idRol) {
    const TRANSICION = await models.transicion.findOne({
      where: {
        objeto_transicion: idObjetoTransicion,
        fid_usuario_asignado: idUsuario
      }
    });
    if (!TRANSICION && idRol!=19 && idRol!=20) { // Rol visualizador
      throw new errors.ValidationError('No existe el proceso, o el mismo no fue ejecutado por el usuario indicado.');
    }
  }

  async function reemplazarUsuario(idUsuario, idAnterior, idNuevo, idProyecto, t) {
    // Duplicar registros de transicion del usuario anterior y cambiar fid_usuario, fid_usuario_asignado al del nuevo usuario
    await sequelize.query(`INSERT INTO transicion (objeto_transicion, tipo_transicion, codigo_estado, observacion, fid_usuario, fid_usuario_rol, fid_usuario_asignado, fid_usuario_asignado_rol, estado, _usuario_creacion, _fecha_creacion, _fecha_modificacion)
      SELECT objeto_transicion, tipo_transicion, codigo_estado, observacion, fid_usuario, fid_usuario_rol, ? fid_usuario_asignado, fid_usuario_asignado_rol, estado, ? _usuario_creacion, _fecha_creacion, _fecha_modificacion
      FROM transicion
      WHERE (fid_usuario_asignado = ?) AND ((objeto_transicion = ? AND tipo_transicion = 'proyecto') OR
      ((tipo_transicion = 'adjudicacion' AND objeto_transicion IN (SELECT ad.id_adjudicacion FROM adjudicacion AS ad WHERE ad.fid_proyecto = ?))
      OR (tipo_transicion = 'supervision' AND objeto_transicion IN (SELECT sup.id_supervision FROM supervision AS sup WHERE sup.fid_proyecto = ?))));`, {
        replacements: [ idNuevo, idUsuario, idAnterior, idProyecto, idProyecto, idProyecto ],
        type: sequelize.QueryTypes.BULKUPDATE,
        transaction: t
      }
    );

    await sequelize.query(`INSERT INTO transicion (objeto_transicion, tipo_transicion, codigo_estado, observacion, fid_usuario, fid_usuario_rol, fid_usuario_asignado, fid_usuario_asignado_rol, estado, _usuario_creacion, _fecha_creacion, _fecha_modificacion)
      SELECT objeto_transicion, tipo_transicion, codigo_estado, observacion, ? fid_usuario, fid_usuario_rol, fid_usuario_asignado, fid_usuario_asignado_rol, estado, ? _usuario_creacion, _fecha_creacion, _fecha_modificacion
      FROM transicion
      WHERE (fid_usuario = ?) AND ((objeto_transicion = ? AND tipo_transicion = 'proyecto') OR
      ((tipo_transicion = 'adjudicacion' AND objeto_transicion IN (SELECT ad.id_adjudicacion FROM adjudicacion AS ad WHERE ad.fid_proyecto = ?))
      OR (tipo_transicion = 'supervision' AND objeto_transicion IN (SELECT sup.id_supervision FROM supervision AS sup WHERE sup.fid_proyecto = ?))));`, {
        replacements: [ idNuevo, idUsuario, idAnterior, idProyecto, idProyecto, idProyecto ],
        type: sequelize.QueryTypes.BULKUPDATE,
        transaction: t
      }
    );

    // Inactivar registros de transicion del usuario anterior
    const transicion = await sequelize.query(`UPDATE transicion SET estado = 'INACTIVO', _usuario_modificacion = ?
      WHERE ((fid_usuario = ?) OR (fid_usuario_asignado = ?)) AND ((objeto_transicion = ? AND tipo_transicion = 'proyecto') OR
      ((tipo_transicion = 'adjudicacion' AND objeto_transicion IN (SELECT ad.id_adjudicacion FROM adjudicacion AS ad WHERE ad.fid_proyecto = ?))
      OR (tipo_transicion = 'supervision' AND objeto_transicion IN (SELECT sup.id_supervision FROM supervision AS sup WHERE sup.fid_proyecto = ?))));`, {
        replacements: [ idUsuario, idAnterior, idAnterior, idProyecto, idProyecto, idProyecto ],
        type: sequelize.QueryTypes.BULKUPDATE,
        transaction: t
      }
    );

    // await app.src.db.models.transicion.update({
    //   estado: 'INACTIVO',
    //   _usuario_modificacion: idUsuario
    // },{
    //   where: {
    //     $or: [ {fid_usuario_asignado: idAnterior}, {fid_usuario: idAnterior} ]
    //   }
    // },{ transaction: t });

    return transicion;
  }

  const getUltimaTransicion = async(id,tipo,estado) => {
    return models.transicion.findOne({
      where: {
        tipo_transicion:tipo,
        objeto_transicion:id,
        codigo_estado: estado
      },
      order: sequelize.literal('id_transicion DESC')
    });
  }

  _app.dao.transicion.registrarTransicion = registrarTransicion;
  // _app.dao.transicion.obtenerUsuariosAsignados = obtenerUsuariosAsignados;
  _app.dao.transicion.verificarProcesoUsuario = verificarProcesoUsuario;
  _app.dao.transicion.reemplazarUsuario = reemplazarUsuario;
  _app.dao.transicion.getUltimaTransicion = getUltimaTransicion;
};
