import seqOpt from '../../../lib/sequelize-options';
import util from '../../../lib/util';
import moment from 'moment';
// import sequelize from 'sequelize';

module.exports = (app) => {
  const _app = app;
  _app.controller.estado = {};
  const estadoController = _app.controller.estado;
  const estadoModel = app.src.db.models.estado;
  const proyectoModel = app.src.db.models.proyecto;
  const sequelize = app.src.db.sequelize;

  const output = {
    model: 'estado',
    fields: {
      codigo: ['str', 'pk'],
      codigo_proceso: ['str'],
      nombre: ['str'],
      tipo: ['str'],
      fid_rol: ['int'],
      fid_rol_asignado: ['int'],
      acciones: ['array'],
      atributos: ['array'],
      requeridos: ['array'],
      automaticos: ['array'],
      areas: ['array'],
      atributos_detalle: ['array'],
      estado: ['str'],
      _usuario_creacion: ['int'],
      _usuario_modificacion: ['int'],
      _fecha_creacion: ['date'],
      _fecha_modificacion: ['date']
    }
  };

  estadoController.get = (req, res) => {
    let anio = req.query.gestion? req.query.gestion: moment(new Date()).format('YYYY');
    let current_project = {};
    let codigo_generado = '';
    let _numero_convenio = 1;
    if(req.params.id){
      proyectoModel.findOne({
        attributes: ['nro_convenio'],
        where: {
          id_proyecto: req.params.id
        }
      }).then((result_base) => {
        codigo_generado = result_base.nro_convenio;
        proyectoModel.findAll({
          attributes: ['nro_convenio'],
          where: sequelize.literal('nro_convenio like \''+codigo_generado+'-%\'')
        }).then((subcodigos) => {
          codigo_generado = codigo_generado+"-"+(subcodigos.length+1);
          estadoModel.findOne({
            // attributes: []
            attributes: { exclude: ['_fecha_creacion'] },
            where: {
              codigo_proceso: req.params.proceso,
              tipo: 'INICIO'
            }
          }).then((result) => {
            result = util.json(result);
            result.fecha_actual = moment(new Date()).format('YYYY-MM-DD');
            result.codigo_generado = codigo_generado;
            res.json({
              finalizado: true,
              mensaje: 'Estado inicial recuperado correctamente.',
              datos: result
            });
          }).catch((error) => {
            res.status(412).json({
              finalizado: false,
              mensaje: error.message,
              datos: null
            });
          });
        });
      });
    } else {
      proyectoModel.findOne({
        attributes: ['nro_convenio'],
        // where: sequelize.literal('extract(YEAR FROM fecha_suscripcion_convenio) = '+anio+' and nro_convenio like \'FDI/DTP/ARTP/%\' '),
        where: sequelize.literal(`nro_convenio LIKE '%ETP/FDI/DTP/____-${anio}' `),
        order: sequelize.literal(`nro_convenio DESC`)
      }).then((result_base) => {
        current_project = result_base;
        if (current_project != null) {
          current_project = util.json(current_project);
          let nro_convenio = current_project.nro_convenio;
          nro_convenio = nro_convenio.replace('ETP/FDI/DTP/','');
          _numero_convenio = parseInt(nro_convenio.split('-')[0]) + 1;
        }

        var str = "" + _numero_convenio;
        var pad = "0000";
        var ans = pad.substring(0, pad.length - str.length) + str;

        codigo_generado = 'ETP/FDI/DTP/'+ans+'-'+anio;

        estadoModel.findOne({
          // attributes: []
          where: {
            codigo_proceso: req.params.proceso,
            tipo: 'INICIO'
          }
        }).then((result) => {
          result = util.json(result);
          result.fecha_actual = moment(new Date()).format('YYYY-MM-DD');
          result.codigo_generado = codigo_generado;
          res.json({
            finalizado: true,
            mensaje: 'Estado inicial recuperado correctamente.',
            datos: result
          });
        }).catch((error) => {
          res.status(412).json({
            finalizado: false,
            mensaje: error.message,
            datos: null
          });
        });

      });
    }
  };

  estadoController.getListado = (req, res) => {
    if (!req.query.order) {
      req.query.order = 'codigo';
    }
    const query = util.paginar(req.query);
    query.where = [
      { estado: { $ne: 'ELIMINADO' } }
    ];

    if (/ACTIVO|INACTIVO|ELIMINADO/.test(req.query.estado)) {
      query.where.push(sequelize.literal(`"estado"."estado" ILIKE '%${req.query.estado}%'`));
    }
    if (req.query.codigo) {
      query.where.push(sequelize.literal(`"estado"."codigo" ILIKE '%${req.query.codigo}%'`));
    }
    if (req.query.codigo_proceso) {
      query.where.push(sequelize.literal(`"estado"."codigo_proceso" ILIKE '%${req.query.codigo_proceso}%'`));
    }
    if (req.query.tipo) {
      query.where.push(sequelize.literal(`"estado"."tipo" ILIKE '%${req.query.tipo}%'`));
    }
    if (req.query.nombre) {
      query.where.push(sequelize.literal(`"estado"."nombre" ILIKE '%${req.query.nombre}%'`));
    }
    if (typeof(query.order) === 'string') {
      query.order = sequelize.literal(query.order);
    }

    estadoModel.findAndCountAll({
      attributes: ['codigo', 'codigo_proceso', 'nombre', 'tipo', 'fid_rol', 'fid_rol_asignado', 'acciones', 'atributos', 'requeridos', 'automaticos', 'areas', 'atributos_detalle', 'estado'],
      where: query.where,
      offset: query.offset,
      limit: query.limit,
      order: query.order,
      // distinct: true
    }).then((result) => {
      // result = util.json(result);
      if (result.rows.length > 0) {
        let optimized = seqOpt.createResult(query, util.json(result.rows), output);
        res.status(200).json({
          finalizado: true,
          mensaje: 'Datos obtenidos correctamente',
          datos: {
            count: result.count,
            rows: optimized
          }
        });
      } else {
        res.status(204).json({
          finalizado: true,
          mensaje: 'No se encontraron registros para esta tabla',
          datos: {
            count: 0,
            rows: []
          }
        });
      }
    }).catch((error) => {
      res.status(412).json({
        finalizado: false,
        mensaje: error.message,
        datos: null
      });
    });
  };

  estadoController.getEditar = (req, res) => {
    const codigo = req.params.codigo;
    estadoModel.findOne({
      attributes: ['codigo', 'codigo_proceso', 'nombre', 'tipo', 'fid_rol', 'fid_rol_asignado', 'acciones', 'atributos', 'requeridos', 'automaticos', 'areas', 'atributos_detalle', 'estado'],
      where: {
        codigo: codigo
      }
    }).then((result) => {
      result = util.json(result);
      res.json({
        finalizado: true,
        mensaje: 'Contenido recuperado correctamente.',
        datos: result
      });
    }).catch((error) => {
      res.status(412).json({
        finalizado: false,
        mensaje: error.message,
        datos: null
      });
    });
  };

  estadoController.putActualizar = (req, res, next) => {
    return sequelize.transaction(async(t) => {
      let codigo = req.params.codigo;
      let datos = req.body;
      let result = util.json(await estadoModel.update(datos, {
        where: {
          codigo: codigo
        },
        transaction: t
      }));
      return result;
    }).then((result) => {
      res.json({
        finalizado: true,
        mensaje: 'Estado modificado correctamente.',
        datos: result
      });
    }).catch((error) => {
      next(error);
    });
  };
};
