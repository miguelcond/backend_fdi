const BaseJoi = require('joi');
const Extension = require('joi-date-extensions');
const Joi = BaseJoi.extend(Extension);

module.exports = {
  obtenerHistorico: {
    params: {
      id_proyecto: Joi.number().min(1).required(),
      id_formulario: Joi.number().min(1).required(),
    },
    query: {
      codigo: Joi.string().min(6).max(20).required(),
    }
  },
  corregirDocumento: {
    params: {
      id_proyecto: Joi.number().min(1).required(),
      nombre: Joi.string().min(3).required(),
      id_formulario: Joi.string().min(1).required(),
    },
    body: {
      codigo_documento: Joi.string().min(6).required(),
      fecha_documento: Joi.string().min(10),
      estado: Joi.string().min(6),
    }
  },
};
