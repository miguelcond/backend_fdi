import sequelizeFormly from 'sequelize-formly';
import { apidoc as APIDOC, field as FIELD } from '../../../lib/apidoc-generator';

module.exports = (app) => {
  const R = app.src.config.config.api.main;

  app.api.options('/estados', sequelizeFormly.formly(app.src.db.models.estado, app.src.db.models));
  app.api.get('/estados', app.controller.estado.getListado);

  app.api.options('/estados/editar', sequelizeFormly.formly(app.src.db.models.estado, app.src.db.models));
  app.api.get('/estados/editar/:codigo', app.controller.estado.getEditar);
  
  app.api.get('/estados/:proceso', app.controller.estado.get);
  app.api.get('/estados/:proceso/:id', app.controller.estado.get);
  APIDOC.get(`${R}/estados/:proceso`, {
    name: 'Obtener estado por codigo de proceso',
    group: 'Estados',
    description: 'Devuelve la información de un estado, a partir del código de proceso (PROYECTO o SUPERVISION).',
    input: {
      params: {
        proceso: FIELD.estado('codigo_proceso', { allowNull: false })
      }
    },
    output: {
      body: {
        codigo: FIELD.estado('codigo'),
        codigo_proceso: FIELD.estado('codigo_proceso'),
        nombre: FIELD.estado('nombre'),
        tipo: FIELD.estado('tipo'),
        fid_rol: FIELD.estado('fid_rol'),
        fid_rol_asignado: FIELD.estado('fid_rol_asignado'),
        aciones: FIELD.estado('acciones'),
        atributos: FIELD.estado('atributos'),
        requeridos: FIELD.estado('requeridos'),
        automaticos: FIELD.estado('automaticos')
      }
    }
  });

  app.api.put('/estados/:codigo', app.controller.estado.putActualizar);
};
