import sequelizeFormly from 'sequelize-formly';
import { apidoc as APIDOC, field as FIELD } from '../../../lib/apidoc-generator';

module.exports = (app) => {

  // app.api.options('/archivo_adjunto', sequelizeFormly.formly(app.src.db.models.archivo_adjunto, app.src.db.models));
  app.api.get('/proyectos/:id/archivo_adjunto', app.controller.archivo_adjunto.getArchivos);
  app.api.get('/proyectos/:id/archivo_adjunto/:id_archivo', app.controller.archivo_adjunto.getArchivoId);
  app.api.put('/proyectos/:id/archivo_adjunto', app.controller.archivo_adjunto.crearArchivo);
  app.api.put('/proyectos/:id/archivo_adjunto/:id_archivo', app.controller.archivo_adjunto.modificarArchivoId);
  // app.api.delete('/proyecto/:id/archivo_adjunto/:id_archivo', app.controller.archivo_adjunto.eliminarArchivo);
};
