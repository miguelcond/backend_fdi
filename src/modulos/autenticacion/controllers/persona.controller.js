module.exports = (app) => {
  const _app = app;
  _app.controller.persona = {};
  const personaController = _app.controller.persona;
  const sequelize = app.src.db.sequelize;

  personaController.get = (req, res) => {
    const persona = {
      tipo_documento: req.query.tipo_documento || 'TD_CI',
      numero_documento: req.query.campo.ci? req.query.campo.ci.toUpperCase(): null,
      fecha_nacimiento: req.query.campo.fecha? req.query.campo.fecha.toUpperCase(): null,
    };
    if (req.query.tipo && req.query.campo) {
      persona.tipo_persona = req.query.tipo;
      persona.complemento = req.query.campo.complemento? req.query.campo.complemento.toUpperCase(): null;
      persona.nombres = req.query.campo.nombres? req.query.campo.nombres.toUpperCase(): null;
      persona.primer_apellido = req.query.campo.primer_apellido? req.query.campo.primer_apellido.toUpperCase(): null;
      persona.segundo_apellido = req.query.campo.segundo_apellido? req.query.campo.segundo_apellido.toUpperCase(): null;
    }
    sequelize.transaction().then((t) => {
      const idUsuario = req.body.audit_usuario.id_usuario;
      app.dao.persona.crearRecuperar(persona, idUsuario, t).then((result) => {
        t.commit();
        res.json({
          finalizado: true,
          mensaje: 'Persona recuperada correctamente.',
          datos: result
        });
      }).catch((error) => {
        t.rollback();
        res.status(412).json({
          finalizado: false,
          mensaje: error.message,
          datos: null
        });
      });
    });
  };
  personaController.getPersonaLocal = (req, res) => {

    sequelize.transaction().then((t) => {
      const idUsuario = req.body.audit_usuario.id_usuario;
      app.dao.persona.buscarLocal(req.query.ci, t).then((result) => {
        t.commit();
        res.json({
          finalizado: true,
          mensaje: 'Persona recuperada correctamente.',
          datos: result
        });
      }).catch((error) => {
        t.rollback();
        res.status(412).json({
          finalizado: false,
          mensaje: error.message,
          datos: null
        });
      });
    });
  };

};
