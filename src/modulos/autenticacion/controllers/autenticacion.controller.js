import jwt from 'jwt-simple';
import crypto from 'crypto';
import errors from '../../../lib/errors';
import util from '../../../lib/util';

module.exports = (app) => {
  const _app = app;
  _app.controller.autenticacion = {};

  async function autenticar (req, res, next) {
    if (!req.body.username || !req.body.password) {
      return next(new errors.ValidationError('Los datos Usuario y Contraseña son obligatorios'));
    }
    const email = req.body.username;
    const contrasena = req.body.password;
    const password = crypto.createHash('md5').update(contrasena).digest('hex');
    try {
      const idRolSolicitado = req.body.id_rol;
      const data = await obtenerDatos(email, password, undefined, idRolSolicitado);
      res.status(200).json(data);
    } catch (error) {
      next(error);
    }
  }

  async function obtenerDatos (email, password, auditUsuario, idRolSolicitado) {
    let rolDevuelto;
    let condiciones = {};
    const rolesAdicionales = [];
    if (auditUsuario && auditUsuario.id_usuario) {
      condiciones = {
        id_usuario: auditUsuario.id_usuario,
        estado: 'ACTIVO'
      };
    } else {
      condiciones = {
        usuario: email,
        contrasena: password,
        estado: 'ACTIVO'
      };
    }

    const datosUsuario = await app.dao.autenticacion.buscarUsuario(condiciones);
    if (datosUsuario && datosUsuario.id_usuario) {
      const datosUsuarioRol = util.json(await app.dao.autenticacion.buscarUsuarioRol(datosUsuario.id_usuario));
      if (datosUsuarioRol.length > 0) {
        if (!idRolSolicitado) {
          let rolDefecto = datosUsuarioRol[0];
          datosUsuarioRol.forEach((r) => {
            if (r.rol.peso < rolDefecto.rol.peso) {
              rolDefecto = r;
            }
          });
          rolDefecto.rol.menu = util.json(await app.dao.autenticacion.buscarRolMenu(rolDefecto.fid_rol));
          rolDefecto.rol.menu = armarMenu(rolDefecto.rol.menu);
          rolesAdicionales.push(rolDefecto.rol);
          datosUsuarioRol.forEach(async(r) => {
            if (r.fid_rol !== rolDefecto.fid_rol) {
              r.rol.menu = util.json(await app.dao.autenticacion.buscarRolMenu(r.fid_rol));
              r.rol.menu = armarMenu(r.rol.menu);
              rolesAdicionales.push(r.rol);
            }
          });
          rolDevuelto = rolDefecto;
        } else {
          datosUsuarioRol.forEach((r) => {
            if (r.fid_rol === idRolSolicitado) {
              rolDevuelto = r;
            }
            rolesAdicionales.push(r.rol);
          });
          if (!rolDevuelto || !rolDevuelto.fid_rol) {
            throw new errors.UnauthorizedError('No cuenta con privilegios para el rol solicitado.');
          }
        }
        const datosRolMenu = util.json(await app.dao.autenticacion.buscarRolMenu(rolDevuelto.fid_rol));
        if (datosRolMenu) {
          const datosRespuesta = await armarRespuesta(datosRolMenu, datosUsuario, rolDevuelto, rolesAdicionales);
          return datosRespuesta;
        }
      } else {
        throw new errors.UnauthorizedError('El usuario no tiene asignado ningún rol.');
      }
    } else {
      throw new errors.UnauthorizedError('Usuario y/o contraseña incorrecto(s).');
    }
    return false;
  }

  function armarMenu(rolMenu) {
    let menuEntrar1 = null;
    const menusDevolverAux = [];
    for (let rm = 0; rm < rolMenu.length; rm += 1) {
      // Obteniendo al padre
      const padre = rolMenu[rm].menu.menu_padre;
      const objPadre = JSON.stringify(padre);
      let existe = false;
      for (let i = 0; i < menusDevolverAux.length; i += 1) {
        if (JSON.stringify(menusDevolverAux[i]) === objPadre) {
          existe = true;
          break;
        }
      }
      if (!existe) {
        menusDevolverAux.push(padre);
      }
    }
    const menusDevolver = [];
    for (let padreI = 0; padreI < menusDevolverAux.length; padreI += 1) {
      const padre = JSON.parse(JSON.stringify(menusDevolverAux[padreI]));
      padre.submenu = [];
      if (padre.url && !menuEntrar1) {
        menuEntrar1 = `/${padre.url}`;
      }
      for (let rmI = 0; rmI < rolMenu.length; rmI += 1) {
        if (padre.id_menu === rolMenu[rmI].menu.fid_menu_padre) {
          const hijo = JSON.parse(JSON.stringify(rolMenu[rmI].menu));
          delete hijo.menu_padre;
          hijo.permissions = {};
          hijo.permissions.read = rolMenu[rmI].method_get;
          hijo.permissions.create = rolMenu[rmI].method_post;
          hijo.permissions.update = rolMenu[rmI].method_put;
          hijo.permissions.delete = rolMenu[rmI].method_delete;
          padre.submenu.push(hijo);
          if (!menuEntrar1) {
            menuEntrar1 = `/${hijo.url}`;
          }
        }
      }
      menusDevolver.push(padre);
    }
    return menusDevolver;
  }

  async function armarRespuesta (rolMenu, usuario, rolDevuelto, rolesAdicionales) {
    let menuEntrar1 = null;
    const menusDevolverAux = [];
    for (let rm = 0; rm < rolMenu.length; rm += 1) {
      // Obteniendo al padre
      const padre = rolMenu[rm].menu.menu_padre;
      const objPadre = JSON.stringify(padre);
      let existe = false;
      for (let i = 0; i < menusDevolverAux.length; i += 1) {
        if (JSON.stringify(menusDevolverAux[i]) === objPadre) {
          existe = true;
          break;
        }
      }
      if (!existe) {
        menusDevolverAux.push(padre);
      }
    }
    const menusDevolver = [];
    for (let padreI = 0; padreI < menusDevolverAux.length; padreI += 1) {
      const padre = JSON.parse(JSON.stringify(menusDevolverAux[padreI]));
      padre.submenu = [];
      if (padre.url && !menuEntrar1) {
        menuEntrar1 = `/${padre.url}`;
      }
      for (let rmI = 0; rmI < rolMenu.length; rmI += 1) {
        if (padre.id_menu === rolMenu[rmI].menu.fid_menu_padre) {
          const hijo = JSON.parse(JSON.stringify(rolMenu[rmI].menu));
          delete hijo.menu_padre;
          hijo.permissions = {};
          hijo.permissions.read = rolMenu[rmI].method_get;
          hijo.permissions.create = rolMenu[rmI].method_post;
          hijo.permissions.update = rolMenu[rmI].method_put;
          hijo.permissions.delete = rolMenu[rmI].method_delete;
          padre.submenu.push(hijo);
          if (!menuEntrar1) {
            menuEntrar1 = `/${hijo.url}`;
          }
        }
      }
      menusDevolver.push(padre);
    }
    // Aqui buscar al Menu de usuario
    const ven = new Date();
    ven.setDate(ven.getDate() + 1);
    const ROLES_ID = [];
    for (let i in rolesAdicionales) {
      ROLES_ID.push(rolesAdicionales[i].id_rol);
    }
    const payload = {
      id_usuario: usuario.id_usuario,
      usuario: usuario.usuario,
      id_rol: rolDevuelto ? rolDevuelto.fid_rol : 0,
      roles: ROLES_ID,
      id_persona: usuario.persona.id_persona,
      vencimiento: ven
    };
    const usuarioEnviar = {
      id_usuario: usuario.id_usuario,
      nombres: usuario.persona.nombres,
      apellidos: `${usuario.persona.primer_apellido} ${usuario.persona.segundo_apellido}`,
      email: usuario.persona.correo,
      usuario: usuario.usuario,
      rol: rolDevuelto.rol,
      estado: usuario.estado,
      roles: rolesAdicionales
    };
    const resultado = {
      finalizado: true,
      mensaje: 'Obtención de datos exitoso.',
      token: jwt.encode(payload, app.settings.secretAGETIC),
      datos: {
        usuario: usuarioEnviar,
        menu: menusDevolver,
        menuEntrar: menuEntrar1
      }
    };
    return resultado;
  }

  async function restablecerContrasenia (req, res, next) {
    const email = req.body.email;
    try {
      let usuario = await app.dao.autenticacion.restablecerContrasena(email);
      res.status(200).json({
        finalizado: true,
        mensaje: 'Se le ha enviado un email de verificación.'
      });
    } catch (error) {
      next(error);
    }
  }

  async function verificarCodigo (req, res, next) {
    const codigo = req.body.codigo;
    try {
      let usuario = await app.dao.autenticacion.verificarCodigo(codigo);
      res.status(200).json({
        finalizado: true,
        mensaje: 'Código verificado'
      });
    } catch (error) {
      next(error);
    }
  }

  async function cambiarContrasenia (req, res, next) {
    const datos = req.body;
    try {
      let usuario = await app.dao.autenticacion.cambiarContrasena(datos);
      res.status(200).json({
        finalizado: true,
        mensaje: 'Su contraseña ha sido actualizada'
      });
    } catch (error) {
      next(error);
    }
  }

  _app.controller.autenticacion.post = autenticar;
  _app.controller.autenticacion.restablecerContrasenia = restablecerContrasenia;
  _app.controller.autenticacion.verificarCodigo = verificarCodigo;
  _app.controller.autenticacion.cambiarContrasenia = cambiarContrasenia;
};
