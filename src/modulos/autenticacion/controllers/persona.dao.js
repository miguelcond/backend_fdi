import util from '../../../lib/util';
import errors from '../../../lib/errors';
import moment from 'moment';
import segip from '../../../services/segip/segip';

module.exports = (app) => {
  app.dao.persona = {};

  const models = app.src.db.models;
  const personaModel = models.persona;

  /**
  * Devuelve un objeto con los campos que se encuentran en el modelo 'persona'.
  * Solamente los campos que devuelve SEGIP.
  * @param {String} numeroDocumento Número del documento de identidad (CI).
  * @param {Date} fechaNacimiento Fecha de nacimiento.
  * @return {Object}
  * @throws {NotFoundError} throw Cuando no se devuelve un único registro.
  */
  async function buscarSEGIP (numeroDocumento, fechaNacimiento) {
    // console.log("DOC = ", numeroDocumento);
    // console.log("FEC_NAC = ", fechaNacimiento);
    let parametros = { ci: numeroDocumento };
    if (fechaNacimiento) {
      parametros.fecha_nacimiento = moment(fechaNacimiento).format('DD/MM/YYYY');
    }
    const resultadoSEGIP = await segip.obtenerPersona(parametros);
    // Verifica que los datos devueltos tengan el formato correcto.
    if ((!resultadoSEGIP) || (resultadoSEGIP.error) || (!resultadoSEGIP.ConsultaDatoPersonaEnJsonResult)) {
      throw new errors.NotFoundError(`La persona no se encuentra registrada en SEGIP.`);
    }
    const codigoRespuestaSEGIP = resultadoSEGIP.ConsultaDatoPersonaEnJsonResult.CodigoRespuesta;
    // 1 = No se encontró el registro [persona ú documento]
    if (codigoRespuestaSEGIP === '1') {
      throw new errors.NotFoundError(`La persona no se encuentra registrada en SEGIP.`);
    }
    // 2 = Se encontro 1 registro [persona ú documento]
    if (codigoRespuestaSEGIP === '2') {
      let datosPersonaSEGIP = JSON.parse(resultadoSEGIP.ConsultaDatoPersonaEnJsonResult.DatosPersonaEnFormatoJson);
      if(datosPersonaSEGIP.PrimerApellido) datosPersonaSEGIP.PrimerApellido = datosPersonaSEGIP.PrimerApellido.replace(/-/g,'');
      if(datosPersonaSEGIP.SegundoApellido) datosPersonaSEGIP.SegundoApellido = datosPersonaSEGIP.SegundoApellido.replace(/-/g,'');
      // Se crea un nuevo formato.
      let datosPersona = {
        numero_documento: datosPersonaSEGIP.NumeroDocumento,
        tipo_documento: 'TD_CI',
        complemento: datosPersonaSEGIP.Complemento,
        complemento_visible: (datosPersonaSEGIP.ComplementoVisible === '1'),
        fecha_nacimiento: moment(datosPersonaSEGIP.FechaNacimiento, 'DD/MM/YYYY').toDate(),
        nombres: datosPersonaSEGIP.Nombres,
        primer_apellido: datosPersonaSEGIP.PrimerApellido,
        segundo_apellido: datosPersonaSEGIP.SegundoApellido,
        // nacionalidad: (datosPersonaSEGIP.LugarNacimientoPais === 'BOLIVIA') ? 'BOLIVIANA' : null,
        correo: null,
        telefono: null,
        direccion: null,
        estado: null
      };
      return datosPersona;
    }
    // 3 = Se encontró mas de un registro [persona ú documento]
    if (codigoRespuestaSEGIP === '3') {
      throw new errors.NotFoundError(`La persona se encuentra registrada en SEGIP, pero está observada. Favor de regularizar esta situación con SEGIP.`);
    }
    // 4 = Registro con observacion.
    if (codigoRespuestaSEGIP === '4') {
      throw new errors.NotFoundError(`La persona se encuentra registrada en SEGIP, pero está observada. Favor de regularizar esta situación con SEGIP.`);
    }
    // 0 = No se realizo la búsqueda
    throw new errors.NotFoundError(`La persona no se encuentra registrada en SEGIP.`);
  }

  async function buscarLocal (numeroDocumento) {
    if (numeroDocumento) {
      return models.persona.findOne({
        attributes: [
          'id_persona',
          'numero_documento',
          'tipo_documento',
          'complemento',
          'complemento_visible',
          'fecha_nacimiento',
          'nombres',
          'primer_apellido',
          'segundo_apellido',
          // 'nacionalidad',
          'correo',
          'telefono',
          'direccion',
          'estado'
        ],
        where: {
          numero_documento: numeroDocumento
        }
      });
    }
  }

  async function crearObtener (datos, idUsuario) {
    // console.log("CREAR OBTENER");
    // console.log("DATOS = ", datos);
    const numeroDocumento = datos.numero_documento;
    const fechaNacimiento = datos.fecha_nacimiento;
    const persona = await buscarLocal(numeroDocumento);
    if (persona) {
      await modificar(datos, persona.tipo_documento, persona.id_persona, idUsuario);
      return buscarLocal(numeroDocumento);
    }
    let datosPersona = {};
    if (datos.tipo_documento === 'TD_CI') {
      datosPersona = await buscarSEGIP(numeroDocumento, fechaNacimiento);
      // datosPersona.lugar_expedicion_documento = datos.lugar_expedicion_documento;
      // datosPersona.nacionalidad = 'BOLIVIANA';
    }
    if (datos.tipo_documento === 'TD_PASS') {
      datosPersona = util.crearObjeto(datos, [
        'numero_documento',
        'fecha_nacimiento',
        'tipo_documento',
        // 'nacionalidad',
        'nombres',
        'primer_apellido',
        'segundo_apellido'
      ]);
    }
    datosPersona.tipo_documento = datos.tipo_documento;
    datosPersona.tipo_persona = datos.tipo_persona;
    datosPersona.correo = datos.correo;
    datosPersona.telefono = datos.telefono;
    datosPersona.direccion = datos.direccion;
    datosPersona.estado = datos.estado;
    datosPersona._usuario_creacion = idUsuario;
    return models.persona.create(datosPersona);
  }

  async function buscarID (idPersona) {
    return models.persona.findOne({where: {id_persona: idPersona}});
  }

  async function validarDatosNacionalExtranjero (datos = {}) {
    if (!datos.tipo_documento) {
      throw new errors.ValidationError(`Se requiere el campo 'Tipo de documento'.`);
    }
    if (datos.tipo_documento === 'CI') {
      if (!datos.fecha_nacimiento || !datos.numero_documento) {
        throw new errors.ValidationError(`Se requieren los campos 'Fecha de nacimiento' y 'Número del documento de identidad'.`);
      }
    }
    if (datos.tipo_documento === 'EXT') {
      if (!datos.fecha_nacimiento || !datos.nombres) {
        throw new errors.ValidationError(`Se requieren los campos 'Nombres' y 'Fecha de nacimiento'.`);
      }
    }
  }

  async function modificar (datos, tipoDocumento, idPersona, idUsuario) {
    let datosPersona = {};
    if (tipoDocumento === 'TD_CI') {
      datosPersona = util.crearObjeto(datos, [
        'tipo_persona',
        'correo',
        'telefono',
        'direccion',
        'estado'
      ]);
    }
    if (tipoDocumento === 'TD_PASS') {
      datosPersona = util.crearObjeto(datos, [
        'tipo_persona',
        'numero_documento',
        'fecha_nacimiento',
        'nombres',
        'primer_apellido',
        'segundo_apellido',
        // 'nacionalidad',
        'correo',
        'telefono',
        'direccion',
        'estado'
      ]);
    }
    await actualizarDatos(idPersona, datosPersona, idUsuario);
  }

  async function actualizarDatos (idPersona, datosPersona, idUsuario) {
    if (Object.keys(datosPersona).length > 0) {
      datosPersona._usuario_modificacion = idUsuario;
      await models.persona.update(datosPersona, {where: {id_persona: idPersona}});
    }
  }

  // Funcion para comunicarse con interoperabilidad y contrastar los datos
  async function contrastar(persona) {
    let mensaje, estado;
    // Crear objeto de datos segun lo explicado en el manual de interoperabilidad
    let campos = {
      'NumeroDocumento': persona.numero_documento,
    };
    if (persona.complemento) campos['Complemento'] = persona.complemento||'';
    if (persona.fecha_nacimiento) campos['FechaNacimiento'] = util.fechaAFormato(persona.fecha_nacimiento, 'DD/MM/YYYY');
    if (persona.nombres) campos['Nombres'] = persona.nombres||'';
    if (persona.primer_apellido) campos['PrimerApellido'] = persona.primer_apellido||'';
    if (persona.segundo_apellido) campos['SegundoApellido'] = persona.segundo_apellido||'';

    // Realizar la consulta
    let result = await segip.consultaDatoPersonaContrastacion(campos, persona.tipo_persona||1);
    if (!result || !result.ConsultaDatoPersonaContrastacionResult) {
      throw new Error('No se logró realizar la conexión para la verificación.');
    }
    // Verificando posibles errores
    result = result.ConsultaDatoPersonaContrastacionResult;
    switch (parseInt(result.CodigoRespuesta)) {
      case 0: mensaje = 'No se realizo la búsqueda.'; break;
      case 1: mensaje = 'No se encontró el registro de la persona.'; break;
      case 2: mensaje = 'Se encontro 1 registro.'; break;
      case 3: mensaje = 'Se encontró mas de un registro con estos datos.'; break;
      case 4: mensaje = 'Registro con observacion.'; break;
    }
    if (parseInt(result.CodigoRespuesta)!=2) {
      throw new Error(mensaje);
    }
    // Verificando los datos requeridos
    let requeridos = ['NumeroDocumento','FechaNacimiento','Nombres'];
    estado = 'ACTIVO';
    result = JSON.parse(result.ContrastacionEnFormatoJson);
    if (!persona.tipo_persona) {
      estado = 'NO_CORRESPONDE';
      throw new Error(`La persona ya ha sido registrada pero no fué verificada.`);
    }
    for (let i in result) {
      if (requeridos.indexOf(i)>=0 && result[i]!=1) {
        estado = 'NO_CORRESPONDE';
        throw new Error(`Por favor revise los datos escritos de la <b>persona</b>.`);
      }
    }
    return result;
  }

  async function crearRecuperar (persona, idUsuario, t) {
    let options = {
      where: {
        numero_documento: persona.numero_documento,
        fecha_nacimiento: persona.fecha_nacimiento,
      },
      transaction: t
    };
    if (persona.complemento) {
      options.where.complemento = persona.complemento;
    }
    let result = await personaModel.findOne(options);
    if (result) {
      persona.fecha_nacimiento = util.fechaAFormato(persona.fecha_nacimiento, 'YYYY-MM-DD');
      result.fecha_nacimiento = util.fechaAFormato(result.fecha_nacimiento, 'YYYY-MM-DD');
      if (persona.fecha_nacimiento != result.fecha_nacimiento) {
        throw new Error('Los datos no coinciden con los registros, por favor verifique los datos ingresados y vuelva a intentar,');
      }
      // estado INACTIVO significa que no se ha verificado con segip
      if (result.estado=='INACTIVO' && !persona.noVerificar) {
        await contrastar(persona);
        result.estado = 'ACTIVO';
        result.tipo_documento = 'TD_CI';
      }
      if (persona.fecha_nacimiento) { result.fecha_nacimiento = persona.fecha_nacimiento; }
      if (persona.primer_apellido) { result.primer_apellido = persona.primer_apellido.replace(/-/g,''); }
      if (persona.segundo_apellido) { result.segundo_apellido = persona.segundo_apellido.replace(/-/g,''); }
      if (persona.nombres) { result.nombres = persona.nombres; }
      if (persona.cargo) { result.cargo = persona.cargo; }
      if (persona.correo) { result.correo = persona.correo; }
      if (persona.telefono) { result.telefono = persona.telefono; }
      if (persona.direccion) { result.direccion = persona.direccion; }
      result._usuario_modificacion = idUsuario;
      return result.save({
        transaction: t
      });
    } else {
      if (persona.tipo_documento === 'TD_CI' && !persona.tipo_persona) {
        let result = await segip.obtenerPersona({
          ci: persona.numero_documento,
          complemento: persona.complemento,
          // fecha_nacimiento: util.fechaAFormato(persona.fecha_nacimiento, 'DD/MM/YYYY')
        });
        if (result && result.ConsultaDatoPersonaEnJsonResult) {
          result = result.ConsultaDatoPersonaEnJsonResult;
          if (result.EsValido && parseInt(result.CodigoRespuesta) === 2) {
            result = JSON.parse(result.DatosPersonaEnFormatoJson);
            if (util.fechaAFormato(persona.fecha_nacimiento, 'DD/MM/YYYY') != result.FechaNacimiento) {
              throw new Error('Los datos no coinciden con el registro de SEGIP, por favor verifique los datos ingresados y vuelva a intentar..');
            }
            persona = await personaModel.create({
              tipo_documento: persona.tipo_documento,
              tipo_persona: persona.tipo_persona,
              complemento_visible: persona.complemento? true: result.ComplementoVisible,
              numero_documento: result.NumeroDocumento,
              complemento: result.Complemento,
              nombres: result.Nombres,
              primer_apellido: result.PrimerApellido? `${result.PrimerApellido}`.replace(/-/g,''): null,
              segundo_apellido: result.SegundoApellido? `${result.SegundoApellido}`.replace(/-/g,''): null,
              fecha_nacimiento: moment(result.FechaNacimiento, 'DD/MM/YYYY').format('YYYY-MM-DD'),
              lugar_nacimiento_pais: result.LugarNacimientoPais,
              lugar_nacimiento_departamento: result.LugarNacimientoDepartamento,
              correo: persona.correo,
              telefono: persona.telefono,
              direccion: persona.direccion,
              _usuario_creacion: idUsuario
            }, {
              transaction: t
            });
            return persona;
          } else {
            switch (parseInt(result.CodigoRespuesta)) {
              case 1:
                throw new Error('Los datos no coinciden con el registro de SEGIP, por favor verifique los datos ingresados y vuelva a intentar.');
              case 3:
                throw new Error('Se encontró más de un registro para los datos ingresados, registre el complemento de la cédula de identidad e intente nuevamente.');
              case 4:
                throw new Error('La cédula de identidad ingresada tiene observaciones, por favor verifique el estado de la cédula en SEGIP.');
              default:
                throw new Error(`INTEROP: ${result.DescripcionRespuesta}`);
            }
          }
        } else {
          throw new Error('Datos de la Persona no disponible en segip.');
        }
      } else {
        let estado = '';
        if(!persona.noVerificar) {
          await contrastar(persona);
          estado = 'ACTIVO'
        } else {
          estado = 'INACTIVO';
        }

        if(estado == 'ACTIVO' || estado == 'INACTIVO') {
          persona = await personaModel.create({
            tipo_documento: persona.tipo_documento,
            tipo_persona: persona.tipo_persona,
            numero_documento: persona.numero_documento,
            complemento: persona.complemento,
            nombres: persona.nombres,
            primer_apellido: persona.primer_apellido? `${persona.primer_apellido}`.replace(/-/g,''): null,
            segundo_apellido: persona.segundo_apellido? `${persona.segundo_apellido}`.replace(/-/g,''): null,
            fecha_nacimiento: persona.fecha_nacimiento,
            correo: persona.correo,
            telefono: persona.telefono,
            direccion: persona.direccion,
            estado,
            _usuario_creacion: idUsuario
          }, {
            transaction: t
          });
          if (persona) {
            persona = util.json(persona)
            if (estado == 'INACTIVO') {
              persona.noVerificar = true;
            }
          }
        }
        return persona;
      }
    }
  };

  async function obtenerApellidoNombre (persona) {
    let nombre = '';
    if (persona.primer_apellido && (persona.primer_apellido !== '')) {
      nombre += `${persona.primer_apellido} `;
    }
    if (persona.segundo_apellido && (persona.segundo_apellido !== '')) {
      nombre += `${persona.segundo_apellido} `;
    }
    if (persona.nombres && (persona.nombres !== '')) {
      nombre += `${persona.nombres} `;
    }
    return nombre.trim();
  }

  async function obtenerNombreApellido (persona) {
    let nombre = '';
    if (persona.nombres && (persona.nombres !== '')) {
      nombre += `${persona.nombres} `;
    }
    if (persona.primer_apellido && (persona.primer_apellido !== '')) {
      nombre += `${persona.primer_apellido} `;
    }
    if (persona.segundo_apellido && (persona.segundo_apellido !== '')) {
      nombre += `${persona.segundo_apellido} `;
    }
    return nombre.trim();
  }

  app.dao.persona.buscarSEGIP = buscarSEGIP;
  app.dao.persona.buscarLocal = buscarLocal;
  app.dao.persona.crearObtener = crearObtener;
  app.dao.persona.buscarID = buscarID;
  app.dao.persona.validarDatosNacionalExtranjero = validarDatosNacionalExtranjero;
  app.dao.persona.modificar = modificar;
  app.dao.persona.actualizarDatos = actualizarDatos;
  app.dao.persona.crearRecuperar = crearRecuperar;
  app.dao.persona.obtenerApellidoNombre = obtenerApellidoNombre;
  app.dao.persona.obtenerNombreApellido = obtenerNombreApellido;
};
