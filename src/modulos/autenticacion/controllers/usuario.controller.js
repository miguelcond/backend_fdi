import errors from '../../../lib/errors';
import util from '../../../lib/util';
import shortid from 'shortid';

module.exports = (app) => {
  const _app = app;
  _app.controller.usuario = {};
  const usuarioController = _app.controller.usuario;
  const UsuarioModel = app.src.db.models.usuario;

  const PersonaModel = app.src.db.models.persona;
  const UsuarioRolModel = app.src.db.models.usuario_rol;
  const RolModel = app.src.db.models.rol;

  const sequelize = app.src.db.sequelize;

  async function getUsuario (req, res) {
    if (!req.query.order) {
      req.query.order = 'id_usuario DESC';
    }
    const query = util.paginar(req.query);
    query.where = [
      { estado: { $ne: 'ELIMINADO' } }
    ];
    if (req.query.order) {
      if (/numero_documento|nombres|primer_apellido|segundo_apellido|correo/.test(query.order.split(' ')[0])) {
        // query.order = [[{model: PersonaModel, as: 'persona'}, query.order.split(' ')[0], 'ASC']];
        // query.order = sequelize.col(` persona.${query.order.split(' ')[0]} DESC`);
        query.order = [[{model: PersonaModel, as: 'persona'}, query.order.split(' ')[0], query.order.split(' ').length === 1 ? 'ASC' : 'DESC']];
        // query.order = [[{model: PersonaModel, as: 'persona'}, query.order.split(' ')[0], query.order.length === 1 ? 'ASC' : 'DESC']];
        // query.order = [['persona.nombres', 'DESC']];
        // query.order = [[query.order.split(' ')[0], query.order.length === 1 ? 'ASC' : 'DESC']];
        // query.order = sequelize.literal(`"persona"."${query.order[0]}" ${query.order.length === 1 ? 'ASC' : 'DESC'}`);
        // query.order = sequelize.literal(`"usuario"."${query.order.split(' ')[0]}" ${query.order.length === 1 ? 'ASC' : 'DESC'}`);
      }
    }
    query.subQuery = false;

    if (/ACTIVO|INACTIVO/.test(req.query.estado)) {
      query.where.push(sequelize.literal(`"usuario"."estado" ILIKE '%${req.query.estado}%'`));
    }
    if (req.query.usuario) {
      query.where.push(sequelize.literal(`"usuario"."usuario" ILIKE '%${req.query.usuario}%'`));
    }
    if (req.query.numero_documento) {
      query.where.push(sequelize.literal(`"persona"."numero_documento" ILIKE '%${req.query.numero_documento}%'`));
    }
    if (req.query.nombres) {
      query.where.push(sequelize.literal(`"persona"."nombres" ILIKE '%${req.query.nombres}%'`));
    }
    if (req.query.primer_apellido) {
      query.where.push(sequelize.literal(`"persona"."primer_apellido" ILIKE '%${req.query.primer_apellido}%'`));
    }
    if (req.query.segundo_apellido) {
      query.where.push(sequelize.literal(`"persona"."segundo_apellido" ILIKE '%${req.query.segundo_apellido}%'`));
    }
    if (req.query.correo) {
      query.where.push(sequelize.literal(`"persona"."correo" ILIKE '%${req.query.correo}%'`));
    }
    if (req.query.roles) {
      query.where.push(sequelize.literal(`(SELECT ARRAY(SELECT DISTINCT r.nombre FROM usuario_rol ur INNER JOIN rol r ON r.id_rol=ur.fid_rol WHERE ur.fid_usuario=id_usuario AND r.estado='ACTIVO' AND ur.estado='ACTIVO'))::TEXT ILIKE '%${req.query.roles}%'`));
    }

    if (typeof(query.order) === 'string') {
      query.order = sequelize.literal(query.order);
    }
    /*
    if (req.query.filter) {
      query.where.push({
        $or: [
          sequelize.literal(`"persona"."nombres" ILIKE '${req.query.filter}%'`),
          sequelize.literal(`"persona"."primer_apellido" ILIKE '${req.query.filter}%'`),
          sequelize.literal(`"usuario"."usuario" ILIKE '${req.query.filter}%'`)
        ]
      });
    }
    */

    try {
      // query.order = query.order.replace('roles', 'id_rol');
      const dataUsuario = await UsuarioModel.buscarIncluye(PersonaModel, UsuarioRolModel, RolModel, query);
      if (dataUsuario.count > 0) {
        const rows = dataUsuario.rows.map(row => {
          row = util.json(row);
          let detalles = [];
          for(let i in row.usuarios_roles) {
            detalles.push(row.usuarios_roles[i].rol.descripcion);
          }
          for(let i in row.departamentos) {
            row.departamentos[i] = util.deptoCod2Nombre(row.departamentos[i]);
          }
          return {
            id_usuario: row.id_usuario,
            usuario: row.usuario,
            // fid_persona: row.fid_persona,
            numero_documento: row.persona.numero_documento,
            nombres: row.persona.nombres,
            primer_apellido: row.persona.primer_apellido,
            segundo_apellido: row.persona.segundo_apellido,
            correo: row.persona.correo,
            estado: row.estado,
            cargo: row.cargo,
            // roles: detalles.join(', '),
            roles: detalles.join('; '),
            departamentos: row.departamentos.join(', ')
          };
        });

        res.status(200).json({
          finalizado: true,
          mensaje: 'Datos obtenidos exitosamente',
          datos: {
            count: dataUsuario.count,
            rows
          }
        });
      } else {
        res.status(204).json({
          finalizado: true,
          mensaje: 'No se encontraron usuarios registrados.',
          datos: {}
        });
      }
    } catch (error) {
      res.status(412).json({
        finalizado: false,
        mensaje: error.message,
        datos: {}
      });
    }
  }

  async function getUsuarioId (req, res) {
    const idUsuario = req.params.id;
    const dataUsuario = await UsuarioModel.buscarIncluyeOne(idUsuario, PersonaModel, UsuarioRolModel, RolModel);
    if (dataUsuario) {
      const usuarioDevolver = JSON.parse(JSON.stringify(dataUsuario));
      res.status(200).json({
        finalizado: true,
        mensaje: 'Datos obtenidos correctamente.',
        datos: usuarioDevolver
      });
    } else {
      res.status(204).json({
        finalizado: false,
        mensaje: 'No existe el usuario solicitado.',
        datos: {}
      });
    }
  }

  async function getPersonaId (req, res) {
    const idPersona = req.params.id_persona;
    const dataUsuario = await UsuarioModel.buscarIncluyePorPersona(idPersona, PersonaModel, UsuarioRolModel, RolModel);
    if (dataUsuario) {
      const usuarioDevolver = JSON.parse(JSON.stringify(dataUsuario));
      res.status(200).json({
        finalizado: true,
        mensaje: 'Datos obtenidos correctamente.',
        datos: usuarioDevolver
      });
    } else {
      res.status(204).json({
        finalizado: false,
        mensaje: 'No existe el usuario solicitado.',
        datos: {}
      });
    }
  }

  async function post (req, res) {
    sequelize.transaction().then(t => {
      app.dao.usuario.crearUsuario(req.body, t)
      .then(() => {
        t.commit();
        return res.json({
          finalizado: true,
          mensaje: ' Usuario creado correctamente.'
        });
      })
      .catch(error => {
        t.rollback();
        app.errorHandler.errorFinish(res, error, 'Error en la creacion de usuario');
      });
    });
  }

  async function put (req, res) {
    const idUsuario = req.params.id;
    const usuarioActualizar = req.body;
    usuarioActualizar._usuario_modificacion = req.body.audit_usuario.id_usuario;

    app.dao.common.crearTransaccion(async (t) => {

      try {
        let persona = await app.dao.persona.crearRecuperar(usuarioActualizar.persona, usuarioActualizar._usuario_modificacion, t);
        usuarioActualizar.fid_persona = persona.id_persona;
        usuarioActualizar.usuario = persona.numero_documento;

        UsuarioModel.update(usuarioActualizar, {
          where: {
            id_usuario: idUsuario
          }
        })
        .then((usuarioResp) => {
          if (usuarioResp) {
            if (!usuarioActualizar.roles || !usuarioActualizar.roles.length || usuarioActualizar.roles.length === 0) {
              throw new Error('Debe seleccionar al menos un rol para el usuario que esta creando.');
            }
            return UsuarioRolModel.update({
              estado: 'ELIMINADO',
              _usuario_modificacion: req.body.audit_usuario.id_usuario
            }, {
              where: {
                fid_usuario: idUsuario,
                estado: 'ACTIVO'
              }
            });
          } else {
            throw new Error('El usuario no se encuentra registrado en el sistema o no esta activo.');
          }
        })
        .then((usuarioRolResp) => {
          const rolesUsuariosCrear = [];
          for (const ur in usuarioActualizar.roles) {
            const crearUR = {
              fid_rol: usuarioActualizar.roles[ur],
              fid_usuario: idUsuario,
              _usuario_creacion: req.body.audit_usuario.id_usuario
            };
            rolesUsuariosCrear.push(crearUR);
          }
          return UsuarioRolModel.bulkCreate(rolesUsuariosCrear);
        })
        .then(() => {
          return res.status(200).json({
            finalizado: true,
            mensaje: 'Actualización de usuario exitoso.',
            datos: { id_usuario: idUsuario }
          });
        })
        .catch((error) => {
          return res.status(412).json({
            finalizado: false,
            mensaje: error.message,
            datos: {}
          });
        });
      } catch (error) {
        return res.status(412).json({
          finalizado: false,
          mensaje: error.message.indexOf('persona_correo_key')>0? 'El correo ya esta siendo utilizado por otra persona.': error.message,
          datos: {}
        });
      }
    });
  }

  async function putContrasena (req, res) {
    const idUsuario = req.params.id;
    const usuarioActualizar = req.body;
    if (req.body.nueva!==req.body.confirmado) {
      throw new errors.ValidationError('Las contraseñas introducidas no son iguales.');
    }
    if (req.body.nueva===req.body.actual) {
      throw new errors.ValidationError('Las contraseñas introducidas son iguales.');
    }
    app.dao.usuario.getUsuarioContrasena(idUsuario, req.body.audit_usuario.usuario, req.body.actual)
    .then((result) => {
      if(!result) {
        throw new errors.ValidationError('Contraseña incorrecta.');
      }
      return app.dao.usuario.actualizarContrasena(idUsuario, req.body.nueva, idUsuario);
    })
    .then(() => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'La contraseña fué modificada correctamente.',
        datos: { id_usuario: idUsuario }
      });
    })
    .catch((error) => {
      return res.status(412).json({
        finalizado: false,
        mensaje: error.message,
        datos: {}
      });
    });
  }

  async function nuevaContrasena (req, res) {
    const idAdmin = req.body.audit_usuario.id_usuario;
    const idUsuario = req.body.id_usuario;
    const correo = req.body.correo;
    const password = shortid.generate();
    app.dao.usuario.getUsuarioPersona(idUsuario).then((result) =>{
      if(result.persona.correo !== correo) {
        throw new errors.ValidationError('El correo es incorrecto.');
      }
      result = util.json(result)
      result.contrasena = password;
      return app.dao.usuario.enviarCorreoNuevaContrasenia(result);
    }).then((result) => {
      return app.dao.usuario.actualizarContrasena(idUsuario, password, idAdmin);
    }).then((result) => {
      return res.status(200).json({
        finalizado: true,
        mensaje: 'La contraseña fué enviada correctamente.',
        datos: { id_usuario: idUsuario }
      });
    }).catch((error) => {
      return res.status(412).json({
        finalizado: false,
        mensaje: error.message,
        datos: {}
      });
    });
  }

  usuarioController.get = getUsuario;
  usuarioController.getId = getUsuarioId;
  usuarioController.getPersonaId = getPersonaId;
  usuarioController.post = post;
  usuarioController.put = put;
  usuarioController.putContrasena = putContrasena;
  usuarioController.nuevaContrasena = nuevaContrasena;
};
