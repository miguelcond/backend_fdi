import validate from 'express-validation';
import paramValidation from './persona.validation';

module.exports = (app) => {
  /**
   * @api {get} /api/v1/persona Obtener datos del segip
   * @apiName Persona
   * @apiGroup Persona
   * @apiDescription Obtener datos del segip
   *
   * @apiParam {String} numero_documento Documento de identidad de la persona
   * @apiParam {String} fecha_nacimiento Fecha de nacimiento de la persona
   *
   * @apiSuccessExample Success-Response:
   * HTTP/1.1 200 OK
   *
   */
  // app.api.get('/personas/:ci/:fecha', validate(paramValidation.get), app.controller.persona.get); // obsoleto
  app.api.get('/personas/contrastacion', validate(paramValidation.getContrastacion), app.controller.persona.get);
  app.api.get('/personas/buscarLocal', app.controller.persona.getPersonaLocal); // solo estoy probando de otra forma para buscar persona desde la base de datos local
};
