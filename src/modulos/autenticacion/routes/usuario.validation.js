import Joi from '../../../lib/joi';

module.exports = {
  getUsuarioId: {
    params: {
      id: Joi.number().required()
    }
  },
  createUsuario: {
    body: {
      // nombres: Joi.string().required(),
      // primer_apellido: Joi.string().min(2).max(25).required(),
      // segundo_apellido: Joi.string().min(2).max(25).required(),
      // numero_documento: Joi.required(),
      // fecha_nacimiento: Joi.date().required(),
      // correo: Joi.string().email(),
      // usuario: Joi.string().required(),
      // roles: Joi.array().min(1).required()
      estado: Joi.string().required(),
      fcod_departamento: Joi.array().min(1).required(),
      persona: Joi.object({
        correo: Joi.string().email().required(),
        direccion: Joi.string().required(),
        fecha_nacimiento: Joi.string().required(),
        nombres: Joi.string().required(),
        numero_documento: Joi.required(),
        primer_apellido: Joi.string().allow('').allow(null).optional(),
        segundo_apellido: Joi.string().allow('').allow(null).optional(),
        telefono: Joi.string().required(),
        tipo_documento: Joi.string().required()
      }),
      roles: Joi.array().min(1).required()
    }
  },
  updateUsuario: {
    body: {
      // nombres: Joi.string().required(),
      // primer_apellido: Joi.string().min(2).max(25).required(),
      // segundo_apellido: Joi.string().min(2).max(25).required(),
      // numero_documento: Joi.required(),
      // fecha_nacimiento: Joi.date().required(),
      // correo: Joi.string().email(),
      // usuario: Joi.string().required(),
      // contrasena: Joi.string().required(),
      // roles: Joi.array().min(1).required()
      estado: Joi.string().required(),
      fcod_departamento: Joi.array().min(1).required(),
      persona: Joi.object({
        correo: Joi.string().email().required(),
        direccion: Joi.string().required(),
        fecha_nacimiento: Joi.string().required(),
        nombres: Joi.string().required(),
        numero_documento: Joi.required(),
        primer_apellido: Joi.string().allow('').allow(null).optional(),
        segundo_apellido: Joi.string().allow('').allow(null).optional(),
        telefono: Joi.string().required(),
        tipo_documento: Joi.string().required()
      }),
      roles: Joi.array().min(1).required()
    },
    params: {
      id: Joi.number().required()
    }
  },
  deleteUsuario: {
    params: {
      id: Joi.number().required()
    }
  },
  updateContrasena: {
    params: {
      id: Joi.number().required()
    },
    body: {
      actual: Joi.string().min(3).max(16).required(),
      nueva: Joi.string().min(3).max(16).required(),
      confirmado: Joi.string().min(3).max(16).required()
    }
  },
  nuevaContrasena: {
    body: {
      id_usuario: Joi.number().required(),
      correo: Joi.string().email().required()
    }
  }
};
