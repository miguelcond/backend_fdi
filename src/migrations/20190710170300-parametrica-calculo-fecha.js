'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
    let parametrica = migration.sequelize.import('../models/parametrica/parametrica');
    return parametrica.findOne({
      where: {
        codigo: 'CFG_CALC_FECHA'
      }
    }).then((result) => {
      if (!result) {
        migration.bulkInsert('parametrica',
          [{
            codigo: 'CFG_CALC_FECHA',
            tipo: 'CONFIG',
            nombre: 'Cálculo de fecha (restar días / 0 o 1)',
            descripcion: '0',
            orden: 1,
            _usuario_creacion: 1,
            _fecha_creacion: new Date(),
            _fecha_modificacion: new Date()
          }]
        , {});
      } else {
        migration.bulkUpdate('parametrica',
        {
          tipo: 'CONFIG',
          nombre: 'Cálculo de fecha (restar días / 0 o 1)',
          descripcion: '0',
          orden: 1,
          _usuario_creacion: 1,
          _fecha_creacion: new Date(),
          _fecha_modificacion: new Date()
        },
        {
          codigo: 'CFG_CALC_FECHA'
        }, {
          returning: true, raw: true
        });
      }
    }).catch((e) => {
      console.error('E', e.message);
    }).finally(done);
  },

  down: (migration, DataTypes, done) => {
    const sql = (
      `DELETE FROM parametrica WHERE codigo = 'CFG_CALC_FECHA';`
    );

    return migration.sequelize.query(sql)
      .finally(done);
  }
};
