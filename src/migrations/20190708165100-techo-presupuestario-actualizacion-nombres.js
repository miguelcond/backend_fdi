'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
    const sql = (
      `
      UPDATE techo_presupuestario SET nombre_municipio = 'Villa Azurduy' WHERE codigo = '010201';
      UPDATE techo_presupuestario SET nombre_municipio = 'Tarvita (Villa Orias)' WHERE codigo = '010202';
      UPDATE techo_presupuestario SET nombre_municipio = 'Villa Zudañez (Tacopaya)' WHERE codigo = '010301';
      UPDATE techo_presupuestario SET nombre_municipio = 'Villa Mojocoya' WHERE codigo = '010303';
      UPDATE techo_presupuestario SET nombre_municipio = 'Icla' WHERE codigo = '010304';
      UPDATE techo_presupuestario SET nombre_municipio = 'Sopachuy' WHERE codigo = '010403';
      UPDATE techo_presupuestario SET nombre_municipio = 'Villa Alcalá' WHERE codigo = '010404';
      UPDATE techo_presupuestario SET nombre_municipio = 'San Pablo de Huacareta' WHERE codigo = '010502';
      UPDATE techo_presupuestario SET nombre_municipio = 'Camataqui (Villa Abecia)' WHERE codigo = '010901';
      UPDATE techo_presupuestario SET nombre_municipio = 'Villa de Huacaya' WHERE codigo = '011002';
      UPDATE techo_presupuestario SET nombre_municipio = 'La Paz' WHERE codigo = '020101';
      UPDATE techo_presupuestario SET nombre_municipio = 'El Alto de La Paz' WHERE codigo = '020105';
      UPDATE techo_presupuestario SET nombre_municipio = 'Guaqui' WHERE codigo = '020802';
      UPDATE techo_presupuestario SET nombre_municipio = 'Sica Sica (Villa Aroma)' WHERE codigo = '021301';
      UPDATE techo_presupuestario SET nombre_municipio = 'Ancoraimes' WHERE codigo = '020202';
      UPDATE techo_presupuestario SET nombre_municipio = 'Carabuco' WHERE codigo = '020403';
      UPDATE techo_presupuestario SET nombre_municipio = 'Chulumani (Villa de la Libertad)' WHERE codigo = '021101';
      UPDATE techo_presupuestario SET nombre_municipio = 'Irupana (Villa de Lanza)' WHERE codigo = '021102';
      UPDATE techo_presupuestario SET nombre_municipio = 'General Juan José  Pérez (Charazani)' WHERE codigo = '021601';
      UPDATE techo_presupuestario SET nombre_municipio = 'San Andrés de Machaca' WHERE codigo = '020805';
      UPDATE techo_presupuestario SET nombre_municipio = 'Jesús de Machaca' WHERE codigo = '020806';
      UPDATE techo_presupuestario SET nombre_municipio = 'Ayopaya (Villa de Independencia)' WHERE codigo = '030301';
      UPDATE techo_presupuestario SET nombre_municipio = 'San Benito (Villa José Quintín Mendoza)' WHERE codigo = '031403';
      UPDATE techo_presupuestario SET nombre_municipio = 'Poopó (Villa Poopó)' WHERE codigo = '040601';
      UPDATE techo_presupuestario SET nombre_municipio = 'Andamarca (Santiago de Andamarca)' WHERE codigo = '041202';
      UPDATE techo_presupuestario SET nombre_municipio = 'Huayllamarca (Santiago de Huayllamarca)' WHERE codigo = '041601';
      UPDATE techo_presupuestario SET nombre_municipio = 'San Pedro de Buena Vista' WHERE codigo = '050501';
      UPDATE techo_presupuestario SET nombre_municipio = 'Sacaca (Villa de Sacaca)' WHERE codigo = '050701';
      UPDATE techo_presupuestario SET nombre_municipio = 'Colcha "K" (Villa Martín)' WHERE codigo = '050901';
      UPDATE techo_presupuestario SET nombre_municipio = 'San Pablo de Lípez' WHERE codigo = '051001';
      UPDATE techo_presupuestario SET nombre_municipio = 'Puna (Villa Talavera)' WHERE codigo = '051101';
      UPDATE techo_presupuestario SET nombre_municipio = 'Caiza "D"' WHERE codigo = '051102';
      UPDATE techo_presupuestario SET nombre_municipio = 'Villamontes' WHERE codigo = '060303';
      UPDATE techo_presupuestario SET nombre_municipio = 'Uriondo (Concepción)' WHERE codigo = '060401';
      UPDATE techo_presupuestario SET nombre_municipio = 'Porongo (Ayacucho)' WHERE codigo = '070103';
      UPDATE techo_presupuestario SET nombre_municipio = 'San Ignacio (San Ignacio de Velasco)' WHERE codigo = '070301';
      UPDATE techo_presupuestario SET nombre_municipio = 'San Miguel (San Miguel de Velasco)' WHERE codigo = '070302';
      UPDATE techo_presupuestario SET nombre_municipio = 'Santa Rosa del Sara' WHERE codigo = '070602';
      UPDATE techo_presupuestario SET nombre_municipio = 'Moro Moro' WHERE codigo = '070803';
      UPDATE techo_presupuestario SET nombre_municipio = 'General Agustín Saavedra' WHERE codigo = '071002';
      UPDATE techo_presupuestario SET nombre_municipio = 'Concepción' WHERE codigo = '071101';
      UPDATE techo_presupuestario SET nombre_municipio = 'Puerto Guayaramerín' WHERE codigo = '080202';
      UPDATE techo_presupuestario SET nombre_municipio = 'Puerto Rurrenabaque' WHERE codigo = '080304';
      UPDATE techo_presupuestario SET nombre_municipio = 'Santa Rosa del Abuná' WHERE codigo = '090401';
      UPDATE techo_presupuestario SET nombre_municipio = 'Ingavi (Humaita)' WHERE codigo = '090402';
      UPDATE techo_presupuestario SET nombre_municipio = 'Villa Nueva (Loma Alta)' WHERE codigo = '090502';
      UPDATE techo_presupuestario SET nombre_municipio = 'Uru Chipaya' WHERE codigo = '040903';
      UPDATE techo_presupuestario SET nombre_municipio = 'Charagua Iyambae' WHERE codigo = '070702';
      `
    );

    return migration.sequelize.query(sql)
      .catch((e) => {
        console.log('E', e.message);
      })
      .finally(done);
  },

  down: (migration, DataTypes, done) => {
    const sql = (
      `
      UPDATE techo_presupuestario SET nombre_municipio = 'Azurduy' WHERE codigo = '010201';
      UPDATE techo_presupuestario SET nombre_municipio = 'Tarvita' WHERE codigo = '010202';
      UPDATE techo_presupuestario SET nombre_municipio = 'Zudáñez' WHERE codigo = '010301';
      UPDATE techo_presupuestario SET nombre_municipio = 'Mojocoya' WHERE codigo = '010303';
      UPDATE techo_presupuestario SET nombre_municipio = 'Villa Ricardo Mugia - Icla' WHERE codigo = '010304';
      UPDATE techo_presupuestario SET nombre_municipio = 'Sopachui' WHERE codigo = '010403';
      UPDATE techo_presupuestario SET nombre_municipio = 'Alcalá' WHERE codigo = '010404';
      UPDATE techo_presupuestario SET nombre_municipio = 'Huacareta' WHERE codigo = '010502';
      UPDATE techo_presupuestario SET nombre_municipio = 'Villa Abecia' WHERE codigo = '010901';
      UPDATE techo_presupuestario SET nombre_municipio = 'Huacaya' WHERE codigo = '011002';
      UPDATE techo_presupuestario SET nombre_municipio = 'Nuestra Señora de La Paz' WHERE codigo = '020101';
      UPDATE techo_presupuestario SET nombre_municipio = 'El Alto' WHERE codigo = '020105';
      UPDATE techo_presupuestario SET nombre_municipio = 'Puerto Mayor de Guaqui' WHERE codigo = '020802';
      UPDATE techo_presupuestario SET nombre_municipio = 'Sica Sica' WHERE codigo = '021301';
      UPDATE techo_presupuestario SET nombre_municipio = 'Villa Ancoraimes' WHERE codigo = '020202';
      UPDATE techo_presupuestario SET nombre_municipio = 'Puerto Mayor de Carabuco' WHERE codigo = '020403';
      UPDATE techo_presupuestario SET nombre_municipio = 'Chulumani' WHERE codigo = '021101';
      UPDATE techo_presupuestario SET nombre_municipio = 'Irupana' WHERE codigo = '021102';
      UPDATE techo_presupuestario SET nombre_municipio = 'Charazani' WHERE codigo = '021601';
      UPDATE techo_presupuestario SET nombre_municipio = '(La Marka) San Andrés de Machaca' WHERE codigo = '020805';
      UPDATE techo_presupuestario SET nombre_municipio = 'Ayopaya' WHERE codigo = '030301';
      UPDATE techo_presupuestario SET nombre_municipio = 'San Benito' WHERE codigo = '031403';
      UPDATE techo_presupuestario SET nombre_municipio = 'Poopó' WHERE codigo = '040601';
      UPDATE techo_presupuestario SET nombre_municipio = 'Belén de Andamarca' WHERE codigo = '041202';
      UPDATE techo_presupuestario SET nombre_municipio = 'Huayllamarca' WHERE codigo = '041601';
      UPDATE techo_presupuestario SET nombre_municipio = 'San Pedro' WHERE codigo = '050501';
      UPDATE techo_presupuestario SET nombre_municipio = 'Sacaca' WHERE codigo = '050701';
      UPDATE techo_presupuestario SET nombre_municipio = 'Colcha K' WHERE codigo = '050901';
      UPDATE techo_presupuestario SET nombre_municipio = 'San Pablo' WHERE codigo = '051001';
      UPDATE techo_presupuestario SET nombre_municipio = 'Puna' WHERE codigo = '051101';
      UPDATE techo_presupuestario SET nombre_municipio = 'Caiza D' WHERE codigo = '051102';
      UPDATE techo_presupuestario SET nombre_municipio = 'Uriondo' WHERE codigo = '060401';
      UPDATE techo_presupuestario SET nombre_municipio = 'Porongo' WHERE codigo = '070103';
      UPDATE techo_presupuestario SET nombre_municipio = 'San Ignacio' WHERE codigo = '070301';
      UPDATE techo_presupuestario SET nombre_municipio = 'San Miguel' WHERE codigo = '070302';
      UPDATE techo_presupuestario SET nombre_municipio = 'Santa Rosa' WHERE codigo = '070602';
      UPDATE techo_presupuestario SET nombre_municipio = 'Moromoro' WHERE codigo = '070803';
      UPDATE techo_presupuestario SET nombre_municipio = 'General Saavedra' WHERE codigo = '071002';
      UPDATE techo_presupuestario SET nombre_municipio = 'Concepción' WHERE codigo = '071101';
      UPDATE techo_presupuestario SET nombre_municipio = 'Guayaramerín' WHERE codigo = '080202';
      UPDATE techo_presupuestario SET nombre_municipio = 'Puerto Menor de Rurrenabaque' WHERE codigo = '080304';
      UPDATE techo_presupuestario SET nombre_municipio = 'Santa Rosa' WHERE codigo = '090401';
      UPDATE techo_presupuestario SET nombre_municipio = 'Ingavi' WHERE codigo = '090402';
      UPDATE techo_presupuestario SET nombre_municipio = 'Villa Nueva' WHERE codigo = '090502';
      UPDATE techo_presupuestario SET nombre_municipio = 'Chipaya' WHERE codigo = '040903';
      UPDATE techo_presupuestario SET nombre_municipio = 'Charagua' WHERE codigo = '070702';
      `
    );

    return migration.sequelize.query(sql)
      .finally(done);
  }
};
