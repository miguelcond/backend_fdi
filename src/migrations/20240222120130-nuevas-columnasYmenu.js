'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {

    const sql = (
       `-- adicionar columnas para  4to y 5to doc convenio extendido
        ALTER TABLE public.proyecto ADD doc_convenio_extendido4 varchar NULL;

        ALTER TABLE public.proyecto ADD doc_convenio_extendido5 varchar NULL;

        -- Adicionar nuevo menu para rol legal
        INSERT INTO menu (nombre, descripcion, orden, ruta, icono, method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_menu_padre)
        VALUES('LISTA-CONVENIOS', 'Lista proyectos convenios', 8, 'lista-proyectos-convenios', 'list', NULL, NULL, NULL, NULL, 'ACTIVO', 1, NULL, now(), now(), 12);

        -- Agregar el menu al rol tecnico_legal
        INSERT INTO rol_menu(method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_menu, fid_rol)
        VALUES(false, false, false, false, 'ACTIVO', 1, NULL, now(), now(), (select id_menu from menu where nombre ='LISTA-CONVENIOS'), 14);

        -- permitir acceso a las rutas departamentos,provincia, municipios y reportes para el rol de legal
        INSERT INTO rol_ruta (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta) VALUES(true, false, false, false, 'ACTIVO', 1, NULL, now(), now(), 14, 14);
        INSERT INTO rol_ruta (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta) VALUES(true, false, false, false, 'ACTIVO', 1, NULL, now(), now(), 14, 15);
        INSERT INTO rol_ruta (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta) VALUES(true, false, false, false, 'ACTIVO', 1, NULL, now(), now(), 14, 16);
        INSERT INTO rol_ruta (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta) VALUES(true, true, false, false, 'ACTIVO', 1, NULL, now(), now(), 14, 25);
        
        `
    );

    return migration.sequelize.query(sql)
      .finally(done);
  },

  down: (migration, DataTypes, done) => {
    return migration.sequelize.query('')
      .finally(done);
  },
};