'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {

    const sql = (
       `INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, false, false, false, 'ACTIVO', 1, NULL, now(), now(), 12, 10);

        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, false, false, false, 'ACTIVO', 1, NULL, now(), now(), 20, 21);

        INSERT INTO public.rol_ruta
        (method_get, method_post, method_put, method_delete, estado, "_usuario_creacion", "_usuario_modificacion", "_fecha_creacion", "_fecha_modificacion", fid_rol, fid_ruta)
        VALUES(true, false, false, false, 'ACTIVO', 1, NULL, now(), now(), 20, 10);

        `
    );

    return migration.sequelize.query(sql)
      .finally(done);
  },

  down: (migration, DataTypes, done) => {
    return migration.sequelize.query('')
      .finally(done);
  },
};