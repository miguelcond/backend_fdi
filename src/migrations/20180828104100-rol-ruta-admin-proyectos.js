'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
    let rol_ruta = migration.sequelize.import('../models/autenticacion/rol_ruta');
    return rol_ruta.findOne({
      where: {
        fid_rol: 12,
        fid_ruta: 24
      }
    }).then((result)=>{
      console.log('result', result&&result.dataValues);
      if(!result) {
        return migration.bulkInsert('rol_ruta',
          [{ fid_ruta: 24, fid_rol: 12, method_get: true, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() }]
        , {});
      }else{
        return migration.bulkUpdate('rol_ruta', { fid_ruta: 24, fid_rol: 12, method_get: true, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
        {
          fid_rol: 12,
          fid_ruta: 24
        },{
          returning: true, raw: true
        });
      }
    }).catch((e)=>{
      console.error('E',e.message);
    }).finally(done);

  },

  down: (migration, DataTypes, done) => {

    const sql = (
      `DELETE FROM rol_ruta WHERE fid_rol = 12 AND fid_ruta = 24;`
    );
    return migration.sequelize.query(sql)
      .finally(done);
  }
};
