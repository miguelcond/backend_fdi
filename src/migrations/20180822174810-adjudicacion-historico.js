'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {

    return migration.bulkUpdate('estado', {
      atributos: '{"17":["monto_total","monto_contraparte","observacion","estado_proyecto"]}'
    },{
      codigo: 'REVISION_MODIFICADO_FDI'
    },{
      returning: true, raw: true
    });

    const sql = (
        `ALTER TABLE adjudicacion_historico DROP CONSTRAINT IF EXISTS adjudicacion_historico_pkey;
         ALTER TABLE adjudicacion_historico ADD PRIMARY KEY (id_adjudicacion,version);`
    );

    migration.sequelize.query(sql)
      .finally(done);
  },

  down: (migration, DataTypes, done) => {

    const sql = (
      ``
    );
    return migration.sequelize.query(sql)
      .finally(done);
  }
};
