// 'use strict';

module.exports = {
  up(migration, DataTypes, done) {
    console.time('estado');
    return migration.addColumn('persona', 'tipo_persona',{
      type: DataTypes.STRING(8),
      xlabel: 'Persona nacional, extranjero',
      allowNull: true
    },{
      returning: true, raw: true
    }).catch((e)=>{
      console.log(e.message);
    }).finally(()=>{
      console.timeEnd('estado');
      return done;
    });
  },

  down(migration,DataTypes, done) {
    return migration.sequelize.query(``).finally(done);
  }
};
