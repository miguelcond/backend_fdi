'use strict';

module.exports = {
  up: (migration, DataType, done) => {
    migration.addColumn('proyecto_historico', 'doc_convenio_extendido', {
      type: DataType.STRING,
      xlabel: 'Documento convenio extendido',
      allowNull: true
    }, {
      returning: true, raw: true
    }).catch((e) => {
      console.error('E: addColumn', e.message);
    });
    migration.addColumn('proyecto_historico', 'doc_convenio_extendido2', {
      type: DataType.STRING,
      xlabel: '2do Documento convenio extendido',
      allowNull: true
    }, {
      returning: true, raw: true
    }).catch((e) => {
      console.error('E: addColumn', e.message);
    });
    return migration.sequelize.query('');
  },

  down: (migration, DataTypes, done) => {
    const sql = (
      `ALTER TABLE proyecto_historico DROP COLUMN IF EXISTS doc_convenio_extendido;`
    );
    return migration.sequelize.query(sql)
      .finally(done);
  }
};
