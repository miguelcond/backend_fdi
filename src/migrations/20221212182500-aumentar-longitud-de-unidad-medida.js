'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {

    const sql = (
       `ALTER TABLE public.item_historico ALTER COLUMN unidad_medida TYPE varchar(50) USING unidad_medida::varchar;
       ALTER TABLE public.item ALTER COLUMN unidad_medida TYPE varchar(50) USING unidad_medida::varchar;`
    );

    return migration.sequelize.query(sql)
      .finally(done);
  },

  down: (migration, DataTypes, done) => {
    return migration.sequelize.query('')
      .finally(done);
  },
};