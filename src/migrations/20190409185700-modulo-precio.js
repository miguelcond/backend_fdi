'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
    console.time('modulo');
    return migration.addColumn('modulo', 'precio', {
      type: DataTypes.FLOAT,
      xlabel: 'Precio de items del módulo',
      allowNull: false,
      defaultValue: 0
    }, {
      returning: true, raw: true
    }).then((resp) => {
      return migration.addColumn('modulo_historico', 'precio', {
        type: DataTypes.FLOAT,
        xlabel: 'Precio de items del módulo',
        allowNull: false,
        defaultValue: 0
      }, {
        returning: true, raw: true
      });
    }).then((resp) => {
      console.timeEnd('modulo');
    }).catch((e) => {
      console.log(e.message);
    }).finally(done);
  },

  down: (migration, DataTypes, done) => {
    const sql = (
      `ALTER TABLE modulo DROP COLUMN IF EXISTS precio;
      ALTER TABLE modulo_historico DROP COLUMN IF EXISTS precio;`
    );
    return migration.sequelize.query(sql)
      .finally(done);
  }
};
