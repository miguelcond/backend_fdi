'use strict';


module.exports = {
  up: (migration, DataTypes, done) => {

    let estado = migration.sequelize.import('../models/flujo/estado');

    console.time('timestado');
    return migration.bulkInsert('estado',[{
      codigo: 'NO_OBJECION_FDI',
      codigo_proceso: 'ADJ-FDI',
      nombre: 'En revisión por el técnico responsable',
      tipo: 'FLUJO',
      fid_rol: '{13}',
      fid_rol_asignado: '{13}',
      acciones: `[{"ruta": "/temporal", "tipo": "ventana"}, {"icon": "fa-rotate-left", "tipo": "boton", "class": "btn-warning", "label": "Observar", "estado": "MODIFICADO_FDI", "observacion": true}, {"icon": "fa-check", "tipo": "boton", "class": "btn-success", "label": "Aprobar", "estado": "ADJUDICADO_FDI", "ejecutar": ["$cerrarItems"]}]`,
      atributos: `{"13": ["observacion", "estado_proyecto"]}`,
      requeridos: `[]`,
      automaticos: `{}`,
      areas: `{"8":["PROYECTO_ADJUDICADO","FORMULARIOS","GARANTIAS"],"12":["DATOS_GENERALES","CONVENIO","DESEMBOLSO","AUTORIDAD_BENEFICIARIA","MODULOS_ITEMS","DOCUMENTO_BASE_CONTRATACION","FORMULARIOS","GARANTIAS","EQUIPO_TECNICO","ORDEN_PROCEDER"],"13":["DATOS_GENERALES","CONVENIO","DESEMBOLSO","AUTORIDAD_BENEFICIARIA","MODULOS_ITEMS","DOCUMENTO_BASE_CONTRATACION","FORMULARIOS","GARANTIAS","EQUIPO_TECNICO","ORDEN_PROCEDER"],"14":["DATOS_GENERALES","CONVENIO"],"15":["DATOS_GENERALES","CONVENIO","DESEMBOLSO","GARANTIAS"],"16":["DATOS_GENERALES","ORDEN_PROCEDER"],"17":["DATOS_GENERALES","ORDEN_PROCEDER"],"18":["DATOS_GENERALES","CONVENIO","DESEMBOLSO","AUTORIDAD_BENEFICIARIA","MODULOS_ITEMS","DOCUMENTO_BASE_CONTRATACION","FORMULARIOS","GARANTIAS","EQUIPO_TECNICO","ORDEN_PROCEDER"]}`,
      atributos_detalle: null,
      estado: `ACTIVO`,
      _usuario_creacion: 1,
      _fecha_creacion: new Date(),
      _fecha_modificacion: new Date()
    }],{
      returning: true, raw: true
    }).then((rows)=>{
      console.log('Insertados -->',rows.length);
      return migration.bulkUpdate('estado', {
        acciones: '[{"ruta": "/temporal", "tipo": "ventana"}, {"icon": "fa-rotate-left", "tipo": "boton", "class": "btn-warning", "label": "Observar", "estado": "MODIFICADO_FDI", "observacion": true}, {"icon": "fa-check", "tipo": "boton", "class": "btn-success", "label": "Aprobar modificación", "estado": "NO_OBJECION_FDI"}]',
        atributos: `{"17": ["fecha_modificacion", "monto_total", "monto_contraparte", "observacion", "estado_proyecto"]}`,
      },{
        codigo_proceso: 'ADJ-FDI',
        codigo: 'REVISION_MODIFICADO_FDI'
      },{
        returning: true, raw: true
      });
    }).then((rows)=>{
      console.log('Actualizado -->',rows.length);
    }).catch((e)=>{
      console.error('ERROR', e&&e.message);
    }).finally(()=>{
      console.timeEnd('timestado');
    });

  },

  down: (migration, DataTypes, done) => {

    let estado = migration.sequelize.import('../models/flujo/estado');

    console.time('estado');
    return migration.bulkUpdate('estado', {
      acciones: '[{"ruta": "/temporal", "tipo": "ventana"}, {"icon": "fa-rotate-left", "tipo": "boton", "class": "btn-warning", "label": "Observar", "estado": "EN_SUPERVISION_FDI", "observacion": true}, {"icon": "fa-check", "tipo": "boton", "class": "btn-success", "label": "Aprobar planilla", "estado": "EN_ARCHIVO_FDI", "ejecutar": ["$registrarCoordenadas", "$cerrarItemsFinalizados", "$generarReportePlanilla"]}]',
      atributos: '{"17": ["estado_supervision", "observacion"]}',
      nombre: 'Planilla enviada al/a la fiscal'
    },{
      codigo_proceso: 'SUPERV-FDI',
      codigo: 'EN_FISCAL_FDI'
    },{
      returning: true, raw: true
    }).then((rows)=>{
      console.timeEnd('estado');
    }).finally(done);

  },
};
