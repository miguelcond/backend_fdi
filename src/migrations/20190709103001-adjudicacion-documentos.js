'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {

    const sql = (
      `ALTER TABLE adjudicacion ADD COLUMN documentos JSONB;
      ALTER TABLE adjudicacion_historico ADD COLUMN documentos JSONB;`
    );

    return migration.sequelize.query(sql).catch((e)=>{
      console.error('E', e.message);
    }).finally(done);
  },

  down: (migration, DataTypes, done) => {

    const sql = (
      `ALTER TABLE adjudicacion DROP COLUMN IF EXISTS documentos;
      ALTER TABLE adjudicacion_historico DROP COLUMN IF EXISTS documentos;`
    );
    return migration.sequelize.query(sql)
      .finally(done);
  }
};
