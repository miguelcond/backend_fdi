'use strict';


module.exports = {
  up: (migration, DataTypes, done) => {

    let estado = migration.sequelize.import('../models/flujo/estado');

    console.time('estado');
    return migration.bulkUpdate('estado', {
        acciones: '[{"ruta": "/temporal", "tipo": "ventana"}, {"icon": "fa-edit", "tipo": "boton", "class": "btn-info", "label": "Editar items", "estado": "REGISTRO_PLANILLA_FDI", "retroceder": true}, {"icon": "fa-check", "tipo": "boton", "class": "btn-success", "label": "Enviar planilla", "estado": "PENDIENTE_FDI", "ejecutar": ["$verificarFotosSupervision"]}]',
    },{
      codigo_proceso: 'SUPERV-FDI',
      codigo: 'EN_SUPERVISION_FDI'
    },{
      returning: true, raw: true
    }).then((rows)=>{
      console.timeEnd('estado');
    }).finally(done);

  },

  down: (migration, DataTypes, done) => {

    let estado = migration.sequelize.import('../models/flujo/estado');

    console.time('estado');
    return migration.bulkUpdate('estado', {
      acciones: '[{"ruta": "/temporal", "tipo": "ventana"}, {"icon": "fa-edit", "tipo": "boton", "class": "btn-info", "label": "Editar items", "estado": "REGISTRO_PLANILLA_FDI", "retroceder": true}, {"icon": "fa-check", "tipo": "boton", "class": "btn-success", "label": "Enviar planilla", "estado": "EN_FISCAL_FDI", "ejecutar": ["$verificarFotosSupervision"]}]'
    },{
      codigo_proceso: 'SUPERV-FDI',
      codigo: 'EN_SUPERVISION_FDI'
    },{
      returning: true, raw: true
    }).then((rows)=>{
      console.timeEnd('estado');
    }).finally(done);

  },
};
