'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
    console.time('municipio');
    return migration.addColumn('municipio', 'codigo_gam', {
      type: DataTypes.STRING(4),
      xlabel: 'Código GAM GAIOC',
      allowNull: true
    }, {
      returning: true, raw: true
    }).then((resp) => {
      console.timeEnd('municipio');
    }).catch((e) => {
      console.log(e.message);
    }).finally(done);
  },

  down: (migration, DataTypes, done) => {
    const sql = (
      `ALTER TABLE municipio DROP COLUMN IF EXISTS codigo_gam;`
    );
    return migration.sequelize.query(sql)
      .finally(done);
  }
};
