'use strict';


module.exports = {
  up: (migration, DataTypes, done) => {
    let estado = migration.sequelize.import('../models/flujo/estado');
    return migration.bulkUpdate('estado', {
        atributos_detalle: '{"item": {"put": ["fid_modulo", "nombre", "unidad_medida", "cantidad", "precio_unitario", "peso_ponderado", "especificaciones", "analisis_precios"], "post": ["fid_modulo", "nombre", "unidad_medida", "cantidad", "precio_unitario", "peso_ponderado", "especificaciones", "analisis_precios"], "acciones": ["post", "put"]}, "modulo": {"post": ["fid_proyecto", "nombre"], "acciones": ["post"]}}',
    }, {
      codigo_proceso: 'ADJ-FDI',
      codigo: 'MODIFICADO_FDI'
    }, {
      returning: true, raw: true
    }).then((rows) => {

    }).finally(done);
  },

  down: (migration, DataTypes, done) => {
    let estado = migration.sequelize.import('../models/flujo/estado');
    return migration.bulkUpdate('estado', {
        atributos_detalle: '{"item": {"put": ["fid_modulo", "nombre", "unidad_medida", "cantidad", "precio_unitario", "especificaciones", "analisis_precios"], "post": ["fid_modulo", "nombre", "unidad_medida", "cantidad", "precio_unitario", "especificaciones", "analisis_precios"], "acciones": ["post", "put"]}, "modulo": {"post": ["fid_proyecto", "nombre"], "acciones": ["post"]}}',
    }, {
      codigo_proceso: 'ADJ-FDI',
      codigo: 'MODIFICADO_FDI'
    }, {
      returning: true, raw: true
    }).then((rows) => {

    }).finally(done);
  },
};
