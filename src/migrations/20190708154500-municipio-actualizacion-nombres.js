'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
    const sql = (
      `
      UPDATE municipio SET nombre = 'Villa Azurduy' WHERE codigo_gam = '1104';
      UPDATE municipio SET nombre = 'Tarvita (Villa Orias)' WHERE codigo_gam = '1105';
      UPDATE municipio SET nombre = 'Villa Zudañez (Tacopaya)' WHERE codigo_gam = '1106';
      UPDATE municipio SET nombre = 'Villa Mojocoya' WHERE codigo_gam = '1108';
      UPDATE municipio SET nombre = 'Icla' WHERE codigo_gam = '1109';
      UPDATE municipio SET nombre = 'Sopachuy' WHERE codigo_gam = '1112';
      UPDATE municipio SET nombre = 'Villa Alcalá' WHERE codigo_gam = '1113';
      UPDATE municipio SET nombre = 'San Pablo de Huacareta' WHERE codigo_gam = '1116';
      UPDATE municipio SET nombre = 'Camataqui (Villa Abecia)' WHERE codigo_gam = '1123';
      UPDATE municipio SET nombre = 'Villa de Huacaya' WHERE codigo_gam = '1127';
      UPDATE municipio SET nombre = 'La Paz' WHERE codigo_gam = '1201';
      UPDATE municipio SET nombre = 'El Alto de La Paz' WHERE codigo_gam = '1205';
      UPDATE municipio SET nombre = 'Guaqui' WHERE codigo_gam = '1207';
      UPDATE municipio SET nombre = 'Sica Sica (Villa Aroma)' WHERE codigo_gam = '1211';
      UPDATE municipio SET nombre = 'Ancoraimes' WHERE codigo_gam = '1225';
      UPDATE municipio SET nombre = 'Carabuco' WHERE codigo_gam = '1248';
      UPDATE municipio SET nombre = 'Chulumani (Villa de la Libertad)' WHERE codigo_gam = '1256';
      UPDATE municipio SET nombre = 'Irupana (Villa de Lanza)' WHERE codigo_gam = '1257';
      UPDATE municipio SET nombre = 'General Juan José  Pérez (Charazani)' WHERE codigo_gam = '1269';
      UPDATE municipio SET nombre = 'San Andrés de Machaca' WHERE codigo_gam = '1278';
      UPDATE municipio SET nombre = 'Jesús de Machaca' WHERE codigo_gam = '1279';
      UPDATE municipio SET nombre = 'Ayopaya (Villa de Independencia)' WHERE codigo_gam = '1310';
      UPDATE municipio SET nombre = 'San Benito (Villa José Quintín Mendoza)' WHERE codigo_gam = '1317';
      UPDATE municipio SET nombre = 'Poopó (Villa Poopó)' WHERE codigo_gam = '1408';
      UPDATE municipio SET nombre = 'Andamarca (Santiago de Andamarca)' WHERE codigo_gam = '1425';
      UPDATE municipio SET nombre = 'Huayllamarca (Santiago de Huayllamarca)' WHERE codigo_gam = '1434';
      UPDATE municipio SET nombre = 'San Pedro de Buena Vista' WHERE codigo_gam = '1515';
      UPDATE municipio SET nombre = 'Sacaca (Villa de Sacaca)' WHERE codigo_gam = '1526';
      UPDATE municipio SET nombre = 'Colcha "K" (Villa Martín)' WHERE codigo_gam = '1521';
      UPDATE municipio SET nombre = 'San Pablo de Lípez' WHERE codigo_gam = '1523';
      UPDATE municipio SET nombre = 'Puna (Villa Talavera)' WHERE codigo_gam = '1528';
      UPDATE municipio SET nombre = 'Caiza "D"' WHERE codigo_gam = '1529';
      UPDATE municipio SET nombre = 'Villamontes' WHERE codigo_gam = '1606';
      UPDATE municipio SET nombre = 'Uriondo (Concepción)' WHERE codigo_gam = '1607';
      UPDATE municipio SET nombre = 'Porongo (Ayacucho)' WHERE codigo_gam = '1703';
      UPDATE municipio SET nombre = 'San Ignacio (San Ignacio de Velasco)' WHERE codigo_gam = '1707';
      UPDATE municipio SET nombre = 'San Miguel (San Miguel de Velasco)' WHERE codigo_gam = '1708';
      UPDATE municipio SET nombre = 'Santa Rosa del Sara' WHERE codigo_gam = '1717';
      UPDATE municipio SET nombre = 'Moro Moro' WHERE codigo_gam = '1727';
      UPDATE municipio SET nombre = 'General Agustín Saavedra' WHERE codigo_gam = '1735';
      UPDATE municipio SET nombre = 'Concepción' WHERE codigo_gam = '1737';
      UPDATE municipio SET nombre = 'Puerto Guayaramerín' WHERE codigo_gam = '1805';
      UPDATE municipio SET nombre = 'Puerto Rurrenabaque' WHERE codigo_gam = '1807';
      UPDATE municipio SET nombre = 'Santa Rosa del Abuná' WHERE codigo_gam = '1911';
      UPDATE municipio SET nombre = 'Ingavi (Humaita)' WHERE codigo_gam = '1912';
      UPDATE municipio SET nombre = 'Villa Nueva (Loma Alta)' WHERE codigo_gam = '1914';
      UPDATE municipio SET nombre = 'Uru Chipaya' WHERE codigo_gam = '3401';
      UPDATE municipio SET nombre = 'Charagua Iyambae' WHERE codigo_gam = '3701';
      `
    );

    return migration.sequelize.query(sql)
      .catch((e) => {
        console.log('E', e.message);
      })
      .finally(done);
  },

  down: (migration, DataTypes, done) => {
    const sql = (
      `
      UPDATE municipio SET nombre = 'Azurduy' WHERE codigo_gam = '1104';
      UPDATE municipio SET nombre = 'Tarvita' WHERE codigo_gam = '1105';
      UPDATE municipio SET nombre = 'Zudáñez' WHERE codigo_gam = '1106';
      UPDATE municipio SET nombre = 'Mojocoya' WHERE codigo_gam = '1108';
      UPDATE municipio SET nombre = 'Villa Ricardo Mugia - Icla' WHERE codigo_gam = '1109';
      UPDATE municipio SET nombre = 'Sopachui' WHERE codigo_gam = '1112';
      UPDATE municipio SET nombre = 'Alcalá' WHERE codigo_gam = '1113';
      UPDATE municipio SET nombre = 'Huacareta' WHERE codigo_gam = '1116';
      UPDATE municipio SET nombre = 'Villa Abecia' WHERE codigo_gam = '1123';
      UPDATE municipio SET nombre = 'Huacaya' WHERE codigo_gam = '1127';
      UPDATE municipio SET nombre = 'Nuestra Señora de La Paz' WHERE codigo_gam = '1201';
      UPDATE municipio SET nombre = 'El Alto' WHERE codigo_gam = '1205';
      UPDATE municipio SET nombre = 'Puerto Mayor de Guaqui' WHERE codigo_gam = '1207';
      UPDATE municipio SET nombre = 'Sica Sica' WHERE codigo_gam = '1211';
      UPDATE municipio SET nombre = 'Villa Ancoraimes' WHERE codigo_gam = '1225';
      UPDATE municipio SET nombre = 'Puerto Mayor de Carabuco' WHERE codigo_gam = '1248';
      UPDATE municipio SET nombre = 'Chulumani' WHERE codigo_gam = '1256';
      UPDATE municipio SET nombre = 'Irupana' WHERE codigo_gam = '1257';
      UPDATE municipio SET nombre = 'Charazani' WHERE codigo_gam = '1269';
      UPDATE municipio SET nombre = '(La Marka) San Andrés de Machaca' WHERE codigo_gam = '1278';
      UPDATE municipio SET nombre = 'Ayopaya' WHERE codigo_gam = '1310';
      UPDATE municipio SET nombre = 'San Benito' WHERE codigo_gam = '1317';
      UPDATE municipio SET nombre = 'Poopó' WHERE codigo_gam = '1408';
      UPDATE municipio SET nombre = 'Belén de Andamarca' WHERE codigo_gam = '1425';
      UPDATE municipio SET nombre = 'Huayllamarca' WHERE codigo_gam = '1434';
      UPDATE municipio SET nombre = 'San Pedro' WHERE codigo_gam = '1515';
      UPDATE municipio SET nombre = 'Sacaca' WHERE codigo_gam = '1526';
      UPDATE municipio SET nombre = 'Colcha K' WHERE codigo_gam = '1521';
      UPDATE municipio SET nombre = 'San Pablo' WHERE codigo_gam = '1523';
      UPDATE municipio SET nombre = 'Puna' WHERE codigo_gam = '1528';
      UPDATE municipio SET nombre = 'Caiza D' WHERE codigo_gam = '1529';
      UPDATE municipio SET nombre = 'Uriondo' WHERE codigo_gam = '1607';
      UPDATE municipio SET nombre = 'Porongo' WHERE codigo_gam = '1703';
      UPDATE municipio SET nombre = 'San Ignacio' WHERE codigo_gam = '1707';
      UPDATE municipio SET nombre = 'San Miguel' WHERE codigo_gam = '1708';
      UPDATE municipio SET nombre = 'Santa Rosa' WHERE codigo_gam = '1717';
      UPDATE municipio SET nombre = 'Moromoro' WHERE codigo_gam = '1727';
      UPDATE municipio SET nombre = 'General Saavedra' WHERE codigo_gam = '1735';
      UPDATE municipio SET nombre = 'Concepción' WHERE codigo_gam = '1737';
      UPDATE municipio SET nombre = 'Guayaramerín' WHERE codigo_gam = '1805';
      UPDATE municipio SET nombre = 'Puerto Menor de Rurrenabaque' WHERE codigo_gam = '1807';
      UPDATE municipio SET nombre = 'Santa Rosa' WHERE codigo_gam = '1911';
      UPDATE municipio SET nombre = 'Ingavi' WHERE codigo_gam = '1912';
      UPDATE municipio SET nombre = 'Villa Nueva' WHERE codigo_gam = '1914';
      UPDATE municipio SET nombre = 'Chipaya' WHERE codigo_gam = '3401';
      UPDATE municipio SET nombre = 'Charagua' WHERE codigo_gam = '3701';
      `
    );

    return migration.sequelize.query(sql)
      .finally(done);
  }
};
