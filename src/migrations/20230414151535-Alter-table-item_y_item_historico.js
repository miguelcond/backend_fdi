'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {

    const sql = (
       `ALTER TABLE public.item ADD regularizar bool NULL DEFAULT false;

        ALTER TABLE public.item ADD variacion numeric NULL DEFAULT 0;

        ALTER TABLE public.item_historico ADD regularizar bool NULL DEFAULT false;

        ALTER TABLE public.item_historico ADD variacion numeric NULL DEFAULT 0;`
    );

    return migration.sequelize.query(sql)
      .finally(done);
  },

  down: (migration, DataTypes, done) => {
    return migration.sequelize.query('')
      .finally(done);
  },
};