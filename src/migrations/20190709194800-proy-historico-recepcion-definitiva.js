'use strict';

module.exports = {
  up: (migration, DataType, done) => {
    return migration.addColumn('proyecto_historico', 'plazo_recepcion_definitiva', {
      type: DataType.INTEGER,
      xlabel: 'Plazo en días de recepción definitiva de proyecto',
      allowNull: false,
      defaultValue: 90
    }, {
      returning: true, raw: true
    }).catch((e) => {
      console.error('E: addColumn', e.message);
    }).finally((resp) => {
      return migration.bulkUpdate('proyecto_historico', { plazo_recepcion_definitiva: 90 },
      {}, {
        returning: true, raw: true
      });
    }).catch((e) => {
      console.log('E', e.message);
    }).finally(done);
  },

  down: (migration, DataTypes, done) => {
    const sql = (
      `ALTER TABLE proyecto_historico DROP COLUMN IF EXISTS plazo_recepcion_definitiva;`
    );
    return migration.sequelize.query(sql)
      .finally(done);
  }
};
