'use strict';

module.exports = {
  up: (migration, DataType, done) => {
    return migration.addColumn('proyecto', 'req_extension_convenio', {
      type: DataType.BOOLEAN,
      xlabel: 'Requiere extension de convenio',
      allowNull: false,
      defaultValue: false
    }, {
      returning: true, raw: true
    }).catch((e) => {
      console.error('E: addColumn', e.message);
    }).finally((resp) => {
      return migration.bulkUpdate('proyecto', { req_extension_convenio: false },
      {}, {
        returning: true, raw: true
      });
    }).catch((e) => {
      console.log('E', e.message);
    }).then(() => {
      return migration.addColumn('proyecto_historico', 'req_extension_convenio', {
        type: DataType.BOOLEAN,
        xlabel: 'Requiere extension de convenio',
        allowNull: false,
        defaultValue: false
      }, {
        returning: true, raw: true
      });
    }).catch((e) => {
      console.log('E', e.message);
    }).finally(() => {
      console.log('End');
    });
  },

  down: (migration, DataTypes, done) => {
    const sql = (
      `ALTER TABLE proyecto DROP COLUMN IF EXISTS req_extension_convenio;
      ALTER TABLE proyecto_historico DROP COLUMN IF EXISTS res_extension_convenio;`
    );

    return migration.sequelize.query(sql)
      .finally(done);
  }
};
