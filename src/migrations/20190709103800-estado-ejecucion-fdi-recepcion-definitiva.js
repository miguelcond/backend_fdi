'use strict';


module.exports = {
  up: (migration, DataTypes, done) => {
    let estado = migration.sequelize.import('../models/flujo/estado');
    return migration.bulkUpdate('estado', {
      acciones: '{"13": [{"ruta": "/temporal", "tipo": "ventana"}, {"icon": "fa-send", "tipo": "boton", "class": "btn-success", "label": "Enviar", "estado": "MODIFICADO_FDI", "mensaje": ""}], "15": [{"icon": "fa-save", "tipo": "boton", "class": "btn-primary", "label": "Guardar", "estado": "EN_EJECUCION_FDI", "guardar": true, "mensaje": "Guardar datos del proyecto"}], "16": [{"ruta": "/temporal", "tipo": "ventana"}, {"icon": "fa-save", "tipo": "boton", "class": "btn-primary", "label": "Guardar", "estado": "EN_EJECUCION_FDI", "guardar": true, "mensaje": "Guardar datos del proyecto"}, {"icon": "fa-check", "tipo": "boton", "class": "btn-success", "label": "Proceder", "estado": "EN_EJECUCION_FDI", "mensaje": "Registrar orden de proceder", "ejecutar": ["$validarOrdenProceder"]}], "17": [{"ruta": "/temporal", "tipo": "ventana"}, {"icon": "fa-save", "tipo": "boton", "class": "btn-primary", "label": "Guardar", "estado": "EN_EJECUCION_FDI", "guardar": true, "mensaje": "Guardar datos del proyecto"}], "18": [{"ruta": "/temporal", "tipo": "ventana"}, {"icon": "fa-send", "tipo": "boton", "class": "btn-success", "label": "Cerrar Proyecto", "estado": "CERRADO_FDI", "mensaje": "Cerrar Proyecto"}]}',
      atributos: '{"13": ["estado_proyecto"], "15": ["cronograma_desembolsos"], "16": ["beneficiarios", "orden_proceder", "doc_especificaciones_tecnicas", "modulos", "estado_proyecto", "adj"], "17": ["plazo_recepcion_definitiva"], "18": ["equipo_tecnico", "estado_proyecto"]}',
      requeridos: '{"16": ["orden_proceder", "modulos"], "17": ["plazo_recepcion_definitiva"]}',
      areas: '{"11": ["DATOS_GENERALES", "DATOS_FINANCIAMIENTO"], "12": ["DATOS_GENERALES", "ASIGNACION_RESPONSABLE", "DATOS_GENERALES_EXTRA", "DATOS_FINANCIAMIENTO", "AUTORIDAD_BENEFICIARIA", "MODULOS_ITEMS", "CONVENIO", "DESEMBOLSO", "EQUIPO_TECNICO", "ORDEN_PROCEDER"], "13": ["DATOS_GENERALES", "DATOS_GENERALES_EXTRA", "DATOS_FINANCIAMIENTO", "AUTORIDAD_BENEFICIARIA", "MODULOS_ITEMS", "EQUIPO_TECNICO", "CONVENIO", "DESEMBOLSO"], "14": ["DATOS_GENERALES", "DATOS_GENERALES_EXTRA", "DATOS_FINANCIAMIENTO", "AUTORIDAD_BENEFICIARIA", "CONVENIO"], "15": ["DATOS_GENERALES", "DATOS_GENERALES_EXTRA", "DATOS_FINANCIAMIENTO", "AUTORIDAD_BENEFICIARIA", "CONVENIO", "DESEMBOLSO"], "16": ["DATOS_GENERALES", "DATOS_GENERALES_EXTRA", "DATOS_FINANCIAMIENTO", "ORDEN_PROCEDER", "MODULOS_ITEMS"], "17": ["DATOS_GENERALES", "ORDEN_PROCEDER", "RECEPCION_DEFINITIVA"], "18": ["DATOS_GENERALES", "DATOS_GENERALES_EXTRA", "DATOS_FINANCIAMIENTO", "AUTORIDAD_BENEFICIARIA", "EQUIPO_TECNICO", "CONVENIO", "DESEMBOLSO"]}',
    }, {
      codigo_proceso: 'PROYECTO-FDI',
      codigo: 'EN_EJECUCION_FDI'
    }, {
      returning: true, raw: true
    }).then((rows) => {

    }).finally(done);
  },

  down: (migration, DataTypes, done) => {
    let estado = migration.sequelize.import('../models/flujo/estado');
    return migration.bulkUpdate('estado', {
      acciones: '{"13": [{"ruta": "/temporal", "tipo": "ventana"}, {"icon": "fa-send", "tipo": "boton", "class": "btn-success", "label": "Enviar", "estado": "MODIFICADO_FDI", "mensaje": ""}], "15": [{"icon": "fa-save", "tipo": "boton", "class": "btn-primary", "label": "Guardar", "estado": "EN_EJECUCION_FDI", "guardar": true, "mensaje": "Guardar datos del proyecto"}], "16": [{"ruta": "/temporal", "tipo": "ventana"}, {"icon": "fa-save", "tipo": "boton", "class": "btn-primary", "label": "Guardar", "estado": "EN_EJECUCION_FDI", "guardar": true, "mensaje": "Guardar datos del proyecto"}, {"icon": "fa-check", "tipo": "boton", "class": "btn-success", "label": "Proceder", "estado": "EN_EJECUCION_FDI", "mensaje": "Registrar orden de proceder", "ejecutar": ["$validarOrdenProceder"]}], "18": [{"ruta": "/temporal", "tipo": "ventana"}, {"icon": "fa-send", "tipo": "boton", "class": "btn-success", "label": "Cerrar Proyecto", "estado": "CERRADO_FDI", "mensaje": "Cerrar Proyecto"}]}',
      atributos: '{"13": ["estado_proyecto"], "15": ["cronograma_desembolsos"], "16": ["beneficiarios", "orden_proceder", "doc_especificaciones_tecnicas", "modulos", "estado_proyecto", "adj"], "18": ["equipo_tecnico", "estado_proyecto"]}',
      requeridos: '{"16": ["orden_proceder", "modulos"]}',
      areas: '{"11": ["DATOS_GENERALES", "DATOS_FINANCIAMIENTO"], "12": ["DATOS_GENERALES", "ASIGNACION_RESPONSABLE", "DATOS_GENERALES_EXTRA", "DATOS_FINANCIAMIENTO", "AUTORIDAD_BENEFICIARIA", "MODULOS_ITEMS", "CONVENIO", "DESEMBOLSO", "EQUIPO_TECNICO", "ORDEN_PROCEDER"], "13": ["DATOS_GENERALES", "DATOS_GENERALES_EXTRA", "DATOS_FINANCIAMIENTO", "AUTORIDAD_BENEFICIARIA", "MODULOS_ITEMS", "EQUIPO_TECNICO", "CONVENIO", "DESEMBOLSO"], "14": ["DATOS_GENERALES", "DATOS_GENERALES_EXTRA", "DATOS_FINANCIAMIENTO", "AUTORIDAD_BENEFICIARIA", "CONVENIO"], "15": ["DATOS_GENERALES", "DATOS_GENERALES_EXTRA", "DATOS_FINANCIAMIENTO", "AUTORIDAD_BENEFICIARIA", "CONVENIO", "DESEMBOLSO"], "16": ["DATOS_GENERALES", "DATOS_GENERALES_EXTRA", "DATOS_FINANCIAMIENTO", "ORDEN_PROCEDER", "MODULOS_ITEMS"], "17": ["DATOS_GENERALES", "ORDEN_PROCEDER"], "18": ["DATOS_GENERALES", "DATOS_GENERALES_EXTRA", "DATOS_FINANCIAMIENTO", "AUTORIDAD_BENEFICIARIA", "EQUIPO_TECNICO", "CONVENIO", "DESEMBOLSO"]}',
    }, {
      codigo_proceso: 'PROYECTO-FDI',
      codigo: 'EN_EJECUCION_FDI'
    }, {
      returning: true, raw: true
    }).then((rows) => {

    }).finally(done);
  },
};
