'use strict';


module.exports = {
  up: (migration, DataTypes, done) => {

    let estado = migration.sequelize.import('../models/flujo/estado');

    console.time('estado');
    return migration.bulkUpdate('estado', {
        acciones: '[{"ruta": "/temporal", "tipo": "ventana"}, {"icon": "fa-check", "tipo": "boton", "class": "btn-success", "label": "Finalizar Planilla", "estado": "EN_ARCHIVO_FDI", "ejecutar": ["$registrarCoordenadas", "$cerrarItemsFinalizados", "$generarReportePlanilla","$revisarComprobante"]}]',
        atributos: '{"17": ["estado_supervision", "observacion","fecha_pago","path_comprobante"]}',
        nombre: 'Planilla Pendiente de Pago'
    },{
      codigo_proceso: 'SUPERV-FDI',
      codigo: 'EN_FISCAL_FDI'
    },{
      returning: true, raw: true
    }).then((rows)=>{
      console.timeEnd('estado');
    }).finally(done);

  },

  down: (migration, DataTypes, done) => {

    let estado = migration.sequelize.import('../models/flujo/estado');

    console.time('estado');
    return migration.bulkUpdate('estado', {
      acciones: '[{"ruta": "/temporal", "tipo": "ventana"}, {"icon": "fa-rotate-left", "tipo": "boton", "class": "btn-warning", "label": "Observar", "estado": "EN_SUPERVISION_FDI", "observacion": true}, {"icon": "fa-check", "tipo": "boton", "class": "btn-success", "label": "Aprobar planilla", "estado": "EN_ARCHIVO_FDI", "ejecutar": ["$registrarCoordenadas", "$cerrarItemsFinalizados", "$generarReportePlanilla"]}]',
      atributos: '{"17": ["estado_supervision", "observacion"]}',
      nombre: 'Planilla enviada al/a la fiscal'
    },{
      codigo_proceso: 'SUPERV-FDI',
      codigo: 'EN_FISCAL_FDI'
    },{
      returning: true, raw: true
    }).then((rows)=>{
      console.timeEnd('estado');
    }).finally(done);

  },
};
