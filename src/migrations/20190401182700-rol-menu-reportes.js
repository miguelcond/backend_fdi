'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
    let rol_menu = migration.sequelize.import('../models/autenticacion/rol_menu');
    return rol_menu.findOne({
      where: {
        fid_menu: 13,
        fid_rol: 19
      }
    }).then((result) => {
      if (!result) {
        migration.bulkInsert('rol_menu',
          [{ fid_menu: 13, fid_rol: 19, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() }]
        , {});
      } else {
        migration.bulkUpdate('rol_menu', { fid_menu: 13, fid_rol: 19, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
        {
          fid_menu: 13,
          fid_rol: 19
        }, {
          returning: true, raw: true
        });
      }
    }).catch((e) => {
      console.error('E', e.message);
    }).finally(done);
  },

  down: (migration, DataTypes, done) => {
    const sql = (
      `DELETE FROM rol_menu WHERE fid_menu = 13 AND fid_rol = 19;`
    );

    return migration.sequelize.query(sql)
      .finally(done);
  }
};
