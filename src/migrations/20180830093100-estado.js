'use strict';


module.exports = {
  up: (migration, DataTypes, done) => {

    let estado = migration.sequelize.import('../models/flujo/estado');

    console.time('estado');
    return migration.bulkUpdate('estado', {
        atributos: '{"16":["orden_proceder","doc_especificaciones_tecnicas","modulos","estado_proyecto","adj"]}',
        atributos_detalle: '{"modulo":{"acciones":["post","put"],"post":["fid_proyecto","fid_adjudicacion","nombre"],"put":["nombre"]},"item":{"acciones":["post","put"],"post":["fid_modulo","nombre","unidad_medida","cantidad","especificaciones","precio_unitario"],"put":["nombre","unidad_medida","cantidad","especificaciones","precio_unitario"]}}',
        requeridos: '{"16":["orden_proceder","modulos"]}'
    },{
      codigo_proceso: 'ADJ-FDI',
      codigo: 'ADJUDICADO_FDI'
    },{
      returning: true, raw: true
    }).then((rows)=>{
      console.timeEnd('estado');
    }).finally(done);

  },

  down: (migration, DataTypes, done) => {

    let estado = migration.sequelize.import('../models/flujo/estado');

    console.time('estado');
    return migration.bulkUpdate('estado', {
      atributos: '{"16":["estado_proyecto"]}',
      atributos_detalle: null,
      requeridos: '[]'
    },{
      codigo_proceso: 'ADJ-FDI',
      codigo: 'ADJUDICADO_FDI'
    },{
      returning: true, raw: true
    }).then((rows)=>{
      console.timeEnd('estado');
    }).finally(done);

  },
};
