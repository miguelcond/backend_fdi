// 'use strict';
module.exports = {
  up(queryInterface) {
    if (process.env.CREATEUSERS) {
      const personas = [
        { nombres: 'AGETIC', primer_apellido: 'AGETIC', segundo_apellido: 'AGETIC' },
        // { nombres: 'LEGAL 1' },
        // { nombres: 'LEGAL 2' },
        // { nombres: 'LEGAL 3' },
        // { nombres: 'FINANCIERO 1' },
        // { nombres: 'FINANCIERO 2' },
        // { nombres: 'FINANCIERO 3' },
        // { nombres: 'REGIONAL 1' },
        // { nombres: 'REGIONAL 2' },
        // { nombres: 'REGIONAL 3' },
        // { nombres: 'DEPARTAMENTAL 1' },
        // { nombres: 'DEPARTAMENTAL 2' },
        // { nombres: 'DEPARTAMENTAL 3' },
        // { nombres: 'DEPARTAMENTAL 4' },
        // { nombres: 'DEPARTAMENTAL 5' },
        // { nombres: 'DEPARTAMENTAL 6' },
        // { nombres: 'DEPARTAMENTAL 7' },
        // { nombres: 'DEPARTAMENTAL 8' },
        // { nombres: 'DEPARTAMENTAL 9' },
        // { nombres: 'TECNICO 1' },
        // { nombres: 'TECNICO 2' },
        // { nombres: 'TECNICO 3' },
        // { nombres: 'TECNICO 4' },
        // { nombres: 'TECNICO 5' },
        // { nombres: 'TECNICO 6' },
        // { nombres: 'TECNICO 7' },
        // { nombres: 'TECNICO 8' },
        // { nombres: 'TECNICO 9' },
        // { nombres: 'TECNICO 10' },
        { nombres: 'RESP RECEPCION' },
        { nombres: 'JEFE REVISION' },
        { nombres: 'TECNICO FDI' },
        { nombres: 'TECNICO2 FDI' },
        { nombres: 'LEGAL FDI' },
        { nombres: 'FINANCIERO FDI' },
        // { nombres: 'SUPERVISOR FDI' },
      ];
      let cnt = 1;
      for(let i in personas) {
        personas[i].tipo_documento = 'TD_CI';
        personas[i].complemento_visible = false;
        personas[i].numero_documento = `${cnt < 10 ? '0' : ''}0000${cnt}`;
        personas[i].complemento = '00';
        personas[i].fecha_nacimiento = `${(cnt%30)+1 < 10 ? '0' : ''}${(cnt%30)+1}/03/1990`;
        personas[i].correo = `email.${cnt}@example.com`;
        personas[i].estado = 'ACTIVO';
        personas[i]._usuario_creacion = 1;
        personas[i]._fecha_creacion = new Date();
        personas[i]._fecha_modificacion = new Date();
        cnt++;
      }
      return queryInterface.bulkInsert('persona', personas, {});
    } else {
      return queryInterface.bulkInsert('persona', [
        {
          tipo_documento: 'TD_CI',
          complemento_visible: false,
          numero_documento: '0000008',
          complemento: '00',
          nombres: 'AGETIC',
          primer_apellido: 'AGETIC',
          segundo_apellido: 'AGETIC',
          fecha_nacimiento: '1980/01/01',
          correo: 'agetic@agetic.gob.bo',
          estado: 'ACTIVO',
          _usuario_creacion: 1,
          _fecha_creacion: new Date(),
          _fecha_modificacion: new Date()
        }
      ], {});
    }
  },

  down() {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  },
};
