// 'use strict';

module.exports = {
  up(queryInterface) {
    return queryInterface.bulkInsert('ruta', [
      // 1
      {
        ruta: '/api/v1/usuario_rol',
        descripcion: 'Ruta para administrar usuarios y sus respectivos roles',
        method_get: true,
        method_post: true,
        method_put: true,
        method_delete: true,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 2
      {
        ruta: '/api/v1/rol_menu',
        descripcion: 'Ruta para administrar roles y sus respectivos menus',
        method_get: true,
        method_post: true,
        method_put: true,
        method_delete: true,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 3
      {
        ruta: '/api/v1/rol_ruta',
        descripcion: 'Ruta para administrar roles y sus respectivas rutas',
        method_get: true,
        method_post: true,
        method_put: true,
        method_delete: true,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 4
      {
        ruta: '/api/v1/menu',
        descripcion: 'Ruta para administrar menús',
        method_get: true,
        method_post: true,
        method_put: true,
        method_delete: true,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 5
      {
        ruta: '/api/v1/rol',
        descripcion: 'Ruta para administrar roles',
        method_get: true,
        method_post: true,
        method_put: true,
        method_delete: true,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 6
      {
        ruta: '/api/v1/ruta',
        descripcion: 'Ruta para administrar rutas',
        method_get: true,
        method_post: true,
        method_put: true,
        method_delete: false,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 7
      {
        ruta: '/api/v1/usuario',
        descripcion: 'Ruta para administrar usuarios',
        method_get: true,
        method_post: true,
        method_put: true,
        method_delete: false,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 8
      {
        ruta: '/api/v1/proyectos',
        descripcion: 'Ruta para administrar proyectos',
        method_get: true,
        method_post: true,
        method_put: true,
        method_delete: true,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 9
      {
        ruta: '/api/v1/modulos',
        descripcion: 'Ruta para administrar modulos',
        method_get: true,
        method_post: true,
        method_put: true,
        method_delete: true,
        estado: 'ACTIVO',
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 10
      {
        ruta: '/api/v1/items',
        descripcion: 'Ruta para administrar items',
        method_get: true,
        method_post: true,
        method_put: true,
        method_delete: true,
        estado: 'ACTIVO',
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 11
      {
        ruta: '/api/v1/supervisiones',
        descripcion: 'Ruta para administrar avances',
        method_get: true,
        method_post: true,
        method_put: true,
        method_delete: false,
        estado: 'ACTIVO',
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 12
      {
        ruta: '/api/v1/fotos',
        descripcion: 'Ruta para administrar fotos de avances',
        method_get: true,
        method_post: true,
        method_put: true,
        method_delete: false,
        estado: 'ACTIVO',
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 13
      {
        ruta: '/api/v1/parametricas',
        descripcion: 'Ruta para administrar parametricas',
        method_get: true,
        method_post: false,
        method_put: false,
        method_delete: false,
        estado: 'ACTIVO',
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 14
      {
        ruta: '/api/v1/departamentos',
        descripcion: 'Ruta para administrar departamentos',
        method_get: true,
        method_post: false,
        method_put: false,
        method_delete: false,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 15
      {
        ruta: '/api/v1/provincias',
        descripcion: 'Ruta para listar provincias',
        method_get: true,
        method_post: false,
        method_put: false,
        method_delete: false,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 16
      {
        ruta: '/api/v1/municipios',
        descripcion: 'Ruta para listar municipios',
        method_get: true,
        method_post: false,
        method_put: false,
        method_delete: false,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 17
      {
        ruta: '/api/v1/personas',
        descripcion: 'Ruta para administrar personas',
        method_get: true,
        method_post: false,
        method_put: false,
        method_delete: false,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 18
      {
        ruta: '/api/v1/estados',
        descripcion: 'Ruta para administrar estados',
        method_get: true,
        method_post: false,
        method_put: true,
        method_delete: false,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 19
      {
        ruta: '/api/v1/empresas',
        descripcion: 'Ruta para acceder a fundempresa',
        method_get: true,
        method_post: false,
        method_put: false,
        method_delete: false,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 20
      {
        ruta: '/api/v1/adjudicar',
        descripcion: 'Ruta para adjudicar proyecto',
        method_get: false,
        method_post: true,
        method_put: false,
        method_delete: false,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 21
      {
        ruta: '/api/v1/computos_metricos',
        descripcion: 'Ruta para gestionar computos métricos',
        method_get: true,
        method_post: true,
        method_put: true,
        method_delete: true,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 22
      {
        ruta: '/api/v1/contrasena',
        descripcion: 'Ruta para modificar contraseña',
        method_get: false,
        method_post: false,
        method_put: true,
        method_delete: false,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 23
      {
        ruta: '/api/v1/boletas',
        descripcion: 'Ruta para modificar boletas',
        method_get: true,
        method_post: true,
        method_put: true,
        method_delete: false,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 24
      {
        ruta: '/api/v1/adminproyectos',
        descripcion: 'Ruta para cambiar usuarios de proyectos',
        method_get: true,
        method_post: true,
        method_put: true,
        method_delete: false,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 25
      {
        ruta: '/api/v1/reportes',
        descripcion: 'Ruta para generar reportes',
        method_get: true,
        method_post: true,
        method_put: true,
        method_delete: false,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 26
      {
        ruta: '/api/v1/contenidos',
        descripcion: 'Ruta para obtener contenidos modelo de formularios',
        method_get: true,
        method_post: false,
        method_put: false,
        method_delete: false,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // 27
      {
        ruta: '/api/v1/techo_presupuestario',
        descripcion: 'Ruta para obtener el monto de techo presupuestario y los montos usado y disponible',
        method_get: true,
        method_post: false,
        method_put: true,
        method_delete: false,
        estado: 'ACTIVO',
        _usuario_creacion: '1',
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      }
    ], {});
  },

  down() {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
