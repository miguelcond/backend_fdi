// 'use strict';

module.exports = {
  up(queryInterface) {
    return queryInterface.bulkInsert('institucion', [
      // Tipos de Documentos de identidad
      {
        sigla: 'UPRE',
        nombre: 'Unidad de Proyectos Especiales',
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      {
        sigla: 'FDI',
        nombre: 'Fondo de Desarrollo Indígena',
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
    ], {});
  },

  down() {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
