/**
 * Módulo que mapea los ROLES_MENUS.
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const rolMenu = sequelize.define('rol_menu', {
    id_rol_menu: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      xlabel: 'Id rol menú'
    },
    method_get: {
      type: DataType.BOOLEAN,
      xlabel: 'Ver',
      defaultValue: false
    },
    method_post: {
      type: DataType.BOOLEAN,
      xlabel: 'Crear',
      defaultValue: false
    },
    method_put: {
      type: DataType.BOOLEAN,
      xlabel: 'Modificar',
      defaultValue: false
    },
    method_delete: {
      type: DataType.BOOLEAN,
      xlabel: 'Eliminar',
      defaultValue: false
    },
    estado: {
      type: DataType.STRING(10),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'rol_menu',
    classMethods: {}
  });

  Object.assign(rolMenu, {
    associate: (models) => {
      models.rol_menu.belongsTo(models.rol, {
        as: 'rol',
        foreignKey: { name: 'fid_rol', allowNull: false, xchoice: 'rol', unique: 'compositeIndex' },
        targetKey: 'id_rol'
      });
      models.rol_menu.belongsTo(models.menu, {
        as: 'menu',
        foreignKey: { name: 'fid_menu', allowNull: false, xchoice: 'menu', unique: 'compositeIndex' },
        targetKey: 'id_menu'
      });
    }
  });

  return rolMenu;
};
