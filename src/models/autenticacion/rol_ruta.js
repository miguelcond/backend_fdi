/**
 * Módulo que mapea los ROLES_RUTAS.
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const rolRuta = sequelize.define('rol_ruta', {
    id_rol_ruta: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      xlabel: 'Id rol ruta'
    },
    method_get: {
      type: DataType.BOOLEAN,
      xlabel: 'Ver',
      defaultValue: false
    },
    method_post: {
      type: DataType.BOOLEAN,
      xlabel: 'Crear',
      defaultValue: false
    },
    method_put: {
      type: DataType.BOOLEAN,
      xlabel: 'Modificar',
      defaultValue: false
    },
    method_delete: {
      type: DataType.BOOLEAN,
      xlabel: 'Eliminar',
      defaultValue: false
    },
    estado: {
      type: DataType.STRING(),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'rol_ruta',
    classMethods: {}
  });

  Object.assign(rolRuta, {
    associate: (models) => {
      models.rol_ruta.belongsTo(models.rol, {
        as: 'rol',
        foreignKey: { name: 'fid_rol', allowNull: false, xchoice: 'rol', unique: 'compositeIndex' },
        targetKey: 'id_rol'
      });
      models.rol_ruta.belongsTo(models.ruta, {
        as: 'ruta',
        foreignKey: { name: 'fid_ruta', allowNull: false, xchoice: 'ruta', unique: 'compositeIndex' },
        targetKey: 'id_ruta'
      });
    }
  });

  return rolRuta;
};
