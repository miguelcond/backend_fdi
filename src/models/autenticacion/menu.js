/**
 * Módulo que mapea los MENUS existentes
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const menu = sequelize.define('menu', {
    id_menu: {
      type: DataType.INTEGER,
      primaryKey: true,
      xlabel: 'Id Menú',
      autoIncrement: true
    },
    nombre: {
      type: DataType.STRING(100),
      xlabel: 'Nombre',
      allowNull: false
    },
    descripcion: {
      type: DataType.STRING(150),
      xlabel: 'Descripción',
      allowNull: true
    },
    orden: {
      type: DataType.INTEGER,
      xlabel: 'Orden',
      allowNull: false
    },
    ruta: {
      type: DataType.STRING(100),
      xlabel: 'Ruta',
      allowNull: true
    },
    icono: {
      type: DataType.STRING(100),
      xlabel: 'Ícono',
      allowNull: true
    },
    method_get: {
      type: DataType.BOOLEAN,
      xlabel: 'Ver',
      allowNull: true
    },
    method_post: {
      type: DataType.BOOLEAN,
      xlabel: 'Crear',
      allowNull: true
    },
    method_put: {
      type: DataType.BOOLEAN,
      xlabel: 'Modificar',
      allowNull: true
    },
    method_delete: {
      type: DataType.BOOLEAN,
      xlabel: 'Eliminar'
    },
    estado: {
      type: DataType.STRING(10),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'menu',
    classMethods: {}
  });

  Object.assign(menu, {
    associate: (models) => {
      menu.belongsTo(models.menu, {
        as: 'menu_padre',
        foreignKey: { name: 'fid_menu_padre', allowNull: true, xchoice: 'nombre' },
        targetKey: 'id_menu'
      });
      menu.hasMany(models.menu, {
        as: 'submenus',
        foreignKey: { name: 'fid_menu_padre', allowNull: true }
      });
      menu.hasMany(models.rol_menu, {
        as: 'rol_menu',
        foreignKey: { name: 'fid_menu', allowNull: false }
      });
    },
    buscarMenusSubmenus: () => menu.findAll({
      attributes: ['id_menu', 'nombre', 'descripcion', 'orden', 'ruta', 'icono', 'method_get', 'method_post', 'method_put', 'method_delete', 'estado', 'fid_menu_padre'],
      where: { estado: 'ACTIVO', fid_menu_padre: null },
      include: [
        {
          model: sequelize.models.menu,
          join: 'left',
          attributes: ['id_menu', 'nombre', 'descripcion', 'orden', 'ruta', 'icono', 'method_get', 'method_post', 'method_put', 'method_delete', 'estado', 'fid_menu_padre'],
          as: 'submenus',
          where: { estado: 'ACTIVO' },
          paranoid: false,
          required: false,
          order: [['orden', 'ASC']]
        }
      ],
      order: [['orden', 'ASC']]
    }),
    buscarPorId: (idMenu) => menu.findById(idMenu, {
      attributes: ['id_menu', 'nombre', 'descripcion', 'orden', 'ruta', 'icono', 'method_get', 'method_post', 'method_put', 'method_delete', 'estado', 'fid_menu_padre']
    })
  });

  menu.beforeCreate((menu, options) => {
    if (menu.nombre === undefined) {
      throw new Error('El campo nombre menú es obligatorio.');
    }
    if (menu.nombre !== null) {
      menu.nombre = menu.nombre.toUpperCase();
    }
  });

  menu.beforeUpdate((menu, options) => {
    if (menu.nombre === undefined) {
      throw new Error('El campo nombre menú es obligatorio.');
    }
    if (menu.nombre !== null) {
      menu.nombre = menu.nombre.toUpperCase();
    }
  });

  return menu;
};
