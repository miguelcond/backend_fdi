
module.exports = (sequelize, DataTypes) => {
  const formulario = sequelize.define('formulario_historico', {
    id_formulario: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      field: 'id_formulario',
      xlabel: 'ID',
      allowNull: false
    },
    fid_proyecto: {
      type: DataTypes.INTEGER,
      field: 'fid_proyecto',
      xlabel: 'Proyecto',
      allowNull: false
    },
    fid_plantilla: {
      type: DataTypes.INTEGER,
      field: 'fid_plantilla',
      xlabel: 'Plantilla',
      allowNull: false
    },
    codigo_estado: {
      type: DataTypes.STRING,
      field: 'codigo_estado',
      xlabel: 'Estado',
      allowNull: false
    },
    version: {
      type: DataTypes.INTEGER,
      field: 'version',
      xlabel: 'Versión',
      allowNull: false
    },
    nombre: {
      type: DataTypes.STRING,
      field: 'nombre',
      xlabel: 'Nombre',
      allowNull: false
    },
    form: {
      type: DataTypes.JSONB,
      field: 'form',
      xlabel: 'Formulario',
      allowNull: true
    },
    form_fields: {
      type: DataTypes.JSONB,
      field: 'form_fields',
      xlabel: 'Campos',
      allowNull: true
    },
    form_model: {
      type: DataTypes.JSONB,
      field: 'form_model',
      xlabel: 'Modelo',
      allowNull: true
    },
    form_options: {
      type: DataTypes.JSONB,
      field: 'form_options',
      xlabel: 'Opciones de formulario',
      allowNull: true
    },
    codigo_documento: {
      type: DataTypes.STRING,
      xlabel: 'Nombre del archivo',
      allowNull: true
    },
    fecha_documento: {
      type: DataTypes.DATE,
      primaryKey: true,
      xlabel: 'Fecha del documento',
      allowNull: false
    },
    estado: {
      type: DataTypes.STRING(30),
      field: 'estado',
      xlabel: 'Estado',
      allowNull: false,
      defaultValue: 'ELABORADO',
      validate: {
        isIn: { args: [['ELABORADO', 'PUBLICADO', 'INACTIVO', 'ELIMINADO']], msg: 'El campo estado sólo permite valores: ELABORADO, PUBLICADO, INACTIVO o ELIMINADO' }
      }
    },
    _usuario_creacion: {
      type: DataTypes.INTEGER,
      field: '_usuario_creacion',
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataTypes.INTEGER,
      field: '_usuario_modificacion',
      xlabel: 'Usuario de modificación'
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'formulario_historico',
    classMethods: {}
  });

  Object.assign(formulario, {
    associate: (models) => {
      // plantilla.belongsTo(models.tipo_documento, {
      //   as: 'tipo_documento',
      //   foreignKey: {
      //     name: 'fid_tipo_documento',
      //     allowNull: false,
      //   },
      // });
    }
  });

  return formulario;
};
