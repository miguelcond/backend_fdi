/**
 * Módulo para documento
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const archivoAdjunto = sequelize.define('archivo_adjunto', {
    id_archivo_adjunto: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      xlabel: 'ID'
    },
    roles: {
      type: DataType.JSONB,
      xlabel: 'Atributos por rol',
      allowNull: false
    },
    codigo_estado: {
      type: DataType.STRING,
      xlabel: 'Estado',
      allowNull: false
    },
    registrable_id: {
      type: DataType.INTEGER,
      xlabel: 'Fuente',
      allowNull: true
    },
    registrable_tipo: {
      type: DataType.STRING(64),
      xlabel: 'Tipo de fuente',
      allowNull: true
    },
    titulo: {
      type: DataType.STRING,
      xlabel: 'Título',
      allowNull: false
    },
    mime_type: {
      type: DataType.STRING,
      xlabel: 'Tipo archivo',
      allowNull: false
    },
    nombre_original: {
      type: DataType.STRING,
      xlabel: 'Nombre original',
      allowNull: false
    },
    ruta: {
      type: DataType.STRING,
      xlabel: 'Ruta de archivo',
      allowNull: true
    },
    descripcion: {
      type: DataType.STRING,
      xlabel: 'Observación',
      allowNull: true
    },
    fecha_inicio: {
      type: DataType.STRING,
      xlabel: 'Fecha inicio vigencia',
      allowNull: true
    },
    fecha_fin: {
      type: DataType.STRING,
      xlabel: 'Fecha fin vigencia',
      allowNull: true
    },
    estado: {
      type: DataType.STRING(10),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'archivo_adjunto',
    classMethods: {}
  });

  return archivoAdjunto;
};
