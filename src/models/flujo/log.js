/**
 * Módulo para log de cambios en proyecto.
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const log = sequelize.define('log', {
    id_log: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      xlabel: 'ID'
    },
    objeto_id: {
      type: DataType.INTEGER,
      xlabel: 'Objeto',
      allowNull: false
    },
    objeto_tipo: {
      type: DataType.STRING(64),
      xlabel: 'Tipo Objeto',
      allowNull: false
    },
    objeto_estado: {
      type: DataType.STRING,
      xlabel: 'Estado',
      allowNull: false
    },
    observacion: {
      type: DataType.STRING,
      xlabel: 'Observación',
      allowNull: true
    },
    fid_usuario: {
      type: DataType.INTEGER,
      xlabel: 'Usuario',
      allowNull: false
    },
    fid_usuario_rol: {
      type: DataType.INTEGER,
      xlabel: 'Rol de Usuario',
      allowNull: false
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'log',
    classMethods: {}
  });

  Object.assign(log, {
    // Creando asociaciones para la entidad
    associate: (models) => {
      models.log.belongsTo(models.proyecto, {
        as: 'proyecto',
        foreignKey: 'objeto_id',
        targetKey: 'id_proyecto',
        constraints: false
      });
      models.log.belongsTo(models.supervision, {
        as: 'supervision',
        foreignKey: 'objeto_id',
        targetKey: 'id_supervision',
        constraints: false
      });
      models.log.belongsTo(models.estado, {
        as: 'estado',
        foreignKey: 'objeto_estado',
        targetKey: 'codigo',
        constraints: false
      });
      models.log.belongsTo(models.usuario, {
        as: 'usuario',
        foreignKey: { name: 'fid_usuario', allowNull: false },
        targetKey: 'id_usuario',
        constraints: false
      });
      models.log.belongsTo(models.rol, {
        as: 'usuario_rol',
        foreignKey: { name: 'fid_usuario_rol', allowNull: false },
        targetKey: 'id_rol',
        constraints: false
      });
    }
  });

  return log;
};
