/**
 * Módulo municipio
 *
 * @module
 *
 **/
module.exports = (sequelize, DataType) => {
  const techoPresupuestario = sequelize.define('techo_presupuestario', {
    codigo: {
      type: DataType.STRING(6),
      primaryKey: true,
      xlabel: 'Código de municipio',
      allowNull: false,
      validate: {
        len: {
          args: [6],
          msg: 'El campo \'código de municipio\' permite 6 caracteres.'
        },
        is: {
          args: /^[0-9]+$/i,
          msg: 'El campo \'código de municipio\' permite sólo numeros.'
        }
      }
    },
    gestion: {
      type: DataType.STRING(16),
      xlabel: 'Gestión',
      primaryKey: true,
      allowNull: true
    },
    nombre_municipio: {
      type: DataType.STRING(100),
      xlabel: 'Nombre de municipio',
      allowNull: false,
      validate: {
        len: {
          args: [3, 100],
          msg: 'El campo \'Nombre de municipio\' permite un mínimo de 3 caracter y un máximo de 100 caracteres'
        }
        // is: {
        //   args: /^[0-9|A-Z|-|-|.]+$/i,
        //   msg: 'El campo \'Nombre de municipio\' permite sólo letras, números, guiones y puntos.'
        // }
      }
    },
    num_concejales: {
      type: DataType.INTEGER,
      xlabel: 'Número de concejales',
      allowNull: false
    },
    techo_presupuestal: {
      type: DataType.FLOAT,
      xlabel: 'Techo presupuestario por municipio',
      allowNull: false
    },
    monto_usado: {
      type: DataType.FLOAT,
      xlabel: 'Monto empleado en proyectos para el municipio',
      allowNull: false,
      defaultValue: 0
    },
    monto_disponible: {
      type: DataType.FLOAT,
      xlabel: 'Monto desponible para nuevos proyectos en el municipio',
      allowNull: false,
      defaultValue: 0
    },
    estado: {
      type: DataType.STRING(),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      field: '_usuario_creacion',
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      field: '_usuario_modificacion',
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'techo_presupuestario',
    classMethods: {}
  });

  Object.assign(techoPresupuestario, {
    associate: function (models) {
      techoPresupuestario.belongsTo(models.municipio, {
        as: 'municipio',
        foreignKey: 'codigo',
        targetKey: 'codigo'
      });
    }
  });

  return techoPresupuestario;
};
