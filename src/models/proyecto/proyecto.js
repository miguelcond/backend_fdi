import util from '../../lib/util';
/**
 * Módulo par proyecto
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const proyecto = sequelize.define('proyecto', {
    id_proyecto: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      xlabel: 'ID',
      comment: 'Identificador del proyecto.'
    },
    version: {
      type: DataType.INTEGER,
      xlabel: 'version',
      allowNull: false
    },
    codigo: {
      type: DataType.STRING(50),
      xlabel: 'Codigo',
      allowNull: true
    },
    tipo: {
      type: DataType.STRING(50),
      xlabel: 'Tipo',
      allowNull: false
    },
    nombre: {
      type: DataType.STRING,
      xlabel: 'Nombre',
      allowNull: false
    },
    nro_convenio: {
      type: DataType.STRING(50),
      xlabel: 'Nro de convenio',
      allowNull: true
    },
    id_proyecto_referencia: {
      type: DataType.INTEGER,
      xlabel: 'id proyecto referencia',
      allowNull: true
    },
    nro_convenio_referencia: {
      type: DataType.STRING(50),
      xlabel: 'Nro de convenio referencia',
      allowNull: true
    },
    monto_fdi: {
      type: DataType.FLOAT,
      xlabel: 'Monto de financiamiento del FDI',
      allowNull: true
    },
    monto_contraparte: {
      type: DataType.FLOAT,
      xlabel: 'Monto de financiamiento de la contraparte',
      allowNull: true
    },
    monto_total_convenio: {
      type: DataType.FLOAT,
      xlabel: 'Monto total referencial',
      allowNull: true
    },
    monto_total: {
      type: DataType.FLOAT,
      xlabel: 'Monto total',
      allowNull: true
    },
    monto_total_ejecutado: {
      type: DataType.FLOAT,
      xlabel: 'Monto total',
      allowNull: true
    },
    plazo_ejecucion: {
      type: DataType.INTEGER,
      xlabel: 'Plazo de ejecución',
      allowNull: true
    },
    plazo_ejecucion_convenio: {
      type: DataType.INTEGER,
      xlabel: 'Plazo de ejecución referencial',
      allowNull: true
    },
    plazo_recepcion_definitiva: {
      type: DataType.INTEGER,
      xlabel: 'Plazo en días de recepción definitiva de proyecto',
      allowNull: false,
      defaultValue: 90
    },
    entidad_beneficiaria: {
      type: DataType.STRING(250),
      xlabel: 'Entidad beneficiaria',
      allowNull: true
    },
    fecha_suscripcion_convenio: {
      type: DataType.DATE,
      xlabel: 'Fecha de suscripción del convenio',
      allowNull: true
    },
    doc_convenio: {
      type: DataType.STRING,
      xlabel: 'Documento convenio',
      allowNull: true
    },
    req_extension_convenio: {
      type: DataType.BOOLEAN,
      xlabel: 'Requiere extension de convenio',
      allowNull: false,
      defaultValue: false
    },
    doc_convenio_extendido: {
      type: DataType.STRING,
      xlabel: 'Documento convenio extendido',
      allowNull: true
    },
    doc_convenio_extendido2: {
      type: DataType.STRING,
      xlabel: '2do Documento convenio extendido',
      allowNull: true
    },
    doc_convenio_extendido3: {
      type: DataType.STRING,
      xlabel: '3er Documento convenio extendido',
      allowNull: true
    },
    doc_convenio_extendido4: {
      type: DataType.STRING,
      xlabel: '4to Documento convenio extendido',
      allowNull: true
    },
    doc_convenio_extendido5: {
      type: DataType.STRING,
      xlabel: '5to Documento convenio extendido',
      allowNull: true
    },
    nro_resmin_cife: {
      type: DataType.STRING(100),
      xlabel: 'Nro resolución ministerial CIFE',
      allowNull: true
    },
    doc_resmin_cife: {
      type: DataType.STRING(100),
      xlabel: 'Documento resolución ministerial',
      allowNull: true
    },
    doc_cert_presupuestaria: {
      type: DataType.STRING(100),
      xlabel: 'Documento certificación presupuestaria',
      allowNull: true
    },
    doc_cert_presupuestaria2: {
      type: DataType.STRING(100),
      xlabel: 'Documento certificación presupuestaria2',
      allowNull: true
    },
    doc_cert_presupuestaria3: {
      type: DataType.STRING(100),
      xlabel: 'Documento certificación presupuestaria3',
      allowNull: true
    },
    doc_ev_externa: {
      type: DataType.STRING(100),
      xlabel: 'Documento de evaluación externa',
      allowNull: true
    },
    desembolso: {
      type: DataType.FLOAT,
      xlabel: 'Desembolso',
      allowNull: true
    },
    porcentaje_desembolso: {
      type: DataType.FLOAT,
      xlabel: '% Desembolso',
      allowNull: true
    },
    fecha_desembolso: {
      type: DataType.DATE,
      xlabel: 'Fecha de desembolso',
      allowNull: true
    },
    anticipo: {
      type: DataType.FLOAT,
      xlabel: 'Tipo de boleta',
      allowNull: true
    },
    porcentaje_anticipo: {
      type: DataType.FLOAT,
      xlabel: 'Tipo de boleta',
      allowNull: true
    },
    sector: {
      type: DataType.STRING(50),
      xlabel: 'Sector',
      allowNull: true
    },
    fid_autoridad_beneficiaria: {
      type: DataType.INTEGER,
      xlabel: 'Autoridad beneficiaria',
      allowNull: true
    },
    cargo_autoridad_beneficiaria: {
      type: DataType.STRING,
      xlabel: 'Cargo autoridad beneficiaria',
      allowNull: true
    },
    fax_autoridad_beneficiaria: {
      type: DataType.STRING(60),
      xlabel: 'Fax autoridad beneficiaria',
      allowNull: true
    },
    fcod_municipio: {
      type: DataType.STRING(6),
      xlabel: 'Municipio',
      allowNull: true
    },
    coordenadas: {
      type: DataType.GEOMETRY('POINT'),
      xlabel: 'Coordenadas',
      allowNull: true
    },
    doc_especificaciones_tecnicas: {
      type: DataType.STRING(100),
      xlabel: 'Especificaciones técnicas items',
      allowNull: true
    },
    doc_base_contratacion: {
      type: DataType.STRING(100),
      xlabel: 'Documento base de contratación',
      allowNull: true
    },
    orden_proceder: {
      type: DataType.DATE,
      xlabel: 'Orden proceder',
      allowNull: true
    },
    fecha_fin_proyecto: {
      type: DataType.DATE,
      xlabel: 'Fecha fin del proyecto',
      allowNull: true
    },
    codigo_acceso: {
      type: DataType.STRING(20),
      xlabel: 'Código de acceso para la empresa',
      allowNull: true
    },
    matricula: {
      type: DataType.STRING(50),
      xlabel: 'Matrícula',
      allowNull: true
    },
    cod_documentacion_exp: {
      type: DataType.STRING(20),
      xlabel: 'Codigo experiencia',
      allowNull: true
    },
    documentacion_exp: {
      type: DataType.JSON,
      xlabel: 'Experiencia',
      allowNull: true
    },
    estado_proyecto: {
      type: DataType.STRING(50),
      xlabel: 'Estado del proyecto',
      allowNull: false,
      defaultValue: 'REGISTRO_CONVENIO'
    },
    fid_usuario_asignado: {
      type: DataType.INTEGER,
      xlabel: 'Usuario asignado',
      allowNull: true
    },
    fid_usuario_asignado_rol: {
      type: DataType.INTEGER,
      xlabel: 'Rol Usuario asignado',
      allowNull: true
    },
    nro_invitacion: {
      type: DataType.STRING(50),
      xlabel: 'Número de invitación',
      allowNull: true
    },
    tipo_modificacion: {
      type: DataType.STRING(50),
      xlabel: 'Tipo de modificación',
      allowNull: true
    },
    plazo_ampliacion: {
      type: DataType.JSON,
      xlabel: 'Ampliación de plazo',
      allowNull: 'true'
    },
    doc_respaldo_modificacion: {
      type: DataType.STRING(100),
      xlabel: 'Documento de respaldo a la modificación',
      allowNull: true
    },
    fecha_modificacion: {
      type: DataType.DATE,
      xlabel: 'Fecha de modificación de volúmenes y plazos',
      allowNull: true
    },
    motivo_rechazo: {
      type: DataType.STRING,
      xlabel: 'Motivo Rechazo',
      allowNull: true
    },
    estado: {
      type: DataType.STRING(10),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
        cartera: {
      type: DataType.INTEGER,
      allowNull: false,
      xlabel: 'Cartera',
    },
    org_sociales: {
      type: DataType.BOOLEAN,
      allowNull: false,
      xlabel: 'org_sociales',
      defaultValue: false
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'proyecto',
    classMethods: {}
  });

  Object.assign(proyecto, {
    // Creando asociaciones para la entidad
    associate: (models) => {
      models.proyecto.belongsTo(models.cartera, {
        as: 'cartera_proyecto',
        foreignKey: {name:'cartera', xchoice:'carteras', xlabel:'Cartera'},
        targetKey :'id_cartera'
      });

      models.proyecto.belongsTo(models.parametrica, {
        as: 'tipo_proyecto',
        foreignKey: { name: 'tipo', xchoice: 'tipos', xlabel: 'Tipo' },
        targetKey: 'codigo'
      });
      models.proyecto.belongsTo(models.municipio, {
        as: 'municipio',
        foreignKey: { name: 'fcod_municipio', xchoice: 'municipios', xlabel: 'Municipio' },
        targetKey: 'codigo'
      });
      models.proyecto.hasMany(models.contrato, {
        as: 'contrato',
        foreignKey: 'fid_proyecto'
      });
      models.proyecto.hasMany(models.equipo_tecnico, {
        as: 'equipo_tecnico',
        foreignKey: 'fid_proyecto'
      });
      models.proyecto.hasMany(models.boleta, {
        as: 'boletas',
        foreignKey: 'fid_proyecto'
      });
      models.proyecto.belongsTo(models.estado, {
        as: 'estado_actual',
        foreignKey: 'estado_proyecto',
        targetKey: 'codigo'
      });
      models.proyecto.hasMany(models.modulo, {
        as: 'modulos',
        foreignKey: 'fid_proyecto'
      });
      models.proyecto.belongsTo(models.usuario, {
        as: 'usuario',
        foreignKey: 'fid_usuario_asignado',
        targetKey: 'id_usuario',
        constraints: false
      });
      models.proyecto.belongsTo(models.rol, {
        as: 'usuario_rol',
        foreignKey: 'fid_usuario_asignado_rol',
        targetKey: 'id_rol',
        constraints: false
      });
      models.proyecto.hasMany(models.transicion, {
        as: 'transicion',
        foreignKey: 'objeto_transicion',
        constraints: false
      });
      models.proyecto.hasOne(models.transicion, {
        as: 'observacion',
        foreignKey: 'objeto_transicion',
        constraints: false
      });
      models.proyecto.belongsTo(models.persona, {
        as: 'autoridad_beneficiaria',
        foreignKey: 'fid_autoridad_beneficiaria',
        targetKey: 'id_persona'
      });
      models.proyecto.hasMany(models.supervision, {
        as: 'supervisiones',
        foreignKey: 'fid_proyecto'
      });
      models.proyecto.belongsTo(models.parametrica, {
        as: 'tipo_modificacion_proyecto',
        foreignKey: { name: 'tipo_modificacion', xchoice: 'tipos', xlabel: 'Tipo Modificación' },
        targetKey: 'codigo'
      });
      models.proyecto.hasMany(models.proyecto_historico, {
        as: 'proyecto_historico',
        foreignKey: 'id_proyecto'
      });
      models.proyecto.hasMany(models.formulario, {
        as: 'formularios',
        foreignKey: 'fid_proyecto'
      });
      models.proyecto.hasMany(models.proyecto, {
        as: 'proyectos',
        foreignKey: 'id_proyecto_referencia',
        constraints: false
      });
      models.proyecto.hasMany(models.adjudicacion, {
        as: 'adjudicaciones',
        foreignKey: 'fid_proyecto',
        constraints: false
      });
    },

    proyectoIncluyePendiente: (consulta, idRol, idUsuario, idAutoridad, esSupervisorOFiscal, esLegal, estadosRol, matricula) => {
      if (esSupervisorOFiscal) {
        if (!consulta.condicionEquipo_tecnico) {
          consulta.condicionEquipo_tecnico = {
            fid_usuario: idUsuario,
            fid_rol: idRol,
            estado: 'ACTIVO'
          };
        }
      }
      let estadosRolSTR = util.convertirArrayAString(estadosRol) || `'DESCONOCIDO'`; // ['A','B','C']   =>  "'A','B','C'"
      const options = {
        include: [
          {
            model: sequelize.models.equipo_tecnico,
            as: 'equipo_tecnico',
            required: esSupervisorOFiscal,
            where: consulta.condicionEquipo_tecnico
          },  {
            model: sequelize.models.adjudicacion,
            as: 'adjudicaciones',
            include: [
              {
                attributes: ['id_supervision', 'fid_adjudicacion', 'fid_proyecto', 'nro_supervision', 'estado_supervision'],
                model: sequelize.models.supervision,
                as: 'supervision',
                required: false,
                include: [
                  {
                    attributes: ['codigo', 'codigo_proceso', 'nombre', 'tipo', 'fid_rol'],
                    model: sequelize.models.estado,
                    as: 'estado_actual'
                  }
                ],
                where: sequelize.literal(`estado_supervision NOT IN ('EN_ARCHIVO_FDI','EN_FISCAL_FDI')`)
              }, {
                attributes: ['codigo', 'codigo_proceso', 'nombre', 'tipo', 'fid_rol'],
                model: sequelize.models.estado,
                as: 'estado_actual'
              }
            ],
            required: false,
            where: {
              estado: 'ACTIVO'
            },
            // where: sequelize.literal(`(id_adjudicacion IN (SELECT fid_adjudicacion FROM equipo_tecnico WHERE fid_rol = ${idRol} and fid_usuario = ${idUsuario})
            //                           OR (estado_adjudicacion = 'REVISION_MODIFICADO_FDI' AND ${idRol} = 17)
            //                           OR ("proyecto".fid_autoridad_beneficiaria = ${idUsuario} AND ${idRol} = 18)) `),
            order: sequelize.literal(`id_adjudicacion ASC`)
          },
          {
            attributes: ['codigo', 'nombre', 'tipo', 'acciones', 'atributos', 'requeridos', 'areas', ['fid_rol', 'roles']],
            model: sequelize.models.estado,
            as: 'estado_actual',
            required: true,
            where: sequelize.literal(`(
              (
                ( ${idRol} = ANY(estado_actual.fid_rol) )
                AND (
                    (fid_usuario_asignado = ${idUsuario} and proyecto.estado_proyecto != 'REGISTRO_SOLICITUD')
                    OR (${idRol} <> ALL(estado_actual.fid_rol_asignado) AND proyecto.estado_proyecto != 'REGISTRO_SOLICITUD')
                    OR (proyecto.estado_proyecto = 'REGISTRO_SOLICITUD' AND proyecto._usuario_creacion = ${idUsuario} )
                )
                AND ( estado_actual.tipo <> 'FIN' )
                AND ( "proyecto".estado_proyecto NOT IN ('ADJUDICACION','REGISTRO_EMPRESA') )
              )
              OR
              (
                ( "proyecto".estado_proyecto IN('ADJUDICACION','REGISTRO_EMPRESA') )
                AND ( (${idRol} <> ALL(estado_actual.fid_rol_asignado) ) )
                AND ( "proyecto".matricula = '${matricula}' )
              )
              OR
              (
                ( estado_proyecto = 'APROBADO_FDI' OR estado_proyecto = 'EN_EJECUCION_FDI' )
                AND (
                  (
                    (
                      SELECT COUNT(*)
                      FROM supervision sup
                      WHERE ( "proyecto".id_proyecto = sup.fid_proyecto )
                      AND ( sup.estado_supervision IN(${estadosRolSTR}) )
                      AND (
                        (sup.estado_supervision <> 'EN_TECNICO_FDI')
                        OR (
                          (sup.estado_supervision = 'EN_TECNICO_FDI')
                          AND (fid_usuario_asignado = ${idUsuario})
                        )
                      )
                    ) > 0
                  )
                  OR (
                    (
                      SELECT count(*)
                      FROM adjudicacion a, equipo_tecnico et
                      WHERE a.fid_proyecto = "proyecto".id_proyecto
                      AND et.fid_adjudicacion = a.id_adjudicacion
                      AND et.fid_usuario = ${idUsuario}
                      AND et.fid_rol IN (16,17)
                      AND et.fid_rol = ${idRol}
                      AND et.estado <> 'INACTIVO'
                    )
                  ) > 0
                )
              )
              OR
              (
                ( estado_proyecto IN ('ASIGNACION_EQUIPO_TECNICO_FDI','APROBADO_FDI','EN_EJECUCION_FDI') )
                AND ( "proyecto".fid_autoridad_beneficiaria = '${idAutoridad}' )
                AND ( "proyecto".fid_autoridad_beneficiaria > 0 )
              )
              OR
              (
                (SELECT count(*) FROM adjudicacion a WHERE a.fid_proyecto = "proyecto".id_proyecto and a.estado_adjudicacion = 'REVISION_MODIFICADO_FDI' AND ${idRol} IN(17) )>0
              )
              OR(
                (SELECT count(*) FROM adjudicacion a WHERE a.fid_proyecto = "proyecto".id_proyecto and a.estado_adjudicacion = 'NO_OBJECION_FDI' AND ${idRol} IN(13) )>0
              )
              OR
              (
                ( estado_proyecto IN ('EN_EJECUCION_FDI') )
                AND ( ${idRol} IN (15,16) )
              )
              OR
              (
                ( estado_proyecto IN ('EN_EJECUCION_FDI') )
                AND ( ${idRol} IN (14) )
                AND ( req_extension_convenio = TRUE )
              )
            )`)
          }, {
            attributes: ['codigo', 'nombre', 'codigo_provincia', 'point'],
            model: sequelize.models.municipio,
            as: 'municipio',
            where: consulta.condicionMunicipio,
            include: {
              attributes: ['codigo', 'nombre', 'codigo_departamento'],
              model: sequelize.models.provincia,
              as: 'provincia',
              include: {
                attributes: ['codigo', 'nombre'],
                model: sequelize.models.departamento,
                as: 'departamento'
              }
            }
          }, {
            attributes: ['id_usuario'],
            model: sequelize.models.usuario,
            as: 'usuario',
            where: sequelize.literal(`false OR usuario.id_usuario = ${idUsuario}`)
          },{
            model: sequelize.models.transicion,
            as: 'observacion',
            attributes: [['observacion', 'mensaje'], 'fid_usuario'],
            where: {
              fid_usuario_asignado: null,
              tipo_transicion: 'proyecto',
              codigo_estado: {
                $eq: sequelize.col('proyecto.estado_proyecto')
              }
            },
            required: false,
            // required: esLegal,
            include: [
              {
                attributes: ['id_usuario', 'fid_persona'],
                model: sequelize.models.usuario,
                as: 'usuario',
                include: [
                  {
                    attributes: ['nombres', 'primer_apellido', 'segundo_apellido'],
                    model: sequelize.models.persona,
                    as: 'persona'
                  }
                ]
              }
            ]
          }, {
            attributes: ['nombre'],
            model: sequelize.models.parametrica,
            as: 'tipo_proyecto'
          },
          {
            model: sequelize.models.supervision,
            as: 'supervisiones',
            attributes: ['id_supervision', 'fid_adjudicacion', 'fid_proyecto', 'nro_supervision', 'estado_supervision'],
            required: false,
            include: [
              {
                attributes: ['id_adjudicacion', 'referencia', 'version', 'monto'],
                model: sequelize.models.adjudicacion,
                as: 'adjudicacion'
              },
              {
                attributes: ['codigo', 'codigo_proceso', 'nombre', 'tipo', 'fid_rol'],
                model: sequelize.models.estado,
                as: 'estado_actual'
              },
              {
                model: sequelize.models.transicion,
                as: 'observacion_proceso',
                attributes: [['observacion', 'mensaje'], 'fid_usuario'],
                where: {
                  fid_usuario_asignado: null,
                  codigo_estado: {
                    $eq: sequelize.col('supervisiones.estado_supervision')
                  }
                },
                include: [
                  {
                    attributes: ['id_usuario', 'fid_persona'],
                    model: sequelize.models.usuario,
                    as: 'usuario',
                    include: [
                      {
                        attributes: ['nombres', 'primer_apellido', 'segundo_apellido'],
                        model: sequelize.models.persona,
                        as: 'persona'
                      }
                    ]
                  }
                ],
                required: false
              }
            ]
          }
        ],
        where: {
          $and: [
            consulta.condicionProyecto,
            sequelize.literal(`(
              ( substring(proyecto.fcod_municipio, 1, 2) = ANY(usuario.fcod_departamento) AND proyecto.estado = 'ACTIVO')
            )`)
          ]
        },
        offset: consulta.offset,
        limit: consulta.limit,
        order: consulta.order || '_fecha_creacion',
        distinct: true
      };
      // console.log('OPTIONS = ');
      // require('../../lib/util').log(options);
      if (typeof(options.order) === 'string') {
        options.order = options.order.indexOf('supervisiones.estado_actual')>=0? '': sequelize.literal(options.order);
      }
      return sequelize.models.proyecto.findAndCountAll(options);
    },


    // Servicio /proyectos
    proyectoIncluye: (consulta, idRol, idUsuario, idAutoridad, esSupervisorOFiscal, esLegal, estadosRol, matricula) => {
      if (esSupervisorOFiscal) {
        if (!consulta.condicionEquipo_tecnico) {
          consulta.condicionEquipo_tecnico = {
            fid_usuario: idUsuario,
            fid_rol: idRol,
            estado: 'ACTIVO'
          };
        }
      }
      let estadosRolSTR = util.convertirArrayAString(estadosRol) || `'DESCONOCIDO'`; // ['A','B','C']   =>  "'A','B','C'"
      const options = {
        include: [
          {
            model: sequelize.models.contrato,
            as: 'contrato',
            include: [
              {
                model: sequelize.models.empresa,
                as: 'empresa'
              }
            ],
            required: false
          }, {
            model: sequelize.models.equipo_tecnico,
            as: 'equipo_tecnico',
            required: esSupervisorOFiscal,
            where: consulta.condicionEquipo_tecnico
          }, {
            model: sequelize.models.adjudicacion,
            as: 'adjudicaciones',
            include: [
              {
                attributes: ['id_supervision', 'fid_adjudicacion', 'fid_proyecto', 'nro_supervision', 'estado_supervision'],
                model: sequelize.models.supervision,
                as: 'supervision',
                required: false,
                include: [
                  {
                    attributes: ['codigo', 'codigo_proceso', 'nombre', 'tipo', 'fid_rol'],
                    model: sequelize.models.estado,
                    as: 'estado_actual'
                  }
                ],
                where: sequelize.literal(`estado_supervision NOT IN ('EN_ARCHIVO_FDI','EN_FISCAL_FDI')`)
              }, {
                attributes: ['codigo', 'codigo_proceso', 'nombre', 'tipo', 'fid_rol'],
                model: sequelize.models.estado,
                as: 'estado_actual'
              }
            ],
            required: false,
            where: {
              estado: 'ACTIVO'
            },
            // where: sequelize.literal(`(id_adjudicacion IN (SELECT fid_adjudicacion FROM equipo_tecnico WHERE fid_rol = ${idRol} and fid_usuario = ${idUsuario})
            //                           OR (estado_adjudicacion = 'REVISION_MODIFICADO_FDI' AND ${idRol} = 17)
            //                           OR ("proyecto".fid_autoridad_beneficiaria = ${idUsuario} AND ${idRol} = 18)) `),
            order: sequelize.literal(`id_adjudicacion ASC`)
          },
          {
            model: sequelize.models.boleta,
            as: 'boletas',
            required: false
          }, {
            attributes: ['codigo', 'nombre', 'tipo', 'acciones', 'atributos', 'requeridos', 'areas', ['fid_rol', 'roles']],
            model: sequelize.models.estado,
            as: 'estado_actual',
            required: true,
            where: sequelize.literal(`(
              (
                ( ${idRol} = ANY(estado_actual.fid_rol) )
                AND (
                    (fid_usuario_asignado = ${idUsuario} and proyecto.estado_proyecto != 'REGISTRO_SOLICITUD')
                    OR (${idRol} <> ALL(estado_actual.fid_rol_asignado) AND proyecto.estado_proyecto != 'REGISTRO_SOLICITUD')
                    OR (proyecto.estado_proyecto = 'REGISTRO_SOLICITUD' AND proyecto._usuario_creacion = ${idUsuario} )
                )
                AND ( estado_actual.tipo <> 'FIN' )
                AND ( "proyecto".estado_proyecto NOT IN ('ADJUDICACION','REGISTRO_EMPRESA') )
              )
              OR
              (
                ( "proyecto".estado_proyecto IN('ADJUDICACION','REGISTRO_EMPRESA') )
                AND ( (${idRol} <> ALL(estado_actual.fid_rol_asignado) ) )
                AND ( "proyecto".matricula = '${matricula}' )
              )
              OR
              (
                ( estado_proyecto = 'APROBADO_FDI' OR estado_proyecto = 'EN_EJECUCION_FDI' )
                AND (
                  (
                    (
                      SELECT COUNT(*)
                      FROM supervision sup
                      WHERE ( "proyecto".id_proyecto = sup.fid_proyecto )
                      AND ( sup.estado_supervision IN(${estadosRolSTR}) )
                      AND (
                        (sup.estado_supervision <> 'EN_TECNICO_FDI')
                        OR (
                          (sup.estado_supervision = 'EN_TECNICO_FDI')
                          AND (fid_usuario_asignado = ${idUsuario})
                        )
                      )
                    ) > 0
                  )
                  OR (
                    (
                      SELECT count(*)
                      FROM adjudicacion a, equipo_tecnico et
                      WHERE a.fid_proyecto = "proyecto".id_proyecto
                      AND et.fid_adjudicacion = a.id_adjudicacion
                      AND et.fid_usuario = ${idUsuario}
                      AND et.fid_rol IN (16,17)
                      AND et.fid_rol = ${idRol}
                      AND et.estado <> 'INACTIVO'
                    )
                  ) > 0
                )
              )
              OR
              (
                ( estado_proyecto IN ('ASIGNACION_EQUIPO_TECNICO_FDI','APROBADO_FDI','EN_EJECUCION_FDI') )
                AND ( "proyecto".fid_autoridad_beneficiaria = '${idAutoridad}' )
                AND ( "proyecto".fid_autoridad_beneficiaria > 0 )
              )
              OR
              (
                (SELECT count(*) FROM adjudicacion a WHERE a.fid_proyecto = "proyecto".id_proyecto and a.estado_adjudicacion = 'REVISION_MODIFICADO_FDI' AND ${idRol} IN(17) )>0
              )
              OR(
                (SELECT count(*) FROM adjudicacion a WHERE a.fid_proyecto = "proyecto".id_proyecto and a.estado_adjudicacion = 'NO_OBJECION_FDI' AND ${idRol} IN(13) )>0
              )
              OR
              (
                ( estado_proyecto IN ('EN_EJECUCION_FDI') )
                AND ( ${idRol} IN (15,16) )
              )
              OR
              (
                ( estado_proyecto IN ('EN_EJECUCION_FDI') )
                AND ( ${idRol} IN (14) )
                AND ( req_extension_convenio = TRUE )
              )
            )`)
          }, {
            attributes: ['codigo', 'nombre', 'codigo_provincia', 'point'],
            model: sequelize.models.municipio,
            as: 'municipio',
            where: consulta.condicionMunicipio,
            include: {
              attributes: ['codigo', 'nombre', 'codigo_departamento'],
              model: sequelize.models.provincia,
              as: 'provincia',
              include: {
                attributes: ['codigo', 'nombre'],
                model: sequelize.models.departamento,
                as: 'departamento'
              }
            }
          }, {
            attributes: ['id_usuario'],
            model: sequelize.models.usuario,
            as: 'usuario',
            where: sequelize.literal(`false OR usuario.id_usuario = ${idUsuario}`)
          }, {
            attributes: ['tipo_documento', 'numero_documento', 'complemento', 'complemento_visible', 'fecha_nacimiento', 'nombres', 'primer_apellido', 'segundo_apellido', 'correo', 'telefono', 'estado'],
            model: sequelize.models.persona,
            as: 'autoridad_beneficiaria'
          }, {
            model: sequelize.models.transicion,
            as: 'observacion',
            attributes: [['observacion', 'mensaje'], 'fid_usuario'],
            where: {
              fid_usuario_asignado: null,
              tipo_transicion: 'proyecto',
              codigo_estado: {
                $eq: sequelize.col('proyecto.estado_proyecto')
              }
            },
            required: false,
            // required: esLegal,
            include: [
              {
                attributes: ['id_usuario', 'fid_persona'],
                model: sequelize.models.usuario,
                as: 'usuario',
                include: [
                  {
                    attributes: ['nombres', 'primer_apellido', 'segundo_apellido'],
                    model: sequelize.models.persona,
                    as: 'persona'
                  }
                ]
              }
            ]
          }, {
            attributes: ['nombre'],
            model: sequelize.models.parametrica,
            as: 'tipo_proyecto'
          },
          {
            model: sequelize.models.supervision,
            as: 'supervisiones',
            attributes: ['id_supervision', 'fid_adjudicacion', 'fid_proyecto', 'nro_supervision', 'estado_supervision'],
            required: false,
            include: [
              {
                attributes: ['id_adjudicacion', 'referencia', 'version', 'monto'],
                model: sequelize.models.adjudicacion,
                as: 'adjudicacion'
              },
              {
                attributes: ['codigo', 'codigo_proceso', 'nombre', 'tipo', 'fid_rol'],
                model: sequelize.models.estado,
                as: 'estado_actual'
              },
              {
                model: sequelize.models.transicion,
                as: 'observacion_proceso',
                attributes: [['observacion', 'mensaje'], 'fid_usuario'],
                where: {
                  fid_usuario_asignado: null,
                  codigo_estado: {
                    $eq: sequelize.col('supervisiones.estado_supervision')
                  }
                },
                include: [
                  {
                    attributes: ['id_usuario', 'fid_persona'],
                    model: sequelize.models.usuario,
                    as: 'usuario',
                    include: [
                      {
                        attributes: ['nombres', 'primer_apellido', 'segundo_apellido'],
                        model: sequelize.models.persona,
                        as: 'persona'
                      }
                    ]
                  }
                ],
                required: false
              }
            ]
          },
          {
            model: sequelize.models.formulario,
            attributes: ['form_model', 'codigo_estado', 'fid_plantilla', 'nombre'],
            as: 'formularios'
          }
        ],
        where: {
          $and: [
            consulta.condicionProyecto,
            sequelize.literal(`(
              ( substring(proyecto.fcod_municipio, 1, 2) = ANY(usuario.fcod_departamento) AND proyecto.estado = 'ACTIVO')
            )`)
          ]
        },
        offset: consulta.offset,
        limit: consulta.limit,
        order: consulta.order || '_fecha_creacion',
        distinct: true
      };
      // console.log('OPTIONS = ');
      // require('../../lib/util').log(options);
      if (typeof(options.order) === 'string') {
        options.order = options.order.indexOf('supervisiones.estado_actual')>=0? '': sequelize.literal(options.order);
      }
      return sequelize.models.proyecto.findAndCountAll(options);
    },
    proyectoIncluyeMatricula: (consulta, idRol) => sequelize.models.proyecto.findAndCount({
      include: [
        {
          model: sequelize.models.contrato,
          as: 'contrato',
          include: [
            {
              model: sequelize.models.empresa,
              as: 'empresa'
            }
          ],
          required: false
        }, {
          model: sequelize.models.equipo_tecnico,
          as: 'equipo_tecnico',
          required: false,
          include: [
            {
              model: sequelize.models.usuario,
              as: 'usuario',
              include: [
                {
                  model: sequelize.models.persona,
                  as: 'persona',
                  where: consulta.condicionPersona
                }
              ],
              where: consulta.condicionUsuario
            }
          ],
          where: consulta.condicionEquipo_tecnico
        }, {
          attributes: ['codigo', 'acciones', 'atributos', 'requeridos', 'areas', ['fid_rol', 'roles']],
          model: sequelize.models.estado,
          as: 'estado_actual',
          required: true
          // TODO comentado para que no salga error al hacer supervisiones con el rol supervisor, fiscal, tecnico
          // where: sequelize.literal(`${idRol} = ANY(estado_actual.fid_rol)`)
        }, {
          attributes: ['codigo', 'nombre', 'codigo_provincia'],
          model: sequelize.models.municipio,
          as: 'municipio',
          include: {
            attributes: ['codigo', 'nombre', 'codigo_departamento'],
            model: sequelize.models.provincia,
            as: 'provincia',
            include: {
              attributes: ['codigo', 'nombre'],
              model: sequelize.models.departamento,
              as: 'departamento'
            }
          }
        }, {
          attributes: ['tipo_documento', 'numero_documento', 'fecha_nacimiento', 'nombres', 'primer_apellido', 'segundo_apellido', 'correo', 'telefono'],
          model: sequelize.models.persona,
          as: 'autoridad_beneficiaria'
        }, {
          model: sequelize.models.transicion,
          as: 'observacion',
          attributes: [['observacion', 'mensaje'], 'fid_usuario'],
          where: {
            fid_usuario_asignado: null,
            codigo_estado: {
              $eq: sequelize.col('proyecto.estado_proyecto')
            }
          },
          required: false,
          // required: esLegal,
          include: [
            {
              attributes: ['id_usuario', 'fid_persona'],
              model: sequelize.models.usuario,
              as: 'usuario',
              include: [
                {
                  attributes: ['nombres', 'primer_apellido', 'segundo_apellido'],
                  model: sequelize.models.persona,
                  as: 'persona'
                }
              ]
            }
          ]
        }
      ],
      where: {
        $and: [
          consulta.condicionProyecto
        ]
      },
      offset: consulta.offset,
      limit: consulta.limit,
      order: consulta.order,
      distinct: true
    }),
    proyectoProcesado: (consulta, idUsuario) => sequelize.models.proyecto.findAndCount({
      include: [
        {
          model: sequelize.models.equipo_tecnico,
          as: 'equipo_tecnico',
          required: false,
          where: {
            fid_usuario: idUsuario
          }
        }, {
          model: sequelize.models.contrato,
          as: 'contrato',
          include: [
            {
              model: sequelize.models.empresa,
              as: 'empresa'
            }
          ],
          required: false
        }, {
          attributes: ['tipo_documento', 'tipo_boleta', 'numero', 'monto', 'fecha_inicio_validez', 'fecha_fin_validez'],
          model: sequelize.models.boleta,
          as: 'boletas',
          require: false
        }, {
          attributes: ['codigo', 'nombre', 'areas'],
          model: sequelize.models.estado,
          as: 'estado_actual'
        }, {
          attributes: ['nombre'],
          model: sequelize.models.parametrica,
          as: 'tipo_proyecto'
        }, {
          attributes: ['codigo', 'nombre', 'codigo_provincia'],
          model: sequelize.models.municipio,
          as: 'municipio',
          include: {
            attributes: ['codigo', 'nombre', 'codigo_departamento'],
            model: sequelize.models.provincia,
            as: 'provincia',
            include: {
              attributes: ['codigo', 'nombre'],
              model: sequelize.models.departamento,
              as: 'departamento'
            }
          }
        }, {
          attributes: ['tipo_documento', 'numero_documento', 'fecha_nacimiento', 'nombres', 'primer_apellido', 'segundo_apellido', 'correo', 'telefono'],
          model: sequelize.models.persona,
          as: 'autoridad_beneficiaria'
        }
      ],
      where: consulta.condicionProyecto,
      offset: consulta.offset,
      limit: consulta.limit,
      order: consulta.order,
      distinct: true
    }),
    proyectoProcesadoMovil: (consulta, idUsuario, idRol) => sequelize.models.proyecto.findAndCount({
      include: [
        {
          model: sequelize.models.equipo_tecnico,
          as: 'equipo_tecnico',
          required: false,
          where: {
            fid_usuario: idUsuario,
            fid_rol: idRol
          }
        }, {
          model: sequelize.models.contrato,
          as: 'contrato',
          include: [
            {
              model: sequelize.models.empresa,
              as: 'empresa'
            }
          ],
          required: false
        }, {
          attributes: ['tipo_documento', 'tipo_boleta', 'numero', 'monto', 'fecha_inicio_validez', 'fecha_fin_validez'],
          model: sequelize.models.boleta,
          as: 'boletas',
          require: false
        }, {
          attributes: ['codigo', 'nombre', 'areas'],
          model: sequelize.models.estado,
          as: 'estado_actual'
        }, {
          attributes: ['nombre'],
          model: sequelize.models.parametrica,
          as: 'tipo_proyecto'
        }, {
          attributes: ['codigo', 'nombre', 'geom', 'codigo_provincia'],
          model: sequelize.models.municipio,
          as: 'municipio',
          include: {
            attributes: ['codigo', 'nombre', 'codigo_departamento'],
            model: sequelize.models.provincia,
            as: 'provincia',
            include: {
              attributes: ['codigo', 'nombre'],
              model: sequelize.models.departamento,
              as: 'departamento'
            }
          }
        }, {
          attributes: ['tipo_documento', 'numero_documento', 'fecha_nacimiento', 'nombres', 'primer_apellido', 'segundo_apellido', 'correo', 'telefono'],
          model: sequelize.models.persona,
          as: 'autoridad_beneficiaria'
        }
      ],
      where: consulta.condicionProyecto,
      offset: consulta.offset,
      limit: consulta.limit,
      order: consulta.order,
      distinct: true
    }),
    proyectoProcesado2: (consulta, idUsuario) => sequelize.models.proyecto.findAndCount({
      include: [
        {
          attributes: ['id_transicion', 'codigo_estado'],
          model: sequelize.models.transicion,
          as: 'transicion',
          required: true,
          where: {
            fid_usuario_asignado: idUsuario
          }
        }
      ],
      where: consulta.condicionProyecto,
      offset: consulta.offset,
      limit: consulta.limit,
      order: consulta.order,
      distinct: true
    }),
    usuarios: (idProyecto, t) => sequelize.query(`SELECT u.id_usuario, concat(p.nombres, ' ', p.primer_apellido, ' ', p.segundo_apellido) nombre, r.nombre rol, r.descripcion rol_nombre, r.id_rol
      FROM persona p, usuario u, usuario_rol ur, rol r, proyecto py
      WHERE p.id_persona = u.fid_persona
      AND u.id_usuario = ur.fid_usuario
      AND ur.fid_rol = r.id_rol
      AND r.id_rol IN(4,5)
      AND substring(py.fcod_municipio, 1, 2) = ANY(u.fcod_departamento)
      AND p.estado='ACTIVO' AND u.estado='ACTIVO' AND ur.estado='ACTIVO' AND r.estado='ACTIVO'
      AND py.id_proyecto = ?`, {
        replacements: [ idProyecto ],
        type: sequelize.QueryTypes.SELECT,
        transaction: t
      }
    )
  });

  return proyecto;
};
