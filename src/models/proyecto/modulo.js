/**
 * Módulo par documento
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const modulo = sequelize.define('modulo', {
    id_modulo: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      xlabel: 'ID'
    },
    fid_proyecto: {
      type: DataType.INTEGER,
      xlabel: 'Proyecto',
      allowNull: false
    },
    fid_adjudicacion: {
      type: DataType.INTEGER,
      xlabel: 'Adjudicacion',
      allowNull: false
    },
    nombre: {
      type: DataType.STRING,
      xlabel: 'Nombre',
      allowNull: false
    },
    precio: {
      type: DataType.FLOAT,
      xlabel: 'Precio de items del módulo',
      allowNull: false,
      defaultValue: 0
    },
    estado: {
      type: DataType.STRING(),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'MODIFICACION', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'modulo',
    classMethods: {}
  });

  Object.assign(modulo, {
    // Creando asociaciones para la entidad
    associate: (models) => {
      models.modulo.belongsTo(models.proyecto, {
        as: 'proyecto',
        foreignKey: { name: 'fid_proyecto', xchoice: 'proyectos', xlabel: 'Proyecto' },
        targetKey: 'id_proyecto'
      });
      models.modulo.belongsTo(models.adjudicacion, {
        as: 'adjudicacion',
        foreignKey: { name: 'fid_adjudicacion', xchoice: 'adjudicaciones', xlabel: 'Adjudicacion' },
        targetKey: 'id_adjudicacion'
      });
      models.modulo.hasMany(models.item, {
        as: 'items',
        foreignKey: 'fid_modulo'
      });
    },
    moduloIncluye: (consulta) => sequelize.models.modulo.findAndCount({
      include: [
        {
          model: sequelize.models.item,
          as: 'items',
          where: {
            $and: [
              {
                estado: { $ne: 'ELIMINADO' }
              },
              consulta.condicionItem
            ]
          },
          required: false
        }
      ],
      where: {
        $and: [
          {
            estado: { $ne: 'ELIMINADO' }
          },
          consulta.condicionModulo
        ]
      }
    }),
    moduloConsulta: (consulta) => sequelize.models.modulo.findAndCount({
      // include: [
      //   {
      //     model: sequelize.models.item,
      //     as: 'items',
      //     where: {
      //       $and: [
      //         {
      //           estado: { $ne: 'ELIMINADO' }
      //         },
      //         consulta.condicionItem
      //       ]
      //     },
      //     required: false
      //   }
      // ],
      where: {
        $and: [
          {
            estado: { $ne: 'ELIMINADO' }
          },
          consulta.condicionModulo
        ]
      }
    })
  });

  return modulo;
};
