/**
 * Módulo para computo_metrico
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const computoMetrico = sequelize.define('computo_metrico', {
    id_computo_metrico: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      xlabel: 'ID'
    },
    fid_item: {
      type: DataType.INTEGER,
      xlabel: 'Item',
      allowNull: false
    },
    fid_supervision: {
      type: DataType.INTEGER,
      xlabel: 'Supervisión',
      allowNull: false
    },
    orden: {
      type: DataType.INTEGER,
      xlabel: 'Orden',
      allowNull: true
    },
    descripcion: {
      type: DataType.STRING,
      xlabel: 'Descripcion',
      allowNull: false
    },
    cantidad: {
      type: DataType.FLOAT,
      xlabel: 'Cantidad',
      allowNull: true
    },
    largo: {
      type: DataType.FLOAT,
      xlabel: 'Largo',
      allowNull: true
    },
    ancho: {
      type: DataType.FLOAT,
      xlabel: 'Ancho',
      allowNull: true
    },
    alto: {
      type: DataType.FLOAT,
      xlabel: 'Alto',
      allowNull: true
    },
    factor_forma: {
      type: DataType.FLOAT,
      xlabel: 'Factor de forma',
      allowNull: true
    },
    area: {
      type: DataType.FLOAT,
      xlabel: 'Área',
      allowNull: true
    },
    volumen: {
      type: DataType.FLOAT,
      xlabel: 'Volumen',
      allowNull: true
    },
    cantidad_parcial: {
      type: DataType.FLOAT,
      xlabel: 'Cantidad parcial',
      allowNull: false
    },
    subtitulo: {
      type: DataType.STRING(255),
      allowNull: true
    },
    uuid: {
      type: DataType.STRING(36),
      allowNull: true
    },
    estado: {
      type: DataType.STRING(10),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    fecha_creacion: {
      type: DataType.DATE,
      xlabel: 'Fecha de creación movil',
      allowNull: true
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    },
    fecha_modificacion: {
      type: DataType.DATE,
      xlabel: 'Fecha de modificación movil',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'computo_metrico',
    classMethods: {}
  });

  Object.assign(computoMetrico, {
    // Creando asociaciones para la entidad
    associate: (models) => {
      models.computo_metrico.belongsTo(models.item, {
        as: 'item',
        foreignKey: { name: 'fid_item', xchoice: 'items', xlabel: 'Item' },
        targetKey: 'id_item'
      });
      models.computo_metrico.belongsTo(models.supervision, {
        as: 'supervision',
        foreignKey: { name: 'fid_supervision', xchoice: 'supervisiones', xlabel: 'Supervisión' },
        targetKey: 'id_supervision'
      });
    },
    getSumCantidadParcial: async (idItem, eq, idSupervision, idComputoMetrico) => {
      const consulta = {
        attributes: [[sequelize.fn('COALESCE', sequelize.fn('SUM', sequelize.col('cantidad_parcial')), 0), 'suma']],
        where: {
          fid_item: idItem,
          estado: { $ne: 'ELIMINADO' }
        }
      };
      switch (eq) {
        case 'eq':
          consulta.where.fid_supervision = { $eq: idSupervision };
          break;
        case 'lt':
          consulta.where.fid_supervision = { $lt: idSupervision };
          break;
        case 'lte':
          consulta.where.fid_supervision = { $lte: idSupervision };
          break;
      }
      if (idComputoMetrico) {
        consulta.where.id_computo_metrico = { $ne: idComputoMetrico };
      }
      const SUMA = await sequelize.models.computo_metrico.findOne(consulta);
      return SUMA.dataValues.suma;
    }
  });

  return computoMetrico;
};
