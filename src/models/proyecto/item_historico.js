/**
 * Módulo par documento
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const itemHistorico = sequelize.define('item_historico', {
    id_item: {
      type: DataType.INTEGER,
      primaryKey: true,
      xlabel: 'ID'
    },
    fid_modulo: {
      type: DataType.INTEGER,
      xlabel: 'Modulo',
      allowNull: true
    },
    version: {
      type: DataType.INTEGER,
      primaryKey: true,
      xlabel: 'Version'
    },
    nro_item: {
      type: DataType.INTEGER,
      xlabel: 'Nro item',
      allowNull: false
    },
    nombre: {
      type: DataType.STRING,
      xlabel: 'Nombre',
      allowNull: false
    },
    unidad_medida: {
      type: DataType.STRING(50),
      xlabel: 'Unidad de medida',
      allowNull: false
    },
    cantidad: {
      type: DataType.FLOAT,
      xlabel: 'Cantidad',
      allowNull: false
    },
    precio_unitario: {
      type: DataType.FLOAT,
      xlabel: 'Precio unitario',
      allowNull: true
    },
    peso_ponderado: {
      type: DataType.FLOAT,
      xlabel: 'Peso ponderado',
      allowNull: false,
      defaultValue: 1
    },
    cantidad_anterior_fis: {
      type: DataType.FLOAT,
      xlabel: 'Cantidad física acumulada en computos previos',
      allowNull: false,
      defaultValue: 0
    },
    cantidad_actual_fis: {
      type: DataType.FLOAT,
      xlabel: 'Cantidad física actual de computos metricos',
      allowNull: false,
      defaultValue: 0
    },
    tiempo_ejecucion: {
      type: DataType.INTEGER,
      xlabel: 'Tiempo de ejecución',
      allowNull: true
    },
    avance_total: {
      type: DataType.FLOAT,
      xlabel: 'Avance total',
      allowNull: true
    },
    especificaciones: {
      type: DataType.STRING,
      xlabel: 'Especificaciones',
      allowNull: true
    },
    analisis_precios: {
      type: DataType.STRING,
      xlabel: 'Análisis de precios unitarios',
      allowNull: true
    },
    regularizar: {
      type: DataType.BOOLEAN,
      allowNull: false,
      xlabel: 'regularizar',
      defaultValue: false
    },
    variacion: {
      type: DataType.FLOAT,
      xlabel: 'variacion',
      allowNull: false,
      defaultValue: 0
    },
    estado: {
      type: DataType.STRING(10),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'CERRADO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'item_historico',
    classMethods: {}
  });

  Object.assign(itemHistorico, {
    // Creando asociaciones para la entidad
    associate: (models) => {
      models.item_historico.belongsTo(models.modulo_historico, {
        as: 'modulo',
        foreignKey: ['fid_modulo', 'version']
      });
      models.item_historico.belongsTo(models.item_historico, {
        as: 'version_anterior',
        foreignKey: 'id_item'
      });
      models.item_historico.belongsTo(models.parametrica, {
        as: 'unidad',
        foreignKey: { name: 'unidad_medida', xchoice: 'unidades', xlabel: 'Unidad de medida' },
        targetKey: 'codigo'
      });
      models.item_historico.hasMany(models.item_historico, {
        as: 'historicos',
        foreignKey: 'id_item'
      });
    }
  });

  return itemHistorico;
};
