/**
 * Módulo par proyecto
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const proyectoHistorico = sequelize.define('proyecto_historico', {
    id_proyecto: {
      type: DataType.INTEGER,
      primaryKey: true,
      xlabel: 'ID'
    },
    version: {
      type: DataType.INTEGER,
      primaryKey: true,
      xlabel: 'Version'
    },
    codigo: {
      type: DataType.STRING(50),
      xlabel: 'Codigo',
      allowNull: true
    },
    tipo: {
      type: DataType.STRING(50),
      xlabel: 'Tipo',
      allowNull: false
    },
    nombre: {
      type: DataType.STRING,
      xlabel: 'Nombre',
      allowNull: false
    },
    nro_convenio: {
      type: DataType.STRING(50),
      xlabel: 'Nro de convenio',
      allowNull: true
    },
    monto_fdi: {
      type: DataType.FLOAT,
      xlabel: 'Monto de financiamiento del FDI',
      allowNull: true
    },
    monto_contraparte: {
      type: DataType.FLOAT,
      xlabel: 'Monto de financiamiento de la contraparte',
      allowNull: true
    },
    monto_total_convenio: {
      type: DataType.FLOAT,
      xlabel: 'Monto total referencial',
      allowNull: true
    },
    monto_total: {
      type: DataType.FLOAT,
      xlabel: 'Monto total',
      allowNull: true
    },
    plazo_ejecucion: {
      type: DataType.INTEGER,
      xlabel: 'Plazo de ejecución',
      allowNull: true
    },
    plazo_ejecucion_convenio: {
      type: DataType.INTEGER,
      xlabel: 'Plazo de ejecución referencial',
      allowNull: true
    },
    plazo_recepcion_definitiva: {
      type: DataType.INTEGER,
      xlabel: 'Plazo en días de recepción definitiva de proyecto',
      allowNull: false,
      defaultValue: 90
    },
    entidad_beneficiaria: {
      type: DataType.STRING(250),
      xlabel: 'Entidad beneficiaria',
      allowNull: true
    },
    fecha_suscripcion_convenio: {
      type: DataType.DATE,
      xlabel: 'Fecha de suscripción del convenio',
      allowNull: true
    },
    doc_convenio: {
      type: DataType.STRING,
      xlabel: 'Documento convenio',
      allowNull: true
    },
    req_extension_convenio: {
      type: DataType.BOOLEAN,
      xlabel: 'Requiere extension de convenio',
      allowNull: false,
      defaultValue: false
    },
    doc_convenio_extendido: {
      type: DataType.STRING,
      xlabel: 'Documento convenio extendido',
      allowNull: true
    },
    doc_convenio_extendido2: {
      type: DataType.STRING,
      xlabel: '2do Documento convenio extendido',
      allowNull: true
    },
    nro_resmin_cife: {
      type: DataType.STRING(100),
      xlabel: 'Nro resolución ministerial CIFE',
      allowNull: true
    },
    doc_resmin_cife: {
      type: DataType.STRING(100),
      xlabel: 'Documento resolución ministerial',
      allowNull: true
    },
    doc_cert_presupuestaria: {
      type: DataType.STRING(100),
      xlabel: 'Documento certificación presupuestaria',
      allowNull: true
    },
    doc_ev_externa: {
      type: DataType.STRING(100),
      xlabel: 'Documento de evaluación externa',
      allowNull: true
    },
    desembolso: {
      type: DataType.FLOAT,
      xlabel: 'Desembolso',
      allowNull: true
    },
    porcentaje_desembolso: {
      type: DataType.FLOAT,
      xlabel: '% Desembolso',
      allowNull: true
    },
    fecha_desembolso: {
      type: DataType.DATE,
      xlabel: 'Fecha de desembolso',
      allowNull: true
    },
    anticipo: {
      type: DataType.FLOAT,
      xlabel: 'Tipo de boleta',
      allowNull: true
    },
    porcentaje_anticipo: {
      type: DataType.FLOAT,
      xlabel: 'Tipo de boleta',
      allowNull: true
    },
    sector: {
      type: DataType.STRING(50),
      xlabel: 'Sector',
      allowNull: true
    },
    fid_autoridad_beneficiaria: {
      type: DataType.INTEGER,
      xlabel: 'Autoridad beneficiaria',
      allowNull: true
    },
    cargo_autoridad_beneficiaria: {
      type: DataType.STRING,
      xlabel: 'Cargo autoridad beneficiaria',
      allowNull: true
    },
    fax_autoridad_beneficiaria: {
      type: DataType.STRING(60),
      xlabel: 'Fax autoridad beneficiaria',
      allowNull: true
    },
    fcod_municipio: {
      type: DataType.STRING(6),
      xlabel: 'Municipio',
      allowNull: true
    },
    coordenadas: {
      type: DataType.GEOMETRY('POINT'),
      xlabel: 'Coordenadas',
      allowNull: true
    },
    doc_especificaciones_tecnicas: {
      type: DataType.STRING(100),
      xlabel: 'Especificaciones técnicas items',
      allowNull: true
    },
    doc_base_contratacion: {
      type: DataType.STRING(100),
      xlabel: 'Documento base de contratación',
      allowNull: true
    },
    orden_proceder: {
      type: DataType.DATE,
      xlabel: 'Orden proceder',
      allowNull: true
    },
    fecha_fin_proyecto: {
      type: DataType.DATE,
      xlabel: 'Fecha fin del proyecto',
      allowNull: true
    },
    codigo_acceso: {
      type: DataType.STRING(20),
      xlabel: 'Código de acceso para la empresa',
      allowNull: true
    },
    matricula: {
      type: DataType.STRING(50),
      xlabel: 'Matrícula',
      allowNull: true
    },
    cod_documentacion_exp: {
      type: DataType.STRING(20),
      xlabel: 'Codigo experiencia',
      allowNull: true
    },
    documentacion_exp: {
      type: DataType.JSON,
      xlabel: 'Experiencia',
      allowNull: true
    },
    estado_proyecto: {
      type: DataType.STRING(50),
      xlabel: 'Estado del proyecto',
      allowNull: false,
      defaultValue: 'REGISTRO_CONVENIO'
    },
    fid_usuario_asignado: {
      type: DataType.INTEGER,
      xlabel: 'Usuario asignado',
      allowNull: true
    },
    fid_usuario_asignado_rol: {
      type: DataType.INTEGER,
      xlabel: 'Rol Usuario asignado',
      allowNull: true
    },
    nro_invitacion: {
      type: DataType.STRING(50),
      xlabel: 'Número de invitación',
      allowNull: true
    },
    tipo_modificacion: {
      type: DataType.STRING(50),
      xlabel: 'Tipo de modificación',
      allowNull: true
    },
    plazo_ampliacion: {
      type: DataType.JSON,
      xlabel: 'Ampliación de plazo',
      allowNull: 'true'
    },
    doc_respaldo_modificacion: {
      type: DataType.STRING(100),
      xlabel: 'Documento de respaldo a la modificación',
      allowNull: true
    },
    fecha_modificacion: {
      type: DataType.DATE,
      xlabel: 'Fecha de modificación de volúmenes y plazos',
      allowNull: true
    },
    estado: {
      type: DataType.STRING(10),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'proyecto_historico',
    classMethods: {}
  });

  Object.assign(proyectoHistorico, {
    // Creando asociaciones para la entidad
    associate: (models) => {
      models.proyecto_historico.belongsTo(models.parametrica, {
        as: 'tipo_proyecto',
        foreignKey: { name: 'tipo', xchoice: 'tipos', xlabel: 'Tipo' },
        targetKey: 'codigo'
      });
      models.proyecto_historico.belongsTo(models.municipio, {
        as: 'municipio',
        foreignKey: { name: 'fcod_municipio', xchoice: 'municipios', xlabel: 'Municipio' },
        targetKey: 'codigo'
      });
      models.proyecto_historico.hasMany(models.contrato, {
        as: 'contrato',
        foreignKey: 'fid_proyecto'
      });
      models.proyecto_historico.hasMany(models.equipo_tecnico, {
        as: 'equipo_tecnico',
        foreignKey: 'fid_proyecto'
      });
      models.proyecto_historico.hasMany(models.boleta, {
        as: 'boletas',
        foreignKey: 'fid_proyecto'
      });
      models.proyecto_historico.belongsTo(models.estado, {
        as: 'estado_actual',
        foreignKey: 'estado_proyecto',
        targetKey: 'codigo'
      });
      models.proyecto_historico.hasMany(models.modulo, {
        as: 'modulos',
        foreignKey: 'fid_proyecto'
      });
      models.proyecto_historico.belongsTo(models.usuario, {
        as: 'usuario',
        foreignKey: 'fid_usuario_asignado',
        targetKey: 'id_usuario',
        constraints: false
      });
      models.proyecto_historico.hasMany(models.transicion, {
        as: 'transicion',
        foreignKey: 'objeto_transicion',
        constraints: false
      });
      models.proyecto_historico.hasOne(models.transicion, {
        as: 'observacion',
        foreignKey: 'objeto_transicion',
        constraints: false
      });
      models.proyecto_historico.belongsTo(models.persona, {
        as: 'autoridad_beneficiaria',
        foreignKey: 'fid_autoridad_beneficiaria',
        targetKey: 'id_persona'
      });
      models.proyecto_historico.hasMany(models.supervision, {
        as: 'supervisiones',
        foreignKey: 'fid_proyecto'
      });
      models.proyecto_historico.belongsTo(models.parametrica, {
        as: 'tipo_modificacion_proyecto',
        foreignKey: { name: 'tipo_modificacion', xchoice: 'tipos', xlabel: 'Tipo Modificación' },
        targetKey: 'codigo'
      });
      models.proyecto_historico.hasMany(models.proyecto_historico, {
        as: 'historicos',
        foreignKey: 'id_proyecto'
      });
    }
  });

  return proyectoHistorico;
};
