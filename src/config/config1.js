import params from './config.json';
import logger from '../lib/logger';

module.exports = () => {
  let env = process.env.NODE_ENV;

  if (!env) {
    env = 'development';
  }

  if (!params.hasOwnProperty(env)) {
    env = 'development';
  }

  const config = {
    database: {
      name: params[env].database,
      username: params[env].username,
      password: params[env].password,
      timezone: '-04:00',
      lang: 'es',
      params: {
        dialect: params[env].dialect,
        port: params[env].port,
        host: params[env].host,
        sync: { force: process.env.FORCE || false
        },
        logging: (sql) => {
          if (env === 'development') {
            logger.log('info', `[${new Date()}] ${sql}`);
          }
        },
        define: {
          underscored: true
        }
      }
    },
    correo: {
      host: 'smtp.entidad.dominio',
      port: 587,
      secure: false,
      ignoreTLS: false,
      tls: { rejectUnauthorized: false },
      auth: {
        user: 'user@example.domain',
        pass: 'pwd'
      }
    },
    sistema: {
      urlBase: 'https://dominio.gob.bo/fdi/#!',
      urlLogin: 'https://dominio.gob.bo/fdi/#!/login',
      urlLogo: 'https://dominio.gob.bo/fdi-api/imagen/logo-fdi.jpg',
      origen: 'SSP <ssp@dominio.gob.bo>',
      soporte: ['user@dominio.gob.bo'],
      idInstitucion: 2,
    },
    api: {
      main: '/api/v1',
      public: '/public',
      crud: '/rest'
    },
    servicio: {
      khipus: '/servicio/khipus/'
    },
    segip: {
      url: 'https://interoperabilidad.agetic.gob.bo',
      path: '/fake/segip/v2/',
      credenciales: {
        apikey: '383a0f19857f4dde885271725bdc1f20'
      },
      tokenKong: 'Bearer <-- solicitar el token a interoperabilidad -->'
    },
    empresas: {
      url: 'http://localhost:4001',
      path: '/service/v1/',
      token: 'Bearer <-- solicitar el token al sistema de empresas -->'
    },
    // configuracion con jwt poner una palabra secreta segura
    jwtSecret: 'AGETIC-2017',
    jwtSession: { session: false },
    puerto: 4000,
    decimales: 2,
    supervision:{
      fotos: {
        requerido: 0
      },
      items:{
        fotos: {
          requerido: 0
        }
      }
    }
  };

  return config;
};
