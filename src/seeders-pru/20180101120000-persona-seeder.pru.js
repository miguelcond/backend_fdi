// 'use strict';
module.exports = {
  up(queryInterface) {
    let random = function(max) {
      return parseInt(Math.random()*max);
    };
    const nom = ['ELIZABETH IVANA','MARIA JULIANA','JEAN FRANCOIS','JENNIFER ARLENE','JUAN RUBEN','ROBERTO CARLOS','MARCO ANTONIO','ROBERTA','JOSE LUIS','HECTOR FABIO'];
    const pat = ['COLQUE','LOVERA','CHOQUE','PEÑARANDA','TERCEROS','OCHOA','CONDORI','ALEJANDRO','GONGORA','HUERTA'];
    const mat = ['ARAMAYO','TUPA','OLLISCO','SANDI','MENDOZA','RAMIREZ','MAYTA','PADILLA','SALMERON','HUACAN'];
    let personas = [];
    for(let i=100; i<2100; i++) {
      personas.push({
        nombres: nom[random(10)],
        primer_apellido: pat[random(10)],
        segundo_apellido: mat[random(10)],
        tipo_documento: 'TD_CI',
        complemento_visible: false,
        numero_documento: `1000${i}`,
        complemento: '00',
        //fecha_nacimiento: `${(i%28+1)<10 ? '0' : ''}${i%28+1}/${random(12)+1}/1990`,
        fecha_nacimiento: `01/01/1990`,
        correo: `email.${i}@macx.tk`,
        lugar_nacimiento_pais: 'BOLIVIA',
        lugar_nacimiento_departamento: 'LA PAZ',
        telefono: '77777777',
        estado: 'ACTIVO',
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      });
    }
    return queryInterface.bulkInsert('persona', personas, {});
  },

  down() {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  },
};
