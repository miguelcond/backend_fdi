const baseIteratee = require('./../../node_modules/lodash/_baseIteratee');
const moment = require('moment');
const fs = require('fs');
const _ = require('lodash');
const NumeroALetras = require('./numeroALetras');
const util = require('util');
const correo = require('./correo');
const config = require('../config/config');
const Big = require('big.js');
const Converter = require('csvtojson');
const jwt = require('jwt-simple');

const funcionCabeceras = (objs) => {
  const cabs = [];
  for (let i = 0; i < objs.length; i++) {
    const obj = objs[i];
    for (const key in obj) {
      const attrName = key;
      // Ocultamos el atributo URL, para no ser mostrado en la vista EJS
      if (attrName === 'url') {} else {
        cabs.push(attrName);
      }
    }
  }
  return cabs;
};

/**
Funcion que asigna un formato a los mensajes de respuesta para una peticion http.
@param {estado} Estado de la peticion http.
@param {mensaje} Mensaje a retornar.
@param {datos} Datos obtenidos o generados para ser retornados.
@return Retorna un {json} con los datos en el formato establecido.
*/
const funcionFormato = (_estado, _mensaje, _datos) => {
  const respuesta = {
    estado: _estado,
    mensaje: _mensaje,
    data: _datos
  };

  return respuesta;
};

/**
* Funcion para paginar y ordenar.
* @param {json} query de la peticion http.
* @return {json} query con datos adicionados.
*/
const paginar = (query) => {
  const respuesta = {};
  // page
  if (query.limit && query.page) {
    respuesta.offset = (query.page - 1) * query.limit;
    respuesta.limit = query.limit;
  }
  // order
  if (query.order) {
    if (query.order.charAt(0) === '-') {
      respuesta.order = `${query.order.substring(1, query.order.length)} DESC`;
    } else {
      respuesta.order = `${query.order}`;
    }
  }
  return respuesta;
};

/**
* Funcion para validar los parametros de la peticion
* @param {Array} Parametros Tiene los valores de la peticion (req.params)
* @param {objReq} Peticion Es el objeto de la peticion (req)
* @return {Array} Con tres valores estado, respuesta (true or false) y mensaje (texto)
*/
const validarParametros = (ParametrosPredefinidos, Peticion) => {
  const Parametros = Object.keys(Peticion.body);
  ParametrosPredefinidos.forEach((value) => {
    if (Parametros.indexOf(value) !== -1) {
      if (!Peticion.body[value]) {
        const respuesta = [];
        respuesta.estado = 412;
        respuesta.respuesta = false;
        respuesta.mensaje = `El dato ${value} es obligatorio.`;
      }
    }
  });
};

/**
* Funcion que genera consulta en sintaxis sequelize.
* @param  {Objeto} pObj     Objeto de parametros de busqueda
* @param  {Objeto} pModelo  Objeto sequelize del modelo base
* @return {Objeto}          Retorna un objeto de consulta en sintaxis sequelize
*/
const formarConsulta = (pObj, pModelo) => {
  // TODO: falta la distincion de busqueda para OR, por el momento se considera OR
  // TODO: considerar los campos relacionados

  // Instancia un objeto contenedor de la respuesta.
  const consulta = {};

  // Establece la lista de campos a omitir en el armado de la consulta.
  const omitir = ['limit', 'page', 'order'];

  // Instancia el objeto contenedor de los modelos relacionados.
  const relacionados = {};

  // Almacena el objeto de campos del modelo base.
  const foraneas = pModelo.associations;

  // Almacena el nombre de la tabla del modelo base.
  const nombreModeloBase = pModelo.getTableName();

  // Agrega los campos del modelo base al contenedor de modelos relacionados.
  relacionados[nombreModeloBase] = pModelo.rawAttributes;

  // TODO: cuantos niveles de relacion se considera?
  // TODO: modificar para hacerlo recursivo, o parametrizar el nivel de busqueda

  // Iteracion de los campos del modelo base.
  for (let k in foraneas) {
    // Almacena la propiedad references (contenedor de relaciones a otros modelos).
    const model = foraneas[k].target;

    // Si el campo es distinto de indefinido, Almacena los campos del modelo relacionado.
    relacionados[model.tableName] = model.rawAttributes;
  }

  // Iteracion de los campos de busqueda.
  for (let k in pObj) {
    // Si el campo de busqueda se encuentra fuera de la lista de omitidos.
    if (omitir.indexOf(k) === -1) {
      // Iteracion de los modelos relacionados.
      // for (let modelo in relacionados) {
        let modelo;
        let campo;
        let nombreCampo;
        let niveles = k.split('.');

        // Almacena el objeto del campo de busqueda.
        if (niveles.length > 2) {
          modelo = niveles[0];
          let submodelo = niveles[1];
          campo = relacionados[modelo] ? (relacionados[modelo][submodelo] ? relacionados[modelo][submodelo][niveles[2].toLowerCase()] : undefined) : undefined;
          nombreCampo = niveles[2];
        } else if (niveles.length > 1) {
          modelo = niveles[0];
          campo = relacionados[modelo] ? relacionados[modelo][niveles[1].toLowerCase()] : undefined;
          nombreCampo = niveles[1];
        } else {
          campo = relacionados[nombreModeloBase][k.toLowerCase()];
          nombreCampo = k;
        }
        // let campo = relacionados[modelo][k.toLowerCase()];

        // Si el campo de busqueda existe dentro del modelo relacionado en iteracion.
        if (campo !== undefined) {
          // Establece el nombre de campo condicion.
          const nombreCondicion = `condicion${_.capitalize(campo.Model.getTableName())}`;

          // Si no existe el objeto de condiciones en el objeto consulta.
          if (!consulta[nombreCondicion]) {
            consulta[nombreCondicion] = { $and: [] };
          }

          // Obtiene el tipo de dato del campo.
          const tipo = campo.type.constructor.key;

          // Instancia un objeto temporal.
          let obj = {};

          // TODO: considerar los otros datatypes del ORM.
          /**
          * Dependiendo del tipo de dato, agrega una consulta de busqueda al objeto
          * @param  {Texto} tipo   Describe el tipo de dato.
          * @return {Objeto}       Adiciona en la consulta del modelo un objeto con sintaxis sequelize.
          */
          switch (tipo) {
            case 'STRING':
              obj[nombreCampo] = {
                $iLike: `%${pObj[k]}%`
              };
              consulta[nombreCondicion].$and.push(obj);
              break;
            case 'INTEGER':
              if (isNaN(parseInt(pObj[k]))) {
                break;
              }
              obj[nombreCampo] = {
                $eq: parseInt(pObj[k])
              };
              consulta[nombreCondicion].$and.push(obj);
              break;
            case 'FLOAT':
              if (isNaN(parseFloat(pObj[k]))) {
                break;
              }
              obj[nombreCampo] = {
                $eq: parseFloat(pObj[k])
              };
              consulta[nombreCondicion].$and.push(obj);
              break;
            case 'DATE':
              obj[nombreCampo] = pObj[k];
              consulta[nombreCondicion].$and.push(obj);
              break;
            case 'ENUM':
              obj[nombreCampo] = pObj[k];
              consulta[nombreCondicion].$and.push(obj);
              break;
          }
        }
      // }
    }
  }

  // Si existen los campos de busqueda "limit" y "page".
  if (pObj.limit && pObj.page) {
    pObj.limit = parseInt(pObj.limit);
    pObj.page = parseInt(pObj.page);
    // Establece el valor de "offset"(paginacion), tomando el cuenta el campo "limit".
    consulta.offset = (pObj.page - 1) * pObj.limit;
    // Establece el valor de "limit".
    consulta.limit = pObj.limit;
  }
  if (pObj.order) {
    if (pObj.order.charAt(0) === '-') consulta.order = `${pObj.order.substr(1, pObj.order.length)} DESC`;
    else consulta.order = `${pObj.order}`;
  }

  // Retorna el objeto con las consultas armadas por modelo.
  return consulta;
};

/**
* Funcion que transforma un objeto del tipo fecha a un formato
* @param  {Objeto} pFecha   Fecha a procesar
* @param  {Texto} pFormato  Formato a aplicar a la fecha
* @return {Objeto}          Fecha con nuevo formato.
*/
const fechaAFormato = (pFecha, pFormato) => {
  let fechaTexto = JSON.stringify(pFecha);
  fechaTexto = fechaTexto? fechaTexto.substr(0, fechaTexto.indexOf('T'))||fechaTexto: '';
  fechaTexto = fechaTexto.replace('"','');
  const formatos = [
    'YYYY-MM-DD', 'DD-MM-YYYY',
    'YYYY/MM/DD', 'DD/MM/YYYY'
  ];
  return moment(fechaTexto, formatos).format(pFormato);
};

const fechaActual = () => {
  let fechaActual = new Date();
  return fechaActual;
}


/**
* Devuelve un objeto en base a otro, incluyendo solamente aquellos campos que sean válidos (diferente de 'undefined').
* Ejemplo: Cuando se recuperan datos desde el body, por seguridad para que no se envíen campos por demas.
* @param {Object} data Objeto que contiene todos los campos.
* @param {String[]} properties Lista de las propiedades a incluir dentro del nuevo objeto.
*/
function crearObjeto (data, properties) {
  let obj = {};
  for (let i in properties) {
    let prop = properties[i];
    if (typeof data[prop] !== 'undefined') {
      obj[prop] = data[prop];
    }
  }
  return obj;
}

/**
* Elimina las propiedades innecesarias de un resultado sequelize,
* devolviendo solamente un objeto únicamente con los registros.
* @param {SequelizeResult} sequelizeData Respuesta de una consulta sequelize.
* @return {Object}
*/
function json (sequelizeData) {
  return JSON.parse(JSON.stringify(sequelizeData));
}

function crearDirectorio (path) {
  if (!fs.existsSync(path)) {
    fs.mkdirSync(path);
  }
}

/**
* Devuelve la edad de una persona a partir de su fecha de nacimiento.
* @param {Date} birthday Fecha de nacimiento.
* @return {Number}
*/
function obtenerEdad (birthday) {
  var ageDifMs = Date.now() - birthday.getTime();
  var ageDate = new Date(ageDifMs);
  return Math.abs(ageDate.getUTCFullYear() - 1970);
}

function sumarDiasAFecha (nroDias, fecha = new Date()) {
  fecha = moment(fecha);
  fecha.add(nroDias, 'day');
  return fecha.format();
}

function log (object, deep = true) {
  console.log(deep ? util.inspect(object, false, null) : object);
}

function enviarEmail (para, titulo, html) {
  correo.enviar({ para, titulo, html }).catch(() => {});
}

const redondear = (numero, decimales) => {
  numero = numero || 0;
  if (typeof numero !== 'number') {
    return;
  }
  decimales = decimales || config().decimales || 2;
  let resultado = new Big(numero);
  return parseFloat(resultado.toFixed(decimales));
};

const _operar = (parametros, conRedondeo = true, decimales, opcion) => {
  let resultado = opcion === 'add' || opcion === 'minus' ? 0 : 1;
  for (let i = 0; i < parametros.length; i++) {
    parametros[i] = typeof parametros[i] === 'number' && !isNaN(parametros[i]) ? parametros[i] : opcion === 'add' || opcion === 'minus' ? 0 : 1;
    let valor = new Big(parametros[i]);
    resultado = valor[opcion](resultado);
  }
  resultado = parseFloat(resultado.valueOf());
  return conRedondeo ? redondear(resultado, decimales) : resultado;
};

const multiplicar = (parametros, redondear, decimales) => {
  return _operar(parametros, redondear, decimales, 'mul');
};

const sumar = (parametros, redondear, decimales) => {
  return _operar(parametros, redondear, decimales, 'add');
};

const restar = (parametro1, parametro2, redondear, decimales) => {
  return _operar([parametro2, parametro1], redondear, decimales, 'minus');
};

const dividir = (parametro1, parametro2, redondear, decimales) => {
  if (parametro2 === 0) {
    return;
  }
  return _operar([parametro2, parametro1], redondear, decimales, 'div');
};

const valorAbsoluto = (valor) => {
  valor = typeof valor === 'number' && !isNaN(valor) ? valor : 0;
  valor = new Big(valor);
  return parseFloat(valor.abs().valueOf());
};

const _sumarDatosLista = (lista, funcionIteracion) => {
  let resultado;
  let index = -1;

  while (++index < lista.length) {
    var valor = funcionIteracion(lista[index]);
    if (valor !== undefined) {
      resultado = resultado === undefined ? valor : sumar([resultado, valor]);
    }
  }
  return resultado;
};

const sumarPor = (lista, funcionIteracion) => {
  return (lista && lista.length) ? _sumarDatosLista(lista, baseIteratee(funcionIteracion, 2)) : 0;
};

 // ['A','B','C']   =>  "'A','B','C'"
function convertirArrayAString (array) {
  let arraySTR = '';
  for (let i in array) {
    arraySTR += `,'${array[i]}'`;
  }
  arraySTR = arraySTR.substr(1);
  return arraySTR;
}

function getNumeroOrdinal (i) {
  if (i === 1 || i === 3) {
    return i + 'er';
  }
  if (i === 2) {
    return i + 'do';
  }
  if (i === 4 || i === 5 || i === 6) {
    return i + 'to';
  }
  if (i === 7 || i === 10) {
    return i + 'mo';
  }
  if (i === 8) {
    return i + 'vo';
  }
  if (i === 9) {
    return i + 'no';
  }
  return i + 'avo';
}

const valorPorcentual = (numero, porcentaje) => {
  return dividir(multiplicar([numero, porcentaje], false), 100);
};

/**
 * Calcula el porcentaje que representa una cantidad con respecto a otra
 * Ejemplo 5 representa el 12.5% de 40
 * @param {Float} parametro1 Cantidad para calcular el porcentaje
 * @param {Float} parametro2 Cantidad total
 */
const calcularPorcentaje = (parametro1, parametro2) => {
  return dividir(multiplicar([parametro1, 100], false), parametro2);
};

function getListaOrdenada (lista, atributoOrdenacion, tipo = 'asc') {
  return _.orderBy(lista, [atributoOrdenacion], [tipo]);
}

function listaACadena (lista) {
  let cadena = '';
  for (let i in lista) {
    cadena += `,'${lista[i]}'`;
  }
  return cadena.substr(1);
}

const esNumero = (valor) => {
  return typeof valor === 'number';
};

/**
* Convierte un número en un string con separador de miles.
* Ejemplos:
*   8432423.455  ->  8,432,423.46
*   8432423.4    ->  8,432,423.40
*   8432423      ->  8,432,423.00
* @param {Number} value Número a convertir (FLOAT).
* @param {Object} options Opciones de configuración.
* @param {String} options.decimalSeparator Caracter que representa al separador de decimales.
* @param {String} options.groupSeparator Caracter que representa al separador de miles.
* @param {String} options.decimals Número de decimales a tomar en cuenta.
* @return {String} Número con separador de miles.
*/
const convertirNumeroAString = (value, options = {}) => {
  const decimalSeparator = options.decimalSeparator || ',';
  const groupSeparator = options.groupSeparator || '.';
  const decimals = options.decimals || 2;
  // Presición
  if (!(value || value === 0)) {
    return '';
  }
  let formattedValue = parseFloat(redondear(value, 2)).toFixed(decimals);
  formattedValue = formattedValue.replace('.', decimalSeparator);
  // Formato
  let parts = ('' + formattedValue).split(decimalSeparator);
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, groupSeparator);
  return parts.join(decimalSeparator);
};

/**
* Convierte un número en su valor literal.
* Ejemplos:
*   8432423.455   ->  OCHO MILLONES CUATROCIENTOS TREINTA Y DOS MIL CUATROCIENTOS VEINTITRES CON 46/100
*   8432423.4     ->  OCHO MILLONES CUATROCIENTOS TREINTA Y DOS MIL CUATROCIENTOS VEINTITRES CON 40/100
*   8432423       ->  OCHO MILLONES CUATROCIENTOS TREINTA Y DOS MIL CUATROCIENTOS VEINTITRES CON 0/100
* @param {Number} value Número a convertir (FLOAT).
* @return {String} Número en formato literal.
*/
const convertirNumeroALiteral = (value) => {
  return (new NumeroALetras()).transformar(value).replace(' Bs.', '');
};


/**
 * Asigna valores de una lista de atributos al objeto "data" de acuerdo a los datos del
 * objeto de entrada
 */
const asignarValorAtributos = (data, objeto, atributos) => {
  // console.log(atributos,"atributos")
  // console.log(objeto,"objeto")
  // console.log(data,"data")
  for (let i = 0; i < atributos.length; i++) {
    const atributo = atributos[i];
    if (objeto[atributo] !== null && objeto[atributo] !== undefined) {
      data[atributo] = objeto[atributo];
    }
  }
};

const esNuloOIndefinido = (valor) => {
  return valor === null || valor === undefined;
};

const csvAjson = async (archivo, delimitador = '\t') => {
  return new Promise((resolve, reject) => {
    var converter = new Converter({
      delimiter: '\t'
    });
    converter.fromFile(archivo, (err, result) => {
      if (!err) {
        return resolve(result);
      }
      return reject(new Error(`Error en el contenido del csv`));
    });
  });
};

function generarToken (data, key, exp = 86400) {
  // Convierte un número en base 36, luego obtiene los 10 primeros caracteres
  // despues del punto decimal
  let tokenId = Math.random().toString(36).substr(2, 10) + '';
  let issuedAt = Date.now();                // tiempo actual en milisegundos
  let expire = issuedAt + (exp * 1000);     // 1 dia = 86400 segundos = 86400000 milisegundos
  let token = {
    iat: issuedAt,         // Issued at: Tiempo en que se generó el token
    jti: tokenId,          // Json Token Id: Identificador único para el token
    exp: expire,           // Tiempo en que el token expirará
    data: data
  };
  return jwt.encode(token, key);
}

function deptoCod2Nombre(cod) {
  switch (cod) {
    case '01': return 'Chuquisaca';
    case '02': return 'La Paz';
    case '03': return 'Cochabamba';
    case '04': return 'Oruro';
    case '05': return 'Potosí';
    case '06': return 'Tarija';
    case '07': return 'Santa Cruz';
    case '08': return 'Beni';
    case '09': return 'Pando';
  }
  return '';
}

module.exports = {
  funcionCabeceras,
  funcionFormato,
  paginar,
  validarParametros,
  formarConsulta,
  fechaAFormato,
  fechaActual,
  crearObjeto,
  json,
  crearDirectorio,
  obtenerEdad,
  sumarDiasAFecha,
  log,
  enviarEmail,
  redondear,
  convertirArrayAString,
  getNumeroOrdinal,
  valorPorcentual,
  calcularPorcentaje,
  getListaOrdenada,
  listaACadena,
  esNumero,
  multiplicar,
  sumar,
  restar,
  dividir,
  valorAbsoluto,
  convertirNumeroAString,
  convertirNumeroALiteral,
  sumarPor,
  asignarValorAtributos,
  esNuloOIndefinido,
  csvAjson,
  generarToken,
  deptoCod2Nombre,
};
