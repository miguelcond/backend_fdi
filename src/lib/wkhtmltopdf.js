const wkhtmltopdf = require('wkhtmltopdf');

const streamToBuffer = (streamObjectToRead)=>{

	const chunks = [];

	return new Promise((resolve,reject)=>{
		streamObjectToRead
			.on('data', chunk => chunks.push(chunk))
			.on('end', ()=> resolve(Buffer.concat(chunks)))
			.on('error', err=> reject(err))
	})
}

const createBuffer = (html, options)=>{

	if(options){
		if (options.debug = true){
			options.debug = (b) =>{
				const s = b.toString();
				if(s) process.stdout.write(s);
			};
		}
		if(options.debugStdOut && !options.output){
			options.debugStOut = false;
			console.error('[lib-wkhtmltopdf] debugStdOut may not be used when wkhtmltopdf\'s output is stdout');
		}
	}

	return new Promise((resolve, reject)=>{
		const stream = wkhtmltopdf(html,options)
		return streamToBuffer(stream).then((buf)=>{
			return resolve(buf);
		}).catch((err)=>{
			return reject(err);
		})
	})

}

wkhtmltopdf.createBuffer = createBuffer;

module.exports = wkhtmltopdf;