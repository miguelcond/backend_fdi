const util = require('./util');
const wkhtmltopdf = require('wkhtmltopdf');

/**
 * @constructor
 * @param {string} plantilla Nombre de la plantilla.
 */
function reporte(plantilla) {
  const self = this;
  const file = `${_path}/src/templates/${plantilla}.html`;
  let html = require('fs').readFileSync(file).toString();
  let handlebars = require('handlebars');
  var filter = require('handlebars.filter');
  filter.registerHelper(handlebars);
  // TODO agregar helper personalizado 'add'
  handlebars.registerHelper('add', function(a, b) {
    return a + b;
  });
  handlebars.registerHelper('breaklines', function(text) {
      text = handlebars.Utils.escapeExpression(text);
      text = text.replace(/(\r\n|\n|\r)/gm, '<br>');
      return new handlebars.SafeString(text);
  });
  handlebars.registerHelper('equal', function(a, b, options) {
    if (a !== b) {
      return options.inverse(this);
    }
    return options.fn(this);
  });
  handlebars.registerHelper('notEqual', function(a, b, options) {
    if (a !== b) {
      return options.fn(this);
    }
    return options.inverse(this);
  });

  handlebars.registerHelper('ifEquals', function(arg1, arg2, options) {
    return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
  });

  handlebars.registerHelper('ifNotEquals', function(arg1, arg2, options) {
    return (arg1 != arg2) ? options.fn(this) : options.inverse(this);
  });

  handlebars.registerHelper('ifEnabled', function(arg1, arg2, options) {
    let compare = false;
    if(arg1.hasOwnProperty(arg2) && arg1[arg2]) compare = true;
    return compare ? options.fn(this) : options.inverse(this);
  });

  handlebars.registerHelper("compare", function( v1, op, v2, options ) {
    var c = {
      "eq": function( v1, v2 ) {
        return v1 == v2;
      },
      "neq": function( v1, v2 ) {
        return v1 != v2;
      }
    }
    if( Object.prototype.hasOwnProperty.call( c, op ) ) {
      return c[ op ].call( this, v1, v2 ) ? options.fn( this ) : options.inverse( this );
    }
    return options.inverse( this );
  });

  handlebars.registerHelper("sep", function(options){
      if(options.data.last) {
          return options.inverse();
      } else {
          return options.fn();
      }
  });

  filter.registerFilter('numberToString', (a) => {
    return util.convertirNumeroAString(a);
  });
  self.template = handlebars.compile(html);
};

/**
 * Función que recibe los datos a reemplazar en la plantilla y devuelve el pdf.
 * @param {Object} datos Datos a reemplazar en la plantilla.
 * @param {Object} opciones Opciones para crear el reporte.
 * @returns {pdf}
 */
reporte.prototype.pdf = function(datos = {}, opciones) {
  const self = this;
  const cssFile = `${_path}/src/templates/estilos.css`;
  datos.css = require('fs').readFileSync(cssFile).toString();
  let html = self.template(datos);
  let opt = {
    pageSize: 'letter',
    marginLeft: '1.5cm',
    marginRight: '1.5cm',
    marginTop: '3cm',
    marginBottom: '3cm',
  };
  if (opciones.orientation) opt.orientation = opciones.orientation;
  if (opciones.marginTop) opt.marginTop = opciones.marginTop;
  if (opciones.marginLeft) opt.marginLeft = opciones.marginLeft;
  if (opciones.marginRight) opt.marginRight = opciones.marginRight;
  if (opciones.marginBotton) opt.marginBotton = opciones.marginBotton;
  if (opciones.pageSize) opt.pageSize = opciones.pageSize;
  if (opciones.output) opt.output = opciones.output;
  if (!opciones.noHeader) {
    opt.headerHtml = `file:///${_path}/src/templates/header.html`;
    opt.footerHtml = `file:///${_path}/src/templates/footer.html`;
  }
  return wkhtmltopdf(html, opt);
};

reporte.prototype.pdfDownload = function(datos = {}, opciones) {
  return new Promise((resolve, reject) => {
    const self = this;
    const cssFile = `${_path}/src/templates/estilos.css`;
    datos.css = require('fs').readFileSync(cssFile).toString();
    let html = self.template(datos);
    let opt = {
      pageSize: 'letter',
      marginLeft: '1.5cm',
      marginRight: '1.5cm',
      marginTop: '3cm',
      marginBottom: '3cm',
    };
    if (opciones.orientation) opt.orientation = opciones.orientation;
    if (opciones.marginTop) opt.marginTop = opciones.marginTop;
    if (opciones.marginLeft) opt.marginLeft = opciones.marginLeft;
    if (opciones.marginRight) opt.marginRight = opciones.marginRight;
    if (opciones.marginBotton) opt.marginBotton = opciones.marginBotton;
    if (opciones.pageSize) opt.pageSize = opciones.pageSize;
    if (opciones.output) opt.output = opciones.output;
    opt.headerHtml = `file:///${_path}/src/templates/header.html`;
    opt.footerHtml = `file:///${_path}/src/templates/footer.html`;

    let stream = wkhtmltopdf(html, opt);

    stream.on('end', function() {
      return resolve();
    });

    stream.on('error', reject);
  });
};

module.exports = reporte;
