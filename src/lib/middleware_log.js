/* eslint prefer-rest-params: 0 */
/* eslint func-names: 0 */
import uuid from 'uuid';
import moment from 'moment';
import cls from 'continuation-local-storage';

const ns = cls.createNamespace('loggerApp123');
const Reporte = require('./reportes');
const util = require('./util');
const configuracion = require('./../config/config')();

module.exports = (app) => {
  // namespace
  app.use((req, res, next) => {
    ns.bindEmitter(req);
    ns.bindEmitter(res);
    ns.run(() => {
      next();
    });
  });

  // setting log id in cls
  app.use((req, res, next) => {
    ns.set('logId', uuid());
    next();
  });

  app.use((req, response, next) => {
    const res = response;
    const oldWrite = response.write;
    const oldEnd = response.end;
    const chunks = [];

    res.write = function (chunk) {
      chunks.push(Buffer.from(chunk));
      oldWrite.apply(res, arguments);
    };

    // save last chunk, then parse body
    res.end = function (chunk) {
      if (chunk) {
        chunks.push(Buffer.from(chunk));
      }

      let body = Buffer.concat(chunks).toString('utf8');
     // Esto lo comente porque elimina el contenido de body 
     // try {
     //   body = {}; //JSON.parse(body);
     // } catch (error) { }

      if (res.statusCode >= 500) {
        const PARA = configuracion.sistema.soporte;
        const TITULO = `Notificación de Error - ${configuracion.sistema.origen}`;
        const reporte = new Reporte('correo_soporte');
        const DATOS = {
          aplicacion: `Seguimiento de proyectos`,
          fecha: moment().format('DD/MM/YYYY HH:MM:SS'),
          url: `${req.headers.host}${req.originalUrl}`,
          metodo: `${req.method}`,
          statusCode: res.statusCode,
          ip: `${req.connection.remoteAddress}`,
          parametros: `${JSON.stringify(req.body, null, 2)}`,
          navegador: `${req.headers['user-agent']}`,
          query: `${JSON.stringify(req.query, null, 2)}`,
          headers: `${JSON.stringify(req.headers, null, 2)}`,
          body: `${JSON.stringify(body, null, 2)}`
        };
        const HTML = reporte.template(DATOS);
        util.enviarEmail(PARA, TITULO, HTML);
      }
      delete response.errorInf;
      oldEnd.apply(res, arguments);
    };
    next();
  });
};
