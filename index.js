import express from 'express';
import consign from 'consign';
import bodyParser from 'body-parser';

global._path = __dirname;

const app = express();

app.use(bodyParser.json({ limit: '50mb' }));

consign({verbose: false})
  .include('src/lib/middleware_log.js')
  .then('src/config/config.js')
  .then('src/lib/util.js')
  .then('src/db.js')
  .then('src/auth.js')
  .then('src/lib/middlewares.js')
  .then('src/modulos')
  .then('src/lib/boot.js')
  .then('src/lib/error-handler.js')
  .into(app);
module.exports = app;
